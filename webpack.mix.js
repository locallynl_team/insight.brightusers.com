let mix = require('laravel-mix');
require('laravel-mix-purgecss');

/**
 * Front end
 */
mix.sass('resources/assets/frontend/sass/app.scss', 'public/assets/frontend/css')
    .version()
    .options({
    processCssUrls: false
}).purgeCss();

mix.copy('resources/assets/frontend/images', 'public/assets/frontend/images');
mix.copy('resources/assets/frontend/fonts', 'public/assets/frontend/fonts');
mix.copy('resources/assets/favicons', 'public');

mix.scripts([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
    'node_modules/bxslider/dist/jquery.bxslider.min.js',
    'node_modules/slick-carousel/slick/slick.min.js',
    'resources/assets/frontend/js/vendor/modernizr.custom.53451.js',
    'resources/assets/frontend/js/vendor/jquery.gallery.js',
    //'node_modules/easy-responsive-tabs/js/easyResponsiveTabs.js',
    //'node_modules/wowjs/dist/wow.min.js',
    'node_modules/retinajs/dist/retina.min.js',
    'node_modules/jasny-bootstrap/dist/js/jasny-bootstrap.min.js',
    'resources/assets/frontend/js/init.js'
], 'public/assets/frontend/js/app.js');

//mix.browserSync('www.surve2.test');

/**
 * Back end
 */
mix.sass('resources/assets/backend/sass/app.scss', 'public/assets/backend/css')
    .version()
    .options({
        processCssUrls: false
    }).purgeCss();

mix.copy('resources/assets/backend/images', 'public/assets/backend/images');
mix.copy('resources/assets/backend/fonts', 'public/assets/backend/fonts');
mix.copy('resources/assets/backend/js', 'public/assets/backend/js');

mix.scripts([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
    'node_modules/jasny-bootstrap/dist/js/jasny-bootstrap.min.js',
    'resources/assets/backend/js/auth-init.js'
], 'public/assets/backend/js/auth.js');

mix.js('resources/assets/backend/vue/app.js', 'public/assets/backend/js/vue');

mix.browserSync('app.surve2.test');