<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Survey;

/**
 * App routes
 *
 * Set subdomain
 */
Route::group([
    'domain' => config('app.sub_app').'.'.config('app.domain'),
    'namespace' => 'Dashboard'
], function() {
    /**
     * All routes with authentication
     */
    Route::middleware('auth')->group(function() {
        //manage account
        Route::get('account', 'AccountController@index')->name('account.index');
        Route::post('account', 'AccountController@update');
        Route::get('account/ip-filters', 'AccountController@ipFilters')->name('account.ip-filters');
        Route::post('account/ip-filters', 'AccountController@updateIpFilters');
        Route::get('account/payment-method', 'AccountController@paymentMethod')->name('account.payment-method');
        Route::post('account/payment-method', 'AccountController@updatePaymentMethod');
        Route::post('account/payment-details', 'AccountController@updatePaymentDetails')->name('account.payment-details');
        Route::get('account/invoices', 'AccountController@invoices')->name('account.invoices');
        Route::get('account/cancel-mandate', 'AccountController@cancelMandate')->name('account.cancel-mandate');
        Route::post('account/cancel-mandate', 'AccountController@cancelMandateConfirmed');

        //modals
        Route::get('modal/{type}/{id?}', 'ModalController@view')->name('modal');

        //invoice
        Route::get('invoice/{invoice}.pdf', 'InvoiceController@view')->name('invoice');

        //roadmap
        Route::get('roadmap', 'RoadmapController@index')->name('roadmap');

        //helpdesk
        Route::get('helpdesk', 'HelpdeskController@index')->name('helpdesk');

        //websites
        Route::get('sites/new', 'SiteController@create')->name('site.create');
        Route::post('sites/new', 'SiteController@store');

        //user needs a website for these
        Route::middleware('has_website')->group(function () {
            //manage websites
            Route::get('sites', 'SiteController@index')->name('site.index');
            Route::get('sites/edit/{site}', 'SiteController@edit')->name('site.edit');
            Route::post('sites/edit/{site}', 'SiteController@update');
            Route::get('sites/delete/{site}', 'SiteController@delete')->name('site.delete');
            Route::post('sites/delete/{site}', 'SiteController@remove');
            Route::get('sites/switch/{site}', 'SiteController@switch')->name('site.switch');
            Route::post('sites/implementation/{site}', 'SiteController@implementation')->name('site.implementation');

            //manage subscription
            Route::get('subscription/manage/{site}', 'SubscriptionController@manage')->name('subscription.manage');
            Route::post('subscription/payment/{site}', 'SubscriptionController@processPayment')->name('subscription.process-payment');
            Route::get('subscription/check-payment/{site}', 'SubscriptionController@checkPayment')->name('subscription.check-payment');
            Route::post('subscription/update-subscription/{site}', 'SubscriptionController@update')->name('subscription.update');
            Route::get('subscription/cancel-subscription/{site}', 'SubscriptionController@cancel')->name('subscription.cancel');
            Route::post('subscription/cancel-subscription/{site}', 'SubscriptionController@cancelConfirmed');

            //use needs a subscription
            Route::group(['middleware' => 'has_subscription'], function () {
                //site dashboard
                Route::get('/', 'DashboardController@index')->name('dashboard');

                //manage surveys & feedbackbuttons
                foreach (['survey', 'feedbackbutton', 'poll'] as $type) {
                    $settings = Survey::getSettingsByType($type);

                    Route::get($settings['url'], $settings['controller'] . '@index')->name($settings['url_key'] . '.index');
                    Route::get($settings['url'] . '/new', $settings['controller'] . '@create')->name($settings['url_key'] . '.create');
                    Route::post($settings['url'] . '/new', $settings['controller'] . '@store' . ucfirst($settings['url_key']));
                    Route::get($settings['url'] . '/edit/{survey}', $settings['controller'] . '@edit')->name($settings['url_key'] . '.edit');
                    Route::post($settings['url'] . '/edit/{survey}', $settings['controller'] . '@update' . ucfirst($settings['url_key']));
                    Route::post($settings['url'] . '/delete/{survey}', $settings['controller'] . '@delete')->name($settings['url_key'] . '.delete');
                    Route::get($settings['url'] . '/toggle/{survey}', $settings['controller'] . '@toggle')->name($settings['url_key'] . '.toggle');

                    Route::get($settings['url'] . '/manage/{survey}', $settings['question_controller'] . '@edit')->name($settings['url_key'] . '.manage');
                    Route::post($settings['url'] . '/manage/{survey}', $settings['question_controller'] . '@update');

                    Route::match(['get', 'post'], $settings['url'] . '/responses/{survey}', $settings['response_controller'] . '@list')->name($settings['url_key'] . '.responses');
                    Route::get($settings['url'] . '/responses/{survey}/delete/{response}', $settings['response_controller'] . '@delete')->name($settings['url_key'] . '.response.delete');
                    Route::match(['get', 'post'], $settings['url'] . '/responses/{survey}/export', $settings['response_controller'] . '@export')->name($settings['url_key'] . '.responses.export');
                    Route::get($settings['url'] . '/responses/{survey}/export/{export}/status', $settings['response_controller'] . '@exportStatus')->name($settings['url_key'] . '.responses.export.status');
                    Route::get($settings['url'] . '/responses/{survey}/export/{export}/download', $settings['response_controller'] . '@download')->name($settings['url_key'] . '.responses.export.download');
                }

                //manage heatmaps
                Route::get('heatmaps', 'HeatmapController@index')->name('heatmap.index');
                Route::get('heatmaps/new', 'HeatmapController@create')->name('heatmap.create');
                Route::post('heatmaps/new', 'HeatmapController@store');
                Route::get('heatmaps/edit/{heatmap}', 'HeatmapController@edit')->name('heatmap.edit');
                Route::post('heatmaps/edit/{heatmap}', 'HeatmapController@update');
                Route::post('heatmaps/delete/{heatmap}', 'HeatmapController@delete')->name('heatmap.delete');
                Route::get('heatmaps/toggle/{heatmap}', 'HeatmapController@toggle')->name('heatmap.toggle');
                Route::get('heatmaps/view/{heatmap}', 'HeatmapController@view')->name('heatmap.view');
                Route::get('heatmaps/refresh/{heatmap}/{profile}', 'HeatmapController@refresh')->name('heatmap.refresh');
                Route::get('heatmaps/reset/{heatmap}', 'HeatmapController@reset')->name('heatmap.reset');

                //manage forms
                Route::get('forms', 'FormController@index')->name('form.index');
                Route::get('forms/new', 'FormController@create')->name('form.create');
                Route::post('forms/new', 'FormController@store');
                Route::get('forms/edit/{form}', 'FormController@edit')->name('form.edit');
                Route::post('forms/edit/{form}', 'FormController@update');
                Route::get('forms/toggle/{form}', 'FormController@toggle')->name('form.toggle');
                Route::get('forms/view/{form}', 'FormController@view')->name('form.view');
                Route::post('forms/delete/{form}', 'FormController@delete')->name('form.delete');
                Route::get('forms/reset/{form}', 'FormController@reset')->name('form.reset');

                Route::post('ajax/save-form', 'FormController@ajaxSave')->name('form.save');
                Route::post('ajax/load-forms', 'FormController@ajaxLoadForms')->name('form.load-forms');

                //manage styling
                Route::get('styling', 'StylingController@index')->name('style.index');
                Route::post('styling', 'StylingController@save');
            });
        });
    });
});