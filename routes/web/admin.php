<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Admin routes
 */
Route::group([
    'domain' => config('app.sub_app').'.'.config('app.domain'),
    'namespace' => 'Dashboard\Admin',
    'middleware' => ['auth', 'is_admin'],
    'prefix' => 'admin'
], function() {
    Route::get("/", 'DashboardController@index')->name('admin.index');

    /**
     * Users
     */
    Route::get('users', 'UserController@index')->name('admin.user.index');
    Route::get('users/edit/{user}', 'UserController@edit')->name('admin.user.edit');
    Route::post('users/edit/{user}', 'UserController@update');
    Route::get('users/delete/{user}', 'UserController@index')->name('admin.user.delete');
    Route::get('users/login/{user}', 'UserController@login')->name('admin.user.login');

    /**
     * Subscriptions
     */
    Route::get('subscription/edit/{subscription}', 'SubscriptionController@edit')->name('admin.subscription.edit');
    Route::post('subscription/edit/{subscription}', 'SubscriptionController@update');

    /**
     * Websites
     */
    Route::get('sites', 'SiteController@index')->name('admin.site.index');

    /**
     * Invoices
     */
    Route::get('invoices', 'InvoiceController@index')->name('admin.invoice.index');
    Route::get('invoices/payment/{invoice}', 'InvoiceController@registerPayment')->name('admin.invoice.payment');

    /**
     * Packages
     */
    Route::get('packages', 'PackageController@index')->name('admin.package.index');
    Route::get('packages/new', 'PackageController@create')->name('admin.package.create');
    Route::post('packages/new', 'PackageController@store')->name('admin.package.store');
    Route::get('packages/edit/{package}', 'PackageController@edit')->name('admin.package.edit');
    Route::post('packages/edit/{package}', 'PackageController@update');
    Route::get('packages/delete/{package}', 'PackageController@delete')->name('admin.package.delete');
    Route::post('packages/remove/{package}', 'PackageController@remove')->name('admin.package.remove');
    Route::get('packages/toggle/{package}', 'PackageController@toggle')->name('admin.package.toggle');

    Route::get('package-options/new', 'PackageOptionController@create')->name('admin.packageoption.create');
    Route::post('package-options/new', 'PackageOptionController@store');

    Route::get('package-options', 'PackageOptionController@index')->name('admin.packageoption.index');
    Route::get('package-options/edit/{packageOption}', 'PackageOptionController@edit')->name('admin.packageoption.edit');
    Route::post('package-options/edit/{packageOption}', 'PackageOptionController@update');
    Route::get('package-options/delete/{packageOption}', 'PackageOptionController@delete')->name('admin.packageoption.delete');

    /**
     * Roadmap
     */
    Route::get('roadmap', 'RoadmapController@index')->name('admin.roadmap.index');
    Route::get('roadmap/new', 'RoadmapController@create')->name('admin.roadmap.create');
    Route::post('roadmap/new', 'RoadmapController@store');
    Route::get('roadmap/edit/{milestone}', 'RoadmapController@edit')->name('admin.roadmap.edit');
    Route::post('roadmap/edit/{milestone}', 'RoadmapController@update');
    Route::get('roadmap/delete/{milestone}', 'RoadmapController@delete')->name('admin.roadmap.delete');
    Route::get('roadmap/toggle/{milestone}', 'RoadmapController@toggle')->name('admin.roadmap.toggle');

    /**
     * Statistics
     */
    Route::get('statistics', 'StatisticsController@index')->name('admin.statistics');
});