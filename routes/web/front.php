<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Front end routes
 */
Route::domain(config('app.sub_front').'.'.config('app.domain'))->group(function() {
    /**
     * Homepage
     */
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('packages', 'PackageController@index')->name('packages');
    Route::get('contact', 'ContactController@index')->name('contact');
    Route::post('contact', 'ContactController@send');
    Route::get('terms', 'TermsController@index')->name('terms');
    Route::get('privacy', 'PrivacyController@index')->name('privacy');

    Route::get('tools', 'ToolController@index')->name('tools');
    Route::get('tools/surveys', 'ToolController@surveys')->name('tools.survey');
    Route::get('tools/feedbackbuttons', 'ToolController@feedbackbuttons')->name('tools.feedbackbutton');
    Route::get('tools/heatmaps', 'ToolController@heatmaps')->name('tools.heatmap');

    Route::get('blog', 'BlogController@index')->name('blog');
    Route::get('blog/{slug}', 'BlogController@view')->name('blog.post');
});