<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Front end routes
 */
Route::domain(config('app.sub_static').'.'.config('app.domain'))->group(function() {
    /**
     * Request routes
     */
    Route::get('request/{site}', 'JavascriptController@show')->name('request');
    Route::get('request', function() {})->name('request.implementation');
});