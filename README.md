# insight.brightusers.com

Briefing for project


```
Als het goed is heb je toegang tot een repo (bitbucket) 'insight.brightusers.com' 

Wil jij daar de files in de bijlage in pleuren, en onderstaand wijzigingen doorvoeren?

Er moeten nog even wat dingen gewijzigd worden voordat het in productie gaat:
- De javascript code in: resources/views/javascript/v1.blade.php gebruikt nu "window.surve".
- In resources/views/backend/parts/implementation.blade.php staat ook nog Surve, ook in de functie: (function(s,u,r,v,e), dat kun je aanpassen naar iets anders van 5 letters en even alle variabelen in de code eronder goedzetten.
- Even composer install draaien
- npm install draaien
- Puppeteer wordt dan ook geïnstalleerd als het goed is, die wordt gebruikt voor de screenshot

De front end code van het project gaan we niet gebruiken, alleen de 'app' en backend. Gaat om het platform https://www.surve.nl/ (app.surve.nl) hebben we lang geleden wel eens naar gekeken, heb ik nu een nieuwe codebase van.

Brenno zorgt er voor dan er een map is op de server waar je het geheel heen kan deployen, laat je weten als het zover is. Even kijken of we dit ding aan de praat kunnen krijgen :p
```