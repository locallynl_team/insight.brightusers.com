<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionAnswer extends Model
{
    use SoftDeletes;

    /**
     * Turn timestamps off
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Soft deleted field
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Hidden fields
     *
     * @var array
     */
    protected $hidden = [
        'question_id',
        'deleted_at'
    ];

    /**
     * Belongs to a question
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
