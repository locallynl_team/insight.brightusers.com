<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    /**
     * Turn off timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Allow mass assignment
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'price_monthly',
        'price_yearly',
        'color',
        'trial',
        'trial_days',
        'active'
    ];

    /**
     * Payment periods
     *
     * @var array
     */
    private $periods = [
        'monthly' => ['period' => 'P1M', 'multiply' => 1, 'text' => 'per maand'],
        'yearly' => ['period' => 'P1Y', 'multiply' => 12, 'text' => 'per jaar']
    ];

    /**
     * Calculate once
     *
     * @var bool
     */
    private $computedValues = false;

    /**
     * Has many package items
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function packageItems()
    {
        return $this->hasMany(PackageItem::class);
    }

    /**
     * Package has many subscriptions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    /**
     * Get package value of single item
     *
     * @param  $identifier
     * @return bool|mixed
     */
    public function getValueByIdentifier($identifier)
    {
        return $this->packageItems()
                ->where('package_option_id', PackageOption::where('identifier', $identifier)->first()->id)
                ->first()
                ->amount ?? false;
    }

    /**
     * Calculate period values
     */
    private function computePeriodValues()
    {
        if ($this->computedValues === false) {
            foreach ($this->periods as $key => $period) {
                $price = 'price_'.$key;
                $this->periods[$key]['costs'] = $this->$price;
                $this->periods[$key]['days'] = (new \DateTime())
                    ->add(new \DateInterval($period['period']))
                    ->diff(new \DateTime())
                    ->days;
            }

            $this->computedValues = true;
        }
    }

    /**
     * Get all periods
     *
     * @return array
     */
    public function getPeriodsAttribute()
    {
        $this->computePeriodValues();

        return $this->periods;
    }

    /**
     * Get period
     *
     * @param  $key
     * @return bool|mixed
     */
    public function period($key)
    {
        $this->computePeriodValues();

        return $this->periods[$key] ?? false;
    }
}
