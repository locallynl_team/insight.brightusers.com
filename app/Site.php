<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Site extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'domain', 'test_domains', 'exclude_ip_addresses', 'active'
    ];

    protected $casts = [
        'active' => 'boolean',
        'code_confirmed' => 'boolean',
        'package_change_allowed' => 'boolean'
    ];

    /**
     * Website belongs to user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Site has many surveys
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function allSurveys()
    {
        return $this->hasMany(Survey::class);
    }

    /**
     * Site has many surveys with type = survey
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function surveys()
    {
        return $this->hasMany(Survey::class)->where('type', 'survey');
    }

    /**
     * Site has many feedbackbuttons with type = 'feedbackbutton'
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feedbackButtons()
    {
        return $this->hasMany(Survey::class)->where('type', 'feedbackbutton');
    }

    /**
     * Site has many feedbackbuttons with type = 'poll'
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function polls()
    {
        return $this->hasMany(Survey::class)->where('type', 'poll');
    }

    /**
     * Site has many heatmaps
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function heatmaps()
    {
        return $this->hasMany(Heatmap::class);
    }

    /**
     * Site has a style, and default fallback
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function style()
    {
        return $this->hasOne(SiteStyle::class)->withDefault(SiteStyle::getDefaultValues());
    }

    /**
     * Has many subscriptions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    /**
     * Has one active subscription
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function activeSubscription()
    {
        return $this->hasOne(Subscription::class)->where('active', true);
    }

    /**
     * Site has many surveys with type = survey
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activeSurveys()
    {
        return $this->hasMany(Survey::class)->where('type', 'survey')->where('status', true);
    }

    /**
     * Site has many feedbackbuttons with type = 'feedbackbutton'
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activeFeedbackButtons()
    {
        return $this->hasMany(Survey::class)->where('type', 'feedbackbutton')->where('status', true);
    }

    /**
     * Site has many polls with type = 'poll'
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activePolls()
    {
        return $this->hasMany(Survey::class)->where('type', 'polls')->where('status', true);
    }

    /**
     * Site has many heatmaps
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activeHeatmaps()
    {
        return $this->hasMany(Heatmap::class)->where('status', true);
    }

    /**
     * Site has many forms
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function forms()
    {
        return $this->hasMany(Form::class);
    }

    /**
     * Site has many active forms
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activeForms()
    {
        return $this->hasMany(Form::class)->where('status', true);
    }

    /**
     * Site statistics
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function siteStatistics()
    {
        return $this->hasMany(SiteStatistic::class);
    }

    /**
     * Site statistics
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function siteStatisticsToday()
    {
        return $this->hasMany(SiteStatistic::class)->where('date', date("Y-m-d"));
    }

    /**
     * Site statistics
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function siteStatisticsYesterday()
    {
        return $this->hasMany(SiteStatistic::class)->where('date', date("Y-m-d", time()-86400));
    }

    /**
     * Site statistics
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function siteStatistics30Days()
    {
        return $this->hasMany(SiteStatistic::class)->where('date', '>', date("Y-m-d", time()-(30*86400)));
    }

    /**
     * Activate this site
     */
    public function activate()
    {
        $this->update(['active' => true]);
    }

    /**
     * Deactivate site
     */
    public function deactivate()
    {
        $this->update(['active' => false]);
    }

    /**
     * Get domains for this site
     *
     * @return array
     */
    public function getDomainsAttribute()
    {
        //remove values
        $removeValues = ['http://www.', 'https://www.', 'http://', 'https://', 'www.'];

        $domains = strlen($this->test_domains) > 0 ? explode(",", str_replace($removeValues, '', $this->test_domains))
            : [];
        $domains[] = str_replace($removeValues, '', $this->domain);

        return array_map('trim', $domains);
    }

    /**
     * Cache key for the javascript
     *
     * @return string
     */
    public function getCacheKey()
    {
        return $cacheKey = 'javascript-site-'.$this->id;
    }

    /**
     * Invalidate the cache for this site
     *
     * @throws \Exception
     */
    public function invalidateCache()
    {
        cache()->forget($this->getCacheKey());
    }
}
