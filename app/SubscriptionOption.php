<?php

namespace App;

use App\Subscription;
use Illuminate\Database\Eloquent\Model;

class SubscriptionOption extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'subscription_id',
        'package_item_id',
        'allowed_usage',
        'current_usage'
    ];

    /**
     * Belongs to subscription
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function packageItem()
    {
        return $this->belongsTo(PackageItem::class);
    }
}
