<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeatmapProfile extends Model
{
    public $timestamps = false;
}
