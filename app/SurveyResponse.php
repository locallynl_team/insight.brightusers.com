<?php

namespace App;

use App\Events\NewResponse;
use Illuminate\Database\Eloquent\Model;

class SurveyResponse extends Model
{
    protected $casts = ['finished' => 'boolean'];

    protected $fillable = ['survey_id', 'finished', 'identifier', 'url', 'platform', 'resolution', 'browser', 'os'];

    /**
     * Belongs to Survey
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    /**
     * Has many items
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(SurveyResponseItem::class);
    }

    /**
     * Dispatch events
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => NewResponse::class,
    ];
}
