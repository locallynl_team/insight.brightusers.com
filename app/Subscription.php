<?php

namespace App;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    /**
     * Mass assignable fields
     *
     * @var array
     */
    protected $fillable = [
        'site_id',
        'package_id',
        'active',
        'trial',
        'ends_at',
        'period',
        'price',
        'notice_status'
    ];

    /**
     * Set date fields
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'ends_at', 'cancelled_at'];

    /**
     * Casts vars as type
     *
     * @var array
     */
    protected $casts = ['trial' => 'boolean'];

    /**
     * Subscription belongs to a site
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * Subscription has one package
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    /**
     * Has many options
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany(SubscriptionOption::class);
    }

    /**
     * Cancel subscription
     *
     * @param bool $immediately
     * @param bool $deactivateSite
     */
    public function cancel($immediately = false, $deactivateSite = true)
    {
        //set cancelled_at date
        $this->cancelled_at = Carbon::now();
        $this->notice_status = 0;

        //cancel right now
        if ($immediately === true) {
            $this->active = false;

            //de-activate this website
            if ($deactivateSite === true) {
                $this->site()->first()->deactivate();
            }
        }

        $this->save();
    }

    /**
     * Calculate free days
     *
     * @param  Package $newPackage
     * @return int
     */
    public function calculateFreeDays(Package $newPackage)
    {
        //this was a trial -> new package
        if ($this->trial === true) {
            return 0;
        }

        //this subscription is over
        if ($this->daysLeft <= 0) {
            return 0;
        }

        //get current package
        $currentPackage = $this->package()->first();

        //same package? free days = days left
        if ($currentPackage->id === $newPackage->id) {
            return $this->daysLeft+1;
        }

        //calculate costs
        $costPerDayOld = $currentPackage->period($this->period)['costs'] /
            $currentPackage->period($this->period)['days'];
        $costPerDayNew = $newPackage->period($this->period)['costs'] /
            $currentPackage->period($this->period)['days'];

        //paid for these current days
        $paidAmount = ($costPerDayOld * $this->daysLeft);

        //return days we could buy with already paid money for price of new package
        return (int) @floor($paidAmount / $costPerDayNew);
    }

    /**
     * Create subscription
     *
     * @param  Package  $package
     * @param  Site     $site
     * @param  $duration
     * @param  $period
     * @param  bool     $trial
     * @param  $price
     * @return mixed
     */
    public static function addSubscription(Package $package, Site $site, $duration, $period, bool $trial, $price)
    {
        //remove old subscription?
        if ($currentSubscription = $site->activeSubscription()->first()) {
            $currentSubscription->cancel(true, false);
        }

        //add subscription
        $subscription = self::create(
            [
            'site_id' => $site->id,
            'package_id' => $package->id,
            'active' => true,
            'trial' => $trial,
            'period' => $period,
            'price' => $price,
            'ends_at' => Carbon::now()->addDays($duration)->subMinute(2)->toDateTimeString()
            ]
        );

        //add subscription options
        foreach ($package->packageItems()->get() as $packageItem) {
            SubscriptionOption::create(
                [
                'subscription_id' => $subscription->id,
                'package_item_id' => $packageItem->id,
                'allowed_usage' => $packageItem->amount
                ]
            );
        }

        //activate site
        $site->activate();

        return $subscription;
    }

    /**
     * Charge the user for this package
     */
    public function charge()
    {
        //get user
        $user = $this->site->user;

        //calculate price
        $totalPrice = $this->price;

        //admin users don't pay
        if ($user->is_admin === true) {
            $totalPrice = 0;
        } else {
            //calculate user discount
            if ($user->discount > 0) {
                $totalPrice = round($user->calculateDiscountPrice($totalPrice));
            }
        }

        //create payment
        if ($totalPrice > 0) {
            //set method
            $method = ($user->pay_on_invoice === true ? 'invoice' : 'recurring');

            //create the payment
            $payment = Payment::create(
                [
                'user_id' => $user->id,
                'site_id' => $this->site->id,
                'package_id' => $this->package->id,
                'payment_id' => null,
                'subscription_id' => $this->id,
                'total_amount' => $totalPrice,
                'tax' => Payment::getTax(),
                'status' => 'open',
                'period' => $this->period,
                'method' => $method
                ]
            );

            //is recurring? boom -> pay up.
            if ($method === 'recurring') {
                $payment->makeRecurringPayment();
            }
        }

        //always renew -> if recurring payment fails we cancel
        $this->renew();
    }

    /**
     * Renew this subscription
     */
    public function renew()
    {
        //renew subscription
        $newDays = $this->package->period($this->period)['days'];
        $this->ends_at = $this->ends_at->addDays($newDays);
        $this->notice_status = 0;
        $this->save();
    }

    /**
     * Get days left
     *
     * @return mixed
     */
    public function getDaysLeftAttribute()
    {
        return Carbon::now()->diffInDays($this->ends_at, false);
    }

    /**
     * Expensive function to get single option
     * TODO: Refactor query
     *
     * @param  $option
     * @return mixed
     */
    public function getOption($option)
    {
        return $this->options()
            ->with(['packageItem', 'packageItem.packageOption'])
            ->get()
            ->where('packageItem.packageOption.identifier', $option)
            ->first();
    }
}
