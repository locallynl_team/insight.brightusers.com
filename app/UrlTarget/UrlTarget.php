<?php
namespace App\UrlTarget;

class UrlTarget
{

    protected static $namespace = 'App\UrlTarget\UrlTargetType\\';
    protected static $types = [
        'simple',
        'exact',
        'starts_with',
        'ends_with',
        'contains',
        'regular_expression'
    ];

    /**
     * Return an object for each url target
     *
     * @return array
     */
    public static function getUrlTargets()
    {
        $urlTargets = [];

        foreach (self::$types as $type) {
            $className = self::$namespace.studly_case($type);
            $urlTargets[$type] = new $className;
        }

        return $urlTargets;
    }

    /**
     * Get single instance of url target by type
     *
     * @param  $type
     * @return mixed
     */
    public static function getUrlTarget($type)
    {
        $className = self::$namespace.studly_case($type);
        return new $className;
    }

    /**
     * Return all types
     *
     * @return array
     */
    public static function getTypes()
    {
        return self::$types;
    }
}
