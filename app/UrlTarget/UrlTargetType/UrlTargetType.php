<?php
namespace App\UrlTarget\UrlTargetType;

/**
 * Abstract url target class
 *
 * Class UrlTarget
 */
abstract class UrlTargetType
{
    abstract protected function getSettings();
}
