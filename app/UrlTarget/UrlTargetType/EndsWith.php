<?php
namespace App\UrlTarget\UrlTargetType;

/**
 * Target all pages ending with this value
 *
 * Class EndsWith
 */
class EndsWith extends UrlTargetType
{
    /**
     * Return settings of url target
     *
     * @return array
     */
    public function getSettings()
    {
        return [
            'order' => 4,
            'title' => __('URL eindigt met'),
            'value' => 'ends_with'
        ];
    }
}
