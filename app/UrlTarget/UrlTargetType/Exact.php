<?php
namespace App\UrlTarget\UrlTargetType;

/**
 * Matches exact urls, including protocol & querystrings
 *
 * Class Exact
 */
class Exact extends UrlTargetType
{
    /**
     * Return settings of url target
     *
     * @return array
     */
    public function getSettings()
    {
        return [
            'order' => 2,
            'title' => __('Exacte URL'),
            'value' => 'exact'
        ];
    }
}
