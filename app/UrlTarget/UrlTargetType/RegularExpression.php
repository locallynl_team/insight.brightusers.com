<?php
namespace App\UrlTarget\UrlTargetType;

/**
 * Match all pages with this regular expression
 *
 * Class RegularExpression
 */
class RegularExpression extends UrlTargetType
{
    /**
     * Return settings of url target
     *
     * @return array
     */
    public function getSettings()
    {
        return [
            'order' => 6,
            'title' => __('Reguliere expressie'),
            'value' => 'regular_expression'
        ];
    }
}
