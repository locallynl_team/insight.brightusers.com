<?php
namespace App\UrlTarget\UrlTargetType;

/**
 * Simple URL match, matches all urls while ignoring: querystrings, fragments, http/https & presence of www.
 *
 * Class Simple
 */
class Simple extends UrlTargetType
{
    /**
     * Return settings of url target
     *
     * @return array
     */
    public function getSettings()
    {
        return [
            'order' => 1,
            'title' => __('Simpele URL'),
            'value' => 'simple'
        ];
    }
}
