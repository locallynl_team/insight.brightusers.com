<?php
namespace App\UrlTarget\UrlTargetType;

/**
 * Target all pages where url contains this value
 *
 * Class Contains
 */
class Contains extends UrlTargetType
{
    /**
     * Return settings of url target
     *
     * @return array
     */
    public function getSettings()
    {
        return [
            'order' => 5,
            'title' => __('URL bevat'),
            'value' => 'contains'
        ];
    }
}
