<?php
namespace App\UrlTarget\UrlTargetType;

/**
 * Match all pages where url starts with this value, including protocol.
 *
 * Class StartsWith
 */
class StartsWith extends UrlTargetType
{
    /**
     * Return settings of url target
     *
     * @return array
     */
    public function getSettings()
    {
        return [
            'order' => 3,
            'title' => __('URL begint met'),
            'value' => 'starts_with'
        ];
    }
}
