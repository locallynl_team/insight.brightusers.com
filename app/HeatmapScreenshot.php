<?php

namespace App;

use Storage;
use App\Jobs\GenerateScreenshot;
use Illuminate\Database\Eloquent\Model;

class HeatmapScreenshot extends Model
{
    /**
     * Fillable fields
     *
     * @var array
     */
    public $fillable = [
        'heatmap_id',
        'heatmap_profile_id',
        'filename',
        'metadata',
        'status',
        'screen_width',
        'screen_height'
    ];

    /**
     * Belongs to heatmap
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function heatmap()
    {
        return $this->belongsTo(Heatmap::class);
    }

    /**
     * Belongs to heatmap profile
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function heatmapProfile()
    {
        return $this->belongsTo(HeatmapProfile::class);
    }

    /**
     * Return screenshot url
     *
     * @return mixed
     */
    public function getUrlAttribute()
    {
        return static_url().Storage::url($this->screenshotFileName);
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getScreenshotFileNameAttribute()
    {
        return 'screenshots/'.$this->filename.'.jpg';
    }

    /**
     * Delete current screenshot image
     */
    public function deleteScreenshot()
    {
        if (!empty($this->filename)) {
            Storage::delete($this->screenshotFileName);
        }
    }

    /**
     * Reset this screenshot
     */
    public function reset()
    {
        /**
         * Delete old file
         */
        $this->deleteScreenshot();

        /**
         * Reset screenshot filename
         */
        $this->update(['filename' => null]);

        /**
         * Dispatch job to generate new screenshot
         */
        dispatch(new GenerateScreenshot($this));
    }
}
