<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageItem extends Model
{
    /**
     * Turn off timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'package_option_id',
        'package_id',
        'amount'
    ];

    /**
     * Belongs to a package
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    /**
     * Belongs to a package option
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function packageOption()
    {
        return $this->belongsTo(PackageOption::class);
    }

    /**
     * Has many subscription items
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptionItems()
    {
        return $this->hasMany(SubscriptionOption::class);
    }
}
