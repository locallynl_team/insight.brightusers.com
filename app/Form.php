<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    public $fillable = [
        'site_id',
        'title',
        'url_target',
        'url_target_type',
        'status',
        'selector_target',
        'selector_target_type',
        'tablet_platform',
        'mobile_platform',
        'desktop_platform'
    ];

    public $casts = [
        'status' => 'boolean',
        'mobile_platform' => 'boolean',
        'desktop_platform' => 'boolean',
        'tablet_platform' => 'boolean'
    ];

    protected $hidden = [
        'site_id',
        'status',
        'title',
        'seconds_active',
        'last_status_change',
        'created_at',
        'updated_at'
    ];

    /**
     * Form belongs to site
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * Form has many sessions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessions()
    {
        return $this->hasMany(FormSession::class);
    }

    /**
     * Form has many fields
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fields()
    {
        return $this->hasMany(FormField::class);
    }

    /**
     * Form has many interactions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function interactions()
    {
        return $this->hasManyThrough(FormInteraction::class, FormSession::class);
    }

    /**
     * Remove all data from this form
     */
    public function deleteComplete()
    {
        $this->fields()->delete();

        $this->deleteSessions();

        $this->delete();
    }

    /**
     * Remove all sessions & interactions for this form
     */
    public function deleteSessions()
    {
        $this->interactions()->delete();
        $this->sessions()->delete();
    }

    /**
     * Get form results
     *
     * @return Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function getResults()
    {
        //get total sessions, successful sessions, send count
        $results = DB::table('form_sessions')
            ->where('form_id', $this->id)
            ->selectRaw('COUNT(*) AS sessions, SUM(successful) AS successful, SUM(send) AS send')
            ->groupBy('form_id')
            ->first();

        //get sessions without interaction
        $results->interactive = $this->sessions()->has('interactions')->count();

        return $results;
    }
}
