<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Milestone extends Model
{
    public $fillable = [
        'title',
        'date',
        'date_expected',
        'description'
    ];

    protected $dates = ['date_expected'];

    protected $casts = ['status' => 'boolean'];
}
