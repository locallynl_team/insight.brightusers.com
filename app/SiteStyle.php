<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteStyle extends Model
{
    protected $guarded = ['site_id'];

    /**
     * Belongs to website
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * Return array with default values
     *
     * @return array
     */
    public static function getDefaultValues()
    {
        return [
            'header_background_color' => '#ffffff',
            'header_text_color' => '#000000',
            'header_text' => 'Survey',
            'header_type' => 'text',
            'header_logo' => '/assets/backend/images/logo.png',
            'survey_background_color' => '#ffffff',
            'survey_border_color' => '#000000',
            'survey_text_color' => '#000000',
            'survey_text_align' => 'default',
            'answer_background_color' => '#555555',
            'answer_text_color' => '#ffffff',
            'survey_position' => 'default',
            'feedbackbutton_background_color' => '#000000',
            'feedbackbutton_text_color' => '#ffffff',
            'button_background_color' => '#000000',
            'button_text_color' => '#ffffff',
            'button_align' => 'default',
            'advanced_styling' => ''
        ];
    }
}
