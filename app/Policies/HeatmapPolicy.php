<?php

namespace App\Policies;

use App\User;
use App\Heatmap;
use Illuminate\Auth\Access\HandlesAuthorization;

class HeatmapPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the site.
     *
     * @param  User    $user
     * @param  Heatmap $heatmap
     * @return mixed
     */
    public function view(User $user, Heatmap $heatmap)
    {
        return $user->id === $heatmap->site()->first()->user_id;
    }

    /**
     * Determine whether the user can create sites.
     *
     * @param  User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the site.
     *
     * @param  User    $user
     * @param  Heatmap $heatmap
     * @return mixed
     */
    public function update(User $user, Heatmap $heatmap)
    {
        return $user->id === $heatmap->site()->first()->user_id;
    }

    /**
     * Determine whether the user can delete the site.
     *
     * @param  User    $user
     * @param  Heatmap $heatmap
     * @return mixed
     */
    public function delete(User $user, Heatmap $heatmap)
    {
        return $user->id === $heatmap->site()->first()->user_id;
    }

    /**
     * User can toggle site status
     *
     * @param  User    $user
     * @param  Heatmap $heatmap
     * @return bool
     */
    public function toggle(User $user, Heatmap $heatmap)
    {
        return $user->id === $heatmap->site()->first()->user_id;
    }

    /**
     * User can toggle site status
     *
     * @param  User    $user
     * @param  Heatmap $heatmap
     * @return bool
     */
    public function refresh(User $user, Heatmap $heatmap)
    {
        return $user->id === $heatmap->site()->first()->user_id;
    }

    /**
     * User can toggle site status
     *
     * @param  User    $user
     * @param  Heatmap $heatmap
     * @return bool
     */
    public function deleteUrl(User $user, Heatmap $heatmap)
    {
        return $user->id === $heatmap->site()->first()->user_id;
    }
}
