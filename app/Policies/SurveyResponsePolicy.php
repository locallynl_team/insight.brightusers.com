<?php

namespace App\Policies;

use App\User;
use App\SurveyResponse;
use Illuminate\Auth\Access\HandlesAuthorization;

class SurveyResponsePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can delete the response.
     *
     * @param  \App\User           $user
     * @param  \App\SurveyResponse $response
     * @return mixed
     */
    public function delete(User $user, SurveyResponse $response)
    {
        return $user->id === $response->survey()->first()->site()->first()->user()->first()->id;
    }
}
