<?php

namespace App\Policies;

use App\User;
use App\Form;
use Illuminate\Auth\Access\HandlesAuthorization;

class FormPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the site.
     *
     * @param  User $user
     * @param  Form $form
     * @return mixed
     */
    public function view(User $user, Form $form)
    {
        return $user->id === $form->site()->first()->user_id;
    }

    /**
     * Determine whether the user can create sites.
     *
     * @param  User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the site.
     *
     * @param  User $user
     * @param  Form $form
     * @return mixed
     */
    public function update(User $user, Form $form)
    {
        return $user->id === $form->site()->first()->user_id;
    }

    /**
     * Determine whether the user can delete the site.
     *
     * @param  User $user
     * @param  Form $form
     * @return mixed
     */
    public function delete(User $user, Form $form)
    {
        return $user->id === $form->site()->first()->user_id;
    }

    /**
     * User can toggle site status
     *
     * @param  User $user
     * @param  Form $form
     * @return bool
     */
    public function toggle(User $user, Form $form)
    {
        return $user->id === $form->site()->first()->user_id;
    }

    /**
     * User can toggle site status
     *
     * @param  User $user
     * @param  Form $form
     * @return bool
     */
    public function refresh(User $user, Form $form)
    {
        return $user->id === $form->site()->first()->user_id;
    }

    /**
     * User can toggle site status
     *
     * @param  User $user
     * @param  Form $form
     * @return bool
     */
    public function deleteUrl(User $user, Form $form)
    {
        return $user->id === $form->site()->first()->user_id;
    }
}
