<?php

namespace App\Policies;

use App\User;
use App\Survey;
use Illuminate\Auth\Access\HandlesAuthorization;

class SurveyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the site.
     *
     * @param  \App\User   $user
     * @param  \App\Survey $survey
     * @return mixed
     */
    public function view(User $user, Survey $survey)
    {
        return $user->id === $survey->site()->first()->user_id;
    }

    /**
     * Determine whether the user can create sites.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the site.
     *
     * @param  \App\User   $user
     * @param  \App\Survey $survey
     * @return mixed
     */
    public function update(User $user, Survey $survey)
    {
        return $user->id === $survey->site()->first()->user_id;
    }

    /**
     * Determine whether the user can delete the site.
     *
     * @param  \App\User   $user
     * @param  \App\Survey $survey
     * @return mixed
     */
    public function delete(User $user, Survey $survey)
    {
        return $user->id === $survey->site()->first()->user_id;
    }

    /**
     * User can toggle site status
     *
     * @param  User   $user
     * @param  Survey $survey
     * @return bool
     */
    public function toggle(User $user, Survey $survey)
    {
        return $user->id === $survey->site()->first()->user_id;
    }

    /**
     * User manage survey
     *
     * @param  User   $user
     * @param  Survey $survey
     * @return bool
     */
    public function manage(User $user, Survey $survey)
    {
        return $user->id === $survey->site()->first()->user_id;
    }

    /**
     * User export survey
     *
     * @param  User   $user
     * @param  Survey $survey
     * @return bool
     */
    public function export(User $user, Survey $survey)
    {
        return $user->id === $survey->site()->first()->user_id;
    }

    /**
     * User download export of survey
     *
     * @param  User   $user
     * @param  Survey $survey
     * @return bool
     */
    public function download(User $user, Survey $survey)
    {
        return $user->id === $survey->site()->first()->user_id;
    }
}
