<?php

namespace App;

use Mollie;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'user_id',
        'site_id',
        'package_id',
        'tax',
        'payment_id',
        'subscription_id',
        'total_amount',
        'total_amount_ex',
        'status',
        'period',
        'method'
    ];

    public static $tax = 21;

    /**
     * Payment belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Payment belongs to site
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * Payment belongs to a package
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    /**
     * Payment belongs to a subscription
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    /**
     * Payment has one invoice
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function invoice()
    {
        return $this->hasOne(Invoice::class);
    }

    /**
     * Execute function on saving
     */
    public static function boot()
    {
        parent::boot();

        self::saved(
            function (Payment $payment) {
                $payment->checkInvoice();
            }
        );
    }

    /**
     * Check if we have to generate invoice
     */
    public function checkInvoice()
    {
        if ($this->invoice()->count() === 0) {
            if ($this->status == 'paid') {
                Invoice::generate($this);
            }
        }
    }

    /**
     * Make payment
     *
     * @param  $paymentMethod
     * @param  $site
     * @param  $customerId
     * @param  $packageId
     * @param  $totalPrice
     * @param  $period
     * @param  $description
     * @param  $type
     * @return bool
     * @throws
     */
    public static function makePayment(
        $paymentMethod,
        Site $site,
        $customerId,
        $packageId,
        $totalPrice,
        $period,
        $description,
        $type
    ) {
        //start payment parameters
        $paymentValues = [
            'amount' => ['value' => (string) self::addTax($totalPrice), 'currency' => 'EUR'],
            'customerId' => $customerId,
            'description' => $description,
            'sequenceType' => $type
        ];

        //only webhook on live
        if (config('app.env') === 'production') {
            $paymentValues['webhookUrl'] = route('payment.webhook');
        }

        //set method & redirect url
        $paymentValues['method'] = ($type === 'first' && $paymentMethod === 'directdebit' ? 'ideal' : $paymentMethod);
        $paymentValues['redirectUrl'] = route('subscription.check-payment', $site->id);

        //create payment object
        $molliePayment = Mollie::api()->payments()->create($paymentValues);

        //save payment in database
        self::create(
            [
            'user_id' => Auth::user()->id,
            'site_id' => $site->id,
            'package_id' => $packageId,
            'payment_id' => $molliePayment->id,
            'total_amount' => $totalPrice,
            'tax' => self::getTax(),
            'status' => $molliePayment->status,
            'period' => $period,
            'method' => $paymentMethod
            ]
        );

        //return url to banking environment
        return $molliePayment->_links->checkout->href;
    }

    /**
     * Let user make their first payment
     *
     * @param  $paymentMethod
     * @param  $customerId
     * @param  $description
     * @return mixed
     */
    public static function makeConfirmationPayment($paymentMethod, $customerId, $description)
    {
        //start payment parameters
        $paymentValues = [
            'amount' => ['value' => '0.01', 'currency' => 'EUR'],
            'customerId' => $customerId,
            'description' => $description,
            'sequenceType' => 'first'
        ];

        //only webhook on live
        if (config('app.env') === 'production') {
            $paymentValues['webhookUrl'] = route('payment.webhook');
        }

        //set method & redirect url
        $paymentValues['method'] = ($paymentMethod === 'directdebit' ? 'ideal' : $paymentMethod);
        $paymentValues['redirectUrl'] = route('account.payment-method');

        //create payment object
        $molliePayment = Mollie::api()->payments()->create($paymentValues);

        //return url to banking environment
        return $molliePayment->_links->checkout->href;
    }

    /**
     * Make a recurring payment for a user
     *
     * @return bool
     */
    public function makeRecurringPayment()
    {
        //check mandates for user
        if ($this->user->hasActivePaymentMethod() === false) {
            $this->subscription->cancel(true);
            return false;
        }

        //start payment parameters
        $paymentValues = [
            'amount' => ['value' => (string) self::addTax($this->total_amount), 'currency' => 'EUR'],
            'customerId' => $this->user->getCustomerId(),
            'description' => __('Terugkerende betaling Surve'),
            'sequenceType' => 'recurring'
        ];

        //only webhook on live
        if (config('app.env') === 'production') {
            $paymentValues['webhookUrl'] = route('payment.webhook');
        }

        //create payment object
        $molliePayment = Mollie::api()->payments()->create($paymentValues);

        //save payment-id
        $this->payment_id = $molliePayment->id;
        $this->save();
    }

    /**
     * Check payment status with Mollie
     *
     * @return boolean
     */
    public function checkPaymentStatus()
    {
        //fetch payment status
        if (!$molliePayment = Mollie::api()->payments()->get($this->payment_id)) {
            return false;
        }

        //update payment status if changed
        if ($molliePayment->status !== $this->status) {
            //cancel subscription if payment fails
            $cancelSubscription = false;

            //check statuses
            switch ($molliePayment->status) {
                case "canceled":
                    $this->update(['status' => 'canceled']);
                    $cancelSubscription = true;
                    break;

                case "pending":
                    $this->update(['status' => 'pending']);
                    break;

                case "expired":
                    $this->update(['status' => 'expired']);
                    $cancelSubscription = true;
                    break;

                case "failed":
                    $this->update(['status' => 'failed']);
                    $cancelSubscription = true;
                    break;

                case "paid":
                    $this->update(['status' => 'paid']);
                    break;
            }

            //no subscription attached to payment yet, so it's a first payment
            if ($this->subscription()->count() === 0 && $molliePayment->status === "paid") {
                //get the package
                $package = $this->package()->first();

                //calculate days
                $days = $package->period($this->period)['days'];
                $price = ($package->period($this->period)['costs'] * $package->period($this->period)['multiply']);

                //get site
                $site = $this->site()->first();

                //get current site subscription
                if ($currentSubscription = $site->activeSubscription()->first()) {
                    //calculate extra days
                    $days += $currentSubscription->calculateFreeDays($package);
                }

                //create subscription & save to payment
                $newSubscription = Subscription::addSubscription($package, $site, $days, $this->period, false, $price);
                $this->update(['subscription_id' => $newSubscription->id]);
            }

            //payment failed, cancel subscription
            if ($cancelSubscription === true && $this->method === 'recurring') {
                //we need to cancel all subscriptions for this site -> if user changes in between check their new
                //subscription has to be canceled as well
                foreach ($this->site()->first()->subscriptions()->get() as $subscription) {
                    $subscription->cancel(true);
                }
            }
        }

        return $this->status;
    }

    /**
     * Get tax value
     *
     * @return float
     */
    public static function getTax()
    {
        return self::$tax;
    }

    /**
     * Return price incl. tax
     *
     * @param  $value
     * @return float|int
     */
    public static function addTax($value)
    {
        return ($value / 100) * (100 + self::getTax());
    }
}
