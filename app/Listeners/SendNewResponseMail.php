<?php

namespace App\Listeners;

use Mail;
use App\Mail\NewResponse as NewResponseMail;
use App\User;
use App\Events\NewResponse;

class SendNewResponseMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewResponse $event
     * @return void
     */
    public function handle(NewResponse $event)
    {
        if ($event->survey->send_responses === true) {
            Mail::to($event->user)
                ->bcc(User::where('is_admin', true)->first())
                ->send(new NewResponseMail($event->user, $event->survey, $event->response));
        }
    }
}
