<?php

namespace App\Listeners;

use Mail;
use App\User;
use App\Mail\UserRegistered;
use Illuminate\Mail\Mailable;
use Illuminate\Auth\Events\Registered;

class SendUserRegisteredMail extends Mailable
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered $event
     * @return void
     */
    public function handle(Registered $event)
    {
        Mail::to(User::where('is_admin', true)->first())->send(new UserRegistered($event->user));
    }
}
