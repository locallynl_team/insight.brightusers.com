<?php

namespace App\Listeners;

use Mail;
use App\Mail\Welcome;
use Illuminate\Mail\Mailable;
use Illuminate\Auth\Events\Registered;

class SendWelcomeEmail extends Mailable
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered $event
     * @return void
     */
    public function handle(Registered $event)
    {
        Mail::to($event->user)->send(new Welcome($event->user));
    }
}
