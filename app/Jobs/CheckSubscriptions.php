<?php

namespace App\Jobs;

use Mail;
use App\User;
use App\Mail\TrialExpiry;
use App\Subscription;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckSubscriptions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //check expired subscriptions
        $this->checkExpiredSubscriptions();

        //check expiry mails for trial subscriptions
        $this->checkTrialExpiryNotices();

        //check expiry mails for cancelled subscriptions
        $this->checkCancelledExpiryNotices();
    }

    /**
     * Check expired subscriptions
     */
    private function checkExpiredSubscriptions()
    {
        $subscriptions = Subscription::with('site', 'site.user', 'package')
            ->where('active', true)
            ->whereDate('ends_at', '<=', Carbon::now())
            ->get();
        foreach ($subscriptions as $subscription) {
            //subscription was trial or cancelled by user
            if ($subscription->cancelled_at <> null || $subscription->trial === true) {
                //TODO: dispatch mail with: subscription ended

                $subscription->cancel(true);
                continue;
            }

            //user has no payment method
            if ($subscription->site->user->hasActivePaymentMethod() === false) {
                $subscription->cancel(true);
                //TODO: dispatch mail with: subscription ended
                continue;
            }

            //payment
            $subscription->charge();
        }
    }

    /**
     * Send mails to users when a trial is about to expire
     */
    private function checkTrialExpiryNotices()
    {
        //manage notices for trial subscriptions
        $subscriptions = Subscription::with('site', 'site.user', 'package')
            ->where('active', true)
            ->where('trial', true)
            ->whereDate('ends_at', '<=', Carbon::now()->addDays(7))
            ->get();

        //check remaining days & notice status
        foreach ($subscriptions as $subscription) {
            //no notices send? send first notice -> expiry in 7 days
            if ($subscription->notice_status === 0) {
                //send first notice
                Mail::to($subscription->site->user)
                    ->bcc(User::where('is_admin', true)->first())
                    ->send(new TrialExpiry($subscription, 7));

                $subscription->update(['notice_status' => 1]);
                continue;
            }

            //send notice when only 3 days are left
            if ($subscription->notice_status === 1 && $subscription->days_left <= 3) {
                //send second notice
                Mail::to($subscription->site->user)
                    ->bcc(User::where('is_admin', true)->first())
                    ->send(new TrialExpiry($subscription, 3));

                $subscription->update(['notice_status' => 2]);
                continue;
            }

            //send notice when only 1 day is left
            if ($subscription->notice_status === 2 && $subscription->days_left === 1) {
                //send third notice
                Mail::to($subscription->site->user)
                    ->bcc(User::where('is_admin', true)->first())
                    ->send(new TrialExpiry($subscription, 1));

                $subscription->update(['notice_status' => 3]);
                continue;
            }
        }
    }

    /**
     * Check subscriptions which are cancelled & let them know they will expire soon
     */
    private function checkCancelledExpiryNotices()
    {
        //manage notices for trial subscriptions
        $subscriptions = Subscription::with('site', 'site.user', 'package')
            ->where('active', true)
            ->where('trial', false)
            ->whereNotNull('cancelled_at')
            ->whereDate('ends_at', '<=', Carbon::now()->addDays(7))
            ->get();

        //check remaining days & notice status
        foreach ($subscriptions as $subscription) {
            //no notices send? send first notice -> expiry in 7 days
            if ($subscription->notice_status === 0) {
                //send first notice
                //TODO

                $subscription->update(['notice_status' => 1]);
                continue;
            }

            //send notice when only 3 days are left
            if ($subscription->notice_status === 1 && $subscription->days_left <= 4) {
                //send second notice
                //TODO

                $subscription->update(['notice_status' => 2]);
                continue;
            }

            //send notice when only 1 day is left
            if ($subscription->notice_status === 2 && $subscription->days_left === 2) {
                //send third notice
                //TODO

                $subscription->update(['notice_status' => 3]);
                continue;
            }
        }
    }
}
