<?php

namespace App\Jobs;

use App\SurveyResponseExport;
use App\Exports\SurveyResponsesExport;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GenerateResponseExport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $export;

    /**
     * GenerateResponseExport constructor.
     *
     * @param SurveyResponseExport $export
     */
    public function __construct(SurveyResponseExport $export)
    {
        $this->export = $export;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new SurveyResponsesExport($this->export))->store('exports/'.$this->export->id.'.'.$this->export->type);

        $this->export->status = true;
        $this->export->save();
    }
}
