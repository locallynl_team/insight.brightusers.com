<?php

namespace App\Jobs;

use App\Payment;
use App\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use ConsoleTVs\Invoices\Classes\Invoice as InvoicePDF;

class GenerateInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $invoice;

    /**
     * GenerateInvoice constructor.
     *
     * @param Invoice $invoice
     */
    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $payment = $this->invoice->payment()->first();

        //create invoice
        $pdfInvoice = InvoicePDF::make(__('Factuur Surve #:invoice', ['invoice' => $this->invoice->id]));
        $pdfInvoice->number($this->invoice->id);
        $pdfInvoice->date($payment->created_at->copy());

        //add items
        foreach ($this->invoice->rows()->get() as $row) {
            $pdfInvoice->addItem($row['title'].PHP_EOL.$row['description'], $row['amount']);
        }

        //add customer
        $pdfInvoice->customer([
            'name' => $this->invoice->full_name,
            'company_name' => $this->invoice->company_name,
            'id' => $payment->user_id,
            'address' => $this->invoice->address,
            'postal_code' => $this->invoice->postal_code,
            'city' => $this->invoice->city
        ]);

        //add footnote
        if ($payment->status <> 'paid') {
            $pdfInvoice->footnote(
                __('Uw betaling ontvangen we graag binnen 30 dagen op bovenstaand rekeningnummer,'.
                    ' onder vermelding van het factuurnummer.')
            );
        }

        //save to storage
        $pdfInvoice->template('invoice');
        $pdfInvoice->save('invoices/'.$this->invoice->id.'.pdf');

        //update invoice created status
        $this->invoice->generated = true;
        $this->invoice->save();

        //send invoice to user
        $this->invoice->send();
    }
}
