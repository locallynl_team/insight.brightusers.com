<?php

namespace App\Jobs;

use Storage;
use Exception;
use App\HeatmapScreenshot;
use App\Heatmap\HeatmapScreenshotServerDriver;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GenerateScreenshot implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $screenshot;
    protected $profile;
    protected $heatmap;

    /**
     * Create a new job instance.
     *
     * @param  $screenshot HeatmapScreenshot
     * @return void
     */
    public function __construct(HeatmapScreenshot $screenshot)
    {
        $this->screenshot = $screenshot;
        $this->profile = $screenshot->heatmapProfile()->first();
        $this->heatmap = $screenshot->heatmap()->first();
    }

    /**
     * Execute the job.
     *
     * @throws Exception
     * @return void
     */
    public function handle()
    {
        /**
         * We need a screenshot URL
         */
        if (empty($this->heatmap->screenshot_url) && empty($this->heatmap->static_screenshot_url)) {
            throw new Exception("No heatmap screenshot URL set");
        }

        /**
         * Set filename & generate screenshot
         */
        $filename = md5($this->heatmap->id."-".$this->profile->id."-".time());
        $output = $this->createScreenshot($filename);

        /**
         * Get image size
         */
        if (file_exists(base_path('public/storage/screenshots/'.$filename.'.jpg'))) {
            $imageSize = getimagesize(base_path('public/storage/screenshots/' . $filename . '.jpg'));
        } else {
            throw new Exception("File not found: ".base_path('public/storage/screenshots/'.$filename.'.jpg'));
        }

        /**
         * Update heatmap screen with data
         */
        $this->screenshot->fill(
            [
            'filename' => $filename,
            'metadata' => $output,
            'screen_width' => $imageSize[0],
            'screen_height' => $imageSize[1]
            ]
        )->save();
    }

    /**
     * @param  $filename
     * @return mixed
     * @throws Exception
     */
    public function createScreenshot($filename)
    {
        /**
         * Static or found url?
         */
        $url = ($this->heatmap->static_screenshot_url ?? $this->heatmap->screenshot_url);

        /**
         * Start new screenshot driver
         */
        $driver = new HeatmapScreenshotServerDriver($url, $this->profile->emulator, $filename);

        // Log to queue log
        echo "Creating screenshot:\nscreenshot_id: " . $this->screenshot->id .
            "\nurl: " . $this->heatmap->screenshot_url .
            "\nemulator: " . $this->profile->emulator .
            "\nfilename: " . $filename . "\n";

        /**
         * If screenshot can't be made, throw exception
         */
        if (!$driver->isCompleted()) {
            echo "\n\nError: " . $driver->getRawOutput();
            echo "\n\n";
            throw new Exception("CreateScreenshot failed, not completed. Output : " . $driver->getRawOutput());
        }

        /**
         * Get output from screenshot driver
         */
        $output = $driver->getOutput();
        if (empty($output)) {
            echo "\n\nError: " . $driver->getRawOutput();
            echo "\n\n";
            throw new Exception("CreateScreenshot failed, no output. Output : " . $driver->getRawOutput());
        }

        /**
         * Display for debug purpose
         */
        echo "Output: " . $driver->getRawOutput();

        return $output;
    }

    /**
     * Delete screenshot if this fails, we will try again with the next session
     *
     * @param  Exception $exception
     * @throws \Exception
     */
    public function failed(Exception $exception)
    {
        $this->screenshot->delete();

        echo "Failed job: ";
        echo $exception;
        echo "\n\n";
    }
}
