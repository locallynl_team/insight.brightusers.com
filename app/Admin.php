<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Admin extends Model
{
    use Notifiable;

    public $name;
    public $email;

    /**
     * Constructor
     *
     * Admin constructor.
     */
    public function __construct()
    {
        $this->name = config('admin.name');
        $this->email = config('admin.email');
    }
}
