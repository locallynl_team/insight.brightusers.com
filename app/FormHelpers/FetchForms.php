<?php
namespace App\FormHelpers;

use App\HtmlForm;
use Illuminate\Support\Collection;

/**
 * Fetch forms from supplied URL
 *
 * Class FetchForms
 *
 * @package App\FormHelpers
 */
class FetchForms
{

    /**
     * Attributes we save for each field
     *
     * @var array
     */
    private static $saveAttributes = ['id', 'placeholder', 'name', 'type', 'required', 'value'];

    /**
     * Get all forms from a url
     *
     * @param  $url
     * @return Collection
     */
    public static function getAllForms($url) : Collection
    {
        //get page HTML
        $html = self::getHTML($url);
        if (empty($html)) {
            return collect();
        }

        //get forms from HTML
        $forms = self::processHTML($html);

        //save all forms to database & save ID
        $forms = $forms->map(
            function ($form) {
                $form['id'] = HtmlForm::saveFromHtml($form);
                return $form;
            }
        );

        //return forms
        return $forms;
    }

    /**
     * Get HTML of a page
     *
     * @param  $url
     * @return string
     */
    private static function getHTML($url) : string
    {
        //get HTML
        $client = new \GuzzleHttp\Client();

        //get page
        $request = $client->get($url);
        if (!$request) {
            return "";
        }

        //return page html
        return $request->getBody()->getContents();
    }

    /**
     * Returns collection with forms & fields from HTML
     *
     * @param  $html
     * @return Collection
     */
    private static function processHTML($html) : Collection
    {
        //save all forms
        $forms = [];

        //load HTML
        $document = new \DOMDocument();
        @$document->loadHTML($html);

        //get all forms
        $htmlForms = $document->getElementsByTagName('form');
        foreach ($htmlForms as $htmlForm) {
            //data holder for this form
            $form = [
                'html_id' => ($htmlForm->hasAttribute('id') ? $htmlForm->getAttribute('id') : null),
                'html_name' => ($htmlForm->hasAttribute('name') ? $htmlForm->getAttribute('name') : null),
                'path' => base64_encode(self::xPathToCss($htmlForm->getNodePath())),
                'fields' => self::findFields($htmlForm),
            ];

            //add to collection
            $forms[] = $form;
        }

        return collect($forms);
    }

    /**
     * Find all form fields in form element
     *
     * @param  \DOMElement $form
     * @return Collection
     */
    private static function findFields(\DOMElement $form) : Collection
    {
        //get all form fields
        $input = self::getFieldsByTag($form, 'input');
        $textarea = self::getFieldsByTag($form, 'textarea');
        $select = self::getFieldsByTag($form, 'select');

        //merge all fields to collection
        $fields = collect([$input, $textarea, $select])->flatten(1)->unique('name')->values();

        //get all labels
        $labels = self::getFormLabels($form);

        //add labels to fields
        return $fields->map(
            function ($field) use ($labels) {
                //field has id?
                if (isset($field['id'])) {
                    //label exists?
                    if ($label = $labels->where('for', $field['id'])->first()) {
                        $field['label'] = $label['value'];
                    }
                }
                return $field;
            }
        );
    }

    /**
     * Find all fields by tag
     *
     * @param  \DomElement $form
     * @param  $tag
     * @return Collection
     */
    private static function getFieldsByTag(\DomElement $form, $tag) : Collection
    {
        //get fields
        $fields = $form->getElementsByTagName($tag);

        //no fields of this type found
        if ($fields->length === 0) {
            return collect();
        }

        //check for hidden fields
        $visibleFields = collect($fields)->reject(
            function ($field) {
                return $field->hasAttribute('type') && in_array($field->getAttribute('type'), ['hidden', 'submit']);
            }
        )->map(
            function ($field) {
                    return self::processField($field);
            }
        );

        return $visibleFields;
    }

    /**
     * Process a field, save all & only data we need
     *
     * @param  $element
     * @return collection
     */
    private static function processField(\DOMElement $element) : Collection
    {
        //hold the field data
        $field = ['type' => 'text'];

        //get standard attributes
        foreach (self::$saveAttributes as $attribute) {
            if ($element->hasAttribute($attribute)) {
                $field[$attribute] = $element->getAttribute($attribute);
            }
        }

        //set type
        if ($element->tagName === 'select' || $element->tagName === 'textarea') {
            $field['type'] = strtolower($element->tagName);
        }

        return collect($field);
    }

    /**
     * Collect all form labels
     *
     * @param  \DOMElement $form
     * @return Collection
     */
    private static function getFormLabels(\DOMElement $form) : Collection
    {
        //hold data
        $labels = [];

        //get all labels
        $formLabels = $form->getElementsByTagName('label');
        foreach ($formLabels as $formLabel) {
            if ($formLabel->hasAttribute('for')) {
                $labels[] = [
                    'for' => $formLabel->getAttribute('for'),
                    'value' => trim(strip_tags(self::saveHTML($formLabel)))
                ];
            }
        }

        return collect($labels);
    }

    /**
     * Convert element to HTML
     *
     * @param  \DOMElement $formLabel
     * @return mixed
     */
    private static function saveHTML(\DOMElement $formLabel)
    {
        return array_reduce(
            iterator_to_array($formLabel->childNodes),
            function ($carry, \DOMNode $child) {
                return $carry.$child->ownerDocument->saveHTML($child);
            }
        );
    }

    /**
     * Convert Xpath to css selector
     *
     * @param  $xPath
     * @return string
     */
    private static function xPathToCss($xPath) : string
    {
        //selector
        $cssSelector = "";

        //split xpath
        $pieces = explode("/", $xPath);

        //loop trough
        foreach ($pieces as $piece) {
            if (empty($piece)) {
                continue;
            }

            //add to selector
            $piece = preg_replace('/\[(\d+)\]/i', ':nth-of-type($1)', $piece);
            $cssSelector .= " > ".$piece;
        }

        //clean up beginning
        $cssSelector = substr($cssSelector, 3);

        return $cssSelector;
    }
}
