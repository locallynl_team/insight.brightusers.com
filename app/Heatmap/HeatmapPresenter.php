<?php

namespace App\Heatmap;

use App\Heatmap;
use App\HeatmapScreenshot;

/**
 * Process heatmap data points to presentation data
 *
 * Class HeatmapPresenter
 *
 * @package App\Heatmap
 */
class HeatmapPresenter
{

    protected $heatmap;
    protected $heatmapScreenshot;
    protected $sessions;
    protected $type;
    protected $transformer;
    protected $heatmapProfile;

    /**
     * We need a heatmap URL & a Heatmap screenshot
     *
     * HeatmapPresenter constructor.
     *
     * @param  Heatmap           $heatmap
     * @param  HeatmapScreenshot $heatmapScreenshot
     * @param  $sessionsQuery
     * @param  string            $type
     * @return $this
     */
    public function __construct(
        Heatmap $heatmap,
        HeatmapScreenshot $heatmapScreenshot,
        $sessionsQuery,
        $type = 'clicks'
    ) {
        $this->heatmap = $heatmap;
        $this->heatmapScreenshot = $heatmapScreenshot;
        $this->sessions = $sessionsQuery;
        $this->type = (in_array($type, ['clicks', 'moves']) ? $type : 'clicks');

        return $this;
    }

    /**
     * Get heatmap data
     *
     * @return array
     * @throws \Exception
     */
    public function getData()
    {
        /**
         * Create new transformer based on current profile & screenshot data
         */
        $transformer = $this->getHeatmapTransformer();
        foreach ($this->sessions->cursor() as $session) {
            $transformer->updateSource($session->data);

            //moves, clicks
            $transformer->transformHeatmap($this->type === 'moves', $this->type === 'clicks');
        }

        if ($this->type === 'clicks') {
            return $transformer->getClicks();
        } else {
            return $transformer->getMoves();
        }
    }

    /**
     * Create new heatmap transformer
     *
     * @return HeatmapTransformer
     */
    public function getHeatmapTransformer()
    {
        return new HeatmapTransformer(
            $this->heatmapScreenshot->metadata,
            $this->heatmapScreenshot->heatmapProfile()->first()->device_ratio
        );
    }
}
