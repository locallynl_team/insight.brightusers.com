<?php

namespace App\Heatmap;

use Exception;
use Illuminate\Support\Str;

/**
 * Class HeatmapTransformer
 *
 * @package app\Heatmap\Presenter
 *
 * Transforms data points from a source (Client) to a target (ClientProfile)
 */
class HeatmapTransformer
{
    /**
     * Contains JSON of HTML fetched by user sessions
     *
     * @var array
     */
    private $source;

    /**
     * Contains JSON of HTML fetched by screenshot engine
     *
     * @var array
     */
    private $target;

    /**
     * The target device ratio
     *
     * @var float the target device ratio
     */
    private $targetDeviceRatio;

    /**
     * Keeps all moves
     *
     * @var array
     */
    private $moves = [];

    /**
     * Keeps all clicks
     *
     * @var array
     */
    private $clicks = [];

    /**
     * Keeps all hashes of moves
     *
     * @var array
     */
    private $moveHashes = [];

    /**
     * Moves hashes map
     *
     * @var array
     */
    private $moveHashesMap = [];

    /**
     * Keeps all hashes of clicks
     *
     * @var array
     */
    private $clickHashes = [];

    /**
     * Clicks hashes map
     *
     * @var array
     */
    private $clickHashesMap = [];


    /**
     * Create a instance that can transform heatmap data to a target
     *
     * @param $target string heatmap metadata of the target
     * @param $target_device_ratio int device ratio
     */
    public function __construct($target, $target_device_ratio)
    {
        $this->updateTarget($target);
        $this->targetDeviceRatio = $target_device_ratio;
    }

    /**
     * Replace the source by a new one
     *
     * @param  string $source
     * @return $this
     */
    public function updateSource($source)
    {
        $this->source = json_decode($source, true);
        
        return $this;
    }

    /**
     * Replace the target by a new one
     *
     * @param  string $target
     * @return $this
     */
    public function updateTarget($target)
    {
        $this->target = json_decode($target, true);
        return $this;
    }

    /**
     * Transform a heatmap from the source to the target
     *
     * @param  $processMoves
     * @param  $processClicks
     * @throws Exception
     */
    public function transformHeatmap($processMoves, $processClicks)
    {
        /**
         * No child elements?
         */
        if (!isset($this->source['c'])) {
            return;
        }

        /**
         * Walk trough child elements
         */
        $this->walk($this->source['c'], $this->target['c'], $processMoves, $processClicks);
    }

    /**
     * Traverse over the heatmap dom tree
     *
     * @param  array $source    A node of the heatmap source (sub)tree
     * @param  array $target    The reference node of the heatmap target. When iterating the source,
     * the target follows the same path
     * @param  bool  $getMoves
     * @param  bool  $getClicks
     * @throws Exception
     */
    private function walk(array $source, array $target, $getMoves = false, $getClicks = false)
    {
        if (count($source) == 0) {
            return;
        }

        /**
         * Loop trough all elements & match with target
         */
        foreach ($source as $key => $value) {
            /**
             * When no match is found, we skip.
             */
            if (isset($target[$key])) {
                //echo "Matched target key: ".$key."<br />";
                $target_key = $key;
            } else {
                list($success, $newKey) = $this->resolveTreeMismatch($target, $key);

                /**
                 * We found a match, set target & continue
                 */
                if ($success) {
                    //echo "Matched target key after Tree Resolve Mismatch: ".$key."<br />";
                    $target_key = $newKey;
                } else {
                    //echo "Nothing matched target key: ".$key."<br />";
                    //no match, skip this part
                    continue;
                }
            }

            /**
             * Compute the offset of the subnode with respect of the parent element
             * In the case where the <body> consists of only one element, loading the surve pixel may change the
             * heatmap paths.
             * For example, if only one <div> tag is present in the body, the path will be body > div.
             * However, if the surve pixel is loaded, more tags will be inserted into the <body>, so that div becomes
             * body > div:nth-child(1)
             *
             * Thus, in the recorded heatmap div:nth-child(1) may correspond to div in the target heatmap
             */
            // If subtree key endswith :nth-child(1) and prefix in target and subtree key not in target, then
            // subtree key = prefix
            if (Str::endsWith($key, ':nth-child(1)') && (!isset($target[$target_key]))) {
                $key = explode(":nth-child(1)", $key)[0];

                //echo "Changed key to: ".$key."<br />";
            }

            /**
             * Get the offset of the heatmap of this level in the target
             * Transform points from the source heatmap (which are relative to the offset) to the target heatmap by:
             * - Adding the offset
             * - Multiplying with the scale
             */
            if (((isset($value['move']) && count($value['move']) > 0 && $getMoves) ||
                    (isset($value['click']) && count($value['click']) > 0 && $getClicks))
                && isset($target[$target_key]['l'])) {
                //echo "We found clicks or moves & target L<br />";

                $targetOffset = [$target[$target_key]['l'], $target[$target_key]['t']];
                $sourceToTargetScale = [
                    ($source[$key]['b']['w'] > 0 ? $target[$target_key]['w'] / $source[$key]['b']['w'] :
                        $source[$key]['b']['w']),
                    ($source[$key]['b']['h'] > 0 ? $target[$target_key]['h'] / $source[$key]['b']['h'] : 0)
                ];

                //echo "Target offset: ".$targetOffset[0]."-".$targetOffset[1]."<br />";
                //echo "Source to target scale: ".$sourceToTargetScale[0]." - ".$sourceToTargetScale[1]."<br />";

                /**
                 * Process moves and clicks where present.
                 * Using a hashing array the number of overlapping points is reduced.
                 */
                if (isset($value['move']) && $getMoves) {
                    //echo "Transforming moves<br />";
                    //var_dump($value['move']);
                    self::transform(
                        $value['move'],
                        $targetOffset,
                        $sourceToTargetScale,
                        $this->moves,
                        $this->moveHashes,
                        $this->moveHashesMap
                    );
                }

                if (isset($value['click']) && $getClicks) {
                    //echo "Transforming clicks: ".count($value['click'])."<br />";
                    //var_dump($value['click']);
                    self::transform(
                        $value['click'],
                        $targetOffset,
                        $sourceToTargetScale,
                        $this->clicks,
                        $this->clickHashes,
                        $this->clickHashesMap
                    );
                }
            }

            /**
             * Check if the current node has nodes, and the node is present in the target.
             * Recursively process these nodes
             */
            // Process child elements if any are present in both the subtree and the target
            if (isset($value['c']) && count($value['c']) && isset($target[$key]['c'])) {
                $this->walk($value['c'], $target[$key]['c'], $getMoves, $getClicks);
            }
        }
    }

    /**
     * Try to find the right key
     *
     * @param  $target
     * @param  $key
     * @return array
     */
    private function resolveTreeMismatch($target, $key)
    {
        //If target has only one key of this tag, use that one
        if (Str::contains($key, 'nth-child(')) {
            $suggestKey = explode(":nth-child(", $key)[0];
            if (array_key_exists($suggestKey, $target)) {
                return [true, $suggestKey];
            }
        }

        return [false, false];
    }

    /**
     * Transform a list of encoded points from a source rectangle to a target rectangle
     *
     * @param $points array a list of encoded points: x1,y1,x2,y2,....
     * @param $targetOffset
     * @param $sourceToTargetScale
     * @param $output array the array to append the points to
     * @param $hashes
     * @param $hashes_count
     */
    private function transform($points, $targetOffset, $sourceToTargetScale, &$output, &$hashes, &$hashes_count)
    {
        if (count($points) % 2 != 0) {
            return;
        }

        for ($i = 0; $i < count($points); $i += 2) {
            if ($points[$i] == null || $points[$i + 1] == null) {
                continue;
            }

            $el = [
                'x' => $this->targetDeviceRatio * (int)($targetOffset[0] +
                        (1 * $points[$i] * $sourceToTargetScale[0])),
                'y' => $this->targetDeviceRatio * (int)($targetOffset[1] +
                        (1 * $points[$i + 1] * $sourceToTargetScale[1])),
                'value' => 1,
            ];

            // Optional usage of hashmap to reduce overlapping pixels
            $use_hashes = true;
            if ($use_hashes) {
                $el_hash = self::hash($el);

                // If the hash is new, add the element to the output.
                // Store the hash and the output index of which point this hash was made
                if (!in_array($el_hash, $hashes)) {
                    $hashes[] = $el_hash;
                    $hashes_count[] = 1;
                    $output[] = $el;
                } else {
                    // Find the index of the element which has the same hash
                    $index = array_search($el_hash, $hashes);
                    $output[$index]['value'] += 1;
                }
            } else {
                $output[] = $el;
            }
        }
    }

    /**
     * Magic.
     *
     * @param  $el
     * @return int
     */
    private function hash($el)
    {
        // Keep track of events in approx. the same area. This value is used as cheap hashing
        return (int)((round($el['x']) * 100000) + round($el['y']));
    }

    /**
     * Return clicks
     *
     * @return array
     */
    public function getClicks()
    {
        return $this->normalize($this->clicks);
    }

    /**
     * Return moves
     *
     * @return array
     */
    public function getMoves()
    {
        return $this->normalize($this->moves);
    }

    /**
     * Normalize an array
     *
     * @param  $elements
     * @return array
     */
    private function normalize($elements)
    {
        // Make a list of all hashes
        $max = 1;
        foreach ($elements as $key => $el) {
            /**
             * Off screen?
             */
            if ($el['x'] < 0 || $el['y'] < 0) {
                unset($elements[$key]);
                continue;
            }

            /**
             * Calculate max of value
             */
            if ($el['value'] > $max) {
                $max = $el['value'];
            }
        }

        // Normalize the values and remove the hash from the array
        $elements = array_map(
            function ($el) use ($max) {
                //TODO Magic number
                $el['value'] = round($el['value'] / (1 * $max), 2);
                $el['value'] = ($el['value'] < 0.3 ? 0.3 : $el['value']);
                return $el;
            },
            $elements
        );

        return array_values($elements);
    }
}
