<?php

namespace App\Heatmap;

/**
 * Process heatmap data points
 *
 * Class HeatmapTree
 *
 * @package App\Heatmap
 */
class HeatmapTree
{
    private $tree;

    /**
     * Start with tree data
     *
     * HeatmapTree constructor.
     *
     * @param string $tree
     */
    public function __construct(string $tree)
    {
        $this->tree = \GuzzleHttp\json_decode($tree, true);
    }

    /**
     * Merge data into the tree
     *
     * @param  string $chunk
     * @return $this for chaining
     */
    public function add(string $chunk)
    {
        $merge = \GuzzleHttp\json_decode($chunk, true);
        $this->tree = $this->merge($this->tree, $merge);

        return $this;
    }

    /**
     * Loop trough data and add new values to tree
     *
     * @param  $existing
     * @param  $adding
     * @param  bool     $DOM_level
     * @return mixed
     */
    private function merge($existing, $adding, $DOM_level = false)
    {
        // If c is set, the level is DOM elements, $adding = ['domel' => [], 'domel2' => [], ...]
        // Otherwise the level is at DOM el, $adding = ['c' => [], m, k, b ]
        foreach ($adding as $key => $value) {
            //key is new, create array
            if (!isset($existing[$key])) {
                $existing[$key] = [];
            }

            if ($DOM_level) {
                // We only expect dom elements
                // Next level is not DOM level
                $existing[$key] = $this->merge($existing[$key], $adding[$key], false);
            } else {
                // Look for cmkb
                if (in_array($key, ['move', 'click'])) {
                    $existing[$key] = array_merge($existing[$key], $adding[$key]);
                } elseif ($key == "c") {
                    // Add DOM elements
                    $existing[$key] = $this->merge($existing[$key], $adding[$key], true);
                } elseif (in_array($key, ['t','l','w','h','b'])) {
                    // These do not have to merged, but may be overwritten
                    $existing[$key] = $adding[$key];
                } else {
                    //echo("Unknown key at not-DOM level: " . $key."\n");
                }
            }
        }

        return $existing;
    }

    /**
     * Return result as JSON
     *
     * @return string
     */
    public function toJson()
    {
        return \GuzzleHttp\json_encode($this->tree);
    }
}
