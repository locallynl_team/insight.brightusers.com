<?php

namespace App\Heatmap;

use App\HeatmapScreenshot;
use Illuminate\Support\Collection;

/**
 * Process heatmap data to scroll image
 *
 * Class HeatmapPresenter
 *
 * @package App\Heatmap
 */
class HeatmapScrollImage
{

    protected $heatmapScreenshot;
    protected $dataPoints;
    protected $totalCount;
    protected $pixels;

    public $colors = [
        'FF0000', 'FE0A00', 'FD1400', 'FD1E00', 'FC2800', 'FC3200', 'FB3C00', 'FB4600',
        'FA5000', 'FA5900', 'F96300', 'F96D00', 'F87700', 'F88000', 'F78A00', 'F79300',
        'F69D00', 'F5A600', 'F5B000', 'F4B900', 'F4C300', 'F3CC00', 'F3D500', 'F2DE00',
        'F2E800', 'F1F100', 'E8F100', 'DDF000', 'D3F000', 'C9EF00', 'BFEF00', 'B5EE00',
        'ABEE00', 'A2ED00', '98EC00', '8EEC00', '84EB00', '7BEB00', '71EA00', '67EA00',
        '5EE900', '54E900', '4BE800', '41E800', '38E700', '2FE700', '25E600', '1CE600',
        '13E500', '09E500', '555555'
    ];

    /**
     * We need a Heatmap screenshot & sessions to generate a scroll image
     *
     * HeatmapPresenter constructor.
     *
     * @param  HeatmapScreenshot $heatmapScreenshot
     * @param  $sessionsQuery
     * @param  $totalCount
     * @return $this
     */
    public function __construct(HeatmapScreenshot $heatmapScreenshot, $sessionsQuery, $totalCount)
    {
        $this->heatmapScreenshot = $heatmapScreenshot;
        $this->dataPoints = clone $sessionsQuery;
        $this->dataPoints = $this->dataPoints->selectRaw('
            COUNT(*) AS session_count,
            scroll_depth AS pixels,
            ROUND(((scroll_depth / '.$heatmapScreenshot->screen_height.')*100), 1) AS scroll_depth_calc
        ')
            ->groupBy('scroll_depth_calc')
            ->having('scroll_depth_calc', '>', 0)
            ->get()
            ->sortBy('scroll_depth_calc');

        $this->totalCount = $totalCount;
        return $this;
    }

    /**
     * Return image & percentages
     *
     * @return array
     */
    public function getData()
    {
        return ['image' => $this->generateImage(), 'pixels' => $this->calculatePixels()];
    }

    /**
     * Calculate amounts with every pixel value
     *
     * @return array|Collection
     */
    public function calculatePixels()
    {
        /**
         * Calculated already?
         */
        if (count($this->pixels) > 0) {
            return $this->pixels;
        }

        /**
         * Calculate percentages & pixel values
         */
        $pixels = [];
        $total = 0;
        $pixelPosition = 0;
        foreach ($this->dataPoints as $session) {
            /**
             * Save value
             */
            $pixelPosition = (($this->heatmapScreenshot->screen_height / 100) * $session->scroll_depth_calc);

            /**
             * Set pixels to value of last piece
             */
            $pixels[$pixelPosition] = round((100 - $total), 2);

            /**
             * Calculate total percentage
             */
            $total += (($session->session_count / $this->totalCount) * 100);
        }

        /**
         * Show 0 if nobody reached the end of the page
         */
        if ($pixelPosition < $this->heatmapScreenshot->screen_height) {
            $pixels[$pixelPosition] = 0;
        }

        return $pixels;
    }

    /**
     * Generate image in base64
     *
     * @return string
     */
    public function generateImage()
    {
        /**
         * Create transparant image
         */
        $scrollDepthImage = imagecreatetruecolor(
            $this->heatmapScreenshot->screen_width,
            $this->heatmapScreenshot->screen_height
        );
        imagesavealpha($scrollDepthImage, true);
        $transparant = imagecolorallocatealpha($scrollDepthImage, 0, 0, 0, 127);
        imagecolortransparent($scrollDepthImage, $transparant);
        imagefill($scrollDepthImage, 0, 0, $transparant);

        /**
         * Holders for pixels & color data
         */
        $pixels = $this->calculatePixels();
        $oldPixelPosition = 0;
        $nextColor = 0;
        foreach ($pixels as $pixel => $percentage) {
            $nextColor = ($percentage === 0 ? 50 : ($nextColor < 0 ? 0 : $nextColor));

            /**
             * Select row color
             */
            $rowColor = imagecolorallocatealpha(
                $scrollDepthImage,
                hexdec(substr($this->colors[$nextColor], 0, 2)),
                hexdec(substr($this->colors[$nextColor], 2, 2)),
                hexdec(substr($this->colors[$nextColor], 4, 2)),
                30
            );

            /**
             * Create new rectangle for this row
             */
            imagefilledrectangle(
                $scrollDepthImage,
                0,
                $oldPixelPosition,
                $this->heatmapScreenshot->screen_width,
                ($pixel - 1),
                $rowColor
            );

            /**
             * Keep values for next row
             */
            $oldPixelPosition = $pixel;
            $nextColor = ((50 - round($percentage / 2)) + 1);
        }

        /**
         * Select row color
         */
        $rowColor = imagecolorallocatealpha(
            $scrollDepthImage,
            hexdec(substr($this->colors[50], 0, 2)),
            hexdec(substr($this->colors[50], 2, 2)),
            hexdec(substr($this->colors[50], 4, 2)),
            30
        );
        imagefilledrectangle(
            $scrollDepthImage,
            0,
            $oldPixelPosition,
            $this->heatmapScreenshot->screen_width,
            $this->heatmapScreenshot->screen_height,
            $rowColor
        );

        /**
         * Output png to buffer to get base64_encoding
         */
        ob_start();
        imagepng($scrollDepthImage);
        $imageData = ob_get_contents();
        ob_end_clean();

        return base64_encode($imageData);
    }
}
