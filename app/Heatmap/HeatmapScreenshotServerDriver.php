<?php

namespace App\Heatmap;

use Illuminate\Support\Str;
use Symfony\Component\Process\Process;

/**
 * Takes a server-side screenshot using the NPM 'Puppeteer' library.
 * Puppeteer uses a headless chrome instance to render the page, run scripts on and take a screenshot.
 *
 * This class interfaces with a script /resources/assets/js/screenshot.js.
 * The script saves a screenshot in the /public/images/sites/heatmaps/ folder.
 * It also attempts to find the selector on the page and return its size.
 *
 * This class calls the script, and does some sanity checks and error handling.
 * Variables cannot easily be moved from JS to PHP. Therefore the output is given as a string in the console log.
 * It starts with 'surve-output-element', followed by the json-encoded string.
 *
 * The json-encoded string has 5 fields, the top/left/height/width of the found element,
 * and 'count', which denotes the number of elements that match the selector
 *
 * Class HeatmapScreenshotServerDriver
 *
 * @package app\Heatmap
 */
class HeatmapScreenshotServerDriver
{
    /**
     * The output from the NPM process
     *
     * @var string
     */
    private $rawOutput;

    /**
     * @var array
     */
    private $parsedOutput;

    /**
     * @var bool
     */
    private $completed = false;

    /**
     * ServerScreenshotDriver constructor.
     *
     * @param $url
     * @param $emulator
     * @param $filename
     */
    public function __construct($url, $emulator, $filename)
    {
        $process = new Process('cd '.base_path().' && npm run-script screenshot "' . $url . '" "'
            . $emulator . '" "' . $filename . '"');
        $process->run();
        $this->rawOutput = $process->getOutput();

        if (!$process->isSuccessful()) {
            $this->rawOutput .= $process->getErrorOutput();
        }

        $this->processOutput();
    }

    /**
     * Get the parsed output as a assoc array with keys top, left, height, width, count
     *
     * @return array
     */
    public function getOutput()
    {
        return $this->parsedOutput;
    }

    /**
     * Indicates if errors were found while taking a screenshot
     *
     * @return bool
     */
    public function isCompleted()
    {
        return $this->completed;
    }

    /**
     * Get the raw output as a string
     */
    public function getRawOutput()
    {
        return $this->rawOutput;
    }

    /**
     * Looks for a line in the output that starts with "surve-output-element"
     * The remainder of that line is parsed as json
     */
    private function processOutput()
    {
        $lines = explode("\n", $this->getRawOutput());

        $key = "surve-output";
        foreach ($lines as $line) {
            if (!Str::startsWith($line, $key)) {
                continue;
            }

            $this->parsedOutput = substr($line, strlen($key));
        }

        $this->completed = true;
    }
}
