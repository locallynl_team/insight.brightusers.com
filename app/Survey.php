<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Survey class
 *
 * Class Survey
 *
 * @package App
 */
class Survey extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'site_id',
        'type',
        'title',
        'status',
        'url_target',
        'url_target_type',
        'desktop_platform',
        'mobile_platform',
        'tablet_platform',
        'time_on_page',
        'time_on_page_miliseconds',
        'leave_intent',
        'external_activation',
        'button_send',
        'button_send_close',
        'mobile_bar_title',
        'mobile_bar_subtitle',
        'mobile_bar_button_yes',
        'mobile_bar_button_no',
        'feedback_button_text',
        'feedback_button_location',
        'send_responses'
    ];

    public static $buttonTexts = [
        'send' => 'Versturen',
        'send_close' => 'Versturen en sluiten'
    ];

    public static $mobileBarTexts = [
        'title' => 'Mogen wij u iets vragen?',
        'subtitle' => 'Met uw hulp verbeteren wij onze website voor u en andere bezoekers.',
        'button_yes' => 'Ja',
        'button_no' => 'Nee'
    ];

    public static $settingsSurvey = [
        'scope' => 'surveys',
        'type' => 'survey',
        'name' => 'Survey',
        'plural' => 'Surveys',
        'url_key' => 'survey',
        'url' => 'surveys',
        'controller' => 'SurveyController',
        'question_controller' => 'SurveyQuestionController',
        'response_controller' => 'SurveyResponseController'
    ];

    public static $settingsFeedbackButton = [
        'scope' => 'feedbackButtons',
        'type' => 'feedbackbutton',
        'name' => 'Feedbackbutton',
        'plural' => 'Feedbackbuttons',
        'url_key' => 'feedbackButton',
        'url' => 'feedback-buttons',
        'controller' => 'FeedbackButtonController',
        'question_controller' => 'FeedbackButtonQuestionController',
        'response_controller' => 'FeedbackButtonResponseController'
    ];

    public static $settingsPoll = [
        'scope' => 'polls',
        'type' => 'poll',
        'name' => 'Poll',
        'plural' => 'Polls',
        'url_key' => 'poll',
        'url' => 'polls',
        'controller' => 'PollController',
        'question_controller' => 'PollQuestionController',
        'response_controller' => 'PollResponseController',
    ];

    protected $casts = [
        'desktop_platform' => 'boolean',
        'tablet_platform' => 'boolean',
        'mobile_platform' => 'boolean',
        'status' => 'boolean',
        'time_on_page' => 'boolean',
        'leave_intent' => 'boolean',
        'external_activation' => 'boolean',
        'send_responses' => 'boolean'
    ];

    protected $hidden = [
        'last_status_change',
        'site_id',
        'title',
        'status',
        'seconds_active',
        'created_at',
        'updated_at',
        'deleted_at',
        'send_responses'
    ];

    /**
     * Survey belongs to a Site
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * Survey has many questions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    /**
     * Responses
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function responses()
    {
        return $this->hasMany(SurveyResponse::class);
    }

    /**
     * Get all type = surveys
     *
     * @param  $query
     * @return mixed
     */
    public function scopeSurveys($query)
    {
        return $query->where('type', 'survey');
    }

    /**
     * Get all type = feedbackbuttons
     *
     * @param  $query
     * @return mixed
     */
    public function scopeFeedbackButtons($query)
    {
        return $query->where('type', 'feedbackbutton');
    }

    /**
     * Get all type = poll
     *
     * @param $query
     * @return mixed
     */
    public function scopePolls($query)
    {
        return $query->where('type', 'poll');
    }

    /**
     * Calculate number of days this survey was live
     *
     * @return float
     */
    public function getDaysActiveAttribute()
    {
        /**
         * Add time it is live since toggle
         */
        $currentTime = ($this->status === true ? (time() - $this->last_status_change) : 0);

        /**
         * Return days
         */
        return round(($this->seconds_active + $currentTime) / (24 * 60 * 60));
    }

    /**
     * Get total opens
     *
     * @return int
     */
    public function getOpens() : int
    {
        return DB::table('survey_statistics')->where('survey_id', $this->id)->sum('opens');
    }

    /**
     * Survey & feedback button are the same thing, but on front-end they are different
     *
     * @param  $type
     * @return array
     */
    public static function getSettingsByType($type) : array
    {
        if ($type === 'feedbackbutton') {
            return self::$settingsFeedbackButton;
        }

        if ($type === 'survey') {
            return self::$settingsSurvey;
        }

        if ($type === 'poll') {
            return self::$settingsPoll;
        }

        return [];
    }

    /**
     * Return button texts
     *
     * @return array
     */
    public static function buttonTexts()
    {
        return self::$buttonTexts;
    }

    /**
     * Return mobile bar texts
     *
     * @return array
     */
    public static function mobileBarTexts()
    {
        return self::$mobileBarTexts;
    }
}
