<?php
/**
 * Display amount
 *
 * @param  $amount
 * @return string
 */
function price($amount)
{
    return number_format($amount, 2, ",", ".");
}

function thousand($amount)
{
    return number_format($amount, 0, "", ".");
}
