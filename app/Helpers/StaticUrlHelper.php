<?php
/**
 * Return current static url
 *
 * @return string
 */
function static_url()
{
    $domain = (config('app.env') === 'local' ? 'http://' : 'https://');
    $domain .= config('app.sub_static');
    $domain .= ".";
    $domain .= config('app.domain');
    return $domain;
}

function response_url()
{
    $domain = (config('app.env') === 'local' ? 'http://' : 'https://');
    $domain .= config('app.sub_response');
    $domain .= ".";
    $domain .= config('app.domain');
    return $domain;
}
