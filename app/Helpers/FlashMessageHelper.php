<?php
/**
 * Send flash message to user
 *
 * @param $title
 * @param $message
 * @param $type
 */
function flashMessage($title, $message, $type = 'success')
{
    Request::session()->flash('flash-message-title', $title);
    Request::session()->flash('flash-message', $message);
    Request::session()->flash('flash-message-type', $type);
}
