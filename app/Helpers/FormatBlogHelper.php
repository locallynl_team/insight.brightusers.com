<?php
/**
 * Format blog content
 *
 * @param  $content
 * @return string
 */
function formatBlog($content)
{
    $postFormatter = new \App\BlogFormatter\PostFormatter();
    return $postFormatter->process($content, true);
}
