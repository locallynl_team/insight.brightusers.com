<?php
/**
 * Check permission for subscriptions
 *
 * @param  $option
 * @return bool
 */
function hasPermission($option)
{
    return \App\SubscriptionManager\SubscriptionManager::instance()->hasPermission($option);
}
