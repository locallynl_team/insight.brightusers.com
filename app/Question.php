<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;

    /**
     * Question types
     *
     * @var array
     */
    public static $types = [
        ['type' => 'text', 'text' => 'Tekstvraag'],
        ['type' => 'multi', 'text' => 'Meerkeuze vraag'],
        ['type' => 'rating', 'text' => 'Rating'],
        ['type' => 'info', 'text' => 'Informatieblok']
    ];

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'survey_id',
        'type',
        'question',
        'intro',
        'position',
        'rating_type',
        'multi_line',
        'button_next',
        'multi_select',
        'random_order',
        'rating_label_start',
        'rating_label_end',
        'after_action'
    ];

    protected $hidden = [
        'survey_id',
        'deleted_at'
    ];

    protected $casts = [
        'multi_line' => 'boolean',
        'multi_select' => 'boolean',
        'random_order' => 'boolean'
    ];

    /**
     * Turn off timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Soft deletes field
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Belongs to a survey
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    /**
     * Has many answers
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(QuestionAnswer::class);
    }

    /**
     * Get types
     *
     * @return array
     */
    public static function getTypes()
    {
        return collect(self::$types);
    }
}
