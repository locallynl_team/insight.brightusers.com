<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageOption extends Model
{
    public $timestamps = false;

    protected $fillable  = [
        'title',
        'identifier',
        'type',
        'sort_order',
        'visible'
    ];

    protected $casts = ['visible' => 'boolean'];

    /**
     * Is used by multiple package items
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function packageItems()
    {
        return $this->hasMany(PackageItem::class);
    }
}
