<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeatmapSession extends Model
{
    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'heatmap_id',
        'heatmap_profile_id',
        'fingerprint',
        'time_on_page',
        'data',
        'platform',
        'browser',
        'os',
        'orientation',
        'screen_width',
        'screen_height',
        'client_width',
        'client_height',
        'scroll_depth',
        'url'
    ];

    /**
     * Belongs to heatmap
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function heatmap()
    {
        return $this->belongsTo(Heatmap::class);
    }

    /**
     * Has one profile
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function profile()
    {
        return $this->belongsTo(HeatmapProfile::class);
    }
}
