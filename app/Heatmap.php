<?php

namespace App;

use Carbon\Carbon;
use App\Jobs\GenerateScreenshot;
use Illuminate\Database\Eloquent\Model;

class Heatmap extends Model
{
    /**
     * Casts props
     *
     * @var array
     */
    protected $casts = [
        'status' => 'boolean',
        'desktop_platform' => 'boolean',
        'tablet_platform' => 'boolean',
        'mobile_platform' => 'boolean'
    ];

    protected $fillable = [
        'title',
        'url_target',
        'url_target_type',
        'status',
        'site_id',
        'platform_desktop',
        'platform_tablet',
        'platform_mobile',
        'static_screenshot_url'
    ];

    protected $hidden = [
        'site_id',
        'status',
        'title',
        'seconds_active',
        'last_status_change',
        'created_at',
        'updated_at',
        'screenshot_url',
        'static_screenshot_url'
    ];

    /**
     * Belongs to site
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * Heatmap has many sessions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessions()
    {
        return $this->hasMany(HeatmapSession::class);
    }

    /**
     * Every url has many screenshots, for every profile_id one
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function screenshots()
    {
        return $this->hasMany(HeatmapScreenshot::class);
    }

    /**
     * Calculate number of days this survey was live
     *
     * @return float
     */
    public function getDaysActiveAttribute()
    {
        /**
         * Add time it is live since toggle
         */
        $currentTime = ($this->status === true ? (time() - $this->last_status_change) : 0);

        /**
         * Return days
         */
        return round(($this->seconds_active + $currentTime) / (24 * 60 * 60));
    }

    /**
     * Do we need a screenshot for this profile
     *
     * @param HeatmapProfile $heatmapProfile
     */
    public function checkScreenshot(HeatmapProfile $heatmapProfile)
    {
        if ($this->screenshots()
                ->where('heatmap_profile_id', $heatmapProfile->id)
                ->where('updated_at', '>', Carbon::now()->subDays(30))
                ->count() === 0) {
            $heatmapScreenshot = HeatmapScreenshot::firstOrCreate(
                [
                'heatmap_id' => $this->id,
                'heatmap_profile_id' => $heatmapProfile->id
                ]
            );

            dispatch(new GenerateScreenshot($heatmapScreenshot));
        }
    }

    /**
     * Delete heatmap & all data
     *
     * @throws \Exception
     */
    public function deleteComplete()
    {
        /**
         * Delete screenshots
         */
        foreach ($this->screenshots()->get() as $screenshot) {
            $screenshot->deleteScreenshot();
            $screenshot->delete();
        }

        /**
         * Delete sessions
         */
        $this->sessions()->delete();

        /**
         * Delete heatmap
         */
        $this->delete();
    }

    /**
     * Check if heatmap reached session limit
     */
    public function checkLimit()
    {
        //get current subscription
        $subscription = $this->site->activeSubscription()->first();
        if (!$subscription) {
            return false;
        }

        //get limit for this subscription
        $option = $subscription->getOption('max_heatmap_views');

        //check if sessions exeed limit
        if ($this->sessions()->count() > $option->allowed_usage) {
            //deactivate heatmap
            $this->status = false;

            //calculate time...
            $this->seconds_active += (time() - $this->last_status_change);
            $this->last_status_change = time();

            //save
            $this->save();
        }

        return true;
    }

    /**
     * Get all profiles with screenshot & session count
     *
     * @return \Illuminate\Support\Collection
     */
    public function getProfilesWithCount()
    {
        return HeatmapProfile::selectRaw(
            "
            `heatmap_profiles`.*,
            (SELECT
                COUNT(*)
            FROM
                `heatmap_sessions`
            WHERE
                `heatmap_sessions`.`heatmap_id` = '".$this->id."'
                    &&
                `heatmap_profiles`.`id` = `heatmap_sessions`.`heatmap_profile_id`
            ) AS session_count
            "
        )->get();
    }
}
