<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SiteStatistic
 *
 * @package App
 */
class SiteStatistic extends Model
{
    public $timestamps = false;
    public $fillable = [
        'site_id',
        'platform',
        'date',
        'visits',
        'views'
    ];

    /**
     * Belongs to a Survey
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    /**
     * Belongs to a Site
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
