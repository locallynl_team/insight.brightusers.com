<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SurveyResponseExport extends Model
{
    public static $types = [
        //'pdf' => 'PDF',
        'xlsx' => 'Excel',
        'csv' => 'CSV'
    ];

    /**
     * Belongs to Survey
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    /**
     * Generate responses query from export
     *
     * @return mixed
     */
    public function getResponsesQuery()
    {
        /**
         * Get survey
         */
        $responsesQuery = $this->survey()->first()->responses();

        /**
         * Apply filters
         */
        if ($this->start_period && $this->end_period) {
            $responsesQuery->whereBetween(
                'created_at',
                [
                $this->formatPeriod($this->start_period).' 00:00:00',
                $this->formatPeriod($this->end_period).' 23:59:59'
                ]
            );
        }

        if ($this->url) {
            $responsesQuery->where('url', $this->url);
        }

        return $responsesQuery;
    }

    /**
     * Return timestamp for period
     *
     * @param  $period
     * @return string
     */
    public static function formatPeriod($period)
    {
        return Carbon::parse($period)->toDateString();
    }
}
