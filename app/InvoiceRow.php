<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceRow extends Model
{
    protected $fillable = [
        'invoice_id',
        'title',
        'description',
        'amount',
        'tax'
    ];

    public $timestamps = false;

    /**
     * Invoice Item belongs to invoice
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}
