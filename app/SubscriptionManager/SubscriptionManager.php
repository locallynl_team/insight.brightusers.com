<?php

namespace App\SubscriptionManager;

use App\Site;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;

class SubscriptionManager
{

    protected $site;
    protected $subscription;
    protected $options = null;

    /**
     * Setup subscription manager
     *
     * SubscriptionManager constructor.
     *
     * @param Site $site
     */
    private function __construct()
    {
        $this->site = Site::find(auth()->user()->currentSite());
        $this->subscription = $this->site->activeSubscription()->first();

        return $this;
    }

    /**
     * Singleton subscription manager
     *
     * @return SubscriptionManager|null
     */
    public static function instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new SubscriptionManager();
        }

        return $inst;
    }

    /**
     * User has permission?
     *
     * @param  $option
     * @return bool
     */
    public function hasPermission($option)
    {
        if ($this->option($option) === null) {
            return false;
        }

        $checkType = Str::camel("check ".$this->option($option)->packageItem->packageOption->type);

        if (method_exists($this, $checkType)) {
            return $this->$checkType($option);
        }

        return false;
    }

    private function checkInteger($option)
    {
        return (int) $this->option($option)->allowed_usage;
    }

    private function checkBoolean($option)
    {
        return (bool) $this->option($option)->allowed_usage;
    }

    private function checkTimePeriod($option)
    {
        //TODO: not used yet
    }

    /**
     * Get single option
     *
     * @param  $option
     * @return mixed
     */
    public function option($option)
    {
        if ($this->options === null) {
            $this->getOptions();
        }

        return $this->options->where('packageItem.packageOption.identifier', $option)->first();
    }

    /**
     * Get options for this subscription
     */
    public function getOptions()
    {
        if ($this->subscription === null) {
            $this->options = new Collection();
            return;
        }

        $this->options = $this->subscription->options()->with(['packageItem', 'packageItem.packageOption'])->get();
    }
}
