<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class HasWebsite
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //go to create website if has no sites
        if (Auth::user()->sites()->count() === 0) {
            return redirect()->route('site.create');
        }

        return $next($request);
    }
}
