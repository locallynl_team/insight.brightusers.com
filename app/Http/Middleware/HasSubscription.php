<?php


namespace App\Http\Middleware;

use App\Site;
use Auth;
use Closure;

class HasSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //get current website
        $currentSite = Site::find(Auth::user()->currentSite());

        //check for active subscription and active status
        if ($currentSite === null || $currentSite->active === false || $currentSite->activeSubscription()->count() === 0) {
            return redirect()->route('subscription.manage', $currentSite->id);
        }

        return $next($request);
    }
}
