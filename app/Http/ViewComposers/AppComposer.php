<?php

namespace App\Http\ViewComposers;

use App\Payment;
use Illuminate\View\View;

class AppComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('user_sites', auth()->user()->sites()->get());

        $view->with('current_site', auth()->user()->sites()->where('id', auth()->user()->currentSite())->first());

        $view->with(
            'open_invoices_user',
            Payment::where('status', 'open')
                ->where('method', 'invoice')
                ->where('user_id', auth()->user()->id)
                ->count()
        );

        //get open invoices if user is admin
        if (auth()->user()->isAdmin()) {
            $view->with('open_invoices', Payment::where('status', 'open')->where('method', 'invoice')->count());
        }
    }
}
