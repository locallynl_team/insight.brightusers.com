<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class BreadcrumbComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        //CSS fix: breadcrumb (purge css removes this class)
        $view->with('global_breadcrumbs', Breadcrumbs::generate());
    }
}
