<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrivacyController extends Controller
{
    public function index()
    {

        $title = 'Surve - Privacyverklaring';
        $description = 'Lees hoe Surve omgaat met uw privacy en de verzamelde data. Onze privacyverklaring bevat hierover alle informatie die u nodig heeft.';

        return view('frontend.privacy')->with(compact('title', 'description'));
    }
}
