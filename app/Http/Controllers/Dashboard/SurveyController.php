<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\StorePollRequest;
use App\Question;
use App\Site;
use App\Survey;
use Illuminate\Http\Request;
use App\UrlTarget\UrlTarget;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSurveyRequest;
use App\Http\Requests\StoreFeedbackButtonRequest;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class SurveyController extends Controller
{
    public $type = 'survey';
    public $settings = [];

    /**
     * Set settings according to type
     *
     * SurveyController constructor.
     */
    public function __construct()
    {
        $this->settings = Survey::getSettingsByType($this->type);
    }

    /**
     * Show overview of surveys
     *
     * @param  Request $request
     * @return $this
     */
    public function index(Request $request)
    {
        $surveys = $request->user()
            ->{$this->settings['scope']}()
            ->where('site_id', $request->user()->currentSite())
            ->withCount('responses')
            ->orderBy('id', 'desc')
            ->get();
        $settings = $this->settings;

        Breadcrumbs::addBreadcrumb($this->settings['plural'], route($this->settings['url_key'].'.index'));

        return view('backend.survey.index')->with(compact('surveys', 'settings'));
    }

    /**
     * Create new survey
     *
     * @param  Request $request
     * @return $this
     */
    public function create(Request $request)
    {
        if (!$request->user()->can('create', Survey::class)) {
            return redirect()->route($this->settings['url_key'].'.index');
        }

        $settings = $this->settings;
        $buttonText = Survey::buttonTexts();
        $mobileBarText = Survey::mobileBarTexts();
        $urlTargets = UrlTarget::getUrlTargets();

        Breadcrumbs::addBreadcrumb($this->settings['plural'], route($this->settings['url_key'].'.index'));
        Breadcrumbs::addBreadcrumb(
            __(':single toevoegen', ['single' => strtolower($this->settings['name'])]),
            route($this->settings['url_key'].'.create')
        );

        return view('backend.survey.create')->with(compact('settings', 'buttonText', 'mobileBarText', 'urlTargets'));
    }

    /**
     * Store new survey
     *
     * @param  StoreSurveyRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeSurvey(StoreSurveyRequest $request)
    {
        return $this->store($request);
    }

    /**
     * Store Feedbackbutton
     *
     * @param  StoreFeedbackButtonRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeFeedbackButton(StoreFeedbackButtonRequest $request)
    {
        return $this->store($request);
    }

    /**
     * Store a Poll
     *
     * @param StorePollRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storePoll(StorePollRequest $request)
    {
        return $this->store($request);
    }

    /**
     * Store survey/feedbackbutton/poll
     *
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $survey = (new Survey())->fill([
            'site_id' => $request->user()->currentSite(),
            'type' => $this->settings['type'],
            'title' => $request->title,
            'send_responses' => (hasPermission('email_notification') && isset($request->send_responses)),
            'status' => false,
            'url_target' => $request->url_target,
            'url_target_type' => $request->url_target_type
        ]);

        // Add platforms
        foreach (['mobile', 'tablet', 'desktop'] as $platform) {
            $key = $platform.'_platform';
            $survey->$key = in_array($platform, $request->platforms) && hasPermission($key);
        }

        // Save triggers
        if ($this->type === 'survey' || $this->type === 'poll') {
            $survey->fill([
                'time_on_page' => (isset($request->time_on_page) ? true : false),
                'time_on_page_miliseconds' => ($request->time_on_page_miliseconds ?? 1000),
                'leave_intent' => (hasPermission('target_leave_intent') && $request->leave_intent),
                'external_activation' => (hasPermission('target_external_activation') && $request->external_activation)
            ]);
        }

        // Button texts
        $buttonTexts = Survey::buttonTexts();
        foreach (['send', 'send_close'] as $button) {
            $key = 'button_'.$button;
            $survey->{$key} = (hasPermission('button_texts') && $request->{$key} && !empty($request->{$key}) ? $request->{$key} : $buttonTexts[$button]);
        }

        // Mobile bar texts
        if ($this->type === 'survey' || $this->type === 'poll') {
            $mobileBarTexts = Survey::mobileBarTexts();
            foreach (['title', 'subtitle', 'button_yes', 'button_no'] as $button) {
                $key = 'mobile_bar_'.$button;
                $survey->{$key} = ((hasPermission('mobile_platform') || hasPermission('tablet_platform')) && $request->{$key} && !empty($request->{$key}) ? $request->{$key} : $mobileBarTexts[$button]);
            }
        }

        if ($this->type === 'feedbackbutton') {
            $survey->fill([
                'feedback_button_text' => $request->feedback_button_text,
                'feedback_button_location' => $request->feedback_button_location
            ]);
        }
        $survey->save();

        // add single poll question
        if ($this->type === 'poll') {
            (new Question())->fill([
                'type' => 'multi',
                'survey_id' => $survey->id,
                'position' => 0,
                'after_action' => -1
            ])->save();

            flashMessage(__(':single toegevoegd', ['single' => $this->settings['name']]), __('Uw nieuwe :single is toegevoegd, u kunt gelijk starten met het instellen van de vraag.', ['single' => strtolower($this->settings['name'])]));
        } else {
            flashMessage(__(':single toegevoegd', ['single' => $this->settings['name']]), __('Uw nieuwe :single is toegevoegd, u kunt gelijk starten met het instellen van de vragen.', ['single' => strtolower($this->settings['name'])]));
        }

        // Success
        return redirect()->route($this->settings['url_key'].'.manage', $survey->id);
    }

    /**
     * Edit survey
     *
     * @param  Request $request
     * @param  Survey  $survey
     * @return $this
     */
    public function edit(Request $request, Survey $survey)
    {
        if (!$request->user()->can('view', $survey)) {
            return redirect()->route($this->settings['url_key'].'.index');
        }

        $settings = $this->settings;
        $buttonText = Survey::buttonTexts();
        $platforms = [
            ($survey->desktop_platform === true ? 'desktop' : ''),
            ($survey->tablet_platform === true ? 'tablet' : ''),
            ($survey->mobile_platform === true ? 'mobile' : '')
        ];
        $urlTargets = UrlTarget::getUrlTargets();

        Breadcrumbs::addBreadcrumb($this->settings['plural'], route($this->settings['url_key'].'.index'));
        Breadcrumbs::addBreadcrumb(
            __(':single wijzigen', ['single' => strtolower($this->settings['name'])]),
            route($this->settings['url_key'].'.edit', $survey->id)
        );

        return view('backend.survey.edit')->with(compact('settings', 'buttonText', 'survey', 'platforms', 'urlTargets'));
    }

    /**
     * Save survey
     *
     * @param  StoreSurveyRequest $request
     * @param  Survey             $survey
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSurvey(StoreSurveyRequest $request, Survey $survey)
    {
        return $this->update($request, $survey);
    }

    /**
     * Save feedback button
     *
     * @param  StoreFeedbackButtonRequest $request
     * @param  Survey                     $survey
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateFeedbackButton(StoreFeedbackButtonRequest $request, Survey $survey)
    {
        return $this->update($request, $survey);
    }

    /**
     * Save poll
     *
     * @param StorePollRequest $request
     * @param Survey $survey
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePoll(StorePollRequest $request, Survey $survey)
    {
        return $this->update($request, $survey);
    }

    /**
     * Save a survey
     *
     * @param  Request $request
     * @param  Survey  $survey
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Survey $survey)
    {
        if (!$request->user()->can('update', $survey)) {
            return redirect()->route($this->settings['url_key'].'.index');
        }

        $survey->fill([
            'title' => $request->title,
            'send_responses' => (hasPermission('email_notification') && isset($request->send_responses)),
            'url_target' => $request->url_target,
            'url_target_type' => $request->url_target_type
        ]);

        // Add platforms
        foreach (['mobile', 'tablet', 'desktop'] as $platform) {
            $key = $platform.'_platform';
            $survey->$key = in_array($platform, $request->platforms) && hasPermission($key);
        }

        // Save triggers for survey
        if ($this->type === 'survey' || $this->type === 'poll') {
            $survey->fill([
                'time_on_page' => (isset($request->time_on_page) ? true : false),
                'time_on_page_miliseconds' => ($request->time_on_page_miliseconds ?? 1000),
                'leave_intent' => (hasPermission('target_leave_intent') && $request->leave_intent),
                'external_activation' => (hasPermission('target_external_activation') && $request->external_activation)
            ]);
        }

        // Button texts
        $buttonTexts = Survey::buttonTexts();
        foreach (['send', 'send_close'] as $button) {
            $key = 'button_'.$button;
            $survey->{$key} = (hasPermission('button_texts') && $request->{$key} && !empty($request->{$key}) ? $request->{$key} : $buttonTexts[$button]);
        }

        // Mobile bar texts -> only if survey
        if ($this->type === 'survey' || $this->type === 'poll') {
            $mobileBarTexts = Survey::mobileBarTexts();
            foreach (['title', 'subtitle', 'button_yes', 'button_no'] as $button) {
                $key = 'mobile_bar_'.$button;
                $survey->{$key} = ((hasPermission('mobile_platform') || hasPermission('tablet_platform')) && $request->{$key} && !empty($request->{$key}) ? $request->{$key} : $mobileBarTexts[$button]);
            }
        }

        if ($this->type === 'feedbackbutton') {
            $survey->fill([
                'feedback_button_text' => $request->feedback_button_text,
                'feedback_button_location' => $request->feedback_button_location
            ]);
        }

        $survey->save();

        //invalidate the cache for this site
        $survey->site()->first()->invalidateCache();

        // Success
        flashMessage(__(':single gewijzigd', ['single' => $this->settings['name']]), __('Uw :single is gewijzigd, het kan tot 5 minuten duren voordat dit op uw website zichtbaar is.', ['single' => strtolower($this->settings['name'])]));
        return redirect()->route($this->settings['url_key'].'.index');
    }

    /**
     * Toggle survey
     *
     * @param  Request $request
     * @param  Survey  $survey
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggle(Request $request, Survey $survey)
    {
        if (!$request->user()->can('toggle', $survey)) {
            return redirect()->route($this->settings['url_key'].'.index');
        }

        // Toggle survey
        if ($survey->status === false) {
            // Check if we can activate
            if (hasPermission($this->settings['type']) > Site::find($request->user()->currentSite())->{$this->settings['scope']}()->where('status', true)->count()) {
                // Activate
                $survey->status = true;

                flashMessage(__(':single geactiveerd', ['single' => $this->settings['name']]), __('Uw :single is geactiveerd, binnen 5 minuten wordt deze zichtbaar op uw website.', ['single' => strtolower($this->settings['name'])]));
            } else {
                $survey->status = false;

                flashMessage(__(':single niet geactiveerd', ['single' => $this->settings['name']]), __('Uw :single is niet geactiveerd, u heeft al het maximale aantal geactiveerd dat uw pakket toelaat.', ['single' => strtolower($this->settings['name'])]), 'error');
            }
        } else {
            // De-activate
            $survey->status = false;

            // Calculate active time and add to active time
            $survey->seconds_active += (time() - $survey->last_status_change);

            flashMessage(__(':single gedeactiveerd', ['single' => $this->settings['name']]), __('Uw :single is gedeactiveerd, binnen 5 minuten wordt deze niet meer getoond op uw website.', ['single' => strtolower($this->settings['name'])]));
        }

        // Save change time to calculate active time
        $survey->last_status_change = time();
        $survey->save();

        //invalidate the cache for this site
        $survey->site()->first()->invalidateCache();

        return redirect()->route($this->settings['url_key'] . '.index');
    }

    /**
     * Delete a site
     *
     * @param  Request $request
     * @param  Survey  $survey
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Request $request, Survey $survey)
    {
        if ($request->user()->can('delete', $survey)) {
            $survey->delete();
        }

        //invalidate the cache for this site
        $survey->site()->first()->invalidateCache();

        flashMessage(__(':single verwijderd', ['single' => $this->settings['name']]), __('Uw :single is verwijderd, binnen 5 minuten wordt deze niet meer getoond op uw website.', ['single' => strtolower($this->settings['name'])]));

        return redirect()->route($this->settings['url_key'].'.index');
    }
}
