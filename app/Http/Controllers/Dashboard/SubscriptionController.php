<?php

namespace App\Http\Controllers\Dashboard;

use Mollie;
use Auth;
use App\Site;
use App\Package;
use App\Payment;
use App\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProcessPaymentMethodRequest;
use App\Http\Requests\UpdatePackageRequest;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class SubscriptionController extends Controller
{
    /**
     * Manage subscription for a site
     *
     * @param  Request $request
     * @param  Site    $site
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manage(Request $request, Site $site)
    {
        //get user
        $user = $request->user();

        //owns website?
        if ($site->user_id !== $user->id) {
            return redirect()->route('site.index');
        }

        //set breadcrumbs
        Breadcrumbs::addBreadcrumb(__('Sites'), route('site.index'));
        Breadcrumbs::addBreadcrumb(__('Beheer abonnement'), route('subscription.manage', $site->id));

        //site has active subscription -> show overview, upgrade possibilities, cancellation stuff
        if ($site->activeSubscription()->count() > 0 && $this->isPaymentSelectionRequest($request) === false) {
            //get current active subscription
            $site->loadMissing(['activeSubscription', 'activeSubscription.package']);
            $subscription = $site->activeSubscription;

            //get packages
            $packages = Package::orderBy('price_monthly', 'ASC')->with('packageItems', 'packageItems.packageOption')->get();

            return view('backend.subscription.overview')->with(compact('user', 'site', 'subscription', 'packages'));

            //site has no active subscription & user has no payment method -> info set? period/package -> show payment-selection
        } elseif ($user->hasActivePaymentMethod() === false && $this->isPaymentSelectionRequest($request) === true) {
            //get package
            $package = Package::find($request->package);

            //calculate price to pay
            $period = $request->period;
            $price = round($user->calculateDiscountPrice($package->period($period)['costs'] * $package->period($period)['multiply']));

            //get payment methods
            $paymentMethods = Mollie::api()->methods()->all(['locale' => 'nl_NL', 'sequenceType' => 'recurring']);

            return view('backend.subscription.payment-selection')->with(compact('user', 'site', 'package', 'price', 'period', 'paymentMethods'));

            //site has no active subscription & info not set -> show options to select, after selection go to option above or below
        } else {
            //get packages
            $packages = Package::orderBy('price_monthly', 'ASC')->with('packageItems', 'packageItems.packageOption')->get();

            return view('backend.subscription.package-selection')->with(compact('user', 'site', 'packages'));
        }
    }

    /**
     * Check if we need to show payment selection page
     *
     * @param  Request $request
     * @return bool
     */
    public function isPaymentSelectionRequest(Request $request)
    {
        return isset($request->period) && in_array($request->period, ['monthly', 'yearly']) && isset($request->package) && Package::where('id', $request->package)->exists();
    }

    /**
     * Update current package
     *
     * @param  UpdatePackageRequest $request
     * @param  Site                 $site
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdatePackageRequest $request, Site $site)
    {
        //get user
        $user = $request->user();

        //owns website?
        if ($site->user_id !== $user->id) {
            return redirect()->route('site.index');
        }

        //get subscription
        $site->loadMissing(['activeSubscription']);

        //get package
        $package = Package::find($request->package);
        $period = ($request->pay_yearly ? 'yearly' : 'monthly');
        $costs = ($package->period($period)['costs'] * $package->period($period)['multiply']);

        //calculate free days
        $freeDays = ($site->activeSubscription <> null ? $site->activeSubscription->calculateFreeDays($package) : 0);

        //same package?
        if ($site->activeSubscription <> null && $site->activeSubscription->cancelled_at === null && $package->id === $site->activeSubscription->package_id && $site->activeSubscription->trial === false) {
            $site->activeSubscription->update(
                [
                'period' => $period,
                'price' => $costs
                ]
            );

            //success
            flashMessage(__('Pakket aangepast'), __('Uw pakket is aangepast.'));
            return redirect()->route('subscription.manage', $site->id);
        }

        //more then 0?
        if ($freeDays > 0) {
            //change package & continue
            Subscription::addSubscription($package, $site, $freeDays, $period, false, $costs);

            //success
            flashMessage(__('Pakket aangepast'), __('Uw nieuwe pakket is actief, uw vorige pakket is omgezet in :days dagen voor uw nieuwe pakket.', ['days' => $freeDays]));

            return redirect()->route('subscription.manage', $site->id);
        }

        //user has active payment method
        if ($user->hasActivePaymentMethod() === true || $user->calculateDiscountPrice($costs) <= 0) {
            //add subscription & charge user
            $subscription = Subscription::addSubscription($package, $site, $package->period($period)['days'], $period, false, $costs);
            $subscription->charge();

            //success
            flashMessage(__('Pakket aangepast'), __('Uw nieuwe pakket is geactiveerd, de betaling wordt uitgevoerd.'));

            return redirect()->route('subscription.manage', $site->id);
        }

        //show payment-method screen
        $redirectUrl = route('subscription.manage', $site->id)."?package=".$package->id."&period=".$period;
        return redirect($redirectUrl);
    }

    /**
     * Show confirmation page before ending subscription
     *
     * @param  Site $site
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cancel(Site $site)
    {
        $site->loadMissing(['activeSubscription']);

        return view('backend.subscription.cancel')->with(compact('site'));
    }

    /**
     * User has confirmed to cancel subscription
     *
     * @param  Request $request
     * @param  Site    $site
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancelConfirmed(Request $request, Site $site)
    {
        //cancel subscription
        $site->activeSubscription->cancel();

        flashMessage(__('Pakket opgezegd'), __('Uw pakket wordt niet langer verlengd en zal aan het einde van de looptijd worden beëindigd.'));

        //back to management
        return redirect()->route('subscription.manage', $site->id);
    }

    /**
     * Redirect user to Mollie for first payment
     *
     * @param  ProcessPaymentMethodRequest $request
     * @param  Site                        $site
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function processPayment(ProcessPaymentMethodRequest $request, Site $site)
    {
        //get user
        $user = $request->user();

        //owns website?
        if ($site->user_id !== $user->id) {
            return redirect()->route('dashboard');
        }

        //save invoice-details
        $user->update($request->only(['company_name', 'address', 'postal_code', 'city']));

        //package
        $package = Package::find($request->package);
        $period = ($request->period === 'yearly' ? 'yearly' : 'monthly');

        //calculate price
        $price = round($user->calculateDiscountPrice($package->period($period)['costs'] * $package->period($period)['multiply']));

        //get mollie customer id (created if necessary)
        $customerId = $user->getCustomerId();

        //make first payment
        $redirectUrl = Payment::makePayment($request->payment_method, $site, $customerId, $package->id, $price, $period, __('Eerste betaling Surve pakket :package', ['package' => $package->title]), 'first');
        return redirect($redirectUrl);
    }

    /**
     * Check payment status
     *
     * @param  Request $request
     * @param  Site    $site
     * @return \Illuminate\Http\RedirectResponse
     */
    public function checkPayment(Request $request, Site $site)
    {
        //get user
        $user = $request->user();

        //owns website?
        if ($site->user_id !== $user->id) {
            return redirect()->route('dashboard');
        }

        //try to find payment-id
        if ($payment = $user->payments()->where('status', '!=', 'paid')->where('site_id', $site->id)->orderBy('id', 'DESC')->first()) {
            $status = $payment->checkPaymentStatus();

            //payment received?
            if ($status == 'paid') {
                //update user active website
                $user->setBaseSite();

                //activate current website
                $user->switchSite($site);

                flashMessage(__('Betaling ontvangen'), __('Uw betaling is succesvol verwerkt, uw pakket is geactiveerd.'));
                return redirect()->route('dashboard');

                //payment is not yet processed, we'll wait for the webhook to be called
            } elseif ($status == 'pending' || $status == 'open') {
                flashMessage(__('Wijziging wordt uitgevoerd'), __('Uw pakket wordt geactiveerd zodra uw betaling is ontvangen, dit kan enkele minuten duren.'));

                //payment failed -> redirect back to subscription management page
            } else {
                flashMessage(__('Betaling mislukt'), __('Uw betaling is niet geslaagd, probeert u het alstublieft opnieuw.'), 'danger');
                $redirect = route('subscription.manage', $site->id)."?package=".$payment->package_id."&period=".$payment->period;
                return redirect($redirect);
            }
        }

        return redirect()->route('site.index');
    }
}
