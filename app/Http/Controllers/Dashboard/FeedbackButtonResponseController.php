<?php

namespace App\Http\Controllers\Dashboard;

class FeedbackButtonResponseController extends SurveyResponseController
{
    public $type = 'feedbackbutton';
}
