<?php

namespace App\Http\Controllers\Dashboard;

use Debugbar;
use Auth;
use Carbon\Carbon;
use App\Site;
use App\SiteStatistic;
use App\Survey;
use App\SurveyResponse;
use App\Heatmap;
use App\HeatmapSession;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Debugbar::disable();

        //hide breadcrumbs
        $hideBreadcrumbs = true;

        //user confirmed code is placed?
        $currentSite = Site::find(Auth::user()->currentSite());
        if ($currentSite->code_confirmed === false) {
            return view('backend.code-implementation')->with(compact('currentSite', 'hideBreadcrumbs'));
        }

        /**
         * Get all sites from this user, with:
         * - Views yesterday, today, last 30 days
         * - Active surveys, buttons & heatmaps
         */
        $site = Site::where('id', $currentSite->id)->with(['siteStatisticsToday', 'siteStatisticsYesterday', 'siteStatistics30Days'])->withCount(['activeSurveys', 'activeFeedbackButtons', 'activeHeatmaps'])->first();

        /**
         * Get ID's we need multiple times
         */
        $surveyIDs = Survey::where('site_id', $site->id)->get()->pluck('id');
        $heatmapIDs = Heatmap::where('site_id', $site->id)->get()->pluck('id');

        /**
         * Get statistics from today
         */
        $visitsToday = SiteStatistic::where('site_id', $site->id)->where('date', date('Y-m-d'))->sum('visits');
        $pageViewsToday = SiteStatistic::where('site_id', $site->id)->where('date', date('Y-m-d'))->sum('views');
        $reactionsToday = SurveyResponse::whereIn('survey_id', $surveyIDs)->whereDate('created_at', Carbon::today())->count();
        $heatmapSessionsToday = HeatmapSession::whereIn('heatmap_id', $heatmapIDs)->whereDate('created_at', Carbon::today())->count();

        /**
         * Get device stats
         */
        $deviceStatistics = SiteStatistic::where('site_id', $site->id)->where('date', '>', date('Y-m-d', (time()-(30*86400))))->groupBy('platform')->selectRaw('platform, SUM(visits) AS visits_count')->orderBy('visits_count', 'DESC')->get();
        $deviceTotalVisits = $deviceStatistics->sum('visits_count');

        /**
         * Get latest reactions
         */
        $latestResponses = SurveyResponse::whereIn('survey_id', $surveyIDs)->with(['survey'])->orderBy('created_at', 'DESC')->limit(5)->get();

        /**
         * Get top surveys
         */
        $topSurveys = SurveyResponse::whereIn('survey_id', $surveyIDs)->selectRaw('survey_id, COUNT(*) AS responses_count')->with(['survey'])->orderBy('responses_count', 'DESC')->groupBy('survey_id')->limit(5)->get();

        /**
         * Get top graph data
         */
        $visitsData = $site->siteStatistics30Days->flatten(1);
        $responseData = SurveyResponse::whereIn('survey_id', $surveyIDs)->selectRaw('COUNT(*) AS responses_count, DATE(created_at) AS date')->groupBy('date')->get()->flatten(1);
        $sessionData = HeatmapSession::whereIn('heatmap_id', $heatmapIDs)->selectRaw('COUNT(*) AS sessions_count, DATE(created_at) AS date')->groupBy('date')->get()->flatten(1);

        return view('backend.dashboard')->with(
            compact(
                'site',
                'visitsToday',
                'pageViewsToday',
                'reactionsToday',
                'heatmapSessionsToday',
                'deviceStatistics',
                'deviceTotalVisits',
                'latestResponses',
                'topSurveys',
                'visitsData',
                'responseData',
                'sessionData',
                'hideBreadcrumbs'
            )
        );
    }
}
