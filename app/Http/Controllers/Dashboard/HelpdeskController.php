<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class HelpdeskController extends Controller
{
    /**
     * Show helpdesk page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        Breadcrumbs::addBreadcrumb('Helpdesk', route('helpdesk'));

        return view('backend.helpdesk.index');
    }
}
