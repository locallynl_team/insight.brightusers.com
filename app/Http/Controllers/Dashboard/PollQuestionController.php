<?php

namespace App\Http\Controllers\Dashboard;

class PollQuestionController extends SurveyQuestionController
{
    public $type = 'poll';
}
