<?php

namespace App\Http\Controllers\Dashboard;

use Carbon\Carbon;
use App\Site;
use App\Heatmap;
use App\HeatmapProfile;
use App\HeatmapScreenshot;
use App\UrlTarget\UrlTarget;
use App\Heatmap\HeatmapScrollImage;
use App\Heatmap\HeatmapPresenter;
use App\Http\Requests\StoreHeatmapRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class HeatmapController extends Controller
{
    /**
     * Show heatmaps
     *
     * @param  Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $heatmaps = $request->user()->heatmaps()->where('site_id', $request->user()->currentSite())->withCount(['sessions'])->orderBy('status', 'DESC')->orderBy('created_at', 'DESC')->get();
        $urlTargets = UrlTarget::getUrlTargets();

        Breadcrumbs::addBreadcrumb('Heatmaps', route('heatmap.index'));

        return view('backend.heatmap.index')->with(compact('heatmaps', 'urlTargets'));
    }

    /**
     * Show heatmap overview of urls
     *
     * @param  Heatmap $heatmap
     * @param  Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function view(Heatmap $heatmap, Request $request)
    {
        if (!$request->user()->can('view', $heatmap)) {
            return redirect()->route('heatmap.index');
        }

        $message = "";
        $totalSessionCount = $heatmap->sessions()->count();

        //we have no data
        if ($totalSessionCount === 0) {
            flashMessage(__('Nog geen data beschikbaar'), __('Er zijn nog geen sessies geregisteerd voor deze heatmap.'), false);
            return redirect()->route('heatmap.index');
        }

        //filters
        $profiles = $heatmap->getProfilesWithCount();
        $periodStart = $request->period_start ?? null;
        $periodEnd = $request->period_end ?? null;
        $selectedType = $request->heatmap_type ?? 'clicks';
        $selectedProfile = $request->heatmap_profile ?? $profiles->where('session_count', '>', '0')->first()->id;

        //select sessions
        $sessionsQuery = $this->getSessionsQuery($heatmap, $request, $selectedProfile);
        $currentSessionCount = $sessionsQuery->count();

        //no data
        if ($currentSessionCount === 0) {
            $message = __("Er zijn geen sessies gevonden die voldoen aan uw selectie.");
            $heatmapScreenshot = null;
        } else {
            //get screenshot by profile & url
            $heatmapScreenshot = HeatmapScreenshot::where('heatmap_id', $heatmap->id)->where('heatmap_profile_id', $selectedProfile)->whereNotNull('filename')->first();
            if ($heatmapScreenshot === null) {
                $message = __("Er is nog geen screenshot beschikbaar voor deze heatmap, probeer het later nog eens.");
            } else {
                //get heatmap data
                if ($selectedType === 'scroll') {
                    //get scroll data from sessions
                    $scrollDepthData = (new HeatmapScrollImage($heatmapScreenshot, $sessionsQuery, $totalSessionCount))->getData();
                } else {
                    //get clicks & moves
                    $heatmapData = collect((new HeatmapPresenter($heatmap, $heatmapScreenshot, $sessionsQuery->limit(500), $selectedType))->getData());
                }

                //get average fold
                $averageFold = $sessionsQuery->limit(500)->avg('client_height');
            }
        }

        Breadcrumbs::addBreadcrumb('Heatmaps', route('heatmap.index'));
        Breadcrumbs::addBreadcrumb('Heatmap \''.$heatmap->title.'\'', route('heatmap.view', $heatmap->id));

        return view(
            'backend.heatmap.view',
            compact(
                'heatmap',
                'profiles',
                'totalSessionCount',
                'currentSessionCount',
                'selectedProfile',
                'selectedType',
                'periodStart',
                'periodEnd',
                'heatmapScreenshot',
                'message',
                'heatmapData',
                'averageFold',
                'scrollDepthData'
            )
        );
    }

    /**
     * Create heatmap
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Breadcrumbs::addBreadcrumb('Heatmaps', route('heatmap.index'));
        Breadcrumbs::addBreadcrumb('Heatmap toevoegen', route('heatmap.create'));

        $urlTargets = UrlTarget::getUrlTargets();

        return view('backend.heatmap.create')->with(compact('urlTargets'));
    }

    /**
     * Store heatmap
     *
     * @param  StoreHeatmapRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreHeatmapRequest $request)
    {
        if (!$request->user()->can('create', Heatmap::class)) {
            return redirect()->route('heatmap.index');
        }

        //save Heatmap
        $heatmap = (new Heatmap())->fill(
            [
            'site_id' => $request->user()->currentSite(),
            'title' => $request->title,
            'url_target' => $request->url_target,
            'url_target_type' => $request->url_target_type,
            'static_screenshot_url' => ($request->static_screenshot_url ?? null),
            'status' => false
            ]
        );

        //add platforms
        foreach (['mobile', 'tablet', 'desktop'] as $platform) {
            $key = $platform.'_platform';
            $heatmap->$key = in_array($platform, $request->platforms) && hasPermission($key);
        }
        $heatmap->save();

        //success
        flashMessage(__('Heatmap toegevoegd'), __('De heatmap is succesvol toegevoegd, om te beginnen met het verzamelen van informatie dient u hem te activeren.'));
        return redirect()->route('heatmap.index');
    }

    /**
     * Edit heatmap
     *
     * @param  $request
     * @param  Heatmap $heatmap
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, Heatmap $heatmap)
    {
        if (!$request->user()->can('update', $heatmap)) {
            return redirect()->route('heatmap.index');
        }

        $platforms = [($heatmap->desktop_platform === true ? 'desktop' : ''), ($heatmap->tablet_platform === true ? 'tablet' : ''), ($heatmap->mobile_platform === true ? 'mobile' : '')];
        $urlTargets = UrlTarget::getUrlTargets();

        //show current sessions to check if url match is correct
        $sessions = $heatmap->sessions()->selectRaw('COUNT(*) AS sessions, url, id')->groupBy('url')->orderBy('sessions', 'DESC')->limit(500)->get();

        Breadcrumbs::addBreadcrumb('Heatmaps', route('heatmap.index'));
        Breadcrumbs::addBreadcrumb('Heatmap wijzigen', route('heatmap.edit', $heatmap->id));

        return view('backend.heatmap.edit')->with(compact('heatmap', 'platforms', 'urlTargets', 'sessions'));
    }

    /**
     * Save update
     *
     * @param  StoreHeatmapRequest $request
     * @param  Heatmap             $heatmap
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreHeatmapRequest $request, Heatmap $heatmap)
    {
        if (!$request->user()->can('update', $heatmap)) {
            return redirect()->route('heatmap.index');
        }

        //regenerate all screenshots with new url
        if ($heatmap->static_screenshot_url <> $request->static_screenshot_url && $request->static_screenshot_url <> null) {
            foreach ($heatmap->screenshots()->get() as $screenshot) {
                $screenshot->reset();
            }
        }

        $heatmap->fill(
            [
            'title' => $request->title,
            'url_target' => $request->url_target,
            'url_target_type' => $request->url_target_type,
            'static_screenshot_url' => ($request->static_screenshot_url ?? null)
            ]
        );

        //add platforms
        foreach (['mobile', 'tablet', 'desktop'] as $platform) {
            $key = $platform.'_platform';
            $heatmap->$key = in_array($platform, $request->platforms) && hasPermission($key);
        }
        $heatmap->save();

        //reset the cache for this site
        $heatmap->site()->first()->invalidateCache();

        //success
        flashMessage(__('Heatmap aangepast'), __('De heatmap is succesvol aangepast!'));
        return redirect()->route('heatmap.index');
    }

    /**
     * Toggle heatmap
     *
     * @param  Request $request
     * @param  Heatmap $heatmap
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggle(Request $request, Heatmap $heatmap)
    {
        if (!$request->user()->can('toggle', $heatmap)) {
            return redirect()->route('heatmap.index');
        }

        //toggle heatmap
        if ($heatmap->status === false) {
            //check if we can activate
            if (hasPermission('heatmaps') > Site::find($request->user()->currentSite())->heatmaps()->where('status', true)->count()) {
                //activate
                $heatmap->status = true;

                flashMessage(__('Heatmap geactiveerd'), __('Uw heatmap is geactiveerd, binnen 10 minuten wordt de dataverzameling op uw website gestart.'));
            } else {
                $heatmap->status = false;

                flashMessage(__('Heatmap niet geactiveerd'), __('Uw heatmap is niet geactiveerd, u heeft al het maximale aantal geactiveerd dat uw pakket toelaat.'), 'error');
            }
        } else {
            //de-activate
            $heatmap->status = false;

            //calculate active time and add to active time
            $heatmap->seconds_active += (time() - $heatmap->last_status_change);

            flashMessage(__('Heatmap gedeactiveerd'), __('Uw heatmap is gedeactiveerd.'));
        }

        //save
        $heatmap->last_status_change = time();
        $heatmap->save();

        //reset the cache for this site
        $heatmap->site()->first()->invalidateCache();

        return redirect()->route('heatmap.index');
    }

    /**
     * Delete a site
     *
     * @param  Request $request
     * @param  Heatmap $heatmap
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Request $request, Heatmap $heatmap)
    {
        if (!$request->user()->can('delete', $heatmap)) {
            return redirect()->route('heatmap.index');
        }

        //delete heatmap
        $heatmap->deleteComplete();

        //reset the cache for this site
        $heatmap->site()->first()->invalidateCache();

        flashMessage(__('Heatmap verwijderd'), __('Uw heatmap is verwijderd, alle bijhorende gegevens zijn eveneens verwijderd.'));

        return redirect()->route('heatmap.index');
    }

    /**
     * Request to renew the screenshot
     *
     * @param  Request        $request
     * @param  Heatmap        $heatmap
     * @param  HeatmapProfile $profile
     * @return \Illuminate\Http\RedirectResponse
     */
    public function refresh(Request $request, Heatmap $heatmap, HeatmapProfile $profile)
    {
        if (!$request->user()->can('refresh', $heatmap)) {
            return redirect()->route('heatmap.index');
        }

        $heatmapScreenshot = HeatmapScreenshot::where('heatmap_id', $heatmap->id)->where('heatmap_profile_id', $profile->id)->first();
        if ($heatmapScreenshot !== null) {
            $heatmapScreenshot->reset();
        }

        return redirect()->back();
    }

    /**
     * Reset all views from this heatmap
     *
     * @param  Request $request
     * @param  Heatmap $heatmap
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(Request $request, Heatmap $heatmap)
    {
        if (!$request->user()->can('update', $heatmap)) {
            return redirect()->route('heatmap.index');
        }

        //show current sessions to check if url match is correct
        $heatmap->sessions()->delete();

        flashMessage(__('Heatmap sessies verwijderd'), __('Uw heatmap sessies zijn verwijderd, nieuwe gegevens worden vanaf nu verzameld.'));

        return redirect()->route('heatmap.index');
    }


    /**
     * Get sessions by filters
     *
     * @param  Heatmap   $heatmap
     * @param  Request   $request
     * @param  $profileId
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getSessionsQuery(Heatmap $heatmap, Request $request, $profileId)
    {
        $sessionsQuery = $heatmap->sessions();

        //filter on date
        if ($request->period_start && $request->period_end) {
            $sessionsQuery->whereBetween(
                'created_at',
                [
                self::formatPeriod($request->period_start).' 00:00:00',
                self::formatPeriod($request->period_end).' 23:59:59'
                ]
            );
        }

        //filter on profile ID
        $sessionsQuery->where('heatmap_profile_id', $profileId);
        $sessionsQuery->orderBy('created_at', 'DESC');

        return $sessionsQuery;
    }

    /**
     * Return timestamp for period
     *
     * @param  $period
     * @return string
     */
    public static function formatPeriod($period)
    {
        return Carbon::parse($period)->toDateString();
    }
}
