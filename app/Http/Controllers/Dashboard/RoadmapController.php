<?php

namespace App\Http\Controllers\Dashboard;

use App\Milestone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class RoadmapController extends Controller
{
    /**
     * Show roadmap
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $milestones = Milestone::orderBy('date_expected', 'ASC')->get();

        //fix css: bg-red bg-yellow bg-green bg-pink bg-blue, timeline-inverted
        $colors = ['red', 'yellow', 'pink', 'blue'];

        Breadcrumbs::addBreadcrumb('Roadmap', route('roadmap'));

        return view('backend.roadmap.index')->with(compact('milestones', 'colors'));
    }
}
