<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use Hash;
use Mollie;
use App\Payment;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\UpdatePaymentDetailsRequest;
use App\Http\Controllers\Controller;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Display profile page
     *
     * @return $this
     */
    public function index()
    {
        //get user
        $user = Auth::user();

        //add breadcrumbs
        Breadcrumbs::addBreadcrumb(__('Mijn gegevens'), route('account.index'));

        return view('backend.account.index')->with(compact('user'));
    }

    /**
     * Show all payments & invoices
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function invoices()
    {
        //get all user payments
        $payments = Payment::with(['site', 'package', 'invoice'])
            ->where('user_id', Auth::user()->id)
            ->where(
                function ($query) {
                    $query->where('status', 'paid')
                        ->orWhere('method', 'invoice')->get();
                }
            )->get();

        //breadcrumbs
        Breadcrumbs::addBreadcrumb(__('Facturen'), route('account.invoices'));

        return view('backend.account.invoices')->with(compact('payments'));
    }

    /**
     * Show IP filters page
     */
    public function ipFilters()
    {
        //add breadcrumbs
        Breadcrumbs::addBreadcrumb('IP Filters', route('account.ip-filters'));

        return view('backend.account.ip-filters');
    }

    /**
     * Save IP filters
     *
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateIpFilters(Request $request)
    {
        //update filters
        Auth::user()->update(['exclude_ip_addresses' => $request->exclude_ip_addresses]);

        //filters updated
        flashMessage(
            __('IP Filters aangepast'),
            __('Uw filters zijn aangepast, de geselecteerde IP-adressen worden nu uitgesloten.')
        );

        return redirect()->route('account.ip-filters');
    }

    /**
     * Update profile
     *
     * @param  UpdateProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateProfileRequest $request)
    {
        //save user data
        $user = $request->user();
        $user->fill(
            $request->only(
                [
                'first_name',
                'last_name',
                'email'
                ]
            )
        );

        //save password if changed
        if ($request->password_old !== null) {
            $user->password = Hash::make($request['password']);
        }
        $user->save();

        //message
        flashMessage(__('Wijzigingen opgeslagen'), __('De wijzigingen aan uw account zijn opgeslagen.'));

        return redirect()->route('account.index');
    }

    /**
     * Show current payment method of user & let them change it
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function paymentMethod()
    {
        //get user
        $user = Auth::user();

        if ($user->getActiveMandate() === false) {
            //get payment methods
            $paymentMethods = Mollie::api()->methods()->allActive(['locale' => 'nl_NL', 'sequenceType' => 'recurring']);
        }

        //add breadcrumbs
        Breadcrumbs::addBreadcrumb(__('Mijn account'), route('account.index'));
        Breadcrumbs::addBreadcrumb(__('Betaalmethode'), route('account.payment-method'));

        return view('backend.account.payment-method')->with(compact('user', 'paymentMethods'));
    }

    /**
     * Update user payment method
     *
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updatePaymentMethod(Request $request)
    {
        //valid payment method?
        if (!isset($request->payment_method) || !in_array($request->payment_method, ['creditcard', 'directdebit'])) {
            return redirect()->route('account.payment-method');
        }

        //perform first payment
        $paymentUrl = Payment::makeConfirmationPayment(
            $request->payment_method,
            Auth::user()->getCustomerId(),
            __('Verstrekking machtiging aan Surve')
        );

        return redirect($paymentUrl);
    }

    /**
     * Update payment details
     *
     * @param  UpdatePaymentDetailsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePaymentDetails(UpdatePaymentDetailsRequest $request)
    {
        //save payment info
        $request->user()->update(
            $request->only(
                [
                'company_name',
                'address',
                'postal_code',
                'city'
                ]
            )
        );

        //success
        flashMessage(__('Wijzigingen opgeslagen'), __('De wijzigingen aan uw factuurgegevens zijn opgeslagen.'));
        return redirect()->route('account.payment-method');
    }

    /**
     * Show confirmation screen before mandate cancel
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cancelMandate()
    {
        //get user
        $user = Auth::user();

        //check if user has active mandate
        if ($user->getActiveMandate() === false) {
            return redirect()->route('account.payment-method');
        }

        //add breadcrumbs
        Breadcrumbs::addBreadcrumb(__('Mijn account'), route('account.index'));
        Breadcrumbs::addBreadcrumb(__('Betaalmethode'), route('account.payment-method'));
        Breadcrumbs::addBreadcrumb(__('Machtiging intrekken'), route('account.cancel-mandate'));

        return view('backend.account.cancel-mandate')->with(compact('user'));
    }

    /**
     * Cancel mandate
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancelMandateConfirmed()
    {
        //get user
        $user = Auth::user();

        //check if user has active mandate
        if ($user->getActiveMandate() === false) {
            return redirect()->route('account.payment-method');
        }

        //cancel current mandate
        $mandate = $user->getActiveMandate();
        if ($mandate->method === 'invoice') {
            $user->update(['pay_on_invoice' => false]);
        } else {
            $mandate->revoke();
        }

        //success
        flashMessage(
            __('Machtiging ingetrokken'),
            __('U heeft de machtiging ingetrokken, selecteer a.u.b. een nieuwe betaalmethode indien nodig.')
        );
        return redirect()->route('account.payment-method');
    }
}
