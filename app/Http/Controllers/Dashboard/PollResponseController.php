<?php

namespace App\Http\Controllers\Dashboard;

class PollResponseController extends SurveyResponseController
{
    public $type = 'poll';
}
