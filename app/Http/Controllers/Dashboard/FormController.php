<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\UpdateFormRequest;
use App\Site;
use App\Form;
use App\FormField;
use App\FormHelpers\FetchForms;
use App\HtmlForm;
use App\UrlTarget\UrlTarget;
use App\Http\Requests\StoreFormRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class FormController extends Controller
{
    /**
     * Show all forms from this user
     *
     * @param  Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $forms = $request->user()->forms()->where('site_id', $request->user()->currentSite())->withCount(['sessions'])->orderBy('status', 'DESC')->orderBy('created_at', 'DESC')->get();
        $urlTargets = UrlTarget::getUrlTargets();

        Breadcrumbs::addBreadcrumb(__('Formulieren'), route('form.index'));

        return view('backend.form.index')->with(compact('forms', 'urlTargets'));
    }

    /**
     * View a form
     *
     * @param  Request $request
     * @param  Form    $form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function view(Request $request, Form $form)
    {
        if (!$request->user()->can('view', $form) || $form->sessions()->count() === 0) {
            return redirect()->route('form.index');
        }

        Breadcrumbs::addBreadcrumb(__('Formulieren'), route('form.index'));
        Breadcrumbs::addBreadcrumb(__('Formulier \':form\'', ['form' => $form->title]), route('form.view', $form->id));

        $fields = $form->fields()->orderBy('sort_order', 'ASC')->get();
        foreach ($fields as $field) {
            $field->results = $field->getResults();
        }

        $formResults = $form->getResults();

        return view('backend.form.view')->with(compact('form', 'fields', 'formResults'));
    }

    /**
     * Add new form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        Breadcrumbs::addBreadcrumb(__('Formulieren'), route('form.index'));
        Breadcrumbs::addBreadcrumb(__('Formulier toevoegen'), route('form.create'));

        $urlTargets = UrlTarget::getUrlTargets();

        return view('backend.form.create')->with(compact('urlTargets'));
    }

    /**
     * Store a form
     *
     * @param  StoreFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreFormRequest $request)
    {
        //get form from temporary table
        if (!$htmlForm = HtmlForm::where('id', $request->selected_form)->where('site_id', $request->user()->currentSite())->first()) {
            return redirect()->back()->withInput();
        }

        //create new form
        $form = (new Form)->fill(
            [
            'site_id' => $request->user()->currentSite(),
            'title' => $request->title,
            'url_target' => $request->url_target,
            'url_target_type' => $request->url_target_type
            ]
        );

        //add platforms
        foreach (['mobile', 'tablet', 'desktop'] as $platform) {
            $key = $platform.'_platform';
            $form->$key = in_array($platform, $request->platforms) && hasPermission($key);
        }

        //determine selector
        //TODO: one day we'll add custom selector tag like surve-form-name="form-name"
        if ($htmlForm->html_id <> null) {
            $form->selector_target_type = "id";
            $form->selector_target = $htmlForm->html_id;
        } else {
            $form->selector_target_type = "path";
            $form->selector_target = base64_decode($htmlForm->path);
        }

        //save form
        $form->save();

        //add fields to form
        $fields = json_decode($htmlForm->fields, true);
        $position = 0;
        foreach ($fields as $field) {
            (new FormField)->fill(
                [
                'form_id' => $form->id,
                'type' => $field['type'],
                'selector' => (isset($field['id']) ? 'id' : 'name'),
                'selector_value' => (isset($field['id']) ? $field['id'] : $field['name']),
                'title' => ucfirst(isset($field['label']) ? $field['label'] : (isset($field['placeholder']) ? $field['placeholder'] : (isset($field['name']) ? $field['name'] : $field['id']))),
                'sort_order' => $position
                ]
            )->save();

            $position++;
        }

        //success
        flashMessage(__('Formulier toegevoegd'), __('Het formulier is succesvol toegevoegd, om te beginnen met het verzamelen van informatie dient u hem te activeren.'));
        return redirect()->route('form.index');
    }


    /**
     * Edit a form
     *
     * @param  Request $request
     * @param  Form    $form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(Request $request, Form $form)
    {
        if (!$request->user()->can('update', $form)) {
            return redirect()->route('form.index');
        }

        //get platforms & targets
        $platforms = [($form->desktop_platform === true ? 'desktop' : ''), ($form->tablet_platform === true ? 'tablet' : ''), ($form->mobile_platform === true ? 'mobile' : '')];
        $urlTargets = UrlTarget::getUrlTargets();

        Breadcrumbs::addBreadcrumb(__('Formulieren'), route('form.index'));
        Breadcrumbs::addBreadcrumb(__('Formulier wijzigen'), route('form.edit', $form->id));

        return view('backend.form.edit')->with(compact('form', 'platforms', 'urlTargets'));
    }

    /**
     * Save updated form
     *
     * @param  UpdateFormRequest $request
     * @param  Form              $form
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateFormRequest $request, Form $form)
    {
        if (!$request->user()->can('update', $form)) {
            return redirect()->route('form.index');
        }

        //save
        $form->fill(
            [
            'title' => $request->title,
            'url_target' => $request->url_target,
            'url_target_type' => $request->url_target_type
            ]
        );

        //add platforms
        foreach (['mobile', 'tablet', 'desktop'] as $platform) {
            $key = $platform.'_platform';
            $form->$key = in_array($platform, $request->platforms) && hasPermission($key);
        }
        $form->save();

        //reset the cache for this site
        $form->site()->first()->invalidateCache();

        //success
        flashMessage(__('Formulier gewijzigd'), __('Het formulier is succesvol aangepast.'));
        return redirect()->route('form.index');
    }

    /**
     * Toggle form
     *
     * @param  Request $request
     * @param  Form    $form
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggle(Request $request, Form $form)
    {
        if (!$request->user()->can('toggle', $form)) {
            return redirect()->route('form.index');
        }

        //toggle heatmap
        if ($form->status === false) {
            //check if we can activate
            if (hasPermission('forms') > Site::find($request->user()->currentSite())->forms()->where('status', true)->count()) {
                //activate
                $form->status = true;

                flashMessage(__('Formulier geactiveerd'), __('Uw formulier is geactiveerd, binnen 10 minuten wordt de dataverzameling op uw website gestart.'));
            } else {
                $form->status = false;

                flashMessage(__('Formulier niet geactiveerd'), __('Uw formulier is niet geactiveerd, u heeft al het maximale aantal geactiveerd dat uw pakket toelaat.'), 'error');
            }
        } else {
            //de-activate
            $form->status = false;

            //calculate active time and add to active time
            $form->seconds_active += (time() - $form->last_status_change);

            flashMessage(__('Formulier gedeactiveerd'), __('Uw formulier is gedeactiveerd.'));
        }

        //save
        $form->last_status_change = time();
        $form->save();

        //reset the cache for this site
        $form->site()->first()->invalidateCache();

        return redirect()->route('form.index');
    }

    /**
     * Delete a form
     *
     * @param  Request $request
     * @param  Form    $form
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Request $request, Form $form)
    {
        if (!$request->user()->can('delete', $form)) {
            return redirect()->route('form.index');
        }

        //delete form
        $form->deleteComplete();

        flashMessage(__('Formulier verwijderd'), __('Uw formulier is verwijderd, alle bijhorende gegevens zijn eveneens verwijderd.'));

        //reset the cache for this site
        $form->site()->first()->invalidateCache();

        return redirect()->route('form.index');
    }


    /**
     * Reset sessions
     *
     * @param  Request $request
     * @param  Form    $form
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(Request $request, Form $form)
    {
        if (!$request->user()->can('delete', $form)) {
            return redirect()->route('form.index');
        }

        //delete form sessions
        $form->deleteSessions();

        flashMessage(__('Sessies verwijderd'), __('Uw sessies zijn verwijderd, het formulier begint opnieuw met data verzamelen.'));

        return redirect()->route('form.index');
    }

    /**
     * Process Ajax save function, returns errors with 422 or success with 200
     *
     * @param  StoreFormRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxSave(StoreFormRequest $request)
    {
        //if we get to this point, all good.
        return response()->json(['message' => 'success'], 200);
    }

    /**
     * Load forms from url, requested with ajax
     *
     * @param  Request $request
     * @return string
     */
    public function ajaxLoadForms(Request $request)
    {
        //check if url is supplied
        if (!$request->url || !filter_var($request->url, FILTER_VALIDATE_URL)) {
            return response()->json(['forms' => []]);
        }

        //get all forms from URL
        $forms = FetchForms::getAllForms($request->url);

        //no forms found
        if ($forms->count() === 0) {
            return response()->json(['forms' => []]);
        }

        //return forms
        return response()->json(['forms' => $forms]);
    }
}
