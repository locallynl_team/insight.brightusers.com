<?php

namespace App\Http\Controllers\Dashboard;

use App\Site;
use App\Package;
use App\Subscription;
use App\Http\Requests\UpdateSiteRequest;
use App\Http\Requests\StoreSiteRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class SiteController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //get sites with all counts
        $sites = $request->user()->sites()->with(['activeSubscription', 'activeSubscription.package'])->orderBy('active', 'DESC')->get();

        //breadcrumbs
        Breadcrumbs::addBreadcrumb('Websites', route('site.index'));

        return view('backend.site.index')->with(compact('sites'));
    }

    /**
     * Create new website
     *
     * @param  Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if (!$request->user()->can('create', Site::class)) {
            return redirect()->route('site.index');
        }

        //get packages
        $packages = Package::orderBy('price_monthly', 'ASC')->with('packageItems', 'packageItems.packageOption')->get();

        //breadcrumbs
        Breadcrumbs::addBreadcrumb('Websites', route('site.index'));
        Breadcrumbs::addBreadcrumb('Website toevoegen', route('site.create'));

        return view('backend.site.create')->with(compact('packages'));
    }

    /**
     * Store new website
     *
     * @param  StoreSiteRequest $request
     * @return $this
     */
    public function store(StoreSiteRequest $request)
    {
        //current user
        $user = $request->user();

        //save website
        $site = (new Site)->fill(
            [
            'domain' => $request->domain,
            'user_id' => $user->id,
            'active' => !$user->had_trial,
            'test_domains' => '' //TODO: Update database with default
            ]
        );
        $site->save();

        //get selected package
        $package = Package::find($request->package);

        //create trial for user
        if ($user->had_trial === false) {
            //create trial subscription
            Subscription::addSubscription($package, $site, $package->trial_days, 'monthly', true, 0);

            //user gets only one trial
            $user->had_trial = true;
            $user->save();
        } else {
            //switch user to new site
            $user->switchSite($site);

            //set period
            $period = ($request->pay_yearly ? 'yearly' : 'monthly');

            //check if user has active payment method
            if ($user->hasActivePaymentMethod()) {
                //activate website
                $site->active = true;
                $site->save();

                //price
                $price = round($package->period($period)['costs'] * $package->period($period)['multiply']);

                //create subscription & create invoice
                $subscription = Subscription::addSubscription($package, $site, $package->period($period)['days'], $period, false, $price);

                //creates invoice & charge user, will cancel subscription if fails
                $subscription->charge();
            } else {
                //show screen to update their payment details to continue
                flashMessage(__('Website toegevoegd'), __('Uw nieuwe website is toegevoegd, update uw betaalmethode om te kunnen beginnen.'));
                $redirect = route('subscription.manage', $site->id)."?package=".$package->id."&period=".$period;
                return redirect($redirect);
            }
        }

        //update user active website
        $request->user()->setBaseSite();

        //success
        flashMessage(__('Website toegevoegd'), __('Uw nieuwe website is toegevoegd, implementeer de integratie code op uw website om te beginnen.'));
        return redirect()->route('dashboard');
    }

    /**
     * Edit website
     *
     * @param  Request $request
     * @param  Site    $site
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, Site $site)
    {
        if (!$request->user()->can('update', $site)) {
            return redirect()->route('site.index');
        }

        //breadcrumbs
        Breadcrumbs::addBreadcrumb('Websites', route('site.index'));
        Breadcrumbs::addBreadcrumb('Website wijzigen', route('site.edit', $site->id));

        return view('backend.site.edit')->with(compact('site'));
    }

    /**
     * Edit website
     *
     * @param  UpdateSiteRequest $request
     * @param  Site              $site
     * @return \Illuminate\Http\RedirectResponse
     * @throws
     */
    public function update(UpdateSiteRequest $request, Site $site)
    {
        if (!$request->user()->can('update', $site)) {
            return redirect()->route('site.index');
        }

        //update website
        $site->update(
            [
            'domain' => $request->domain,
            'exclude_ip_addresses' => $request->exclude_ip_addresses,
            'test_domains' => $request->test_domains ?? ''
            ]
        );

        //check if this is new base site
        if ($request->has('base_site')) {
            $request->user()->setBaseSite($site);
        }

        //reset the cache for this site
        $site->invalidateCache();

        //success
        flashMessage(__('Website gewijzigd'), __('De website is succesvol gewijzigd.'));
        return redirect()->route('site.index');
    }

    /**
     * Delete a website screen
     *
     * @param  Request $request
     * @param  Site    $site
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, Site $site)
    {
        if (!$request->user()->can('delete', $site)) {
            return redirect()->route('site.index');
        }

        //only in-active sites
        if ($site->active === true) {
            flashMessage(__('Website niet verwijderd'), __('Deze website is nog actief, u kunt deze verwijderen zodra uw pakket is afgelopen.'), 'danger');
            return redirect()->route('site.index');
        }

        //breadcrumbs
        Breadcrumbs::addBreadcrumb('Websites', route('site.index'));
        Breadcrumbs::addBreadcrumb('Website verwijderen', route('site.delete', $site->id));

        return view('backend.site.delete')->with(compact('site'));
    }

    /**
     * Delete a site
     *
     * @param  Request $request
     * @param  Site    $site
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Request $request, Site $site)
    {
        if ($request->user()->can('delete', $site)) {
            $site->delete();
        }

        //only in-active sites
        if ($site->active === true) {
            flashMessage(__('Website niet verwijderd'), __('Deze website is nog actief, u kunt deze verwijderen zodra uw pakket is afgelopen.'), 'danger');
            return redirect()->route('site.index');
        }

        //check if we need to update active website
        $request->user()->setBaseSite();

        flashMessage(__('Website verwijderd'), __('Uw website is verwijderd, alle bijhorende data is eveneens verwijderd.'));

        return redirect()->route('site.index');
    }

    /**
     * Switch user to use other website as current website
     *
     * @param  Request $request
     * @param  Site    $site
     * @return \Illuminate\Http\RedirectResponse
     */
    public function switch(Request $request, Site $site)
    {
        if ($request->user()->can('switch', $site)) {
            $request->user()->switchSite($site);
        }

        return redirect()->route('dashboard');
    }

    /**
     * User confirmed implementation
     *
     * @param  Request $request
     * @param  Site    $site
     * @return \Illuminate\Http\RedirectResponse
     */
    public function implementation(Request $request, Site $site)
    {
        if ($request->user()->id === $site->user_id) {
            $site->code_confirmed = true;
            $site->save();
        }

        return redirect()->route('dashboard');
    }
}
