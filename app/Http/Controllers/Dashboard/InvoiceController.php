<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use Storage;
use App\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
    /**
     * Return invoice from storage, only to user it belongs to or an Admin user
     *
     * @param  Invoice $invoice
     * @return \Illuminate\Http\RedirectResponse
     */
    public function view(Invoice $invoice)
    {
        /**
         * Check if user owns this invoice or is admin
         */
        if (Auth::user()->isAdmin() || $invoice->payment()->first()->user_id === Auth::user()->id) {
            /**
             * If invoice is not yet created
             */
            if ($invoice->created === false) {
                flashMessage(__('Factuur wordt gemaakt'), __('Deze factuur wordt op dit moment gemaakt, probeer het later nog eens.'));
                return redirect()->route('account.index');
            }

            /**
             * Return invoice
             */
            return response()->file($invoice->getUrl());
        }

        /**
         * Bye fucker
         */
        return abort(403, 'Unauthorized action.');
    }
}
