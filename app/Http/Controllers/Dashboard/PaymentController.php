<?php

namespace App\Http\Controllers\Dashboard;

use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    /**
     * Receives a callback from Mollie
     *
     * @param  Request $request
     * @return string
     */
    public function webhook(Request $request)
    {
        //we need an ID
        if (!isset($request->id)) {
            return 2;
        }

        //payment exists?
        if (!$payment = Payment::where('payment_id', $request->id)->first()) {
            return 3;
        }

        return (string) $payment->checkPaymentStatus();
    }
}
