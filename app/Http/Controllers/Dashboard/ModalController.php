<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

class ModalController extends Controller
{
    /**
     * Show a modal
     *
     * @param  $type
     * @param  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($type, $id = null)
    {
        //set view
        $view = "backend.modals.".$type;

        //view exists?
        if (view()->exists($view)) {
            return view($view)->with(['id' => $id]);
        }

        //modal doesn't exist, load modal with close JS
        return view('backend.modals.close');
    }
}
