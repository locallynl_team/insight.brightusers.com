<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Package;
use App\PackageItem;
use App\Http\Controllers\Controller;
use App\PackageOption;
use App\Subscription;
use App\SubscriptionOption;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\UpdatePackageRequest;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class PackageController extends Controller
{
    /**
     * Display profile page
     *
     * @return $this
     */
    public function index()
    {
        $packages = Package::get()->sortBy('price');

        //breadcrumbs
        Breadcrumbs::addBreadcrumb('Admin', route('admin.index'));
        Breadcrumbs::addBreadcrumb('Pakketten', route('admin.package.index'));

        return view('backend.admin.packages.index')->with(compact('packages'));
    }

    /**
     * Toggle packages
     *
     * @param  Package $package
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggle(Package $package)
    {
        $package->active = !$package->active;
        $package->save();

        return redirect()->route('admin.package.index');
    }

    /**
     * Edit Package
     *
     * @param  Package $package
     * @return $this
     */
    public function edit(Package $package)
    {
        //get package options with value of package items
        $packageOptions = PackageOption::with(
            ['packageItems' => function ($query) use ($package) {
                $query->where('package_id', $package->id);
            }]
        )->get();

        //breadcrumbs
        Breadcrumbs::addBreadcrumb('Admin', route('admin.index'));
        Breadcrumbs::addBreadcrumb('Pakketten', route('admin.package.index'));
        Breadcrumbs::addBreadcrumb('Pakket wijzigen', route('admin.package.edit', $package->id));

        return view('backend.admin.packages.edit')->with(compact('package', 'packageOptions'));
    }

    /**
     * Update package
     *
     * @param  Package              $package
     * @param  UpdatePackageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Package $package, UpdatePackageRequest $request)
    {
        //update package
        $package->fill($request->only(['title', 'price_monthly', 'price_yearly']))->save();

        //update subscriptions later
        $subscriptions = Subscription::where('package_id', $package->id)->with(['options'])->get();

        //save package items
        if ($request->options) {
            foreach ($request->options as $option => $value) {
                PackageOption::findOrFail($option);

                //find or create the package items
                $packageItem = PackageItem::firstOrCreate(
                    ['package_option_id' => $option, 'package_id' => $package->id]
                )->fill(
                    [
                    'amount' => intval($value)
                    ]
                );
                $packageItem->save();

                //loop all subscriptions to check if we need to add or update the values
                if ($subscriptions->count() > 0) {
                    foreach ($subscriptions as $subscription) {
                        $subscriptionOption = SubscriptionOption::firstOrCreate(
                            ['subscription_id' => $subscription->id, 'package_item_id' => $packageItem->id]
                        );
                        $subscriptionOption->fill(
                            ['allowed_usage' =>
                                (isset($subscriptionOption->allowed_usage) &&
                                $subscriptionOption->allowed_usage > intval($value) ? $subscriptionOption->allowed_usage
                                    : intval($value))
                            ]
                        );
                        $subscriptionOption->save();
                    }
                }
            }
        }

        flashMessage(__('Pakket gewijzigd'), __('Het pakket is succesvol gewijzigd.'));
        return redirect()->route('admin.package.index');
    }
}
