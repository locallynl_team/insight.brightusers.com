<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Invoice;
use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class InvoiceController extends Controller
{
    /**
     * Show all invoices
     *
     * @return $this
     */
    public function index()
    {
        $payments = Payment::orderBy('id', 'desc')->with(['user', 'invoice'])->paginate(30);

        Breadcrumbs::addBreadcrumb(__('Admin'), route('admin.index'));
        Breadcrumbs::addBreadcrumb(__('Facturen'), route('admin.invoice.index'));

        return view('backend.admin.invoices.index')->with(compact('payments'));
    }

    /**
     * Register payment for invoice
     *
     * @param  Invoice $invoice
     * @return \Illuminate\Http\RedirectResponse
     */
    public function registerPayment(Invoice $invoice)
    {
        $invoice->payment->status = 'paid';
        $invoice->payment->save();

        return redirect()->back();
    }
}
