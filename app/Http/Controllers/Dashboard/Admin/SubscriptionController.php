<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Package;
use App\PackageOption;
use App\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UpdateSubscriptionRequest;

class SubscriptionController extends Controller
{
    /**
     * Edit existing subscription
     *
     * @param  Subscription $subscription
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Subscription $subscription)
    {
        $subscription->loadMissing(['package', 'package.packageItems', 'options']);

        $packages = Package::where('active', true)->get();

        //get package options with value of package items
        $packageOptions = PackageOption::with(
            ['packageItems.subscriptionItems' => function ($query) use ($subscription) {
                $query->where('subscription_id', $subscription->id);
            }]
        )->get();

        return view('backend.admin.subscription.edit')->with(compact('subscription', 'packages', 'packageOptions'));
    }

    /**
     * Update subscription
     *
     * @param  Subscription              $subscription
     * @param  UpdateSubscriptionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Subscription $subscription, UpdateSubscriptionRequest $request)
    {
        $package = $subscription->package_id;

        //save subscription
        $subscription->fill(
            [
            'package_id' => $request->package,
            'active' => (isset($request->active) ? true : false),
            'trial' => (isset($request->trial) ? true : false),
            'period' => $request->period,
            'cancelled_at' => ($request->cancelled_at ?? null),
            'ends_at' => $request->ends_at,
            'price' => $request->price
            ]
        )->save();

        //save options
        foreach ($request->options as $option => $value) {
            //get old package option id
            $packageOption = PackageOption::where('id', $option)->with(
                ['packageItems' => function ($query) use ($package) {
                    $query->where('package_id', $package);
                }]
            )->first();

            //locate current subscription option id
            $subscriptionOption = $subscription->options()->where('package_item_id', $packageOption->packageItems->first()->id)->firstOrNew(
                [
                'package_item_id' => $packageOption->packageItems->first()->id,
                'subscription_id' => $subscription->id
                ]
            );

            //update value
            $subscriptionOption->allowed_usage = intval($value);

            ///package changed?
            if ($package <> $request->package) {
                $newPackage = $request->package;
                $packageOptionNew = PackageOption::where('id', $option)->with(
                    ['packageItems' => function ($query) use ($newPackage) {
                        $query->where('package_id', $newPackage);
                    }]
                )->first();

                $subscriptionOption->package_item_id = $packageOptionNew->packageItems->first()->id;
            }

            $subscriptionOption->save();
        }

        flashMessage(__('Abonnement gewijzigd'), __('Het abonnement is succesvol gewijzigd.'));
        return redirect()->route('admin.site.index');
    }
}
