<?php

namespace App\Http\Controllers\Dashboard\Admin;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use App\Http\Requests\Admin\UpdateUserRequest;

class UserController extends Controller
{
    /**
     * Show users
     *
     * @return $this
     */
    public function index()
    {
        $users = User::orderBy('id', 'asc')->with(['payments'])->withCount(['sites', 'surveys', 'feedbackbuttons', 'heatmaps', 'forms'])->paginate(30);

        Breadcrumbs::addBreadcrumb(__('Admin'), route('admin.index'));
        Breadcrumbs::addBreadcrumb(__('Gebruikers'), route('admin.user.index'));

        return view('backend.admin.users.index')->with(compact('users'));
    }

    /**
     * Show edit page
     *
     * @param  User $user
     * @return $this
     */
    public function edit(User $user)
    {
        return view('backend.admin.users.edit')->with(compact('user'));
    }

    /**
     * Update user
     *
     * @param  UpdateUserRequest $request
     * @param  User              $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user->fill(
            $request->all()
        );
        $user->pay_on_invoice = (isset($request->pay_on_invoice) ? true : false);
        $user->save();

        flashMessage(__('Gebruiker gewijzigd'), __('De gebruiker is succesvol gewijzigd.'));
        return redirect()->route('admin.user.edit', $user->id);
    }

    /**
     * Login as user
     *
     * @param  User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(User $user)
    {
        Auth::login($user);
        return redirect()->route('dashboard');
    }
}
