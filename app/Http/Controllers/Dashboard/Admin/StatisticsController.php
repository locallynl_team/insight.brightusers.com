<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Payment;
use App\SiteStatistic;
use App\SurveyResponse;
use App\User;
use App\Site;
use App\Heatmap;
use App\Survey;
use App\HeatmapSession;
use App\Http\Controllers\Controller;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class StatisticsController extends Controller
{
    public function index()
    {
        $users = User::count();
        $sites = Site::count();
        $surveys = Survey::surveys()->count();
        $feedbackButtons = Survey::feedbackButtons()->count();
        $responses = SurveyResponse::count();
        $heatmaps = Heatmap::count();
        $heatmapSessions = HeatmapSession::count();
        $visits = SiteStatistic::sum('visits');
        $views = SiteStatistic::sum('views');
        $totalRevenue = Payment::sum('total_amount');

        /**
         * Get top graph data
         */
        $visitsData = Site::with('siteStatistics30Days')->get()->pluck('siteStatistics30Days')->flatten(1);
        $responseData = SurveyResponse::selectRaw('COUNT(*) AS responses_count, DATE(created_at) AS date')->groupBy('date')->get()->flatten(1);
        $sessionData = HeatmapSession::selectRaw('COUNT(*) AS sessions_count, DATE(created_at) AS date')->groupBy('date')->get()->flatten(1);

        Breadcrumbs::addBreadcrumb(__('Admin'), route('admin.index'));
        Breadcrumbs::addBreadcrumb(__('Statistieken'), route('admin.statistics'));

        return view('backend.admin.statistics.index')->with(
            compact(
                'users',
                'sites',
                'surveys',
                'feedbackButtons',
                'responses',
                'heatmaps',
                'heatmapSessions',
                'visits',
                'views',
                'totalRevenue',
                'visitsData',
                'responseData',
                'sessionData'
            )
        );
    }
}
