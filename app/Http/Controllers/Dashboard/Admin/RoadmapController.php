<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Http\Requests\Admin\StoreMilestoneRequest;
use App\Milestone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class RoadmapController extends Controller
{
    /**
     * Show milestones
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $milestones = Milestone::orderBy('date_expected', 'ASC')->paginate(50);

        Breadcrumbs::addBreadcrumb(__('Admin'), route('admin.index'));
        Breadcrumbs::addBreadcrumb(__('Roadmap'), route('admin.roadmap.index'));

        return view('backend.admin.roadmap.index')->with(compact('milestones'));
    }

    /**
     * Create new milestone
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        Breadcrumbs::addBreadcrumb(__('Admin'), route('admin.index'));
        Breadcrumbs::addBreadcrumb(__('Roadmap'), route('admin.roadmap.index'));
        Breadcrumbs::addBreadcrumb(__('Milestone toevoegen'), route('admin.roadmap.create'));

        return view('backend.admin.roadmap.create');
    }

    /**
     * Store a milestone
     *
     * @param  StoreMilestoneRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreMilestoneRequest $request)
    {
        (new Milestone())->fill($request->all())->save();

        return redirect()->route('admin.roadmap.index');
    }

    /**
     * Edit milestone
     *
     * @param  Milestone $milestone
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Milestone $milestone)
    {
        Breadcrumbs::addBreadcrumb(__('Admin'), route('admin.index'));
        Breadcrumbs::addBreadcrumb(__('Roadmap'), route('admin.roadmap.index'));
        Breadcrumbs::addBreadcrumb(__('Milestone wijzigen'), route('admin.roadmap.edit', $milestone->id));

        return view('backend.admin.roadmap.edit')->with(compact('milestone'));
    }

    /**
     * Update milestone
     *
     * @param  StoreMilestoneRequest $request
     * @param  Milestone             $milestone
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreMilestoneRequest $request, Milestone $milestone)
    {
        $milestone->fill($request->all())->save();

        return redirect()->route('admin.roadmap.index');
    }

    /**
     * Toggle milestone
     *
     * @param  Milestone $milestone
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggle(Milestone $milestone)
    {
        $milestone->status = !$milestone->status;
        $milestone->save();

        return redirect()->route('admin.roadmap.index');
    }

    public function delete(Milestone $milestone)
    {
        $milestone->delete();

        return redirect()->route('admin.roadmap.index');
    }
}
