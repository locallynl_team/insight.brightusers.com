<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Site;
use App\Http\Controllers\Controller;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class SiteController extends Controller
{
    /**
     * Show users
     *
     * @return $this
     */
    public function index()
    {
        //get sites with all counts
        $sites = Site::with(['user', 'activeSubscription', 'activeSubscription.package'])->orderBy('active', 'DESC')->get();

        Breadcrumbs::addBreadcrumb(__('Admin'), route('admin.index'));
        Breadcrumbs::addBreadcrumb(__('Websites'), route('admin.site.index'));

        return view('backend.admin.sites.index')->with(compact('sites'));
    }
}
