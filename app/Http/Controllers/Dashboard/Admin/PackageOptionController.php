<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\PackageOption;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StorePackageOptionRequest;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class PackageOptionController extends Controller
{
    /**
     * Show packageoptions
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $packageOptions = PackageOption::orderBy('sort_order')->get();

        Breadcrumbs::addBreadcrumb('Admin', route('admin.index'));
        Breadcrumbs::addBreadcrumb('Pakketten', route('admin.package.index'));
        Breadcrumbs::addBreadcrumb('Pakket opties', route('admin.packageoption.index'));

        return view('backend.admin.packageoptions.index')->with(compact('packageOptions'));
    }

    /**
     * Display profile page
     *
     * @return $this
     */
    public function create()
    {
        Breadcrumbs::addBreadcrumb('Admin', route('admin.index'));
        Breadcrumbs::addBreadcrumb('Pakketten', route('admin.package.index'));
        Breadcrumbs::addBreadcrumb('Pakket opties', route('admin.packageoption.index'));
        Breadcrumbs::addBreadcrumb('Pakket optie toevoegen', route('admin.packageoption.create'));

        return view('backend.admin.packageoptions.create');
    }

    /**
     * Save package option
     *
     * @param  StorePackageOptionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StorePackageOptionRequest $request)
    {
        /**
         * Create new package option
         */
        $packageOption = (new PackageOption)->fill($request->only(['title', 'identifier', 'type', 'sort_order']));
        $packageOption->visible = isset($request->visible);
        $packageOption->save();

        flashMessage(__('Pakket optie toegevoegd'), __('Pakket optie is succesvol toegevoegd.'));
        return redirect()->route('admin.packageoption.index');
    }

    /**
     * Edit package option
     *
     * @param  PackageOption $packageOption
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(PackageOption $packageOption)
    {
        Breadcrumbs::addBreadcrumb('Admin', route('admin.index'));
        Breadcrumbs::addBreadcrumb('Pakketten', route('admin.package.index'));
        Breadcrumbs::addBreadcrumb('Pakket opties', route('admin.packageoption.index'));
        Breadcrumbs::addBreadcrumb('Pakket optie wijzigen', route('admin.packageoption.edit', $packageOption->id));

        return view('backend.admin.packageoptions.edit')->with(compact('packageOption'));
    }

    /**
     * Update package option
     *
     * @param  StorePackageOptionRequest $request
     * @param  PackageOption             $packageOption
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StorePackageOptionRequest $request, PackageOption $packageOption)
    {
        $packageOption->fill($request->only(['title', 'identifier', 'type', 'sort_order']));
        $packageOption->visible = isset($request->visible);
        $packageOption->save();

        flashMessage(__('Pakket optie gewijzigd'), __('Pakket optie is succesvol gewijzigd.'));
        return redirect()->route('admin.packageoption.index');
    }

    /**
     * Delete a package option
     *
     * @param  PackageOption $packageOption
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(PackageOption $packageOption)
    {
        foreach ($packageOption->packageItems() as $packageItem) {
            $packageItem->subscriptionItems()->delete();
        }
        $packageOption->packageItems()->delete();

        $packageOption->delete();

        flashMessage(__('Pakket optie verwijderd'), __('Pakket optie is succesvol verwijderd.'));
        return redirect()->route('admin.packageoption.index');
    }
}
