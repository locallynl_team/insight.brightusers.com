<?php

namespace App\Http\Controllers\Dashboard;

use View;
use HTMLMin;
use App\Http\Controllers\Controller;
use App\Jobs\DeleteOldLogo;
use Illuminate\Http\Request;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use App\Http\Requests\SaveStylingRequest;

class StylingController extends Controller
{
    /**
     * Show styling module
     *
     * @param  Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        //Get current active website, with style (has default))
        $site = $request->user()->sites()->where('id', $request->user()->currentSite())->with('style')->first();

        //Request has errors -> refill from flash values?
        if ($request->session()->get('errors')) {
            foreach ($request->session()->get('_old_input') as $key => $value) {
                if ($site->style->{$key} && !in_array($key, ['site_id', 'header_logo_deleted', 'header_logo'])) {
                    $site->style->{$key} = $value;
                }
            }
        }

        //Generate CSS for preview
        $surveyCss = HTMLMin::css(
            View::make('javascript.css.survey')->with(['style' => $site->style])->render()
        );
        $surveyMobileCss = HTMLMin::css(
            View::make('javascript.css.mobile')->with(['style' => $site->style])->render()
        );

        Breadcrumbs::addBreadcrumb(__('Opmaak'), route('style.index'));

        return view('backend.style.index')->with(compact('site', 'surveyCss', 'surveyMobileCss'));
    }

    /**
     * Save styling
     *
     * @param  SaveStylingRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(SaveStylingRequest $request)
    {
        //Get current active website, with style (has default))
        $site = $request->user()->sites()->where('id', $request->user()->currentSite())->with('style')->first();

        //Set all fields
        $site->style->fill(
            $request->only(
                [
                'header_background_color',
                'header_text_color',
                'header_text',
                'header_type',
                'survey_background_color',
                'survey_border_color',
                'survey_text_color',
                'survey_text_align',
                'answer_background_color',
                'answer_text_color',
                'survey_position',
                'feedbackbutton_background_color',
                'feedbackbutton_text_color',
                'button_background_color',
                'button_text_color',
                'button_align',
                'advanced_styling'
                ]
            )
        );

        //Upload logo if set
        if ($request->hasFile('header_logo') && $request->file('header_logo')->isValid()) {
            $path = $request->file('header_logo')->store('public/logos');
            $site->style->header_logo = 'storage/'.str_replace('public/', '', $path);

            //delete image
        } elseif ($site->style->header_logo !== null && $request->header_logo_deleted == 'true') {
            DeleteOldLogo::dispatch(str_replace('storage/', 'public/', $site->style->header_logo))->delay(now()->addHour(1)); //delete after 1 hour

            $site->style->header_logo = null;
        }

        //Save
        $site->style->save();

        //reset the cache for this site
        $site->invalidateCache();

        //Success
        flashMessage(__('Opmaak gewijzigd'), __('De opmaak voor uw website is aangepast, het kan tot 5 minuten duren voordat dit zichtbaar is.'));
        return redirect()->route('style.index');
    }
}
