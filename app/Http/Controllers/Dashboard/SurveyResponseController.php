<?php

namespace App\Http\Controllers\Dashboard;

use DB;
use Auth;
use App\Survey;
use App\SurveyResponse;
use App\SurveyResponseExport;
use App\Jobs\GenerateResponseExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use App\Exports\SurveyResponsesExport;

class SurveyResponseController extends Controller
{
    public $type = 'survey';
    public $settings = [];

    /**
     * Set settings according to type
     *
     * SurveyController constructor.
     */
    public function __construct()
    {
        $this->settings = Survey::getSettingsByType($this->type);
    }

    /**
     * Show list of responses for a survey
     *
     * @param  Survey  $survey
     * @param  Request $request
     * @return $this
     */
    public function list(Survey $survey, Request $request)
    {
        if (!Auth::user()->can('view', $survey)) {
            return redirect()->route($this->settings['url_key'].'.index');
        }

        //css fix: fa-mobile-alt fa-tablet-alt

        $totalResponses = $survey->responses()->count();

        /**
         * Get responses + answers, sort by date
         */
        $export = $this->createExportFromRequest($survey, $request);

        /**
         * Filters
         */
        $periodStart = $request->period_start ?? null;
        $periodEnd = $request->period_end ?? null;
        $selectedUrl = $request->filter_url ?? null;

        /**
         * Get responses and paginate
         */
        $responses = $export->getResponsesQuery()->with(['items', 'items.answer'])->orderBy('created_at', 'DESC')->paginate(50);

        /**
         * Get questions
         */
        $questions = $survey->questions()->orderBy('position', 'ASC')->where('type', '!=', 'info')->with('answers')->get();

        /**
         * Get all unique urls from responses
         */
        $urls = DB::table('survey_responses')->select('url')->where('survey_id', $survey->id)->distinct()->get();

        /**
         * Export types
         */
        $exportTypes = SurveyResponseExport::$types;

        /**
         * Add Breadcrumbs
         */
        Breadcrumbs::addBreadcrumb($this->settings['plural'], route($this->settings['url_key'].'.index'));
        Breadcrumbs::addBreadcrumb(__(':single reacties', ['single' => strtolower($this->settings['name'])]), route($this->settings['url_key'].'.responses', $survey->id));

        $settings = $this->settings;
        return view('backend.survey-response.list')->with(compact('totalResponses', 'survey', 'questions', 'responses', 'settings', 'urls', 'selectedUrl', 'periodStart', 'periodEnd', 'exportTypes'));
    }

    /**
     * Delete a response to a survey
     *
     * @param  Survey         $survey
     * @param  SurveyResponse $response
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Survey $survey, SurveyResponse $response)
    {
        if (!Auth::user()->can('delete', $response) && $survey->id === $response->survey_id) {
            return redirect()->route($this->settings['url_key'].'.index');
        }

        $response->delete();

        flashMessage(__('Reactie verwijderd'), __('De reactie op deze :single is verwijderd.', ['single' => strtolower($this->settings['name'])]));

        return redirect()->route($this->settings['url_key'].'.responses', $survey->id);
    }

    /**
     * Export survey responses
     *
     * @param  Survey  $survey
     * @param  Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function export(Survey $survey, Request $request)
    {
        if (!Auth::user()->can('export', $survey)) {
            return redirect()->route($this->settings['url_key'].'.index');
        }

        /**
         * Check export type
         */
        if (!isset(SurveyResponseExport::$types[$request->type])) {
            return redirect()->route($this->settings['url_key'].'.responses', $survey->id);
        }

        /**
         * Create export
         */
        $export = $this->createExportFromRequest($survey, $request);
        $export->save();

        /**
         * Dispatch background job
         */
        GenerateResponseExport::dispatch($export);

        /**
         * Get response count & query
         */
        $responseCount = $export->getResponsesQuery()->count();

        /**
        * Add Breadcrumbs
        */
        Breadcrumbs::addBreadcrumb($this->settings['plural'], route($this->settings['url_key'].'.index'));
        Breadcrumbs::addBreadcrumb(__(':single reacties', ['single' => strtolower($this->settings['name'])]), route($this->settings['url_key'].'.responses', $survey->id));
        Breadcrumbs::addBreadcrumb(__('Exporteren'), route($this->settings['url_key'].'.responses.export', $survey->id));

        $settings = $this->settings;
        return view('backend.survey-response.export')->with(compact('settings', 'survey', 'responseCount', 'export'));
    }

    /**
     * Return status
     *
     * @param  SurveyResponseExport $export
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function exportStatus(Survey $survey, SurveyResponseExport $export)
    {
        return response($export->status, 200)->header('Content-Type', 'text/plain');
    }

    /**
     * Download survey export
     *
     * @param  Survey               $survey
     * @param  SurveyResponseExport $export
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download(Survey $survey, SurveyResponseExport $export)
    {
        if (!Auth::user()->can('download', $survey)) {
            return redirect()->route($this->settings['url_key'].'.index');
        }

        if ($export->status === false || $export->survey_id !== $survey->id) {
            return redirect()->route($this->settings['url_key'].'.responses');
        }

        return response()->download(storage_path('app/exports/' . $export->id . '.' . $export->type));
    }

    /**
     * Create export
     *
     * @param  Survey  $survey
     * @param  Request $request
     * @return SurveyResponseExport
     */
    private function createExportFromRequest(Survey $survey, Request $request)
    {
        /**
         * New export
         */
        $export = new SurveyResponseExport();
        $export->survey_id = $survey->id;
        $export->type = $request->type;

        /**
         * Filter on date
         */
        if ($request->period_start && $request->period_end) {
            $export->start_period = SurveyResponseExport::formatPeriod($request->period_start).' 00:00:00';
            $export->end_period = SurveyResponseExport::formatPeriod($request->period_end).' 23:59:59';
        }

        /**
         * Filter on URL
         */
        if ($request->filter_url) {
            $export->url = $request->filter_url;
        }

        return $export;
    }
}
