<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use View;
use HTMLMin;
use App\Survey;
use App\Question;
use App\QuestionAnswer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class SurveyQuestionController extends Controller
{
    public $type = 'survey';
    public $settings = [];

    /**
     * Set settings according to type
     *
     * SurveyController constructor.
     */
    public function __construct()
    {
        $this->settings = Survey::getSettingsByType($this->type);
    }

    /**
     * Manage surveys
     *
     * @param  Request $request
     * @param  Survey  $survey
     * @return $this
     */
    public function edit(Request $request, Survey $survey)
    {
        if (!Auth::user()->can('manage', $survey) || $survey->type <> $this->settings['type']) {
            return redirect()->route($this->settings['url_key'].'.index');
        }

        //get all preview data
        $survey->loadMissing(['site', 'questions', 'questions.answers']);
        $site = $survey->site;
        $style = $survey->site->style;
        unset($survey->site);

        //add breadcrumbs
        Breadcrumbs::addBreadcrumb($this->settings['plural'], route($this->settings['url_key'].'.index'));
        if ($this->settings['type'] === 'poll') {
            Breadcrumbs::addBreadcrumb(__(':single vraag beheren', ['single' => strtolower($this->settings['name'])]), route($this->settings['url_key'] . '.manage', $survey->id));
        } else {
            Breadcrumbs::addBreadcrumb(__(':single vragen beheren', ['single' => strtolower($this->settings['name'])]), route($this->settings['url_key'] . '.manage', $survey->id));
        }

        return view('backend.survey.manage')->with(
            [
            'survey' => $survey,
            'settings' => $this->settings,
            'questionTypes' => Question::getTypes(),
            'questions' => $survey->questions()->orderBy('position', 'ASC')->with(
                ['answers' => function ($query) {
                    $query->orderBy('position', 'ASC');
                }]
            )->get(),
            'site' => $site,
            'surveys' => collect([$survey]),
            'heatmaps' => collect([]),
            'forms' => collect([]),
            'surveyCss' => str_replace("\n", " ", HTMLMin::css(View::make('javascript.css.survey')->with(compact('style'))->render())),
            'surveyMobileCss' => str_replace("\n", " ", HTMLMin::css(View::make('javascript.css.mobile')->with(compact('style'))->render())),
            'feedbackHtml' => HTMLMin::blade(View::make('javascript.html.feedback')->with(compact('style'))->render()),
            'modalHtml' => HTMLMin::blade(View::make('javascript.html.modal')->with(compact('style'))->render()),
            'barHtml' => HTMLMin::blade(View::make('javascript.html.bar')->render())
            ]
        );
    }

    /**
     * Save questions
     *
     * @param  Request $request
     * @param  Survey  $survey
     * @return \Illuminate\Http\RedirectResponse
     * @throws
     */
    public function update(Request $request, Survey $survey)
    {
        if (!Auth::user()->can('manage', $survey)) {
            return redirect()->route($this->settings['url_key'].'.index');
        }

        /**
         * Has questions?
         */
        if ($request->question && count($request->question) > 0) {
            /**
             * Save index + id for adding answers & after_actions later
             */
            $questions = [];

            /**
             * Loop trough questions
             */
            for ($index = 0; $index < count($request->question); $index++) {
                /**
                 * Find existing question or create new
                 */
                if (!isset($request->id[$index]) || !$question = Question::where('survey_id', $survey->id)->where('id', $request->id[$index])->first()) {
                    $question = new Question();
                    $question->type = $request->type[$index] ?? 'text';
                    $question->survey_id = $survey->id;
                }

                /**
                 * Set values
                 */
                $question->fill([
                    'position' => $request->order[$index] ?? 0,
                    'question' => $request->question[$index] ?? '',
                    'intro' => $request->intro[$index] ?? null,
                    'rating_type' => (isset($request->rating_type[$index]) && in_array($request->rating_type[$index], ['image', 'text']) ? $request->rating_type[$index] : ($question->type === 'rating' ? 'text' : null)),
                    'multi_line' => isset($request->multi_line[$index]),
                    'button_next' => $request->button_next[$index] ?? __('Volgende'),
                    'multi_select' => isset($request->multi_select[$index]),
                    'random_order' => isset($request->random_order[$index]),
                    'rating_label_start' => $request->rating_label_start[$index] ?? __('Heel ontevreden'),
                    'rating_label_end' => $request->rating_label_end[$index] ?? __('Heel erg tevreden'),
                    'after_action' => $request->after_action_question[$index] ?? -1
                ]);
                $question->save();

                /**
                 * Save ID
                 */
                $questions[$index] = $question;
            }

            /**
             * Add answers
             */
            foreach ($questions as $index => $question) {
                if ($question->type === 'multi' && isset($request->answer[$index])) {
                    /**
                     * Save answers, missing answers can be deleted later
                     */
                    $answers = [];

                    /**
                     * Loop trough answers
                     */
                    for ($answerIndex = 0; $answerIndex < count($request->answer[$index]); $answerIndex++) {
                        /**
                         * Check if answer exists and belongs to this user
                         */
                        if (!isset($request->answer_id[$index][$answerIndex]) || !$answer = QuestionAnswer::where('question_id', $question->id)->where('id', $request->answer_id[$index][$answerIndex])->first()) {
                            $answer = new QuestionAnswer();
                            $answer->question_id = $question->id;
                        }

                        /**
                         * Set values
                         */
                        $answer->answer = $request->answer[$index][$answerIndex] ?? '';
                        $answer->position = $answerIndex;

                        /**
                         * Calculate after action, uses question index
                         */
                        $afterAction = -1;
                        if ($request->after_action[$index][$answerIndex] == '-1' || $request->after_action[$index][$answerIndex] == '-2') {
                            $afterAction = $request->after_action[$index][$answerIndex];
                        } elseif (isset($questions[($request->after_action[$index][$answerIndex])])) {
                            if (Question::where('id', $questions[$request->after_action[$index][$answerIndex]]->id)->where('survey_id', $survey->id)->count() > 0) {
                                $afterAction = $questions[$request->after_action[$index][$answerIndex]]->id;
                            }
                        }
                        $answer->after_action = $afterAction;
                        $answer->save();

                        /**
                         * Save ID's
                         */
                        $answers[] = $answer->id;
                    }

                    /**
                     * Loop trough answers -> delete if ID not found
                     */
                    foreach ($question->answers()->get() as $answer) {
                        if (!in_array($answer->id, $answers)) {
                            $answer->delete();
                        }
                    }
                }
            }

            /**
             * Find deleted questions
             */
            foreach ($survey->questions()->get() as $question) {
                foreach ($questions as $item) {
                    if ($item->id === $question->id) {
                        continue 2;
                    }
                }

                /**
                 * Not found
                 */
                $question->delete();
            }
        } else {
            /**
             * No questions
             */
            $survey->questions()->delete();

            /**
             * Return
             */
            return redirect()->route($this->settings['url_key'].'.manage', $survey->id);
        }

        //invalidate the cache for this site
        $survey->site()->first()->invalidateCache();

        /**
         * Success
         */
        flashMessage(__(':single gewijzigd', ['single' => $this->settings['name']]), __('Uw :single is gewijzigd, het kan tot 5 minuten duren voordat dit op uw website zichtbaar is.', ['single' => strtolower($this->settings['name'])]));
        return redirect()->route($this->settings['url_key'].'.manage', $survey->id);
    }
}
