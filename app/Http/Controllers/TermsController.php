<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TermsController extends Controller
{
    public function index()
    {
        $title = 'Surve - Algemene voorwaarden';
        $description = 'Algemene voorwaarden van Surve omschrijven de rechten en plichten van klanten en Surve.';

        return view('frontend.terms')->with(compact('title', 'description'));
    }
}
