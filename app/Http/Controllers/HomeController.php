<?php

namespace App\Http\Controllers;

use Debugbar;
use App\Package;
use Corcel\Model\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Surve - Feedback, Heatmaps en Surveys voor uw website';
        $description = 'Optimaliseer uw website, verhoog uw klanttevredenheid en boost uw conversie met de tools van Surve. Gratis proefaccount voor 7 dagen.';

        $companies = ['UMCG', 'ANWB', 'HifiCorner', 'Vogelbescherming', 'CoutureFashion'];

        $blogs = Post::type('post')->with('thumbnail')->published()->newest()->limit(4)->get();

        return view('frontend.home')->with(compact('packages', 'companies', 'title', 'description', 'blogs'));
    }
}
