<?php

namespace App\Http\Controllers;

use App\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function index()
    {

        $title = 'Surve - Pakketten voor iedere website';
        $description = 'Bekijk welk pakket voldoet aan uw wensen en start gelijk met het optimaliseren van uw website met Surve. Gratis proefaccount voor 7 dagen.';

        return view('frontend.packages')->with(compact('packages', 'title', 'description'));
    }
}
