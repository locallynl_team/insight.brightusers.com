<?php

namespace App\Http\Controllers;

use App\Admin;
use Mail;
use App\Http\Requests\SendContactRequest;
use App\Mail\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Show contact form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $title = 'Surve - Contact opnemen met Surve';
        $description = 'De helpdesk van Surve zit voor u klaar als u vragen of opmerkingen heeft over Surve.'.
            ' Stuur ons een bericht, dan reageren wij zo snel mogelijk.';

        return view('frontend.contact')->with(compact('title', 'description'));
    }

    /**
     * Send contact form
     *
     * @param  SendContactRequest $request
     * @param  Admin              $admin
     * @return \Illuminate\Http\RedirectResponse
     */
    public function send(SendContactRequest $request, Admin $admin)
    {
        Mail::to($admin)->send(new Contact($request));

        return redirect()->back()
            ->with('message', __('Uw bericht is verstuurd, wij nemen zo spoedig mogelijk contact met u op!'));
    }
}
