<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ToolController extends Controller
{
    public function index()
    {
        $title = 'Surve - Tools voor Conversie Optimalisatie en Klanttevredenheidsonderzoek';
        $description = 'Met de tools van Surve optimaliseert u uw website eenvoudig. Daarmee gaat uw klanttevredenheid en gebruiksgemak van uw website omhoog en zal uw conversie stijgen.';

        return view('frontend.tools')->with(compact('title', 'description'));
    }
}
