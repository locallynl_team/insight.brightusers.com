<?php

namespace App\Http\Controllers\Response;

use Log;
use App;
use App\Visitor;
use App\Survey;
use App\SurveyStatistic;
use App\Site;
use App\SiteStatistic;
use App\Question;
use App\SurveyResponse;
use App\SurveyResponseItem;
use App\Heatmap;
use App\HeatmapSession;
use App\HeatmapProfile;
use App\Form;
use App\FormSession;
use App\FormInteraction;
use App\Heatmap\HeatmapTree;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogController extends Controller
{
    protected $payload = null;
    protected $site = null;
    protected $visitor = null;
    protected $new = false;

    /**
     * Log data
     *
     * @param  $request Request
     * @return mixed|string
     */
    public function dispatchLog(Request $request)
    {
        $this->noDebugBar();

        //check if is screenshot-bot
        if ($this->isScreenshotBot()) {
            return 0;
        }

        //process incoming data (json, base64)
        $this->payload = $this->processPayLoad($request->data);

        //set site
        if (!$this->site = Site::where('id', $this->payload->siteId)->where('active', true)->first()) {
            return 0;
        }

        //find log type, unknown? no result.
        $response = 0;
        if (isset($this->payload) && isset($this->payload->fingerprint)) {
            //process data by type
            if (isset($this->payload->postData)) {
                foreach ($this->payload->postData as $action) {
                    $function = 'process' . ucfirst($action->log);
                    if (method_exists($this, $function)) {
                        $response += $this->$function($action);
                    }
                }
            }
        }

        return $response;
    }

    /**
     * Log a visit
     *
     * @param  $action
     * @return int
     */
    private function processVisit($action)
    {
        //find visitor by fingerprint
        $this->findVisitor();

        //add visit to website
        $log = SiteStatistic::firstOrCreate(
            [
            'site_id' => $this->site->id,
            'platform' => $this->getPlatform(),
            'date' => date('Y-m-d')
            ]
        );

        //check fingerprint in visitors table (refreshes every 30 minutes)
        if ($this->new === true) {
            $log->increment('visits');
        }
        $log->increment('views');

        return 1;
    }

    /**
     * Log open of survey
     *
     * @param  $action
     * @return int
     */
    private function processOpen($action)
    {
        return $this->addSurveyLog($action->surveyId, 'opens');
    }

    /**
     * Log view of survey
     *
     * @param  $action
     * @return int
     */
    private function processView($action)
    {
        return $this->addSurveyLog($action->surveyId, 'views');
    }

    /**
     * Add survey log
     *
     * @param  $surveyId
     * @param  $type
     * @return int
     */
    private function addSurveyLog($surveyId, $type)
    {
        $survey = $this->getSurvey($surveyId);
        if ($survey === false) {
            return 0;
        }

        $log = SurveyStatistic::firstOrCreate(
            [
            'survey_id' => $survey->id,
            'platform' => $this->getPlatform(),
            'date' => date('Y-m-d')
            ]
        );
        $log->increment($type);

        return 1;
    }

    /**
     * Process results
     *
     * @param  $action
     * @return int
     */
    private function processResults($action)
    {
        $survey = $this->getSurvey($action->surveyId);
        $question = $this->getQuestion($action->surveyId, $action->questionId);
        if ($survey === false || $question === false) {
            return 0;
        }

        //get current survey response
        $currentSurveyResponse = $this->getCurrentSurveyResponse($survey, $question);

        //answer data
        foreach ($action->results as $answer) {
            if (!empty($answer)) {
                $surveyResponseItem = SurveyResponseItem::firstOrNew(
                    [
                    'survey_response_id' => $currentSurveyResponse->id,
                    'question_id' => $question->id
                    ]
                );
                $surveyResponseItem->fill(
                    [
                    'answer_id' => ($question->type === 'multi' ? $answer : 0),
                    'answer_text' => ($question->type !== 'multi' ? $answer : '')
                    ]
                )->save();
            }
        }

        return 1;
    }

    /**
     * Process heatmap data
     *
     * @param  $action
     * @return int
     */
    private function processHeatmap($action)
    {
        $heatmap = $this->getHeatmap($action->heatmapId);
        if ($heatmap === null) {
            return 0;
        }

        //get current heatmap profile
        $heatmapProfile = $this->getHeatmapProfile($action->client);

        //check if we need a screenshot
        $heatmap->checkScreenshot($heatmapProfile);

        //get heatmap session
        $heatmapSession = $this->getCurrentHeatmapSession($heatmap, $heatmapProfile, $action);

        //first batch?
        if ($heatmapSession->data === null) {
            $heatmapSession->data = \GuzzleHttp\json_encode($action->data);
        } else {
            //add data to existing tree
            $heatmapSession->data = (new HeatmapTree($heatmapSession->data))->add(\GuzzleHttp\json_encode($action->data))->toJson();
        }

        //update time on page
        $heatmapSession->time_on_page = $action->time;

        //update scroll depth
        $heatmapSession->scroll_depth = $action->scrollDepth;

        //save
        $heatmapSession->save();

        return 1;
    }

    /**
     * Process form data
     *
     * @param  $action
     * @return int
     */
    private function processForm($action)
    {
        //loop trough all events & log data
        if (!isset($action->events)) {
            return 0;
        }

        //has form id?
        if ($action->formId > 0) {
            $form = $this->getForm($action->formId);
            if ($form === null) {
                return 0;
            }

            //get current form session
            $formSession = $this->getCurrentFormSession($form);
        }

        //log form events
        foreach ($action->events as $event) {
            //we don't need to log 'view', it'l just trigger the function above
            if (in_array($event->type, ['field_leave', 'successful_submit', 'unsuccessful_submit'])) {
                //successful submit, we update the form session that belongs to this interaction, update drop-off to false on last field
                if ($event->type === 'successful_submit' || $event->type === 'unsuccessful_submit') {
                    //get previous session
                    $previousFormSession = FormSession::where('fingerprint', $this->payload->fingerprint)
                        ->where('page_session_id', '!=', $this->payload->pageSessionId)
                        ->where('send', false)
                        ->latest()
                        ->first();

                    //no previous session found, probably manual submit submission
                    if (!$previousFormSession) {
                        $previousFormSession = FormSession::where('fingerprint', $this->payload->fingerprint)
                            ->where('send', false)
                            ->latest()
                            ->first();
                    }

                    //found a session?
                    if ($previousFormSession) {
                        $previousFormSession->send = true;
                        $previousFormSession->successful = ($event->type === 'successful_submit' ? true : false);
                        $previousFormSession->save();

                        //drop off -> previous field to false
                        $latestFormInteraction = FormInteraction::where('form_session_id', $previousFormSession->id)->orderBy('last_update_timestamp', 'DESC')->limit(1)->first();
                        if ($latestFormInteraction) {
                            $latestFormInteraction->update(['drop_off' => false]);
                        }
                    }

                    //field leave, add or find form interaction & update accordingly with things as redo & empty
                } elseif ($event->type === 'field_leave') {
                    //find form interaction for this field
                    $formInteraction = FormInteraction::firstOrNew(['form_session_id' => $formSession->id, 'form_field_id' => $event->field_id]);

                    //get current values
                    $currentValues = explode(",", $event->hash);

                    //checks redo
                    if (!empty($formInteraction->id) && $formInteraction->redo === false) {
                        $redo = false;

                        //check redo values
                        $values = explode(",", $formInteraction->value_hash);
                        for ($i = 0; $i < count($values); $i++) {
                            if ($values[$i] <> $currentValues[$i]) {
                                $redo = true;
                            }
                        }

                        $formInteraction->redo = $redo;
                    }

                    //check empty
                    $empty = true;
                    foreach ($currentValues as $value) {
                        if ($value <> md5($this->payload->fingerprint)) {
                            $empty = false;
                            break;
                        }
                    }
                    $formInteraction->empty = $empty;

                    //add timing to interaction
                    if ($event->timing <> null) {
                        $formInteraction->timing += $event->timing;
                    }

                    //save
                    $formInteraction->drop_off = true;
                    $formInteraction->value_hash = $event->hash;
                    $formInteraction->last_update_timestamp = (microtime(true) * 1000);
                    $formInteraction->save();

                    //drop off -> previous field to false
                    $latestFormInteraction = FormInteraction::where('form_session_id', $formSession->id)->where('id', '!=', $formInteraction->id)->orderBy('last_update_timestamp', 'DESC')->limit(1)->first();
                    if ($latestFormInteraction) {
                        $latestFormInteraction->update(['drop_off' => false]);
                    }
                }
            }
        }

        return 1;
    }

    /**
     * Get current form session
     *
     * @param  Form $form
     * @return mixed
     */
    private function getCurrentFormSession(Form $form)
    {
        $formSession = FormSession::firstOrCreate(
            [
            'form_id' => $form->id,
            'fingerprint' => $this->payload->fingerprint,
            'page_session_id' => $this->payload->pageSessionId,
            'send' => false
            ],
            [
            'platform' => $this->getPlatform(),
            'url' => $this->payload->url
            ]
        );

        return $formSession;
    }

    /**
     * Get current heatmap session
     *
     * @param  Heatmap        $heatmap
     * @param  HeatmapProfile $heatmapProfile
     * @param  $action
     * @return HeatmapSession
     */
    private function getCurrentHeatmapSession(Heatmap $heatmap, HeatmapProfile $heatmapProfile, $action)
    {
        //find current session
        $currentHeatmapSession = HeatmapSession::where('heatmap_id', $heatmap->id)
            ->where('fingerprint', $this->payload->pageSessionId)
            ->where('orientation', $this->getOrientation())
            ->where('created_at', '>=', \Carbon\Carbon::now()->subMinutes(10)->toDateTimeString())
            ->latest()
            ->first();

        //not found, or session is too old
        if ($currentHeatmapSession === null || $currentHeatmapSession->time_on_page > $action->time || $currentHeatmapSession->sroll_depth > $action->scrollDepth) {
            //create new session
            $currentHeatmapSession = (new HeatmapSession)->fill(
                [
                'heatmap_id' => $heatmap->id,
                'heatmap_profile_id' => $heatmapProfile->id,
                'fingerprint' => $this->payload->pageSessionId,
                'platform' => $this->getPlatform(),
                'browser' => $this->payload->browser,
                'os' => $this->payload->os,
                'orientation' => $this->getOrientation(),
                'screen_width' => $action->client->sw,
                'screen_height' => $action->client->sh,
                'client_width' => $action->client->cw,
                'client_height' => $action->client->ch,
                'time_on_page' => $action->time,
                'scroll_depth' => $action->scrollDepth,
                'data' => null,
                'url' => $this->payload->url,
                ]
            );
            $currentHeatmapSession->save();
        }

        //check heatmap limits
        $heatmap->checkLimit();

        return $currentHeatmapSession;
    }

    /**
     * Get heatmap profile ID based on $this->>data->client values
     *
     * @param  $client
     * @return HeatmapProfile
     */
    private function getHeatmapProfile($client)
    {
        //get type of device & orientation
        if ($this->getPlatform() === "desktop") {
            $slug = ($client->sw < 1400 ? "desktop-normal" : "desktop-large");
        } else {
            $slug = $this->getPlatform()."-".$this->getOrientation();
        }

        return HeatmapProfile::where('slug', $slug)->firstOrFail();
    }

    /**
     * Return orientation of visitor based on client sizes
     *
     * @return string
     */
    private function getOrientation()
    {
        return ((!isset($this->payload->client->sw) || ($this->payload->client->sw > $this->payload->client->sh)) ? "landscape" : "portrait");
    }

    /**
     * Get current response
     *
     * @param  $survey Survey
     * @param  $question Question
     * @return SurveyResponse object
     */
    private function getCurrentSurveyResponse(Survey $survey, Question $question)
    {
        //get current response
        $currentResponse = SurveyResponse::where('survey_id', $survey->id)
            ->where('url', $this->payload->url)
            ->where('finished', false)
            ->where('identifier', $this->payload->pageSessionId)
            ->where('created_at', '>=', \Carbon\Carbon::now()->subMinutes(30)->toDateTimeString())
            ->latest()
            ->first();

        //not found -> create a new one.
        if ($currentResponse === null) {
            $currentResponse = (new SurveyResponse)->fill(
                [
                'survey_id' => $survey->id,
                'identifier' => $this->payload->pageSessionId,
                'url' => $this->payload->url,
                'platform' => $this->getPlatform(),
                'resolution' => $this->payload->resolution,
                'browser' => $this->payload->browser,
                'os' => $this->payload->os
                ]
            );
            $currentResponse->save();
        }

        //check if the current question already has an answer -> create new response
        if ($currentResponse->items()->where('question_id', $question->id)->where(
            function ($query) {
                        $query->where('answer_id', '>', 0)
                            ->orWhere('answer_text', '!=', '');
            }
        )->count() > 0
        ) {
            //we already have a filled answer for this question -> start new survey response
            $currentResponse->update(['finished' => true]);

            //create a new session
            return self::getCurrentSurveyResponse($survey, $question);
        }

        return $currentResponse;
    }

    /**
     * Log site visit & site view
     */
    private function findVisitor()
    {
        //delete old visitors
        Visitor::where('created_at', '<=', \Carbon\Carbon::now()->subMinutes(30)->toDateTimeString())->delete();

        //find current visitor or create new
        $visitor = Visitor::firstOrNew(
            [
            'site_id' => $this->site->id,
            'fingerprint' => $this->payload->fingerprint
            ]
        );

        //visitor is a new visitor
        if (empty($visitor->id)) {
            $this->new = true;
        }

        $visitor->save();
    }

    /**
     * Get platform
     *
     * @return string
     */
    private function getPlatform()
    {
        return ($this->payload->platform && in_array($this->payload->platform, ['mobile', 'desktop', 'tablet']) ? $this->payload->platform : "unknown");
    }

    /**
     * Return array of post data
     *
     * @param  $data
     * @return bool|string|object
     */
    private function processPayLoad($data)
    {
        try {
            $response = \GuzzleHttp\json_decode(base64_decode($data));
        } catch (\Exception $e) {
            return false;
        }

        //fix decoding
        $urldecodeFields = ['platform', 'url', 'title', 'browser', 'os', 'resolution'];
        foreach ($urldecodeFields as $field) {
            $response->$field = urldecode($response->$field);
        }

        return $response ?? false;
    }

    /**
     * Check if question exists
     *
     * @param  $surveyId
     * @param  $questionId
     * @return bool|Question
     */
    private function getQuestion($surveyId, $questionId)
    {
        return (Question::where('survey_id', $surveyId)->where('id', $questionId)->first() ?? false);
    }

    /**
     * Check if survey exists & active
     *
     * @param  $surveyId
     * @return bool|Survey
     */
    private function getSurvey($surveyId)
    {
        return (Survey::select('id')->where('site_id', $this->site->id)->where('id', $surveyId)->where('status', true)->first() ?? false);
    }

    /**
     * Check if heatmap exists & active
     *
     * @param  $heatmapId
     * @return bool|Heatmap
     */
    private function getHeatmap($heatmapId)
    {
        //get heatmap
        $heatmap = Heatmap::where('site_id', $this->site->id)->where('id', $heatmapId)->where('status', true)->first();
        if ($heatmap === null) {
            return false;
        }

        //we need to save the first URL for a screenshot
        if ($heatmap->screenshot_url === null) {
            $heatmap->screenshot_url = $this->payload->url;
            $heatmap->save();
        }

        //check IP-addresses
        if ($this->isExcludedIPAddress() === true) {
            return false;
        }

        return $heatmap;
    }

    /**
     * Getform
     *
     * @param  $formId
     * @return bool|Form
     */
    private function getForm($formId)
    {
        return (Form::where('site_id', $this->site->id)->where('id', $formId)->where('status', true)->first() ?? false);
    }

    /**
     * Check if is screenshot bot -> don't process data
     *
     * @return bool
     */
    private function isScreenshotBot()
    {
        //local is always 127.0.0.1 / 127.0.0.1 -> log
        if (App::environment('local')) {
            return false;
        }

        //IP from visitor is server-ip? we are a bot.
        if (request()->ip() === $_SERVER['SERVER_ADDR']) {
            return true;
        }

        return false;
    }

    /**
     * Check if this IP-address has to be excluded from reports
     *
     * @return bool
     */
    private function isExcludedIPAddress()
    {
        //gather IP-addresses to skip & split on new line & merge both arrays
        $excludedIPAddresses = array_unique(array_merge(preg_split("/\r\n|\r|\n/", $this->site->excluded_ip_addresses), preg_split("/\r\n|\r|\n/", $this->site->user()->first()->exclude_ip_addresses)));

        //check IP-addresses versus current IP-address
        foreach ($excludedIPAddresses as $ip) {
            if ((filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) || filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) && inet_pton(request()->ip()) == $ip) {
                return true;
            }
        }

        return false;
    }

    /**
     * Don't output debugbar -> fucks up ajax requests
     */
    public function noDebugBar()
    {
        //no debugbar
        if (App::environment('local')) {
            \Debugbar::disable();
        }
    }
}
