<?php

namespace App\Http\Controllers\Request;

use App;
use View;
use HTMLMin;
use App\Site;
use App\Http\Controllers\Controller;

class JavascriptController extends Controller
{
    /**
     * hows javascript compiled with all site/survey/css data
     *
     * @param  Site $site
     * @return \Illuminate\Contracts\Cache\Repository|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \Exception
     */
    public function show(Site $site)
    {
        //check if site is active
        if ($site->active === false) {
            return view('javascript.inactive-website');
        }

        //get page from cache
        $response = cache()->get($site->getCacheKey());
        if ($response <> null && !app()->environment('local')) {
            return response($response, 200)->header('Content-Type', 'application/x-javascript');
        }

        //generate & minify css
        $style = $site->style;

        $surveyCss = str_replace(
            "\n",
            " ",
            HTMLMin::css(
                View::make('javascript.css.survey')->with(compact('style'))->render()
            )
        );
        $surveyMobileCss = str_replace(
            "\n",
            " ",
            HTMLMin::css(
                View::make('javascript.css.mobile')->with(compact('style'))->render()
            )
        );

        //generate HTML pieces
        $barHtml = HTMLMin::blade(
            View::make('javascript.html.bar')->render()
        );
        $feedbackHtml = HTMLMin::blade(
            View::make('javascript.html.feedback')->with(compact('style'))->render()
        );
        $modalHtml = HTMLMin::blade(
            View::make('javascript.html.modal')->with(compact('style'))->render()
        );

        //get all surveys & heatmaps
        $surveys = $site->allSurveys()->where('status', true)->with('questions', 'questions.answers')->get();
        $heatmaps = $site->heatmaps()->where('status', true)->get();
        $forms = $site->forms()->where('status', true)->with('fields')->get();

        //no debugbar
        if (App::environment('local')) {
            \Debugbar::disable();

            return response(
                view('javascript.v1')->with(
                    compact(
                        'site',
                        'surveyCss',
                        'surveyMobileCss',
                        'surveys',
                        'heatmaps',
                        'barHtml',
                        'feedbackHtml',
                        'modalHtml',
                        //'urlTargets',
                        'forms'
                    )
                ),
                200
            )->header('Content-Type', 'application/x-javascript');
        }

        //cache & return view
        $response = HTMLMin::js(
            view('javascript.v1')->with(
                compact(
                    'site',
                    'surveyCss',
                    'surveyMobileCss',
                    'surveys',
                    'heatmaps',
                    'barHtml',
                    'feedbackHtml',
                    'modalHtml',
                    //'urlTargets',
                    'forms'
                )
            )
        );

        //put response in cache
        cache()->put($site->getCacheKey(), $response, now()->addMinutes(5));

        return response($response, 200)->header('Content-Type', 'application/x-javascript');
    }
}
