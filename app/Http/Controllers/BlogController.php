<?php

namespace App\Http\Controllers;

use Corcel\Model\Post;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Show blog overview
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = Post::type('post')->with('thumbnail')->published()->newest()->get();

        $title = 'Usability, Gebruikersonderzoek en Conversieoptimalisatie - Blog - Surve';
        $description = 'Lees de blog van Surve over usability, gebruikersonderzoek en conversieoptimalisatie,'.
            ' optimaliseer uw website met onze handige tips en profiteer gelijk!';

        return view('frontend.blog.index')->with(compact('title', 'posts', 'description'));
    }

    /**
     * Show single post
     *
     * @param  $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function view($slug)
    {
        //find post by slug
        $post = Post::slug($slug)->with('thumbnail')->published()->first();

        //post not found, redirect to blog
        if ($post === null) {
            return redirect()->route('blog');
        }

        //get related posts
        $posts = Post::type('post')
            ->with('thumbnail')
            ->where('ID', '!=', $post->ID)
            ->published()
            ->orderBy('ID', 'DESC')
            ->limit(6)
            ->get();

        //set title
        $title = $post->title.' - Surve';
        $description = 'Lees nu de blog \''.$post->title.'\' op de blog van Surve.';

        //set og-settings
        $og_settings = [
            'title' => $post->title.' - Surve',
            'description' => 'Lees nu de blog \''.$post->title.'\' op de blog van Surve.',
            'image' => $post->thumbnail->size('featured-image-blog')['url'] ?? $post->thumbnail,
            'url' => route('blog.post', $post->post_name)
        ];
        return view('frontend.blog.view')->with(compact('title', 'description', 'post', 'posts', 'og_settings'));
    }
}
