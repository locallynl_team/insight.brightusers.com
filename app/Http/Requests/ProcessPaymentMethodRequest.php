<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProcessPaymentMethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'package' => 'required|exists:packages,id',
            'period' => 'required|in:monthly,yearly',
            'payment_method' => 'required|in:creditcard,directdebit',
            'company_name' => 'required|string|max:100',
            'address' => 'required|string|max:150',
            'postal_code' => 'required|string|max:25',
            'city' => 'required|string|max:100'
        ];
    }
}
