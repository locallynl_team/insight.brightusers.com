<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use App\UrlTarget\UrlTarget;
use Illuminate\Foundation\Http\FormRequest;

class StoreFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:150',
            'url_target' => 'required|string|min:2',
            'url_target_type' => ['required', Rule::in(UrlTarget::getTypes())],
            'platforms' => 'required|array|min:1|in:mobile,tablet,desktop',
            'selected_form' => 'required|exists:html_forms,id',
        ];
    }
}
