<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'package' => 'required|exists:packages,id'
        ];
    }

    /**
     * Process extra rule
     *
     * @param $validator
     */
    public function withValidator($validator)
    {

        //user has to confirm his auto payment
        $validator->after(
            function ($validator) {
                if (Auth::user()->hasActivePaymentMethod() === true && Auth::user()->pay_on_invoice === false) {
                    if (!isset($this->payment_confirmation)) {
                        $validator->errors()->add(
                            'payment_confirmation',
                            __('U heeft geen toestemming gegeven voor een automatisch betaling.')
                        );
                    }
                }
            }
        );
    }
}
