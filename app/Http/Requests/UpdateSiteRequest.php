<?php

namespace App\Http\Requests;

use App\Site;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSiteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'domain' => [
                'required',
                function ($attribute, $value, $fail) {
                    if (!preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $value) ||
                        !preg_match("/^.{1,253}$/", $value) ||
                        !preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $value)) {
                        return $fail(__('De domeinnaam is niet geldig.'));
                    }
                }
            ]
        ];
    }
}
