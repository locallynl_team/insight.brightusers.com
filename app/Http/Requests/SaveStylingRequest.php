<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\Color;

class SaveStylingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'header_background_color' => ['required', new Color],
            'header_text_color' => ['required', new Color],
            'header_text' => 'required_if:header_type,text',
            'header_type' => 'required|in:text,logo',
            'header_logo' => 'image|file|max:2048:mimes:jpeg,png',
            'survey_background_color' => ['required', new Color],
            'survey_border_color' => ['required', new Color],
            'survey_text_color' => ['required', new Color],
            'survey_text_align' => 'required|in:default,left,right',
            'answer_background_color' => ['required', new Color],
            'answer_text_color' => ['required', new Color],
            'survey_position' => 'required|in:default,left,right',
            'feedbackbutton_background_color' => ['required', new Color],
            'feedbackbutton_text_color' => ['required', new Color],
            'button_background_color' => ['required', new Color],
            'button_text_color' => ['required', new Color],
            'button_align' => 'required|in:default,wide,left,right'
        ];
    }
}
