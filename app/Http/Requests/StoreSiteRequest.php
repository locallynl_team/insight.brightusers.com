<?php

namespace App\Http\Requests;

use Auth;
use App\Site;
use Illuminate\Foundation\Http\FormRequest;

class StoreSiteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('create', Site::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'domain' => [
                'required',
                function ($attribute, $value, $fail) {
                    if (!preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $value) || !preg_match("/^.{1,253}$/", $value) || !preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $value)) {
                        return $fail(__('De domeinnaam is niet geldig.'));
                    }
                }
            ],
            'package' => 'required|exists:packages,id',
            'process_confirmation' => 'accepted',
        ];
    }

    /**
     * Process extra rule
     *
     * @param $validator
     */
    public function withValidator($validator)
    {

        //user has to confirm his auto payment
        $validator->after(
            function ($validator) {
                if (Auth::user()->had_trial === true && Auth::user()->hasActivePaymentMethod() === true && Auth::user()->pay_on_invoice === false) {
                    if (!isset($this->payment_confirmation)) {
                        $validator->errors()->add('payment_confirmation', __('U heeft geen toestemming gegeven voor een automatisch betaling.'));
                    }
                }
            }
        );
    }
}
