<?php

namespace App\Http\Requests;

use Auth;
use Hash;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.Auth::user()->id,
            'password' => 'nullable|string|min:6|confirmed'
        ];
    }

    /**
     * Check if current password matches
     *
     * @param $validator
     */
    public function withValidator($validator)
    {
        if ($this->password !== null) {
            $validator->after(
                function ($validator) {
                    if (!Hash::check($this->password_old, Auth::user()->password)) {
                        $validator->errors()->add('password', __('Het huidige wachtwoord is onjuist.'));
                    }
                }
            );
        }
    }
}
