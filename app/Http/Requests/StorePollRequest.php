<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use App\UrlTarget\UrlTarget;
use Illuminate\Foundation\Http\FormRequest;

class StorePollRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:150',
            'url_target' => 'required|string|min:2',
            'url_target_type' => ['required', Rule::in(UrlTarget::getTypes())],
            'platforms' => 'required|array|min:1|in:mobile,tablet,desktop',
            'time_on_page' => 'required_without_all:leave_intent,external_activation',
            'time_on_page_miliseconds' => 'required_with:time_on_page|numeric',
            'external_activation' => 'required_without_all:leave_intent,time_on_page',
            'leave_intent' => 'required_without_all:time_on_page,external_activation'
        ];
    }
}
