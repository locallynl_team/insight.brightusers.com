<?php

namespace App\Http\Requests\Admin;

use Auth;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StorePackageOptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * User is admin?
         */
        if (!Auth::user()->isAdmin()) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:150',
            'identifier' => 'required|string|max:50',
            'type' => 'required|in:integer,boolean,time_period',
            'sort_order' => 'required|integer'
        ];
    }
}
