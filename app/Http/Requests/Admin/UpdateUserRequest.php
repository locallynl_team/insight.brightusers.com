<?php

namespace App\Http\Requests\Admin;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * User is admin?
         */
        if (!Auth::user()->isAdmin()) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$this->route()->parameters()['user']->id,
            'company_name' => 'required|string|max:100',
            'address' => 'required|string|max:150',
            'postal_code' => 'required|string|max:25',
            'city' => 'required|string|max:100'
        ];
    }
}
