<?php

namespace App\Http\Requests\Admin;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSubscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * User is admin?
         */
        if (!Auth::user()->isAdmin()) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'package' => 'required|exists:packages,id',
            'period' => 'required|in:trial,monthly,quarterly,yearly',
            'cancelled_at' => 'nullable|date_format:"Y-m-d H:i:s"',
            'ends_at' => 'required|date_format:"Y-m-d H:i:s"',
        ];
    }
}
