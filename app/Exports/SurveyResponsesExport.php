<?php
namespace App\Exports;

use App\SurveyResponseExport;
use Maatwebsite\Excel\Concerns\FromQuery;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Log;
use Carbon\Carbon;

class SurveyResponsesExport implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize
{
    use Exportable;

    protected $export;
    protected $questions;

    /**
     * Set export
     *
     * InvoicesExport constructor.
     *
     * @param SurveyResponseExport $export
     */
    public function __construct(SurveyResponseExport $export)
    {
        $this->export = $export;
        $this->questions = $this->export->survey()->first()->questions()->get();
    }

    /**
     * Set query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function query()
    {
        return $this->export->getResponsesQuery()->with(['items', 'items.answer'])->orderBy('created_at', 'DESC');
    }

    /**
     * Set fields
     *
     * @param  $response
     * @return array
     */
    public function map($response): array
    {
        /**
         * Basic fields
         */
        $fields = [
            $response->created_at->toDateTimeString(),
            $response->url
        ];

        /**
         * Add questions
         */
        foreach ($this->questions as $question) {
            $answers = [];
            foreach ($response->items->where('question_id', $question->id) as $answer) {
                if ($answer->answer === null) {
                    $answers[] = ($answer->answer_text ?? '-');
                } else {
                    $answers[] = $answer->answer->answer;
                }
            }
            $fields[] = (count($answers) === 0 ? "-" : implode("\n", $answers));
        }

        return $fields;
    }

    /**
     * Set headings
     *
     * @return array
     */
    public function headings(): array
    {
        $fields = [
            __('Datum'),
            __('Pagina')
        ];

        foreach ($this->questions as $question) {
            $fields[] = $question->question;
        }

        return $fields;
    }
}
