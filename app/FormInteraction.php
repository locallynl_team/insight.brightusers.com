<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormInteraction extends Model
{
    /**
     * Mass fillable
     *
     * @var array
     */
    protected $fillable = [
        'form_session_id',
        'form_field_id',
        'redo',
        'empty',
        'drop_off',
        'value_hash',
        'timing',
        'last_update_timestamp'
    ];

    /**
     * Cast values to
     *
     * @var array
     */
    protected $casts = [
        'redo' => 'boolean',
        'empty' => 'boolean',
        'drop_off' => 'boolean'
    ];
}
