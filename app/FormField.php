<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class FormField extends Model
{
    /**
     * Mass assignment
     *
     * @var array
     */
    public $fillable = [
        'form_id',
        'selector',
        'selector_value',
        'type',
        'title',
        'sort_order'
    ];

    /**
     * Fields to hide in JS code
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'sort_order',
        'title',
        'form_id'
    ];

    /**
     * Belongs to Form
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    /**
     * FormField has many interactions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function interactions()
    {
        return $this->hasMany(FormInteraction::class);
    }

    /**
     * Collect all interaction data
     *
     * @return Model|\Illuminate\Database\Query\Builder|null|object
     */
    public function getResults()
    {
        return DB::table('form_interactions')
            ->where('form_field_id', $this->id)
            ->selectRaw('COUNT(*) AS interactions,
            ((100 / COUNT(*)) * SUM(redo)) AS redo,
            ((100 / COUNT(*)) * SUM(empty)) AS empty,
            ((100 / COUNT(*)) * SUM(drop_off)) AS drop_off,
            SUM(drop_off) AS drop_off_count,
            (AVG(timing) / 1000) AS timing')
            ->groupBy('form_field_id')
            ->first();
    }
}
