<?php

namespace App;

use App\Mail\NewInvoice;
use Mail;
use App\Jobs\GenerateInvoice;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'payment_id',
        'first_name',
        'last_name',
        'company_name',
        'address',
        'postal_code',
        'city',
        'amount',
        'tax',
        'created'
    ];

    public $timestamps = false;

    protected $casts = ['generated' => 'boolean'];

    /**
     * Invoice belongs to a payment
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    /**
     * Invoice has many rows
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rows()
    {
        return $this->hasMany(InvoiceRow::class);
    }

    /**
     * Generate a new invoice based on a payment
     *
     * @param  Payment $payment
     * @return bool|null
     */
    public static function generate(Payment $payment)
    {
        /**
         * Empty invoices -> nope
         */
        if ($payment->amount === 0) {
            return false;
        }

        /**
         * Get user
         */
        $user = $payment->user()->first();

        /**
         * Create invoice
         */
        $invoice = self::create(
            [
            'payment_id' => $payment->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'company_name' => $user->company_name,
            'address' => $user->address,
            'postal_code' => $user->postal_code,
            'city' => $user->city
            ]
        );

        /**
         * Add row to invoice with description
         */
        $package = $payment->package()->first();
        $period = $package->period($payment->period);
        $site = $payment->site()->first();

        /**
         * Calculate discount
         */
        $description = __('Betaling :period', ['period' => $period['text']]);
        if ($user->discount > 0) {
            $description .= "<br />" . __('Inclusief accountkorting van :discount%', ['discount' => $user->discount]);
        }

        (new InvoiceRow())->fill(
            [
            'invoice_id' => $invoice->id,
            'title' => __('Pakket :package voor :site', ['package' => $package->title, 'site' => $site->domain]),
            'description' => $description,
            'amount' => $payment->total_amount,
            'tax' => $payment->tax,
            ]
        )->save();

        /**
         * Dispatch job to queue to create PDF
         */
        GenerateInvoice::dispatch($invoice);

        return true;
    }

    /**
     * Get full name
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * Get invoice location
     *
     * @return string
     */
    public function getUrl()
    {
        return storage_path('app').('/invoices/'.$this->id.'.pdf');
    }

    /**
     * Send this email to the user
     */
    public function send()
    {
        Mail::to($this->payment()->first()->user()->first())
            ->bcc(User::where('is_admin', true)->first())
            ->send(new NewInvoice($this));
    }
}
