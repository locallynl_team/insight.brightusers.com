<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SurveyStatistic
 *
 * @package App
 */
class SurveyStatistic extends Model
{
    public $timestamps = false;
    public $fillable = [
        'survey_id',
        'platform',
        'date',
        'views',
        'opens'
    ];

    /**
     * Belongs to a Survey
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }
}
