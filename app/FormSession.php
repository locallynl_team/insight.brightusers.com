<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormSession extends Model
{
    protected $fillable = [
        'form_id',
        'page_session_id',
        'fingerprint',
        'send',
        'platform',
        'url',
        'successful'
    ];

    protected $casts = [
        'successful' => 'boolean',
        'send' => 'boolean'
    ];

    /**
     * Belongs to a form
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    /**
     * Form has many interactions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function interactions()
    {
        return $this->hasMany(FormInteraction::class);
    }
}
