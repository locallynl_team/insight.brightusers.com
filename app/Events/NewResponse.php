<?php

namespace App\Events;

use App\SurveyResponse;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class NewResponse
{
    use Dispatchable, SerializesModels;

    public $response;
    public $user;
    public $survey;

    /**
     * Create a new event instance.
     *
     * NewResponse constructor.
     *
     * @param SurveyResponse $response
     */
    public function __construct(SurveyResponse $response)
    {
        $this->response = $response;
        $this->user = $response->survey->site->user;
        $this->survey = $response->survey;
    }
}
