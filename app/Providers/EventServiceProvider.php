<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\NewResponse' => [
            'App\Listeners\SendNewResponseMail'
        ],
        'Illuminate\Auth\Events\Registered' => [
            'App\Listeners\SendWelcomeEmail',
            'App\Listeners\SendUserRegisteredMail'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
