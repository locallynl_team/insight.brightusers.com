<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\Site::class => \App\Policies\SitePolicy::class,
        \App\Survey::class => \App\Policies\SurveyPolicy::class,
        \App\SurveyResponse::class => \App\Policies\SurveyResponsePolicy::class,
        \App\Heatmap::class => \App\Policies\HeatmapPolicy::class,
        \App\Form::class => \App\Policies\FormPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
