<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        include_once app_path('Helpers/FlashMessageHelper.php');
        include_once app_path('Helpers/PriceHelper.php');
        include_once app_path('Helpers/PermissionHelper.php');
        include_once app_path('Helpers/FormatBlogHelper.php');
        include_once app_path('Helpers/StaticUrlHelper.php');
    }
}
