<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapRequestRoutes();
        $this->mapResponseRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group(
            [
            'middleware' => 'web',
            'namespace' => $this->namespace
            ],
            function ($router) {
                include base_path('routes/web/auth.php');
                include base_path('routes/web/dashboard.php');
                include base_path('routes/web/admin.php');
                include base_path('routes/web/front.php');
            }
        );
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "request" route for the JS
     */
    protected function mapRequestRoutes()
    {
        Route::middleware(['bindings', 'no_session'])
            ->namespace($this->namespace.'\Request')
            ->group(base_path('routes/request.php'));
    }

    /**
     * Define the "response" routes for responses from JS
     */
    protected function mapResponseRoutes()
    {
        Route::middleware('no_http_cache')
            ->namespace($this->namespace.'\Response')
            ->group(base_path('routes/response.php'));
    }
}
