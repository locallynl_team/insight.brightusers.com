<?php

namespace App\Mail;

use App\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewInvoice extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $invoice;
    public $subject;
    public $payment;
    public $user;
    public $business;

    /**
     * Create a new message instance.
     *
     * @param Invoice $invoice
     */
    public function __construct(Invoice $invoice)
    {
        /**
         * Set e-mail content
         */
        $this->invoice = $invoice;
        $this->payment = $invoice->payment()->first();
        $this->user = $this->payment->user()->first();
        $this->business = config('invoices.business_details');

        /**
         * Set subject
         */
        $this->subject = __('Uw Surve factuur staat voor u klaar');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.new-invoice')
            ->attach($this->invoice->getUrl())
            ->subject($this->subject);
    }
}
