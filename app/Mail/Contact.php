<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    public $formRequest;

    /**
     * Create a new message instance.
     *
     * @param  Request $formRequest
     * @return void
     */
    public function __construct(Request $formRequest)
    {
        $this->formRequest = $formRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.contact')->with(
            [
            'subject' => 'Nieuw bericht via Surve.nl',
            'name' => $this->formRequest->name,
            'email' => $this->formRequest->email,
            'phone' => $this->formRequest->phone,
            'userMessage' => $this->formRequest->message
            ]
        );
    }
}
