<?php

namespace App\Mail;

use App\Survey;
use App\SurveyResponse;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewResponse extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $survey;
    public $response;
    public $type;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @param User           $user
     * @param Survey         $survey
     * @param SurveyResponse $response
     */
    public function __construct(User $user, Survey $survey, SurveyResponse $response)
    {
        $this->user = $user;
        $this->survey = $survey;
        $this->response = $response;
        $this->type = strtolower(Survey::getSettingsByType($survey->type)['name']);

        $this->subject = __('Nieuwe reactie op uw :type', ['type' => $this->type]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.new-response')->subject($this->subject);
    }
}
