<?php

namespace App\Mail;

use App\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TrialExpiry extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $subscription;
    public $user;
    public $daysLeft;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @param Subscription $subscription
     * @param $daysLeft
     */
    public function __construct(Subscription $subscription, $daysLeft)
    {
        $this->subscription = $subscription;
        $this->daysLeft = $daysLeft;

        $this->subject = trans_choice(
            'Uw proefperiode van Surve verloopt morgen|Uw proefperiode van Surve verloopt over :days dagen',
            $this->daysLeft,
            ['days' => $this->daysLeft]
        );
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.trial-expiry')->subject($this->subject);
    }
}
