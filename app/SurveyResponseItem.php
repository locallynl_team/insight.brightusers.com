<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyResponseItem extends Model
{
    protected $fillable = [
        'survey_response_id',
        'question_id',
        'answer_id',
        'answer_text'
    ];

    /**
     * Belongs to Survey Response
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function surveyResponse()
    {
        return $this->belongsTo(SurveyResponse::class);
    }

    /**
     * Answer -> question
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function answer()
    {
        return $this->belongsTo(QuestionAnswer::class);
    }
}
