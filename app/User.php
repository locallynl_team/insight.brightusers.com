<?php

namespace App;

use App\Notifications\ResetPassword;
use Mollie;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    public $customer;
    public $mandates;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'company_name',
        'address',
        'postal_code',
        'city',
        'pay_on_invoice',
        'discount',
        'exclude_ip_addresses',
        'mollie_customer_id',
        'last_logged_in',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Always return boolean on is_admin
     *
     * @var array
     */
    protected $casts = [
        'is_admin' => 'boolean',
        'pay_on_invoice' => 'boolean',
        'had_trial' => 'boolean',
        'last_logged_in' => 'datetime'
    ];

    /**
     * User has many sites
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sites()
    {
        return $this->hasMany(Site::class);
    }

    /**
     * Surveys trough
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasManyThrough
     */
    public function surveys()
    {
        return $this->hasManyThrough(Survey::class, Site::class)->surveys();
    }

    /**
     * Feedbackbuttons trough
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasManyThrough
     */
    public function feedbackButtons()
    {
        return $this->hasManyThrough(Survey::class, Site::class)->feedbackButtons();
    }

    /**
     * Polls
     *
     * @return mixed
     */
    public function polls()
    {
        return $this->hasManyThrough(Survey::class, Site::class)->polls();
    }

    /**
     * Heatmaps for this user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function heatmaps()
    {
        return $this->hasManyThrough(Heatmap::class, Site::class);
    }

    /**
     * Forms for this user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function forms()
    {
        return $this->hasManyThrough(Form::class, Site::class);
    }

    /**
     * Get all active websites
     *
     * @return object
     */
    public function activeSites()
    {
        return $this->sites()->where('active', true);
    }

    /**
     * User has many payments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token, $this));
    }

    /**
     * Get or request new customer ID with mollie
     *
     * @return mixed
     */
    public function getCustomerId()
    {
        //customer has no mollie ID?
        if ($this->mollie_customer_id === null) {
            //create new customer in Mollie
            $customer = Mollie::api()->customers()->create(
                [
                'name' => $this->fullName,
                'email' => $this->email
                ]
            );

            //save mollie ID
            $this->mollie_customer_id = $customer->id;
            $this->save();
        }

        return $this->mollie_customer_id;
    }

    /**
     * Return the customer for this user
     *
     * @return mixed
     */
    public function getCustomer()
    {
        if ($this->customer === null) {
            $this->customer = Mollie::api()->customers()->get($this->getCustomerId());
        }
        return $this->customer;
    }

    /**
     * Return the mandates of this customer
     *
     * @return mixed
     */
    public function getMandates()
    {
        if ($this->mandates === null) {
            $this->mandates = $this->getCustomer()->mandates();
        }
        return $this->mandates;
    }

    /**
     * Checks if user has an active payment method
     *
     * @return bool
     */
    public function hasActivePaymentMethod()
    {
        //user has active mandate
        if ($this->getActiveMandate() === false) {
            return false;
        }

        return true;
    }

    /**
     * Get current active payment method
     *
     * @param  $mandate object Supply a mandate
     * @return array|null|string
     */
    public static function getPaymentMethodString($mandate)
    {
        //user can pay on invoice
        if ($mandate->method === 'invoice') {
            return __('betaling op factuur');
        } elseif ($mandate->method === "directdebit") {
            return __('automatisch incasso (:account)', ['account' => $mandate->details->consumerAccount]);
        } elseif ($mandate->method === "creditcard") {
            return __('creditcard (xxxx-xxxx-xxxx-:card)', ['card' => $mandate->details->cardNumber]);
        }

        return __('onbekend');
    }

    /**
     * Get active mandate for this user
     */
    public function getActiveMandate()
    {
        //user can pay on invoice -> return our own mandate
        if ($this->pay_on_invoice === true) {
            $mandate = new \stdClass();
            $mandate->method = 'invoice';
            return $mandate;
        }

        //no mollie ID -> no mandate possible
        if ($this->mollie_customer_id === null) {
            return false;
        }

        //get mandates with Mollie
        $mandates = $this->getMandates();

        //no mandates found
        if (count($mandates) === 0) {
            return false;
        }

        //loop trough mandates
        foreach ($mandates as $mandate) {
            if ($mandate->status === 'valid') {
                return $mandate;
            }
        }

        return false;
    }

    /**
     * Get current user website
     *
     * @return mixed
     */
    public function currentSite()
    {
        //user selected a website?
        if (!session()->exists('active_site') && $this->sites()->count() > 0) {
            //check if user has base website
            if ($this->base_site_id && $this->sites()->where('id', $this->base_site_id)->count() > 0) {
                $active_site = $this->base_site_id;
            } else {
                $active_site = $this->sites()->first()->id;
            }

            session(['active_site' => $active_site]);

            //website exists?
        } elseif (session()->exists('active_site') && $this->sites()
                ->where('id', session()->get('active_site'))
                ->count() === 0) {
            session()->forget('active_site');

            return $this->currentSite();
        }

        //return active website
        return session()->get('active_site', false);
    }

    /**
     * Switch current active website for user
     *
     * @param  $site
     * @return bool
     */
    public function switchSite(Site $site)
    {
        if ($site->user_id !== $this->id) {
            return false;
        }

        session(['active_site' => $site->id]);
        return true;
    }

    /**
     * Set base website
     *
     * @param  Site|null $site
     * @return bool
     */
    public function setBaseSite(Site $site = null)
    {
        //site selected?
        if ($site && $this->sites()->where('id', $site->id)->count() > 0) {
            $this->base_site_id = $site->id;
        } else {
            //user base_id is null or base id is not an active website
            if ($this->base_site_id === null || $this->sites()->where('id', $this->base_site_id)->count() === 0) {
                if ($this->sites()->count() === 0) {
                    $this->base_site_id = null;
                } else {
                    $this->base_site_id = $this->sites()->first()->id;
                }
            }
        }

        return $this->save();
    }

    /**
     * Get full name of user
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * Check if user has admin permissions
     *
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->is_admin;
    }

    /**
     * Calculate discount price with user discount
     *
     * @param  $price
     * @return float|int
     */
    public function calculateDiscountPrice($price)
    {
        return ($price * ((100 - $this->discount) / 100));
    }
}
