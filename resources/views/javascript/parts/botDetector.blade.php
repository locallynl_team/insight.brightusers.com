<?php if (false) { ?><script> <?php } ?>

    /**
     * Detect mobile/desktop/tablet devices
     */
    @include('javascript.parts.botList')

    surve.botDetector = (function () {
        var regex = new RegExp('(' + surve.botList.join('|') + ')', 'ig');
        var ua = navigator.userAgent;
        var detect = (function(s) {
            if (s === undefined) {
                s = ua;
            }
            regex.lastIndex = 0;
            return regex.test(s);
        });

        return {
            isBot: detect
        };
    }());

<?php if (false) { ?> </script> <?php } ?>