<?php if (false) { ?> <script> <?php } ?>

    //find forms with the correct triggers
    surve.activateForms = surve.tryCatch(function() {
        //surve.debug(surveSettings.forms);

        //get tracking ID's from session storage
        surve.trackingIDs = surve.getTrackingIDs();

        //loop[ trough forms
        surve.each(surveSettings.forms, function(form) {
            if (surve.matchTargets(form, window.location.href) === true && surve.activeForm === null) {
                //surve.debug('Activate form: ' + form.id);

                //activate form
                surve.startForm(form);
            }

            //we have to check for this form
            if (surve.trackingIDs.length > 0 && surve.trackingIDs.indexOf(form.id) !== -1) {
                surve.checkFormResult(form);
            }
        });
    }, "activateForm");

    //check if form exists in page, otherwise continue
    surve.startForm = function(form) {
        surve.activeForm = form;

        //form not found?
        var formElement = surve.getFormFromPage(surve.activeForm);
        if (formElement === null) {
            //surve.debug('Form not found on page, continue.');

            surve.activeForm = null;
            return false;
        }

        //add form view
        surve.addFormEventToQueue({
            form_id: surve.activeForm.id,
            type: "view",
        });

        //set form element
        surve.activeForm.htmlElement = formElement;
        surve.activateFormTriggers();
    };

    //activate all triggers
    surve.activateFormTriggers = surve.tryCatch(function() {
        //set trigger for submit
        surve.addTrigger('submit', document, function(e) {
            //check if the submitted form is the form we are tracking
            var target = e.target || e.srcElement; //IE 6-8
            if (target === surve.activeForm.htmlElement) {
                surve.addTrackingID(surve.activeForm.id);
            }
        });

        //loop trough array of types & set all triggers
        surve.addFieldTriggers('focus', ['*'], ['checkbox', 'radio'], surve.startFocusTimer);
        surve.addFieldTriggers('blur', ['*'], ['checkbox', 'radio'], surve.addFieldInteraction);
        surve.addFieldTriggers('change', ['checkbox'], [], surve.addMultiFieldInteraction);
        surve.addFieldTriggers('focus', ['radio'], [], surve.addMultiFieldInteraction);
    });

    //add triggers on fields
    surve.addFieldTriggers = function(trigger, fields, exclude, callback) {
        //surve.debug('Adding triggers: ' + trigger);

        //loop trough fields
        surve.each(surve.activeForm.fields, function(field) {
            //check field type
            if ((fields.indexOf('*') > -1 || fields.indexOf(field.type) > -1) && exclude.indexOf(field.type) === -1) {
                //get field
                var fieldElements = surve.getFieldsBySelector(field.selector, field.selector_value), i;

                //set trigger on all fields we found
                for (i = 0; i < fieldElements.length; ++i) {
                    var triggerField = fieldElements[i];
                    surve.addTrigger(trigger, triggerField, function(e) { callback(e, field); });
                }
            }
        });
    };

    //add new interaction with field
    surve.addFieldInteraction = surve.tryCatch(function(event, field) {
        //surve.debug('Add field interaction');
        //check if we have a timer
        if (surve.focusTimer === null) {
            return false;
        }

        //get element & value
        var value, element = event.target || event.srcElement;
        if (field.type === 'select') {
            value = Array.prototype.slice.call(element.querySelectorAll('option:checked'),0).map(function(v,i,a) {
                return surve.hash(v.value, surve.getFingerPrint());
            }).join(',');
        } else {
            value = surve.hash(element.value, surve.getFingerPrint());
        }

        //add log event
        surve.addFormEventToQueue({
            form_id: surve.activeForm.id,
            type: "field_leave",
            field_id: field.id,
            timing: new Date - surve.focusTimer,
            hash: value
        });

        //reset timer
        surve.focusTimer = null;
    }, "addFieldInteraction");

    //add multi field interaction (checkbox)
    surve.addMultiFieldInteraction = surve.tryCatch(function(event, field) {
        //surve.debug('Add multi-field interaction');

        //get element & value
        var i, values = [], elements = surve.getFieldsBySelector(field.selector, field.selector_value);

        //get all selected values
        for (i = 0; i < elements.length; i += 1) {
            values.push(surve.hash((elements[i].checked ? elements[i].value : ''), surve.getFingerPrint()));
        }

        //add log event
        surve.addFormEventToQueue({
            form_id: surve.activeForm.id,
            type: "field_leave",
            field_id: field.id,
            timing: null,
            hash: values.join(',')
        });
    }, "addMultiFieldInteraction");

    //add form events to queue
    surve.addFormEventToQueue = surve.tryCatch(function(event) {
        //surve.debug('Logging form event');
        //surve.debug(event);

        //add to queue
        surve.formEventQueue.push(event);

        //send events
        if (surve.formEventQueue.length >= {{ (config('app.env') === "production" ? 5 : 1) }}) {
            surve.debug('Log form events to server');
            surve.processFormQueue();
        }
    });

    //log form events
    surve.processFormQueue = surve.tryCatch(function() {
        //surve.debug('Add form data to queue');
        if (surve.formEventQueue.length > 0) {
            surve.logFormEvents((surve.activeForm === null ? 0 : surve.activeForm.id), surve.formEventQueue);
            surve.formEventQueue = [];
        }
    }, "formQueue");

    //get all fields with this selector
    surve.getFieldsBySelector = function(selector, selector_value) {
        //surve.debug('Getting field by selector: ' + selector + '=' + selector_value);

        return surve.activeForm.htmlElement.querySelectorAll('*[' + selector + '="' + selector_value.replace(/'/g, "\\'") + '"]');
    };

    //start new focus timer
    surve.startFocusTimer = surve.tryCatch(function() {
        surve.focusTimer = new Date;
    }, 'startFocusTimer');

    //add ID to tracking in session storage
    surve.addTrackingID = function(id) {
        //surve.debug('Adding tracking ID to session storage');
        if (surve.trackingIDs.indexOf(id) === -1) {
            surve.trackingIDs.push(id);

            sessionStorage.setItem("_SurveFormTrackingIDs", JSON.stringify(surve.trackingIDs));
        }
    };

    //remove an ID from tracking
    surve.removeTrackingID = function(id) {
        //surve.debug('Delete ID from tracking ID\'s.');
        if (surve.trackingIDs.indexOf(id) > -1) {
            surve.trackingIDs.splice(surve.trackingIDs.indexOf(id), 1);
        }

        sessionStorage.setItem("_SurveFormTrackingIDs", JSON.stringify(surve.trackingIDs));
    };

    //get tracking IDs
    surve.getTrackingIDs = function() {
        var trackingIDs = JSON.parse(sessionStorage.getItem("_SurveFormTrackingIDs")) || [];

        //surve.debug('Getting tracking IDs from session storage');
        //surve.debug(trackingIDs);

        return trackingIDs;
    };

    //get current form
    surve.getFormFromPage = function(form) {
        if (form === null) {
            return null;
        }

        //find by type
        var formElement = null;
        if (form.selector_target_type === 'id') {
            formElement = document.getElementById(form.selector_target);
        } else if (form.selector_target_type === 'path') {
            formElement = document.querySelector(form.selector_target);
        }

        return formElement;
    };

    //manual form submit function
    surve.formSubmit = surve.tryCatch(function(type) {
        //manual submit log
        //surve.debug('Logging manual form submission result');

        //get ID from stored ID's
        var trackingIDs = surve.getTrackingIDs();
        if (surve.activeForm !== null && trackingIDs.indexOf(surve.activeForm.id) > -1) {
            //check if is successful
            if (type === 'successful') {
                surve.registerSubmitEvent(surve.activeForm.id, true);
            } else if (type === 'unsuccessful') {
                surve.registerSubmitEvent(surve.activeForm.id, false);
            }

            //remove ID from tracking ID's
            surve.removeTrackingID(surve.activeForm.id);
        }
    }, "formSubmit");

    //check if form was successful
    surve.checkFormResult = function(form) {
        if (surve.matchTargets(form, document.referrer) === true) {
            if (surve.getFormFromPage(form) === null) {
                surve.registerSubmitEvent(form.id, true);
            } else {
                surve.registerSubmitEvent(form.id, false);
            }

            surve.removeTrackingID(form.id);
        }
    };

    //register submit event
    surve.registerSubmitEvent = surve.tryCatch(function(id, result) {
        //surve.debug('Form (ID: ' + id + ') submit registered as: ' + result);

        //add to event queue
        surve.addFormEventToQueue({
            form_id: id,
            type: (result === true ? "successful_submit" : "unsuccessful_submit")
        });
    }, "registerSubmitEvent");

<?php if (false) { ?> </script> <?php } ?>