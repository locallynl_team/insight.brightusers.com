<?php if (false) { ?> <script> <?php } ?>
        
    surve.startScrollTracker = surve.tryCatch(function() {
        surve.ready(function() {
            var surveScrollTracker = SurveScrollTracker();

            surveScrollTracker.start();
        });
    }, "startSurveScrollTracker");

     /**
     * Copyright(c) 2017 LunaMetrics, LLC.
     * Written by @notdanwilkerson
     * Licensed under the MIT License
     * For full license text, visit https://opensource.org/licenses/MIT
     */
    (function(window) {

        'use strict';
        // Won't work on IE8, so we install a mock.
        if (window.navigator.userAgent.match(/MSIE [678]/gi)) return installMock();

        /**
         * @constructor
         *
         * @returns {SurveScrollTracker}
         */
        function SurveScrollTracker() {

            if (!(this instanceof SurveScrollTracker)) return new SurveScrollTracker();

            var boundAndThrottledDepthCheck = throttle(this.checkDepth.bind(this), 500);
            var boundUpdate = this.start.bind(this);
            var throttledUpdate = throttle(boundUpdate, 500);

            window.addEventListener('scroll', boundAndThrottledDepthCheck, true);
            window.addEventListener('resize', throttledUpdate);
        }

        /**
         * Start with first check
         */
        SurveScrollTracker.prototype.start = function() {
            this.checkDepth();
        };

        /**
         * Checks all marks and triggers appropriate handlers
         */
        SurveScrollTracker.prototype.checkDepth = function() {
            var currentDepth = parseInt(this.currentDepth());
            var contextHeight = parseInt(this.pageHeight());

            if (currentDepth > surve.scrollDepth && surve.scrollDepth < contextHeight) {
                surve.scrollDepth = currentDepth;

                //surve.debug('New depth: ' + surve.scrollDepth);
            }
        };

        /**
         * Returns the height of the scrolling context
         *
         * @returns {number}
         */
        SurveScrollTracker.prototype.pageHeight = function() {
            return Math.max(
                document.body.scrollHeight, document.documentElement.scrollHeight,
                document.body.offsetHeight, document.documentElement.offsetHeight,
                document.body.clientHeight, document.documentElement.clientHeight
            );
        };

        /**
         * Returns the current depth we've scrolled into the context
         *
         * @returns {number}
         */
        SurveScrollTracker.prototype.currentDepth = function() {
            var scrollDepth = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop;
            var viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

            return scrollDepth + viewportHeight;
        };

        /**
         * Does nothing
         */
        function noop() {}

        /**
         * Throttle function borrowed from:
         * Underscore.js 1.5.2
         * http://underscorejs.org
         * (c) 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
         * Underscore may be freely distributed under the MIT license.
         */
        function throttle(func, wait) {
            var context, args, result;
            var timeout = null;
            var previous = 0;
            var later = function() {
                previous = new Date;
                timeout = null;
                result = func.apply(context, args);
            };
            return function() {
                var now = new Date;
                if (!previous) previous = now;
                var remaining = wait - (now - previous);
                context = this;
                args = arguments;
                if (remaining <= 0) {
                    clearTimeout(timeout);
                    timeout = null;
                    previous = now;
                    result = func.apply(context, args);
                } else if (!timeout) {
                    timeout = setTimeout(later, remaining);
                }
                return result;
            };
        }

        /**
         * Installs a noop'd version of SurveScrollTracker on the window
         */
        function installMock() {

            var fake = {};
            var key;

            for (key in SurveScrollTracker) {

                fake[key] = noop;

            }

            window.SurveScrollTracker = fake;

        }

        window.SurveScrollTracker = SurveScrollTracker;

    })(this);
    /*
     * v2.0.3
     * Created by the Google Analytics consultants at http://www.lunametrics.com/
     * Written by @notdanwilkerson
     * Documentation: https://github.com/lunametrics/gascroll/
     * Licensed under the MIT License
     */

<?php if (false) { ?> </script> <?php } ?>