<?php if (false) { ?><script> <?php } ?>

    /**
     * Detect mobile/desktop/tablet devices
     */
    surve.deviceDetector = (function () {
        var ua = navigator.userAgent.toLowerCase();
        var detect = (function(s) {
            if (s === undefined) {
                s = ua;
            } else {
                s = s.toLowerCase();
            }
            if(/(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(s) && (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch)) {
                return 'tablet';
            } else {
                if (/(mobi|ipod|phone|blackberry|opera mini|fennec|minimo|symbian|psp|nintendo ds|archos|skyfire|puffin|blazer|bolt|gobrowser|iris|maemo|semc|teashark|uzard)/.test(ua) && (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch)) {
                    return 'mobile';
                } else {
                    return 'desktop';
                }
            }
        });
        return {
            device: detect(),
            detect: detect,
            isMobile: (detect() !== 'desktop'),
            userAgent: ua
        };
    }());

<?php if (false) { ?> </script> <?php } ?>
