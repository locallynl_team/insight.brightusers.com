<?php if (false) { ?> <script> <?php } ?>

    //find surveys with the correct triggers
    surve.activateHeatmaps = surve.tryCatch(function() {
        surve.debug(surveSettings.heatmaps);

        surve.each(surveSettings.heatmaps, function(heatmap) {
            if (surve.matchTargets(heatmap, window.location.href) === true) {
                surve.debug('Activate heatmap: ' + heatmap.id);

                surve.activateHeatmap(heatmap);
            }
        });
    }, "activateHeatmaps");

    //activate a heatmap
    surve.activateHeatmap = surve.tryCatch(function(heatmap) {
        if (surve.loaded.heatmap === false) {
            surve.loaded.heatmap = true;
            surve.currentHeatmap = heatmap;

            surve.stopwatch.start();
            surve.setHeatmapListeners();
            surve.startScrollTracker();
        }
    }, "activateHeatmap");

    //add all listeners for heatmaps
    surve.setHeatmapListeners = surve.tryCatch(function() {
        var bodyElement = document.querySelector('body');

        //register click event
        surve.addTrigger('click', bodyElement, function(event) {
            surve.heatmapClickEvent(event);
        });

        surve.addTrigger('touchstart', bodyElement, function(event){
            surve.heatmapClickEvent(event);
        });

        //register move events
        surve.addTrigger('mousemove', bodyElement, function(event) {
            surve.heatmapMoveEvent(event);
        });

        surve.addTrigger('touchmove', bodyElement, function(event) {
            surve.heatmapMoveEvent(event);
        });
    }, "setHeatmapListeners");

<?php if (false) { ?> </script> <?php } ?>