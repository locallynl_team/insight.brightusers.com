<?php if (false) { ?> <script> <?php } ?>

     /**
     * Log & debug functions
     */
    surve.log = function(error, reference) {
        console.log(error.name + ': ' + error.message + '\n' + 'Reference: ' + reference);
    };
    surve.debug = function(message) {
        if (surveSettings.showDebug === true) {
            console.log(message);
        }
    };

    /**
     * Try catch callback function
     */
    surve.tryCatch = function(code, position) {
        return function() {
            try {
                code.apply(this, arguments);
            } catch(error) {
                surve.log(error, position || "Default")
            }
        }
    };

    //create a new page session ID
    surve.generateNewPageSessionID = function(fingerprint) {
        return surve.hash(new Date, fingerprint);
    };

    /**
     * Foreach function for objects and arrays
     */
    surve.each = surve.tryCatch(function(object, callBack) {
        var tObject = Object.prototype.toString.call(object), i, l;

        if (tObject !== "[object Array]" && tObject !== "[object Object]") {
            throw new TypeError("'object' must be an array or object");
        }

        if (Object.prototype.toString.call(callBack) !== "[object Function]") {
            throw new TypeError("'callBack' must be a function");
        }

        if (tObject === "[object Array]") {
            i = 0;
            l = object.length;

            while (i < l) {
                callBack(object[i], i);

                i += 1;
            }
            return;
        }

        for (i in object) {
            if (object.hasOwnProperty(i)) {
                callBack(object[i], i);
            }
        }
    }, "each");

    surve.tryDecodeURIComponent = function(url) {
        try {
            return decodeURIComponent(url);
        } catch(e) {
            return url;
        }
    };

    /**
     * Trigger function
     */
    surve.addTrigger = surve.tryCatch(function(trigger, element, callback) {
        if (element.addEventListener) {
            element.addEventListener(trigger, function(event) {
                callback.apply(this, arguments);
            }, true);
        } else if (element.attachEvent) {
            element.attachEvent("on" + trigger, function(event) {
                callback.apply(this, arguments);
            });
        }
    });

    //Css class functions
    surve.hasClass = function(el, className) {
        if (el.classList) {
            return el.classList.contains(className);
        } else {
            return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
        }
    };

    surve.addClass = function(el, className) {
        if (el.classList) {
            el.classList.add(className)
        } else if (!surve.hasClass(el, className)) {
            el.className += " " + className
        }
    };

    surve.removeClass = function(el, className) {
        if (el.classList) {
            el.classList.remove(className);
        } else if (surve.hasClass(el, className)) {
            var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
            el.className=el.className.replace(reg, ' ');
        }
    };

    surve.hide = function(id) {
        document.getElementById(id).style.display = 'none';
    };

    surve.show = function(id) {
        document.getElementById(id).style.display = 'block';
    };

    surve.html = function(id, html) {
        document.getElementById(id).innerHTML = html;
    };

    surve.random = function(a, b, c, d) {
        c=a.length;while(c)b=Math.random()*c--|0,d=a[c],a[c]=a[b],a[b]=d
        return a;
    };

    //cookie functions
    surve.setCookie = function(cookieName) {
        surve.debug("Set cookie: " + cookieName);

        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();

        document.cookie = cookieName + "=true; " + expires + "; " + "path=/";
    };

    surve.getCookie = function(cookieName) {
        //surve.debug("Get cookie: " + cookieName);

        var name = cookieName + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };

    surve.getMetaData = function() {
        var unknown = 'unknown';

        // screen
        var screenSize = '';
        if (screen.width) {
            width = (screen.width) ? screen.width : '';
            height = (screen.height) ? screen.height : '';
            screenSize += '' + width + " x " + height;
        }

        // browser
        var nVer = navigator.appVersion;
        var nAgt = navigator.userAgent;
        var browser = navigator.appName;
        var version = '' + parseFloat(navigator.appVersion);
        var majorVersion = parseInt(navigator.appVersion, 10);
        var nameOffset, verOffset, ix;

        // Opera
        if ((verOffset = nAgt.indexOf('Opera')) != -1) {
            browser = 'Opera';
            version = nAgt.substring(verOffset + 6);
            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }
        // Opera Next
        if ((verOffset = nAgt.indexOf('OPR')) != -1) {
            browser = 'Opera';
            version = nAgt.substring(verOffset + 4);
        }
        // Edge
        else if ((verOffset = nAgt.indexOf('Edge')) != -1) {
            browser = 'Microsoft Edge';
            version = nAgt.substring(verOffset + 5);
        }
        // MSIE
        else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(verOffset + 5);
        }
        // Chrome
        else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
            browser = 'Chrome';
            version = nAgt.substring(verOffset + 7);
        }
        // Safari
        else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
            browser = 'Safari';
            version = nAgt.substring(verOffset + 7);
            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }
        // Firefox
        else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
            browser = 'Firefox';
            version = nAgt.substring(verOffset + 8);
        }
        // MSIE 11+
        else if (nAgt.indexOf('Trident/') != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(nAgt.indexOf('rv:') + 3);
        }
        // Other browsers
        else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
            browser = nAgt.substring(nameOffset, verOffset);
            version = nAgt.substring(verOffset + 1);
            if (browser.toLowerCase() == browser.toUpperCase()) {
                browser = navigator.appName;
            }
        }
        // trim the version string
        if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

        if (isNaN(majorVersion)) {
            version = '' + parseFloat(navigator.appVersion);
            majorVersion = parseInt(navigator.appVersion, 10);
        }

        // system
        var os = unknown;
        var clientStrings = [
            {s: 'Windows 10', r: /(Windows 10.0|Windows NT 10.0)/},
            {s: 'Windows 8.1', r: /(Windows 8.1|Windows NT 6.3)/},
            {s: 'Windows 8', r: /(Windows 8|Windows NT 6.2)/},
            {s: 'Windows 7', r: /(Windows 7|Windows NT 6.1)/},
            {s: 'Windows Vista', r: /Windows NT 6.0/},
            {s: 'Windows Server 2003', r: /Windows NT 5.2/},
            {s: 'Windows XP', r: /(Windows NT 5.1|Windows XP)/},
            {s: 'Windows 2000', r: /(Windows NT 5.0|Windows 2000)/},
            {s: 'Windows ME', r: /(Win 9x 4.90|Windows ME)/},
            {s: 'Windows 98', r: /(Windows 98|Win98)/},
            {s: 'Windows 95', r: /(Windows 95|Win95|Windows_95)/},
            {s: 'Windows NT 4.0', r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/},
            {s: 'Windows CE', r: /Windows CE/},
            {s: 'Windows 3.11', r: /Win16/},
            {s: 'Android', r: /Android/},
            {s: 'Open BSD', r: /OpenBSD/},
            {s: 'Sun OS', r: /SunOS/},
            {s: 'Linux', r: /(Linux|X11)/},
            {s: 'iOS', r: /(iPhone|iPad|iPod)/},
            {s: 'Mac OS X', r: /Mac OS X/},
            {s: 'Mac OS', r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/},
            {s: 'QNX', r: /QNX/},
            {s: 'UNIX', r: /UNIX/},
            {s: 'BeOS', r: /BeOS/},
            {s: 'OS/2', r: /OS\/2/},
            {s: 'Search Bot', r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/}
        ];
        for (var id in clientStrings) {
            var cs = clientStrings[id];
            if (cs.r.test(nAgt)) {
                os = cs.s;
                break;
            }
        }

        var osVersion = unknown;

        if (/Windows/.test(os)) {
            osVersion = /Windows (.*)/.exec(os)[1];
            os = 'Windows';
        }

        switch (os) {
            case 'Mac OS X':
                osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                break;

            case 'Android':
                osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                break;

            case 'iOS':
                osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                if (osVersion === null) {
                    osVersion = 0;
                } else {
                    osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                }
                break;
        }

        return {
            resolution: screenSize,
            browser: browser + ' ' + version,
            os: os + ' ' + osVersion
        };
    };

    surve.hash = function(key, seed) {
        var hex_chr = "0123456789abcdef";
        function rhex(num)
        {
            str = "";
            for(j = 0; j <= 3; j++)
                str += hex_chr.charAt((num >> (j * 8 + 4)) & 0x0F) +
                    hex_chr.charAt((num >> (j * 8)) & 0x0F);
            return str;
        }

        /*
         * Convert a string to a sequence of 16-word blocks, stored as an array.
         * Append padding bits and the length, as described in the MD5 standard.
         */
        function str2blks_MD5(str)
        {
            nblk = ((str.length + 8) >> 6) + 1;
            blks = new Array(nblk * 16);
            for(i = 0; i < nblk * 16; i++) blks[i] = 0;
            for(i = 0; i < str.length; i++)
                blks[i >> 2] |= str.charCodeAt(i) << ((i % 4) * 8);
            blks[i >> 2] |= 0x80 << ((i % 4) * 8);
            blks[nblk * 16 - 2] = str.length * 8;
            return blks;
        }

        /*
         * Add integers, wrapping at 2^32. This uses 16-bit operations internally
         * to work around bugs in some JS interpreters.
         */
        function add(x, y)
        {
            var lsw = (x & 0xFFFF) + (y & 0xFFFF);
            var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
            return (msw << 16) | (lsw & 0xFFFF);
        }

        /*
         * Bitwise rotate a 32-bit number to the left
         */
        function rol(num, cnt)
        {
            return (num << cnt) | (num >>> (32 - cnt));
        }

        /*
         * These functions implement the basic operation for each round of the
         * algorithm.
         */
        function cmn(q, a, b, x, s, t)
        {
            return add(rol(add(add(a, q), add(x, t)), s), b);
        }
        function ff(a, b, c, d, x, s, t)
        {
            return cmn((b & c) | ((~b) & d), a, b, x, s, t);
        }
        function gg(a, b, c, d, x, s, t)
        {
            return cmn((b & d) | (c & (~d)), a, b, x, s, t);
        }
        function hh(a, b, c, d, x, s, t)
        {
            return cmn(b ^ c ^ d, a, b, x, s, t);
        }
        function ii(a, b, c, d, x, s, t)
        {
            return cmn(c ^ (b | (~d)), a, b, x, s, t);
        }

        /*
         * Take a string and return the hex representation of its MD5.
         */
        function calcMD5(str)
        {
            x = str2blks_MD5(str);
            a =  1732584193;
            b = -271733879;
            c = -1732584194;
            d =  271733878;

            for(i = 0; i < x.length; i += 16)
            {
                olda = a;
                oldb = b;
                oldc = c;
                oldd = d;

                a = ff(a, b, c, d, x[i+ 0], 7 , -680876936);
                d = ff(d, a, b, c, x[i+ 1], 12, -389564586);
                c = ff(c, d, a, b, x[i+ 2], 17,  606105819);
                b = ff(b, c, d, a, x[i+ 3], 22, -1044525330);
                a = ff(a, b, c, d, x[i+ 4], 7 , -176418897);
                d = ff(d, a, b, c, x[i+ 5], 12,  1200080426);
                c = ff(c, d, a, b, x[i+ 6], 17, -1473231341);
                b = ff(b, c, d, a, x[i+ 7], 22, -45705983);
                a = ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
                d = ff(d, a, b, c, x[i+ 9], 12, -1958414417);
                c = ff(c, d, a, b, x[i+10], 17, -42063);
                b = ff(b, c, d, a, x[i+11], 22, -1990404162);
                a = ff(a, b, c, d, x[i+12], 7 ,  1804603682);
                d = ff(d, a, b, c, x[i+13], 12, -40341101);
                c = ff(c, d, a, b, x[i+14], 17, -1502002290);
                b = ff(b, c, d, a, x[i+15], 22,  1236535329);

                a = gg(a, b, c, d, x[i+ 1], 5 , -165796510);
                d = gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
                c = gg(c, d, a, b, x[i+11], 14,  643717713);
                b = gg(b, c, d, a, x[i+ 0], 20, -373897302);
                a = gg(a, b, c, d, x[i+ 5], 5 , -701558691);
                d = gg(d, a, b, c, x[i+10], 9 ,  38016083);
                c = gg(c, d, a, b, x[i+15], 14, -660478335);
                b = gg(b, c, d, a, x[i+ 4], 20, -405537848);
                a = gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
                d = gg(d, a, b, c, x[i+14], 9 , -1019803690);
                c = gg(c, d, a, b, x[i+ 3], 14, -187363961);
                b = gg(b, c, d, a, x[i+ 8], 20,  1163531501);
                a = gg(a, b, c, d, x[i+13], 5 , -1444681467);
                d = gg(d, a, b, c, x[i+ 2], 9 , -51403784);
                c = gg(c, d, a, b, x[i+ 7], 14,  1735328473);
                b = gg(b, c, d, a, x[i+12], 20, -1926607734);

                a = hh(a, b, c, d, x[i+ 5], 4 , -378558);
                d = hh(d, a, b, c, x[i+ 8], 11, -2022574463);
                c = hh(c, d, a, b, x[i+11], 16,  1839030562);
                b = hh(b, c, d, a, x[i+14], 23, -35309556);
                a = hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
                d = hh(d, a, b, c, x[i+ 4], 11,  1272893353);
                c = hh(c, d, a, b, x[i+ 7], 16, -155497632);
                b = hh(b, c, d, a, x[i+10], 23, -1094730640);
                a = hh(a, b, c, d, x[i+13], 4 ,  681279174);
                d = hh(d, a, b, c, x[i+ 0], 11, -358537222);
                c = hh(c, d, a, b, x[i+ 3], 16, -722521979);
                b = hh(b, c, d, a, x[i+ 6], 23,  76029189);
                a = hh(a, b, c, d, x[i+ 9], 4 , -640364487);
                d = hh(d, a, b, c, x[i+12], 11, -421815835);
                c = hh(c, d, a, b, x[i+15], 16,  530742520);
                b = hh(b, c, d, a, x[i+ 2], 23, -995338651);

                a = ii(a, b, c, d, x[i+ 0], 6 , -198630844);
                d = ii(d, a, b, c, x[i+ 7], 10,  1126891415);
                c = ii(c, d, a, b, x[i+14], 15, -1416354905);
                b = ii(b, c, d, a, x[i+ 5], 21, -57434055);
                a = ii(a, b, c, d, x[i+12], 6 ,  1700485571);
                d = ii(d, a, b, c, x[i+ 3], 10, -1894986606);
                c = ii(c, d, a, b, x[i+10], 15, -1051523);
                b = ii(b, c, d, a, x[i+ 1], 21, -2054922799);
                a = ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
                d = ii(d, a, b, c, x[i+15], 10, -30611744);
                c = ii(c, d, a, b, x[i+ 6], 15, -1560198380);
                b = ii(b, c, d, a, x[i+13], 21,  1309151649);
                a = ii(a, b, c, d, x[i+ 4], 6 , -145523070);
                d = ii(d, a, b, c, x[i+11], 10, -1120210379);
                c = ii(c, d, a, b, x[i+ 2], 15,  718787259);
                b = ii(b, c, d, a, x[i+ 9], 21, -343485551);

                a = add(a, olda);
                b = add(b, oldb);
                c = add(c, oldc);
                d = add(d, oldd);
            }
            return rhex(a) + rhex(b) + rhex(c) + rhex(d);
        }

        return calcMD5(key + '' + seed);
    };


    @include('javascript.parts.deviceDetector')
    @include('javascript.parts.loadCSS')
    @include('javascript.parts.loadHTML')

<?php if (false) { ?> </script> <?php } ?>