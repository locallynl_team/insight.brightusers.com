<?php if (false) { ?> <script> <?php } ?>

    /**
     * Stopwatch function
     */
    surve.stopwatch = {
        startTime: null,

        start: function () {
            this.startTime = new Date();
        },

		timing: function () {
            var now = new Date();
            return Math.round((now.getTime() - this.startTime.getTime()) / 1000);
        }
	};

<?php if (false) { ?> </script> <?php } ?>