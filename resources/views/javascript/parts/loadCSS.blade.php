<?php if (false) { ?> <script> <?php } ?>

    surve.loadCSS = surve.tryCatch(function() {
        //Only load css once
        if (surve.loadedCSS === true) {
            //surve.debug('CSS already loaded');
            return false;
        }
        surve.loadedCSS = true;

        //Append css to head
        var s = document.createElement("style");
        s.innerHTML = (surve.deviceDetector.device === 'desktop' ? surveSettings.css : surveSettings.css + ' ' + surveSettings.mobileCss);
        document.getElementsByTagName("head")[0].appendChild(s);

        //surve.debug('Loaded CSS');
    }, "loadCSS");

<?php if (false) { ?> </script> <?php } ?>