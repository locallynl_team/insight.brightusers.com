<?php if (false) { ?> <script> <?php } ?>

    //get unique selector for clicked element
    surve.getPath = function (node) {
        var path = [];
        var ignore = ["script", "style", "link"];
        while (node) {
            //get name
            var name = node.localName;

            //no name? continue
            if (!name) break;

            //lowercase name
            name = name.toLowerCase();

            //ignore some elements
            if (ignore.indexOf(name) > 0) {
                return [];
            }

            //get parent
            var parent = node.parentNode;

            //get sibblings with same tag, we need to know the nth-child
            var sameTagSiblings = [].filter.call(parent.children, function(e) { return e.localName.toLowerCase() == name});
            if (sameTagSiblings.length > 1) {
                var allSiblings = parent.children;
                var index = [].indexOf.call(allSiblings, node) + 1;
                if (index > 1) {
                    name += ':nth-child(' + index + ')';
                }
            }
            path.push(name);
            node = parent;
        }

        if (path[1] === "head") {
            return [];
        }

        path.reverse();
        return path.slice(2);
    };

    //get mouse position
    surve.getMousePosition = function(event) {
        // Init pagex, pagey, event, x and y (which are the positions from the click with respect to the top-left of the page)
        var px = 0, py = 0, e = event.target, x = 0, y = 0;

        // If the event is a tap, use the first touch event as position
        if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
            px = event.originalEvent.touches[0].pageX;
            py = event.originalEvent.touches[0].pageY;
        } else {
            px = event.pageX;
            py = event.pageY;
        }

        do {
            var style = getComputedStyle(e, null);
            y += surve.prop(style, "border-top-width") + e.offsetTop;
            x += surve.prop(style, "border-left-width") + e.offsetLeft;
        } while (e = e.offsetParent);

        return {x: px - x, y: py - y};
    };

    //Get the value of a css property for a computed style
    surve.prop = function (style, prop) {
        return parseInt(style.getPropertyValue(prop), 10);
    };

    /**
     * Add an element to an array tree. Child nodes are stored as key=>node under the 'c' key
     * @param tree the tree
     * @param path the array of keys to the object
     * @param on_create
     * @param on_end
     * @param depth
     * @returns array
     */
    surve.nestedPath = function (tree, path, on_create, on_end, depth) {
        if (depth === undefined) {
            depth = 0;
        }

        // For first element
        if (!tree.hasOwnProperty('c')) {
            tree['c'] = {};
        }

        // Call callback when the end of the path has been reached
        if (depth === path.length) {
            tree = on_end(tree);
            return tree;
        }

        var current = path[depth];
        var subtree;

        if (tree['c'].hasOwnProperty(current)) {
            subtree = tree['c'][current];
        } else {
            subtree = on_create(depth);
        }

        tree['c'][current] = surve.nestedPath(subtree, path, on_create, on_end, depth + 1);
        return tree;
    };

    /**
     * Get the javascript css selector for an array of path elements
     * @param path
     * @returns {*|jQuery|HTMLElement}
     */
    surve.selectPath = function (path) {
        //escape all
        var escaped_path = path.map(function(e) {
            e.data = surve.escapeCSS(e.data);
            return e;
        });

        var p = escaped_path.length ? " > " + escaped_path.join(" > ") : "";
        return document.querySelector("html > body" + p);
    };

    /**
     * Get the bounding box for an element
     * @param  el
     * @returns  {t: number, l: number, h: number, w: number}
     */
    surve.getBoundingBox = function (el) {
        var r = el.getBoundingClientRect(), x = 0, y = 0;
        while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
            x += el.offsetLeft - el.scrollLeft;
            y += el.offsetTop - el.scrollTop;
            el = el.offsetParent;
        }
        return { t: y, l: x, h: ~~r.height, w: ~~r.width};
    };

    /**
     * Get information about the viewport of the client
     */
    surve.client = function () {
        return {
            'ts': (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) ? true : false,     // Touch status
            'r': (window.devicePixelRatio || 1), // Device pixel ratio
            'sw': ~~screen.width,  // Width and height of the screen
            'sh': ~~screen.height,
            'cw': ~~document.documentElement.clientWidth, // Width and height of the window (the window does not have to fill the whole screen)
            'ch': ~~document.documentElement.clientHeight, // Width and height of the window (the window does not have to fill the whole screen)
            'agent': navigator.userAgent
        };
    };

  surve.escapeCSS = function(value) {
        var string = String(value);
        var length = string.length;
        var index = -1;
        var codeUnit;
        var result = '';
        var firstCodeUnit = string.charCodeAt(0);
        while (++index < length) {
            codeUnit = string.charCodeAt(index);
            // Note: there’s no need to special-case astral symbols, surrogate
            // pairs, or lone surrogates.

            // If the character is NULL (U+0000), then the REPLACEMENT CHARACTER
            // (U+FFFD).
            if (codeUnit == 0x0000) {
                result += '\uFFFD';
                continue;
            }

            if (
                // If the character is in the range [\1-\1F] (U+0001 to U+001F) or is
            // U+007F, […]
                (codeUnit >= 0x0001 && codeUnit <= 0x001F) || codeUnit == 0x007F ||
                // If the character is the first character and is in the range [0-9]
                // (U+0030 to U+0039), […]
                (index == 0 && codeUnit >= 0x0030 && codeUnit <= 0x0039) ||
                // If the character is the second character and is in the range [0-9]
                // (U+0030 to U+0039) and the first character is a `-` (U+002D), […]
                (
                    index == 1 &&
                    codeUnit >= 0x0030 && codeUnit <= 0x0039 &&
                    firstCodeUnit == 0x002D
                )
            ) {
                // https://drafts.csswg.org/cssom/#escape-a-character-as-code-point
                result += '\\' + codeUnit.toString(16) + ' ';
                continue;
            }

            if (
                // If the character is the first character and is a `-` (U+002D), and
            // there is no second character, […]
                index == 0 &&
                length == 1 &&
                codeUnit == 0x002D
            ) {
                result += '\\' + string.charAt(index);
                continue;
            }

            // If the character is not handled by one of the above rules and is
            // greater than or equal to U+0080, is `-` (U+002D) or `_` (U+005F), or
            // is in one of the ranges [0-9] (U+0030 to U+0039), [A-Z] (U+0041 to
            // U+005A), or [a-z] (U+0061 to U+007A), […]
            if (
                codeUnit >= 0x0080 ||
                codeUnit == 0x002D ||
                codeUnit == 0x005F ||
                codeUnit >= 0x0030 && codeUnit <= 0x0039 ||
                codeUnit >= 0x0041 && codeUnit <= 0x005A ||
                codeUnit >= 0x0061 && codeUnit <= 0x007A
            ) {
                // the character itself
                result += string.charAt(index);
                continue;
            }

            // Otherwise, the escaped character.
            // https://drafts.csswg.org/cssom/#escape-a-character
            result += '\\' + string.charAt(index);

        }
        return result;
    };

<?php if (false) { ?> </script> <?php } ?>