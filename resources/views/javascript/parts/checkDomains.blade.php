<?php if (false) { ?> <script> <?php } ?>

    /**
     * Check domains function
     */
   surve.checkDomains = function() {
       var found = false;
       var domain = surve.getRootDomain;
       surveSettings.domains.forEach(function(item) {
           if (item.indexOf(domain) >= 0) {
               //surve.debug(item + ' ' + domain);
               found = true;
           }
       });

       return found;
   };

    /**
     * Get domain incl. toplevel domain
     */
   surve.getRootDomain = (function() {
        var i=0,domain=document.domain,p=domain.split('.'),s='_gd'+(new Date()).getTime();
        while(i<(p.length-1) && document.cookie.indexOf(s + '=' + s) == -1) {
            domain = p.slice(-1-(++i)).join('.');
            document.cookie = s+"="+s+";domain="+domain+";";
        }
        document.cookie = s+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT;domain="+domain+";";
        return domain;
    })();

<?php if (false) { ?> </script> <?php } ?>