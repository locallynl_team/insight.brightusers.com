<?php if (false) { ?> <script> <?php } ?>

    /**
     * Select a survey
     */
    surve.selectSurvey = function(survey) {
        surve.loadCSS();
        surve.loadHTML();

        if (surve.deviceDetector.device !== 'desktop') {
            surve.addTrigger('click', document.getElementById('surve-bar-yes'), function () {
                surve.showSurveyModal(survey);
            });

            surve.addTrigger('click', document.getElementById('surve-bar-no'), function () {
                surve.disableSurveyBar();
            });
        }

        //fill in survey modal with current survey
        surve.current = survey;

        //external activation?
        @if (!isset($preview))
            if (survey.external_activation === false) {
                surve.start();
            }

            surve.logView(survey.id);
        @else
                surve.start();
        @endif
    };

    //show surve, hide bar
    surve.showSurveyModal = function(survey) {
        //surve.debug('Clicked on yes, show surve: ' + survey.id);

        surve.hideMobileBar();
        surve.showMobile();
    };

    //disable survey
    surve.disableSurveyBar = function() {
        //surve.debug('Clicked on no, show nothing, and stop.');

        surve.turnOffOuibounce();
        surve.hideMobileBar();
    };

    //hide mobile bar
    surve.hideMobileBar = surve.tryCatch(function() {
        if (surve.deviceDetector.device !== 'desktop') {
            surve.hide('surve-bar');
        }
    }, "hideMobileBar");

<?php if (false) { ?> </script> <?php } ?>