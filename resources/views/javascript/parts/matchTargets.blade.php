<?php if (false) { ?> <script> <?php } ?>

    /**
     * Check if we need to activate survey/heatmap
     */
    surve.matchTargets = function(item, matchUrl) {
        //find URL triggers
        var urlMatch = false;
        if (surve.checkUrlTarget(item.url_target, item.url_target_type, matchUrl) === true) {
            urlMatch = true;

            //surve.debug('Url match, ' + item.url_target_type + ': ' + item.url_target);
        }

        //find platform triggers
        var platformMatch = false;
        surve.each({ mobile: item.mobile_platform, tablet: item.tablet_platform, desktop: item.desktop_platform }, function(value, platform) {
            if (value === true && surve.matchPlatform(platform) === true) {
                platformMatch = true;

                //surve.debug('Platform: ' + platform);
            }
        });

        return platformMatch === true && urlMatch === true;
    };

    /**
    * Match platform settings with current platform
    */
    surve.matchPlatform = function(platform) {
        return surve.deviceDetector.device === platform;
    };

    /**
     * UrlTarget function
     */
    surve.checkUrlTarget = function(url, type, matchUrl) {
        var matchPattern = (url.length > 0 ? url : '/');

        if (type !== "regular_expression") {
            matchPattern = surve.tryDecodeURIComponent(matchPattern);
        }

        var match = false;
        switch(type) {
            case "simple":
                matchPattern = matchPattern.toLowerCase().split("#")[0].split("?")[0].replace("http://www.", "").replace("https://www.", "").replace("http://", "").replace("https://", "").replace(/\/$/, "");
                matchUrl = matchUrl.toLowerCase().split("#")[0].split("?")[0].replace("http://www.", "").replace("https://www.", "").replace("http://", "").replace("https://", "").replace(/\/$/, "");
                match = matchPattern === matchUrl;
                break;
            case "exact":
                match = (matchUrl === matchPattern);
                break;
            case "starts_with":
                match = (matchUrl.indexOf(matchPattern) === 0);
                break;
            case "ends_with":
                var start = (matchUrl.length - matchPattern.length === -1 ? 0 : matchUrl.length - matchPattern.length);
                match = (matchUrl.substring(start, matchUrl.length) === matchPattern);
                break;
            case "contains":
                match = (matchUrl.indexOf(matchPattern) !== -1);
                break;
            case "regular_expression":
                match = RegExp(matchPattern).test(matchUrl)
        }

        return match;
    };

<?php if (false) { ?> </script> <?php } ?>