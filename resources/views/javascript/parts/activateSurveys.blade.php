<?php if (false) { ?> <script> <?php } ?>

    //find surveys with the correct triggers
    surve.activateSurveys = surve.tryCatch(function() {
        surve.each(surveSettings.surveys, function(survey) {
            if (surve.matchTargets(survey, window.location.href) === true) {
                if (survey.questions.length > 0) {
                    surve.activateSurvey(survey);
                } else {
                    surve.debug('Survey has no questions, skip and continue.');
                }
            }
        });
    }, "activateSurveys");

    //activate selected survey & set triggers
    surve.activateSurvey = surve.tryCatch(function(survey) {
        surve.debug('Activating ' + survey.type + ': ' + survey.id);

        //activate survey or feedbackbutton
        if ((survey.type === 'survey' || survey.type === 'poll') && surve.loaded.survey === false) {
            @if (!isset($preview))
                if (surve.getCookie('surve-' + survey.id) !== "true") {
                    surve.loaded.survey = true;
                    surve.selectSurvey(survey);
                } else {
                    surve.debug("Cookie set: survey not shown");
                }
            @else
                surve.selectSurvey(survey);
            @endif
        } else if (survey.type === 'feedbackbutton' @if (!isset($preview)) && surve.loaded.feedbackButton === false @endif ) {
            surve.loaded.feedbackButton = true;

            surve.selectFeedbackButton(survey);
        } else {
            surve.debug('Cannot activate survey, wrong type: ' + surve.type + ', or already activated other of this type');
        }
    }, "activateSurvey");

    //activate survey with ID
    surve.activateSurveyExternal = surve.tryCatch(function(surveyId) {
        surve.each(surveSettings.surveys, function(survey) {
            if (survey.id === surveyId) {
                surve.activateSurvey(survey);
            }
        });
    });

<?php if (false) { ?> </script> <?php } ?>