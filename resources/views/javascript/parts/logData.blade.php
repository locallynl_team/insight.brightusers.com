<?php if (false) { ?> <script> <?php } ?>

    surve.addQueueData = function(data) {
        @if (isset($preview))
            return;
        @endif

        surve.dataQueue.push(data);

        if (surve.dataQueue.length >= {{ (config('app.env') === "production" ? 3 : 1) }} || surve.deviceDetector.isMobile) {
            surve.processQueueRequest(false);
        }
    };

    surve.processQueueRequest = function(final) {
        @if (isset($preview))
            return;
        @endif

        if (final === true) {
            surve.finalRequest = true;
            surve.debug("Sending final data");

            //last request, add heatmap data
            if (surve.currentHeatmap !== null) {
                surve.processHeatmapQueue();
            }

            //add form data
            surve.processFormQueue();
        }

        //only send if we have data.
        if (surve.dataQueue.length > 0 && (final === true || (final === false && surve.finalRequest === false))) {
            var sendData = surve.dataQueue;
            surve.dataQueue = [];
            surve.ajax(surveSettings.responseHost + '/log', null, sendData, final);
        }
    };

    surve.logVisit = function() {
        surve.addQueueData({'log': 'visit'});
    };

    surve.logOpen = function(surveyId) {
        surve.addQueueData({'log': 'open', 'surveyId': surveyId});
    };

    surve.logView = function(surveyId) {
        surve.addQueueData({'log': 'view', 'surveyId': surveyId});
    };

    surve.logResults = function(surveyId, questionId, results) {
        surve.addQueueData({
            'log': 'results',
            'surveyId': surveyId,
            'questionId': questionId,
            'results': results
        });
    };

    surve.logHeatmapData = function(heatmapId, data, client, time) {
        surve.debug('Log heatmap data');

        surve.addQueueData({
            'log': 'heatmap',
            'heatmapId': heatmapId,
            'data': data,
            'client': client,
            'time': time,
            'scrollDepth': surve.scrollDepth
        });
    };

    surve.logFormEvents = function(formId, events) {
        surve.addQueueData({
            'log': 'form',
            'formId': formId,
            'events': events
        })
    };

    surve.addCloseListeners = function() {
        if (window.addEventListener) {
            window.addEventListener("beforeunload", function () {
                surve.processQueueRequest(true);
            });
        } else if (window.attachEvent) {
            window.attachEvent("onbeforeunload", function () {
                surve.processQueueRequest(true);
            });
        }
    };

<?php if (false) { ?> </script> <?php } ?>