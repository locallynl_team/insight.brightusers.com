<?php if (false) { ?> <script> <?php } ?>

    //prepare survey window
    surve.prepareSurvey = surve.tryCatch(function() {
        //surve.debug('Preparing modal for id: ' + surve.current.id);

        surve.currentPosition = 0;
        surve.showQuestion(surve.getQuestionByPosition(surve.currentPosition));
    }, "prepareSurvey");

    //render question
    surve.showQuestion = surve.tryCatch(function(question) {
        if (question === false) {
            //surve.debug('No question found with ID, hide modal');
            surve.closeModal();
            surve.active = false;
            return false;
        }

        //surve.debug('Show question with id: ' + question.id + ', type: ' + question.type);

        //show title & intro
        if (question.question !== null) {
            surve.show('surve-question');
            surve.html('surve-question', question.question);
        } else {
            surve.hide('surve-question');
        }

        if (question.intro !== null) {
            surve.show('surve-intro');
            surve.html('surve-intro', question.intro);
        } else {
            surve.hide('surve-intro');
        }

        var html = '';

        //multiple choice
        if (question.type === 'multi') {
            //add each answer
            var answers = (question.random_order === false ? question.answers : surve.random(question.answers));
            surve.each(answers, function(item) {
                html += '<input id="surve_id_' + item.id + '" class="surve-radio-multi" name="surve_answer[]" value="' + item.id + '" type="' + (question.multi_select === false ? 'radio' : 'checkbox') + '">';
                html += '<label for="surve_id_' + item.id + '">' + item.answer + '</label>';
            });

        //text question
        } else if (question.type === 'text') {
            if (question.multi_line === false) {
                html += '<input type="text" name="surve_answer" />';
            } else {
                html += '<textarea name="surve_answer"></textarea>';
            }
        //rating question
        } else if (question.type === 'rating') {
            html += '<div class="surve-rating">';
            //show text rating
            if (question.rating_type === 'text') {
                for (var i = 0; i <= 10; i++) {
                    html += '<span><input type="radio" class="surve-radio-rating" name="surve_answer[]" value="' + i +'" id="surve_id_' + question.id + '_' + i + '" />';
                    html += '<label for="surve_id_' + question.id + '_' + i + '">' + i + '</label></span>';
                }

            //show image rating
            } else {
                for (var i = 1; i <= 5; i++) {
                    html += '<span><input type="radio" class="surve-radio-rating-img" name="surve_answer[]" value="' + (i * 2) +'" id="surve_id_' + question.id + '_' + i + '" />';
                    html += '<label for="surve_id_' + question.id + '_' + i + '"><img src="' + surveSettings.staticHost + '/assets/backend/images/ratings/' + i + '.png" alt="" /></label></span>';
                }
            }
            html += '</div>';

            //add rating labels
            html += '<div class="surve-rating-labels">';
            html += '<div class="surve-rating-label-start">' + question.rating_label_start + '</div>';
            html += '<div class="surve-rating-label-end">' + question.rating_label_end + '</div>';
            html += '</div>';
        }

        //set button text
        var button = '';
        if (question.type === 'info') {
            button = question.button_next;
        } else if (surve.current.questions.length === 1 || question.after_action === -2) {
            button = surve.current.button_send_close;
        } else {
            button = surve.current.button_send;
        }
        html += surve.button(button);

        //set html
        surve.html('surve-form-content', html);
    }, "showQuestion");

    //submit form
    surve.submit = function() {
        //surve.debug('Form submitted, process data & continue');

        var results = [];
        var currentQuestion = surve.getQuestionByPosition(surve.currentPosition);
        if (currentQuestion.type === 'multi' || currentQuestion.type === 'rating') {
            //surve.debug('Process multi & rating questions');

            //get all surve_answer fields & get selected fields
            var inputs = document.getElementsByName('surve_answer[]');
            if (inputs.length > 0) {
                for (var i = 0; i < inputs.length; i++) {
                    if (inputs[i].checked) {
                        results.push(inputs[i].value);
                    }
                }
            }

            if (results.length === 0) {
               //surve.debug('No input, always continue.');
            }
        } else {
            //get data from field by name: surve_answer
            var input = document.getElementsByName('surve_answer');
            if (typeof input[0] !== "undefined") {
                if (input[0].value === "") {
                    //surve.debug('No input, continue');
                } else {
                    results.push(input[0].value);
                }
            } else {
                //surve.debug('No input, continue');
            }
        }

        //get next action from answer & select next question
        if (results.length > 0 && currentQuestion.type === 'multi' && currentQuestion.multi_select === false) {
            surve.each(currentQuestion.answers, function(item) {
                if (item.id == results[0]) {
                    if (item.after_action === -2) {
                        surve.currentPosition = -1;
                    } else if (item.after_action === -1) {
                        surve.currentPosition += 1;
                    } else {
                        var nextQuestion = surve.getQuestionById(item.after_action);
                        if (nextQuestion !== false) {
                            surve.currentPosition = nextQuestion.position;
                        } else {
                            surve.currentPosition += 1;
                        }
                    }
                }
            });
        } else {
            if (currentQuestion.after_action === -2) {
                surve.currentPosition = -1;
            } else if (currentQuestion.after_action === -1) {
                surve.currentPosition += 1;
            } else {
                surve.currentPosition = currentQuestion.after_action;
            }
        }

        //send data if not info
        if (currentQuestion.type !== 'info' && results.length > 0) {
            surve.logResults(surve.current.id, currentQuestion.id, results);
        }

        //show next question
        surve.showQuestion(surve.getQuestionByPosition(surve.currentPosition));
    };

    //return button HTML
    surve.button = function(text) {
        return '<p class="surve-button-wrapper"><input id="surve-submit" name="submit"  value="' + text + '" type="submit" /></p>';
    };

    //get question by position
    surve.getQuestionByPosition = function(position) {
        var question = false;
        surve.each(surve.current.questions, function(item) {
            if (item.position === position) {
                question = item;
            }
        });

        return question;
    };

    //get question by id
    surve.getQuestionById = function(id) {
        var question = false;
        surve.each(surve.current.questions, function(item) {
            if (item.id === id) {
                question = item;
            }
        });

        return question;
    };

<?php if (false) { ?> </script> <?php } ?>