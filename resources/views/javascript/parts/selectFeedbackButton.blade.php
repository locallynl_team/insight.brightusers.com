<?php if (false) { ?> <script> <?php } ?>

    /**
     * Select a feedback button as current
     */
    surve.selectFeedbackButton = function(feedbackButton) {
        surve.loadCSS();
        surve.loadHTML();

        surve.html('surve-feedback-button-text', feedbackButton.feedback_button_text);
        surve.setFeedbackButtonLocation(feedbackButton.feedback_button_location);
        surve.show('surve-feedback');

        surve.addTrigger('click', document.querySelector('#surve-feedback div'), function() {
            surve.showFeedbackModal(feedbackButton);
        });

        surve.logView(feedbackButton.id);
    };

    /**
     * Called after click on feedback button, shows modal
     */
    surve.showFeedbackModal = surve.tryCatch(function(feedbackButton) {
        //surve.debug('Clicked on feedbackbutton: ' + feedbackButton.id);

        //Hide feedback button
        surve.hide('surve-feedback');
        surve.hideMobileBar();

        //Prepare and show survey
        surve.current = feedbackButton;

        //Show modal
        surve.start();
    }, "showFeedbackModal");

    surve.setFeedbackButtonLocation = surve.tryCatch(function(location) {
        var button = document.getElementById('surve-feedback');
        if (location === 'right') {
            surve.removeClass(button, 'surve-feedback-bottom');
            surve.removeClass(button, 'surve-feedback-left');
        } else if (location === 'left') {
            surve.removeClass(button, 'surve-feedback-bottom');
            surve.addClass(button, 'surve-feedback-left');
        } else if (location === 'bottom') {
            surve.removeClass(button, 'surve-feedback-left');
            surve.addClass(button, 'surve-feedback-bottom');
        }
    });

<?php if (false) { ?> </script> <?php } ?>