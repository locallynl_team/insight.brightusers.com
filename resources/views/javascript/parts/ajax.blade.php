<?php if (false) { ?><script> <?php } ?>

    surve.ajax = function(url, callback, postData, async) {
        var requestType = null;

        //check beacon
        if (('navigator' in window && 'sendBeacon' in window.navigator) && async === true) {
            requestType = 'beacon';
        } else {
            //check req
            var req = surve.createXMLHTTPObject();
            if (req) {
                requestType = 'xmlhttp';
            } else {
                return false;
            }
        }

        //surve.debug(requestType);

        //set postdata
        var requestData = {};
        requestData.postData = (postData ? postData : []);
        requestData.siteId = surveSettings.siteId;
        requestData.fingerprint = surve.getFingerPrint();
        requestData.pageSessionId = surveSettings.pageSessionID;
        requestData.platform = encodeURIComponent(surve.deviceDetector.device);
        requestData.url = encodeURIComponent(document.URL);
        requestData.title = encodeURIComponent(document.title);
        requestData.browser = encodeURIComponent(surve.metaData.browser);
        requestData.os = encodeURIComponent(surve.metaData.os);
        requestData.resolution = encodeURIComponent(surve.metaData.resolution);
        requestData = surve.base64encode(JSON.stringify(requestData));

        //callback
        if (requestType === 'beacon') {
            //surve.debug('Sending beacon');

            //send as form-data to beacon
            var formData = new FormData();
            formData.append('data', requestData);

            //send data using beacon API
            navigator.sendBeacon(url, formData);

            //callback direct
            if (typeof callback === "function") {
                callback();
            }
        } else {
            req.open("POST", url, true);
            req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            req.onreadystatechange = function () {
                if (req.readyState !== 4 || req.status !== 200) {
                    //surve.debug('Ajax State changed: ' + req.readyState + ' / ' + req.status);
                    return false;
                }

                //surve.debug('Ajax returned data');

                if (typeof callback === "function") {
                    callback(req);
                }
            };

            if (req.readyState === 4) {
                return false;
            }

            //send request
            req.send('data=' + requestData);
        }
    };

    surve.createXMLHTTPObject = function() {
        var factories = [
            function () {return new XMLHttpRequest()},
            function () {return new ActiveXObject("Msxml3.XMLHTTP")},
            function () {return new ActiveXObject("Msxml2.XMLHTTP.6.0")},
            function () {return new ActiveXObject("Msxml2.XMLHTTP.3.0")},
            function () {return new ActiveXObject("Msxml2.XMLHTTP")},
            function () {return new ActiveXObject("Microsoft.XMLHTTP")}
        ];
        var xmlhttp = false;

        for (var i = 0; i < factories.length; i++) {
            try {
                xmlhttp = factories[i]();
            }
            catch (e) {
                continue;
            }
            break;
        }
        return xmlhttp;
    };

<?php if (false) { ?></script> <?php } ?>