<?php if (false) { ?> <script> <?php } ?>

    surve.loadHTML = surve.tryCatch(function() {
        if (surve.loaded.HTML === false) {
            var html = '';
            if (surve.deviceDetector.device !== 'desktop') {
                html += surveSettings.barHtml;
            }
            html += surveSettings.feedbackHtml;
            html += surveSettings.modalHtml;

            var el = document.createElement('div');
            el.id = "surve";
            el.innerHTML = html;

            document.body.appendChild(el);

            surve.addTrigger('click', document.getElementById('surve-close'), function() {
                surve.closeModal();
            });

            surve.addTrigger('submit', document.getElementById('surve-form'), function(e) {
                e.preventDefault();

                surve.submit();
            });

            surve.loaded.HTML = true;

            //surve.debug('Loaded HTML');
        }
    }, "loadHTML");

<?php if (false) { ?> </script> <?php } ?>