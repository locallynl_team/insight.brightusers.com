<?php if (false) { ?> <script> <?php } ?>

    //click event
    surve.heatmapClickEvent = surve.tryCatch(function(event) {
        surve.addHeatmapData(surve.heatmapData, surve.getPath(event.target), "click", event);

        surve.countHeatmapEvent('click');
    }, "clickEvent");

    //mouse move event
    surve.heatmapMoveEvent = surve.tryCatch(function(event) {
        surve.addHeatmapData(surve.heatmapData, surve.getPath(event.target), "move", event);

        surve.countHeatmapEvent('move');
    }, "moveEvent");

    //add heatmap data
    surve.addHeatmapData = surve.tryCatch(function(heatmap, path, type, event) {
        surve.nestedPath(heatmap, path,
            function (index) {
                return {
                    c: {},
                    b: surve.getBoundingBox(surve.selectPath(path.slice(0, index + 1)))
                }
            }, function (tree) {
                if (!tree.hasOwnProperty('b')) {
                    tree['b'] = surve.getBoundingBox(surve.selectPath(path))
                }

                if (!tree.hasOwnProperty(type)) {
                    tree[type] = [];
                }

                p = surve.getMousePosition(event);
                tree[type].push(p.x);
                tree[type].push(p.y);

                return tree;
            }
        );
    }, "addHeatmapData");

    //count all events, send events to server after x events
    surve.countHeatmapEvent = surve.tryCatch(function(event) {
        if (event === 'click') {
            surve.heatmapClickCount += 1;
        } else {
            surve.heatmapMoveCount += 1;
        }

        if ((surve.heatmapMoveCount + surve.heatmapClickCount) >= 250) {
            surve.processHeatmapQueue();
        }
    }, "countHeatmapEvent");

    //process heatmap data
    surve.processHeatmapQueue = surve.tryCatch(function() {
        if (surve.heatmapQueueData === null && surve.currentHeatmap !== null) {
            surve.debug('Heatmap queue processing data');

            /**
             * Check for match with triggers again, when url has changed...
             */
            if (surve.matchTargets(surve.currentHeatmap, window.location.href) === true) {
                surve.heatmapMoveCount = 0;
                surve.heatmapClickCount = 0;
                surve.heatmapQueueData = surve.heatmapData;
                surve.heatmapData = {};

                surve.logHeatmapData(surve.currentHeatmap.id, surve.heatmapQueueData, surve.client(), surve.stopwatch.timing()); //after send, empty queue.
                surve.heatmapQueueData = null;
            } else {
                surve.debug('Heatmap doesn\'t match anymore.');

                surve.heatmapMoveCount = 0;
                surve.heatmapClickCount = 0;
                surve.heatmapData = {};
                surve.heatmapQueueData = null;
            }
        }
    }, "processHeatmapQueue");

<?php if (false) { ?> </script> <?php } ?>