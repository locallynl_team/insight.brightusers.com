<?php if (false) { ?> <script> <?php } ?>

    /**
     * Load current selected survey or feedback button
     * Feedback button: load immidiatly
     * Survey on desktop: load after triggers
     * Survey on mobile: load bar after triggers
     */
    surve.start = surve.tryCatch(function() {
        //surve.debug('Surve start called -> let\'s show some modals');

        //no survey found
        if (surve.current === null) {
            //surve.debug('No current survey found, nothing to show');
            return false;
        }

        //Set cookie-name
        surve.cookieName = 'surve-' + surve.current.id;
        surve.turnOffOuibounce();

        @if (isset($preview))
            if (surve.current.type === 'survey' || surve.current.type === 'poll') {
                if (surve.active === false) {
                    surve.active = true;
                    if (surve.deviceDetector.device === 'desktop') {
                        surve.showDesktop();
                    } else if (surve.current.type === 'poll') {
                        surve.showMobile();
                    } else {
                        surve.showMobileBar();
                    }
                }
            } else if (surve.current.type === 'feedbackbutton') {
                surve.active = true;
                if (surve.deviceDetector.device === 'desktop') {
                    surve.showDesktop();
                } else {
                    surve.showMobile();
                }
            }
            return;
        @endif

        //Show survey
        if (surve.current.type === 'survey' || surve.current.type === 'poll') {
            //Time on page trigger
            var waitTime = (surve.current.time_on_page === true ? surve.current.time_on_page_miliseconds : 0);

            //Leave intent trigger
            if (surve.current.leave_intent === true) {
                //surve.debug('Setting trigger for survey with leave intent');

                var settings = {
                    aggressive: true,
                    callback: function() {
                        if (surve.active === false) {
                            surve.active = true;
                            if (surve.deviceDetector.device === 'desktop') {
                                surve.showDesktop();
                            } else if (surve.current.type === 'poll') {
                                surve.showMobile();
                            } else {
                                surve.showMobileBar();
                            }
                        }
                    }
                };

                surve.ouibounce_modal = surve.ouibounce(false, settings);
            }

            // Add trigger for time on page
            if (surve.current.time_on_page === true) {
                //surve.debug('Setting trigger for survey with timeout: ' + waitTime);

                //Set timeout & show desktop or bar
                window.setTimeout(function() {
                    if (surve.active === false) {
                        surve.turnOffOuibounce();

                        surve.active = true;
                        if (surve.deviceDetector.device === 'desktop') {
                            surve.showDesktop();
                        } else if (surve.current.type === 'poll') {
                            surve.showMobile();
                        } else {
                            surve.showMobileBar();
                        }
                    }
                }, waitTime);
            }
        } else if (surve.current.type === 'feedbackbutton') {
            if (surve.active === true) {
                surve.hideModal();
            }

            surve.active = true;
            if (surve.deviceDetector.device === 'desktop') {
                surve.showDesktop();
            } else {
                surve.showMobile();
            }
        }
    }, "start");

    /**
     * Show survey on desktop
     */
    surve.showDesktop = function() {
        //surve.debug('Showing modal for ' + surve.current.type + ' in desktop style');

        surve.prepareSurvey();
        surve.openModal();
    };

    /**
     * Show mobile bar
     */
    surve.showMobileBar = function() {
        //surve.debug('Showing mobile-bar');

        //Set texts in mobile bar
        surve.html('surve-bar-title', surve.current.mobile_bar_title);
        surve.html('surve-bar-subtitle', surve.current.mobile_bar_subtitle);
        surve.html('surve-bar-yes', surve.current.mobile_bar_button_yes);
        surve.html('surve-bar-no', surve.current.mobile_bar_button_no);

        //Show mobile bar
        surve.show('surve-bar');
    };

    /**
     * Show survey on mobile
     */
    surve.showMobile = function() {
        //surve.debug('Showing modal for ' + surve.current.type + ' in mobile style');

        //TODO, add mobile css class

        surve.prepareSurvey();
        surve.openModal();
    };

    surve.openModal = function() {
        if (surve.active === true) {
            surve.show('surve-modal');

            surve.logOpen(surve.current.id);
        }
    };

    surve.hideModal = function() {
        //surve.debug('Hiding modal');

        surve.active = false;
        surve.hide('surve-modal');
    };

    surve.closeModal = function() {
        surve.hideModal();

        if (surve.current.type === 'survey' || surve.current.type === 'poll') {
            surve.setCookie('surve-' + surve.current.id);
        }
    };

    /**
     * Turn off ouibounce
     */
    surve.turnOffOuibounce = surve.tryCatch(function() {
        if (surve.ouibounce_modal !== null) {
            surve.ouibounce_modal.disable();
            surve.ouibounce_modal = null;
        }
    }, "turnOffOuiBounce");

<?php if (false) { ?> </script> <?php } ?>