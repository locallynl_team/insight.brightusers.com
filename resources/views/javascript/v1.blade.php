<?php if (false) { ?> <script> <?php } ?>

window.surve = window.surve || {};
(function(window) {
    window.surveSettings = window.surveSettings || {
        siteId: {{ $site->id }},
        surveys: {!! $surveys !!},
        heatmaps: {!! $heatmaps !!},
        forms: {!! $forms !!},
        domains: {!! json_encode($site->domains) !!},
        css: '{!! $surveyCss !!}',
        mobileCss: '{!! $surveyMobileCss !!}',
        feedbackHtml: '{!! $feedbackHtml !!}',
        modalHtml: '{!! $modalHtml !!}',
        barHtml: '{!! $barHtml !!}',
        showDebug: {{ (config('app.env') === "production" ? "false" : "true") }},
        staticHost: '{{ static_url() }}',
        responseHost: '{{ response_url() }}',
        fingerprint: null
    };
    surve.loaded = {
        survey: false,
        feedbackButton: false,
        CSS: false,
        HTML: false,
        heatmap: false
    };
    surve.current = null;
    surve.cookieName = null;
    surve.ouibounce_modal = null;
    surve.active = false;
    surve.currentPosition = 0;
    surve.activeHeatmap = null;
    surve.heatmapEvents = [];
    surve.heatmapClickCount = 0;
    surve.heatmapMoveCount = 0;
    surve.scrollDepth = 0;
    surve.heatmapData = {};
    surve.heatmapQueueData = null;
    surve.dataQueue = [];
    surve.activeForm = null;
    surve.formEventQueue = [];
    surve.finalRequest = false;

    @include('javascript.parts.ready')
    @include('javascript.parts.helpers')
    @include('javascript.parts.heatmapHelpers')
    @include('javascript.parts.scrollDepth')
    @include('javascript.parts.stopwatch')
    @include('javascript.parts.ajax')
    @include('javascript.parts.base64')
    @include('javascript.parts.matchTargets')
    @include('javascript.parts.selectSurvey')
    @include('javascript.parts.selectFeedbackButton')
    @include('javascript.parts.prepareSurvey')
    @include('javascript.parts.start')
    @include('javascript.parts.heatmapEvents')
    @include('javascript.parts.activateSurveys')
    @include('javascript.parts.activateHeatmaps')
    @include('javascript.parts.ouibounce')
    @include('javascript.parts.logData')
    @include('javascript.parts.fingerPrinter')
    @include('javascript.parts.botDetector')
    @include('javascript.parts.checkDomains')
    @include('javascript.parts.activateForms')

    //find and activate surveys
    surve.main = surve.tryCatch(function() {
        @if (!isset($preview))
            if (surve.botDetector.isBot === true) {
                //surve.debug('Bot -> stop.');
                return false;
            }

            if (surve.checkDomains() === false) {
                //surve.debug('Domain check failed');
                return false;
            }
        @endif

        //start Surve execution
        surve.ready(function() {
            new SurveFingerprint().get(function (result) {
                //set fingerprint
                surveSettings.fingerprint = result;
                //surve.debug('Fingerprint: ' + result);

                //generate new page session id
                surveSettings.pageSessionID = surve.generateNewPageSessionID(surveSettings.fingerprint);
                //surve.debug('PageSessionID: ' + surveSettings.pageSessionID);

                //collect all meta-data
                surve.metaData = surve.getMetaData();

                //activate surveys
                surve.activateSurveys();

                //activate heatmaps
                @if (!isset($preview))
                    surve.activateHeatmaps();

                    //activate forms
                    surve.activateForms();
                @endif

                //add listeners for window close
                surve.addCloseListeners();

                //log visit
                @if (!isset($preview))
                    surve.logVisit();
                @endif
            });
        });
    }, "main");

    surve.main();
})(window);

<?php if (false) { ?> </script> <?php } ?>