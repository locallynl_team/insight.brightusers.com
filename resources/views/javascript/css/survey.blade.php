#surve-modal * {
    box-sizing:border-box;
    font-family: "Open Sans", sans-serif !important;
}

#surve-modal {
    font-family: "Open Sans", sans-serif;
    font-size: 14px;
    line-height: 1.1em;
    display: none;
    position: fixed;
    bottom: 0;
    width: 304px;
    border-radius: 6px 6px 0 0;
    z-index: 2147483646;
    border: 1px solid {{ $style->survey_border_color }};
    border-bottom: none;
    {{ $style->survey_position === 'left' ? 'left' : 'right'}}: 20px;
}

#surve-modal .surve-modal {
    border-radius: 6px 6px 0 0;
    width: 100%;
    z-index: 2147483647;
    margin: auto;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    -webkit-animation: popin .3s;
    animation: popin .3s;
    background-color: {{ $style->survey_background_color }};
}

#surve-modal .surve-modal-title {
    font-size: 18px;
    padding: 15px 10px 10px 10px;
    margin: 0;
    border-radius: 6px 6px 0 0;
    text-align: left;
    background-color: {{ $style->header_background_color }};
    color: {{ $style->header_text_color }};
}

#surve-modal p {
    line-height: 1.3em;
    color: {{ $style->survey_text_color }};
}

#surve-question {
    line-height: 1.2em;
    padding: 0;
    font-weight: bold;
    font-size: 16px;
    font-style: normal;
    text-transform: none;
    margin: 10px 10px 0px 10px;
    color: {{ $style->survey_text_color }};
    text-align: {{ ($style->survey_text_align === 'default' ? 'left' : $style->survey_text_align) }};
}

#surve-intro {
    line-height: 1.2em;
    text-transform: none;
    font-weight: 500;
    font-size: 16px;
    margin: 10px 10px 0;
    padding: 0;
    color: {{ $style->survey_text_color }};
    text-align: {{ ($style->survey_text_align === 'default' ? 'left' : $style->survey_text_align) }};
}

#surve-modal form {
    margin: 0;
    text-align: {{ ($style->survey_text_align === 'default' ? 'left' : $style->survey_text_align) }};
}

#surve-modal form label {
    font-weight:normal;
    cursor:pointer;
    float:none;
    padding:0;
    vertical-align:top;
    display: block;
}

#surve-modal form input[type="text"], #surve-modal form input[type="email"] {
    margin: 10px;
    padding: 12px;
    font-size: 16px;
    border-radius: 4px;
    border: 1px solid #ccc;
    color: #555;
    background-color: #fff;
    -webkit-font-smoothing: antialiased;
    width: 280px !important;
}

#surve-modal form input[type="submit"] {
    margin: 12px 0 0;
    background: none;
    text-transform: uppercase;
    font-weight: 700;
    padding: 12px;
    font-size: 1.1em;
    border-radius: 4px;
    border: none;
    cursor: pointer;
    -webkit-font-smoothing: antialiased;
    color: {{ $style->button_text_color }};
    background-color: {{ $style->button_background_color }};
    @if ($style->button_align === 'wide')
        width: 100%;
    @else
        float: {{ $style->button_align }};
    @endif
}

#surve-modal .surve-button-wrapper {
    padding:3px 0 10px 0;
    margin: 0 10px 0 10px;
}

#surve-modal .surve-button-wrapper:after {
    clear:both;
    content: " ";
    display:table;
}

#surve-modal form input[type="checkbox"], #surve-modal form input[type="radio"] {
    border:none;
    width:0;
    height:0;
    font-size:0;
    visibility: hidden;
    display: inline;
    position: fixed;
    webkit-appearance: none;
    -moz-appearance: none;
    -ms-appearance: none;
    -o-appearance: none;
    appearance: none;
}

#surve-modal form input[type="radio"].surve-radio-multi + label {
    display: block;
    margin: 8px 10px 0px;
    border-radius: 5px;
    padding: 15px 5px 15px 34px;
    background: {{ $style->answer_background_color }} 10px center no-repeat url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAwMi8wOC8xNryOStIAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzbovLKMAAAA1UlEQVRIie2WMQ6DIBSGP5kcOYKrm72CN/Ck3sAr1I3VIzC62YFHg0ZtqGCXfgmDwfwfyIuPYlkWTtBALaPamZ8AI8MehRQHEg20QHO2gg0jMOzJ9iQ10AFlhMAzAz1uZ4eShwiu0gNP/6CCCb+DFHSSt5LohIJQpENJy3dncEYpuSixxVRRDA2gFcG3y0R9m6TKLKnU53eu85dES6bMjkmx+WNmwNwmsbiGk4MRsP7gB1zDScksue/qsrhGk5JeclclbBKKVi34Jz3ek/22spVdvne9AK5JQReEKTNHAAAAAElFTkSuQmCC);
    background-size: 16px;
    color: {{ $style->answer_text_color}}
}

#surve-modal form input[type="checkbox"].surve-radio-multi + label {
    display: block;
    margin: 8px 10px 0px;
    border-radius: 5px;
    padding: 15px 5px 15px 34px;
    background: {{ $style->answer_background_color }} 10px center no-repeat url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAwMi8wOC8xNryOStIAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzbovLKMAAABnElEQVRIibXW0XHaQBDG8f+eVnpFVMCEBkgHxB14aMC1uYNQAUkJrsB0YPnx0N5tHiwZCFEGCbwzmtHT/bSnT3sSd+erS++10OFwkO5WAAeoqsoB5B6ddEAdtFwWgdrN3iz7K9BUVeU3IyfAQxHkUWABvs9mW8u+A5qbtusv4ElgDcxAVkG1VjMs+24yMgxAB62DaqPW7sMXAMfq3sRo5ErgHfffObVby7zi7ldfMUaJMc7blDfZfevujV9W4zlvUxs3McZ5jFH0/AkFEacPXJ/zCR08W2bHaYQ7oC5KXRb4LJk1yfnM+S0AgMQYe+ChEHkEFnjeJ7OfyfkFNLcAAHoCPNEvIGFVaDnDWpLzErT8PhUA0KLUb10H5zkX+VFoKeK8SJDVVABA3fkYaZc1Q2QdhBVQTwUAiPEwb1Pe+HAk/1UXMf1f9BW8yWY7U0WDwNDXO6WDfru6iF4LjQagO7SuhCYBn8gV0GTgDBmA3oEF7vtu2I0GYOD47cdMUF2qUOfUvlk+jpkxwCByhEAEcT//Mbgbcs+adDKOrT+lYAJzqeBpswAAAABJRU5ErkJggg==);
    background-size: 16px;
    color: {{ $style->answer_text_color }}
}

#surve-modal form input[type="checkbox"].surve-radio-multi:checked + label, #surve-modal form input[type="radio"].surve-radio-multi:checked + label {
    background-color: {{ $style->button_background_color }};
    color: {{ $style->button_text_color }};
}

#surve-modal form input[type="radio"].surve-radio-rating + label {
    width: 24px;
    line-height: 40px;
    text-align: center;
    background-color: {{ $style->answer_background_color }};
    color: {{ $style->answer_text_color }};
}

#surve-modal form input[type="radio"].surve-radio-rating:checked + label {
    background-color: {{ $style->button_background_color }};
    color: {{ $style->button_text_color }};
}

#surve-modal form input[type="radio"].surve-radio-rating-img + label {
    width: 45px;
    display: inline-block;
    position: relative;
    outline: none;
}

#surve-modal form input[type="radio"].surve-radio-rating-img + label img {
    max-width: 45px;
    pointer-events: none;
    user-select: none;
    -webkit-user-select: none;
    -webkit-touch-callout: none;
}

#surve-modal form input[type="radio"].surve-radio-rating-img:checked + label {
    background-color: {{ $style->button_background_color }};
    color: {{ $style->button_text_color }};
    border-radius: 100%;
}

#surve-modal form .surve-rating {
    display: flex;
    justify-content: space-between;
    margin: 10px 10px 0;
}

#surve-modal form .surve-rating-labels {
    display: flex;
    justify-content: space-between;
    line-height: 1.2em;
    text-transform: none;
    font-weight: 500;
    font-size: 14px;
    margin: 5px 10px 0;
    padding: 0;
    color: {{ $style->survey_text_color }};
}

#surve-modal form .surve-rating-labels .surve-rating-label-start {
    display: flex;
}

#surve-modal form .surve-rating-labels .surve-rating-label-end {
    display: flex;
}

#surve-modal form p {
    text-align: left;
    margin-left: 35px;
    margin-top: 1px;
    padding-top: 1px;
    font-size: .9em
}

#surve-modal br {
    display: none;
}

#surve-modal .surve-modal-footer {
    position: absolute;
    bottom: 20px;
    text-align: center;
    width: 100%
}

#surve-modal .surve-modal-footer p {
    text-transform: none;
    cursor: pointer;
    display: inline;
    border-bottom: 1px solid #344a5f
}

#surve-modal form {
    text-align: left;
}

#surve-modal textarea {
    height: 85px !important;
    border-radius: 5px;
    padding: 10px;
    font-size: 16px;
    width: 280px !important;
    margin: 10px 10px 6px 10px !important;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border: 1px solid #ccc;
    background: #fff;
    color: #555;
}

#surve-modal .promo-link {
    padding: 0 0 5px;
    text-align: center;
    display: block!important;
    color: {{ $style->survey_text_color }};
    font-size: 11px!important;
    text-decoration: none!important;
}

#surve-modal .promo-link:hover {
    text-decoration: none!important;
}

.surve-arrow-down {
    width: 0;
    height: 0;
    margin-left: 30px;
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    border-top: 10px solid {{ $style->header_background_color }};
}

#surve-close {
    cursor: pointer;
    float: right;
    height:13px;
    width:14px;
    fill: #ffffff;
    background-size: 12px;
}

#surve-feedback {
    display: none;
    box-sizing: border-box;
    top: 50%;
    right:-1px;
    position: fixed;
    z-index: 2147483642;
    transform: rotate(-90deg);
    -webkit-transform: rotate(-90deg);
    -moz-transform: rotate(-90deg);
    -o-transform: rotate(-90deg);
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
}

#surve-feedback div {
    box-sizing:border-box;
    display:block;
    height: 36px;
    position: absolute;
    top: calc(100% - 36px);
    padding: 8px 15px;
    font: 14px "Open Sans", sans-serif !important;
    text-decoration: none;
    text-transform: none;
    cursor: pointer;
    white-space: nowrap;
    background-color: {{ $style->feedbackbutton_background_color }};
    color: {{ $style->feedbackbutton_text_color }};
    transform: translate(-50%);
}

#surve-feedback.surve-feedback-left {
    right: auto;
    left: -1px;
    transform: rotate(90deg);
    -webkit-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=1);
}

#surve-feedback.surve-feedback-bottom {
    top: auto;
    bottom: -1px;
    left: 40px;
    transform: none;
    -webkit-transform: none;
    -moz-transform: none;
    -o-transform: none;
    filter: none;
}

#surve-feedback.surve-feedback-bottom div {
    transform: none;
}

@media (max-width: 570px) {
    #surve-feedback div {
        font: 13px "Open Sans", sans-serif !important;
        padding: 7px 10px;
        height: 30px;
        top: calc(100% - 30px);
    }
}

@-webkit-keyframes fadein {
    0% {
        opacity: 0
    }
    100% {
        opacity: 1
    }
}

@-ms-keyframes fadein {
    0% {
        opacity: 0
    }
    100% {
        opacity: 1
    }
}

@keyframes fadein {
    0% {
        opacity: 0
    }
    100% {
        opacity: 1
    }
}

@-webkit-keyframes popin {
    0% {
        -webkit-transform: scale(0);
        transform: scale(0);
        opacity: 0
    }
    85% {
        -webkit-transform: scale(1.05);
        transform: scale(1.05);
        opacity: 1
    }
    100% {
        -webkit-transform: scale(1);
        transform: scale(1);
        opacity: 1
    }
}

@-ms-keyframes popin {
    0% {
        -ms-transform: scale(0);
        transform: scale(0);
        opacity: 0
    }
    85% {
        -ms-transform: scale(1.05);
        transform: scale(1.05);
        opacity: 1
    }
    100% {
        -ms-transform: scale(1);
        transform: scale(1);
        opacity: 1
    }
}

@keyframes popin {
    0% {
        -webkit-transform: scale(0);
        -ms-transform: scale(0);
        transform: scale(0);
        opacity: 0
    }
    85% {
        -webkit-transform: scale(1.05);
        -ms-transform: scale(1.05);
        transform: scale(1.05);
        opacity: 1
    }
    100% {
        -webkit-transform: scale(1);
        -ms-transform: scale(1);
        transform: scale(1);
        opacity: 1
    }
}

{{ $style->advanced_styling }}