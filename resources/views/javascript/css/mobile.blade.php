#surve-bar, #surve-bar * {
    box-sizing: border-box !important;
}

#surve-bar {
    font-family: "Open Sans", sans-serif;
    font-size: 18px;
    line-height: 1.2em;
    display: none;
    position: fixed;
    bottom: 0;
    right: 0;
    width: 100%;
    min-height: 55px;
    height: auto !important;
    z-index: 10000;
    background-color: {{ $style->survey_background_color }};
    color: {{ $style->survey_text_color }};
    padding: 15px 10px 5px;
    border-top: 1px solid {{ $style->survey_border_color }};
}

#surve-bar .surve-bar-inner {
    text-align: left;
    float: left;
    width: 100%;
    max-width: 400px;
}

#surve-bar .surve-bar-title {
    display: block;
}

#surve-bar .surve-bar-subtitle {
    font-size: 12px;
}

#surve-bar img {
    max-height: 30px;
}

.surve-noborder {
    border: none !important;
}

.surve-bar-btn {
    float: left;
    vertical-align: middle;
    margin: 8px 15px 5px 0;
    border: thin solid #fff;
    border-radius: 3px;
    padding: 6px 14px;
    cursor: pointer;
    background-color: {{ $style->button_background_color }};
    color: {{ $style->button_text_color }};
}

.surve-bar-btn:hover {
    text-decoration: none;
}

@media(max-width:570px){
    .surve-buttons {
        margin-top: 4px;
        width: 100%;
        clear: both;
    }
}