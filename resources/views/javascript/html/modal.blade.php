<div id="surve-modal-wrapper">
    <div id="surve-modal">
        <div class="surve-modal">
            <div class="surve-modal-title">
                @if ($style->header_type == 'logo')
                    <div style="display: inline-block; margin-top: 6px;">
                        <img src="//static.{{ config('app.domain') }}/{{ $style->header_logo }}" style="margin-top: -9px; max-width: 240px; max-height: 50px;">
                    </div>
                @else
                    <div style="display: inline-block; margin-bottom: 6px;">
                        {{ $style->header_text }}
                    </div>
                @endif

                <div id="surve-close">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 348.3 348.3"><path d="M336.6 68.6L231 174.2l105.5 105.5c15.7 15.7 15.7 41.1 0 56.9 -7.8 7.8-18.1 11.8-28.4 11.8 -10.3 0-20.6-3.9-28.4-11.8L174.2 231 68.6 336.6c-7.8 7.8-18.1 11.8-28.4 11.8 -10.3 0-20.6-3.9-28.4-11.8 -15.7-15.7-15.7-41.1 0-56.9l105.5-105.5L11.8 68.6c-15.7-15.7-15.7-41.1 0-56.8 15.7-15.7 41.1-15.7 56.8 0l105.6 105.6L279.7 11.8c15.7-15.7 41.1-15.7 56.8 0C352.3 27.5 352.3 52.9 336.6 68.6z" fill="{{ $style->header_text_color }}"/></svg>
                </div>
            </div>

            <div class="surve-modal-body">
                <h3 id="surve-question"></h3>
                <p id="surve-intro"></p>
                <div style="clear: both"></div>
                <form id="surve-form">
                    <div id="surve-form-content">

                    </div>
                </form>
            </div>
            <a href="https://www.surve.nl" class="promo-link" style="display: block!important;">Powered by Surve.nl</a>
        </div>
    </div>
</div>