<div id="surve-bar">
    <div class="surve-bar-inner">
        <span class="surve-bar-title" id="surve-bar-title"></span>
        <span class="surve-bar-subtitle" id="surve-bar-subtitle"></span>
    </div>

    <div class="surve-buttons">
        <a class="surve-bar-btn" id="surve-bar-yes"></a>
        <a class="surve-bar-btn" id="surve-bar-no"></a>
    </div>
</div>