@extends('emails.base')

@section('content')
    <span class="preheader" style="color: transparent;display: none;height: 0;max-height: 0;max-width: 0;opacity: 0;overflow: hidden;mso-hide: all;visibility: hidden;width: 0;font-size: 15px !important;">{{ __('Uw aanmelding bij Surve.nl is gelukt, welkom!') }}</span>
    <table class="main" style="border-collapse: separate;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;background: #ffffff;border-radius: 0 !important;border-left-width: 0 !important;border-right-width: 0 !important;">
        <tr>
            <td class="wrapper" style="font-family: sans-serif;font-size: 15px !important;vertical-align: top;box-sizing: border-box;padding: 10px !important;">
                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;">
                    <tr>
                        <td style="font-family: sans-serif;font-size: 15px !important;vertical-align: top;">
                            <h1 style="font-size: 30px; font-weight: 300; text-align: center; text-transform: capitalize;">{{ __('Welkom bij Surve') }}</h1>
                            <p style="font-family: sans-serif;line-height:200%;font-size: 15px !important;font-weight: bold;margin: 0;margin-bottom: 15px;">{{ __('Beste :name,', ['name' => $user->full_name]) }}</p>
                            <p style="font-family: sans-serif;line-height:200%;font-size: 15px !important;font-weight: normal;margin: 0;margin-bottom: 15px;">{{ __('Uw account bij Surve is aangemaakt. U kunt gelijk beginnen met het toevoegen van uw eerste website.') }}</p>
                            <p style="font-family: sans-serif;line-height:200%;font-size: 15px !important;font-weight: normal;margin: 0;margin-bottom: 15px;">{{ __('Uw trial van 14 dagen start automatisch zodra u uw website toegevoegd heeft.') }}</p>
                            <p style="font-family: sans-serif;line-height:200%;font-size: 15px !important;font-weight: normal;margin: 0;margin-bottom: 15px;">{!! __('Mocht u een vraag hebben of kunnen wij u ergens bij helpen dan kunt u <a href=":url">contact</a> met ons opnemen.', ['url' => config('app.url').route('contact')]) !!}</p>
                            <p style="font-family: sans-serif;line-height:200%;font-size: 15px !important;font-weight: bold;margin: 0;margin-bottom: 15px;">{{ __('Met vriendelijke groet,') }}<br /><br />{{ __('Matthijs Huisman') }}<br />{{ __('Surve.nl') }}</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection