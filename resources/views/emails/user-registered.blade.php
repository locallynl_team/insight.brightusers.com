@extends('emails.base')

@section('content')
    <span class="preheader" style="color: transparent;display: none;height: 0;max-height: 0;max-width: 0;opacity: 0;overflow: hidden;mso-hide: all;visibility: hidden;width: 0;font-size: 15px !important;">{{ __(':name heeft zich aangemeld op Surve.nl', ['name' => $user->full_name]) }}</span>
    <table class="main" style="border-collapse: separate;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;background: #ffffff;border-radius: 0 !important;border-left-width: 0 !important;border-right-width: 0 !important;">
        <tr>
            <td class="wrapper" style="font-family: sans-serif;font-size: 15px !important;vertical-align: top;box-sizing: border-box;padding: 10px !important;">
                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;">
                    <tr>
                        <td style="font-family: sans-serif;font-size: 15px !important;vertical-align: top;">
                            <h1 style="font-size: 30px; font-weight: 300; text-align: center; text-transform: capitalize;">{{ __('Nieuwe aanmelding') }}</h1>
                            <p style="font-family: sans-serif;line-height:200%;font-size: 15px !important;font-weight: bold;margin: 0;margin-bottom: 15px;">{{ __('Beste beheerder,') }}</p>
                            <p style="font-family: sans-serif;line-height:200%;font-size: 15px !important;font-weight: normal;margin: 0;margin-bottom: 15px;">{{ __('Er is een nieuwe gebruiker (:name) geregistreerd op Surve.nl.', ['name' => $user->full_name]) }}</p>
                            <p style="font-family: sans-serif;line-height:200%;font-size: 15px !important;font-weight: bold;margin: 0;margin-bottom: 15px;">{{ __('Met vriendelijke groet,') }}<br /><br />{{ __('Matthijs Huisman') }}<br />{{ __('Surve.nl') }}</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection