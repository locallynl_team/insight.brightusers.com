@extends('emails.base')

@section('content')
    <span class="preheader" style="color: transparent;display: none;height: 0;max-height: 0;max-width: 0;opacity: 0;overflow: hidden;mso-hide: all;visibility: hidden;width: 0;font-size: 15px !important;">{{ __('Uw proefperiode van Surve verloopt bijna') }}</span>
    <table class="main" style="border-collapse: separate;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;background: #ffffff;border-radius: 0 !important;border-left-width: 0 !important;border-right-width: 0 !important;">
        <tr>
            <td class="wrapper" style="font-family: sans-serif;font-size: 15px !important;vertical-align: top;box-sizing: border-box;padding: 10px !important;">
                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;">
                    <tr>
                        <td style="font-family: sans-serif;font-size: 15px !important;vertical-align: top;">
                            <h1 style="font-size: 30px; font-weight: 300; text-align: center; text-transform: capitalize;">{{ __('Uw proefperiode') }}</h1>
                            <p style="font-family: sans-serif;line-height:200%;font-size: 15px !important;font-weight: bold;margin: 0;margin-bottom: 15px;">{{ __('Beste :name,', ['name' => $subscription->site->user->full_name]) }}</p>
                            <p style="font-family: sans-serif;line-height:200%;font-size: 15px !important;font-weight: normal;margin: 0;margin-bottom: 15px;">{{ trans_choice('Uw proefperiode van Surve voor uw website \':domain\' verloopt morgen.|Uw proefperiode van Surve voor uw website \':domain\' verloopt over :days dagen.', $daysLeft, ['domain' => $subscription->site->domain, 'days' => $daysLeft]) }}</p>
                            <p style="font-family: sans-serif;line-height:200%;font-size: 15px !important;font-weight: normal;margin: 0;margin-bottom: 15px;">{!! __('<a href=":url">Log in bij Surve om een pakket te selecteren.</a>', ['url' => config('app.app_url')]) !!}</p>
                            <p style="font-family: sans-serif;line-height:200%;font-size: 15px !important;font-weight: bold;margin: 0;margin-bottom: 15px;">{{ __('Met vriendelijke groet,') }}<br /><br />{{ __('Matthijs Huisman') }}<br />{{ __('Surve.nl') }}</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection