<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ $invoice->name }}</title>
    <link type="text/css" rel="stylesheet" href="{{ config('app.app_url') }}/assets/backend/css/app.css">
    <style>
        body {
            background: #fff;
            color: #555;
            font-family: "Roboto", sans-serif!important;
        }

        * {
            font-family: "Roboto", sans-serif!important;
            color: #555!important;
            font-size: 14px!important;
        }

        strong {
            font-weight: 500;
        }

        th {
            font-weight: bold!important;
        }

        h3 {
            font-size: 22px!important;
            font-weight: bold;
        }
    </style>
</head>
<body>
<div style="position: absolute; right: 95px; top: 60px; line-height: 180%;">
    <strong>{{ $invoice->business_details->get('company_name') }}</strong><br />
    {{ $invoice->business_details->get('address') }}<br />
    {{ $invoice->business_details->get('postal_code') }} {{ $invoice->business_details->get('city') }}<br />
    <br />
    {{ $invoice->business_details->get('email') }}<br />
    {{ $invoice->business_details->get('phone') }}<br />
    <br />
    <strong>{{ __('KVK') }}:</strong> {{ $invoice->business_details->get('coc') }}<br />
    <strong>{{ __('BTW') }}:</strong> {{ $invoice->business_details->get('tax') }}<br />
    <strong>{{ __('Bank') }}:</strong> {{ $invoice->business_details->get('bank') }}
</div>
<table style="margin: 60px; width: 95%;">
    <tr>
        <td colspan="2" style="padding: 0 20px 20px 0;"><img height="{{ $invoice->logo_height }}" src="{{ config('app.app_url') }}/{{ $invoice->logo }}"></td>
    </tr>
    <tr>
        <td style="width: 50%; padding: 20px 20px 20px 0;">
            <p style="line-height: 180%;">
                {{ $invoice->customer_details->get('company_name') }}<br />
                {{ __('t.a.v.') }} {{ $invoice->customer_details->get('name') }}<br />
                {{ $invoice->customer_details->get('address') }}<br />
                {{ $invoice->customer_details->get('postal_code') }} {{ $invoice->customer_details->get('city') }}
            </p>
        </td>
        <td></td>
    </tr>
    <tr>
        <td style="padding: 20px 20px 20px 0;">
            <h3 style="margin-top: 40px;">{{ $invoice->name }}</h3>
        </td>
        <td style="padding: 20px 0 20px 100px;">
            <p style="line-height: 180%; margin-top: 40px;">
                <strong>{{ __('Datum') }}:</strong> {{ $invoice->date->formatLocalized('%d %B %Y') }}<br />
                <strong>{{ __('Verloopdatum') }}:</strong> {{ $invoice->date->copy()->addDays(30)->formatLocalized('%d %B %Y') }}
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="table table-condensed" style="margin-top: 30px;">
                <tr>
                    <td style="width: 10%; border-top: 0; text-transform: uppercase; font-weight: 700;">{{ __('Aantal') }}</td>
                    <td style="border-top: 0; text-transform: uppercase; font-weight: 700;">{{ __('Product') }}</td>
                    <td style="width: 20%; border-top: 0; text-transform: uppercase; font-weight: 700;">{{ __('Prijs') }}</td>
                </tr>
                @foreach ($invoice->items as $item)
                    <tr>
                        <td>{{ $item->get('ammount') }}</td>
                        <td>{!! nl2br($item->get('name')) !!}</td>
                        <td>
                            &euro;{{ price(str_replace(",", "", $item->get('price'))) }}
                        </td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <table style="width: 100%;" class="table table-condensed">
                <tbody>
                <tr>
                    <td style="border-top: 0;"><strong>{{ __('Subtotaal') }}:</strong></td>
                    <td style="width: 40%; border-top: 0;">&euro;{{ price(str_replace(",", "", $invoice->subTotalPriceFormatted())) }}</td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            {{ __('BTW') }}: {{ $invoice->tax_type == 'percentage' ? '(' . $invoice->tax . '%)' : '' }}
                        </strong>
                    </td>
                    <td>&euro;{{ price(str_replace(",", "", $invoice->taxPriceFormatted())) }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Totaal:') }}</strong></td>
                    <td><strong>&euro;{{ price(str_replace(",", "", $invoice->totalPriceFormatted())) }}</strong></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
   <tr>
       <td colspan="2">
           @if ($invoice->footnote)
               <br /><br />
               <br /><br />
               <br /><br />
               <div class="well" style="text-align: center; line-height: 180%;">
                   {{ $invoice->footnote }}
               </div>
           @endif
       </td>
   </tr>
</table>
</body>
</html>
