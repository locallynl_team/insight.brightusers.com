@extends('backend.layouts.app')

@section('content')
<div class="container-fluid pt-25">
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <form method="post" action="{{ route($settings['url_key'].'.responses', $survey->id) }}">
                    @csrf
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body" style="padding: 8px 0;">
                                    <label class="control-label col-md-4" style="line-height: 40px;" for="period">{{ __('Periode') }}:</label>
                                    <div class="col-md-8">
                                        <span class="input-group">
                                            <div id="reportrange" style="color: #212121; background: #fff; cursor: pointer; line-height: 42px; padding: 0px 12px; height: 42px; border: 1px solid rgba(33, 33, 33, 0.12); width: 100%">
                                                <i class="fa fa-calendar mr-5"></i>&nbsp;
                                                <span></span> <i class="fa fa-caret-down ml-5"></i>

                                                <input type="hidden" name="period_start" />
                                                <input type="hidden" name="period_end" />
                                            </div>
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary ml-10">{{ __('Filter') }}</button>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body" style="padding: 8px 0;">
                                    <label class="control-label col-md-4" style="line-height: 40px;" for="filter_url">{{ __('URL') }}:</label>
                                    <div class="col-md-8">
                                        <span class="input-group">
                                            <select class="form-control" id="filter_url" name="filter_url" onchange="$('#export_filter_url').val()">
                                                <option value="">{{ __('alles tonen') }}</option>
                                                @foreach ($urls as $url)
                                                    <option value="{{ $url->url }}" @if ($selectedUrl == $url->url) selected @endif>{{ $url->url }}</option>
                                                @endforeach
                                            </select>
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary ml-10">{{ __('Filter') }}</button>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <form method="post" action="{{ route($settings['url_key'].'.responses.export', $survey->id) }}">
                    @csrf

                    <input type="hidden" name="period_start" />
                    <input type="hidden" name="period_end" />
                    <input type="hidden" name="filter_url" id="export_filter_url" value="{{ $selectedUrl }}" />

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body" style="padding: 8px 0;">
                                    <label class="control-label col-md-4" style="line-height: 40px;" for="type">{{ __('Selectie exporteren') }}:</label>
                                    <div class="col-md-8">
                                        <span class="input-group">
                                            <select class="form-control" id="type" name="type">
                                                @foreach ($exportTypes as $type => $text)
                                                    <option value="{{ $type }}">{{ $text }}</option>
                                                @endforeach
                                            </select>
                                            <span class="input-group-btn">
                                                <input type="submit" value="{{ __('Exporteren') }}" name="export" class="btn btn-primary ml-10" />
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box">
                                    <div class="container-fluid">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ $totalResponses }}</span></span>
                                            <span class="weight-500 uppercase-font block font-13">{{ __('reacties') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box">
                                    <div class="container-fluid">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ $survey->getOpens() }}</span></span>
                                            <span class="weight-500 uppercase-font block font-13">{{ __('keer geopend') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box">
                                    <div class="container-fluid">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ $survey->days_active }}</span></span>
                                            <span class="weight-500 uppercase-font block font-13">{{ __('dagen actief') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box">
                                    <div class="container-fluid">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="{{ ($survey->status === true ? 'txt-primary' : 'txt-danger') }} block counter">{{ ($survey->status === true ? __('actief') : __('niet actief')) }}</span>
                                            <span class="weight-500 uppercase-font block font-13">{{ __('huidige status') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h5 class="mb-20 pull-left">{{ __('Reacties op :type \':name\'', ['type' => $settings['name'], 'name' => $survey->title]) }}</h5>
            <span class="pull-right">{{ __('uw selectie bevat:') }} {{ $responses->total() }} {{ ($responses->total() === 1 ? __('reactie') : __('reacties')) }}</span>
            <div class="clearfix"></div>
            <div class="table-wrap">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th style="width: 20px; text-align: center;"><i class="fa fa-desktop"></i></th>
                            <th>{{ __('Datum') }}</th>
                            <th>{{ __('Pagina') }}</th>
                            @foreach ($questions as $question)
                                <th>{{ $question->question }}</th>
                            @endforeach
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($responses as $response)
                            <tr>
                                <td style="text-align: center;">
                                    <i class="fa fa-{{ ($response->platform === 'desktop' ? 'desktop' : $response->platform.'-alt') }}" data-toggle="tooltip" data-placement="right" data-html="true" title="{!! __('<strong>:device</strong><br /><strong>Browser:</strong> :browser<br /><strong>OS:</strong> :os<br /><strong>Scherm:</strong> :resolution', ['device' => ($response->platform === 'mobile' ? 'Telefoon' : ($response->platform === 'desktop' ? 'Computer' : 'Tablet')), 'browser' => htmlspecialchars($response->browser), 'os' => htmlspecialchars($response->os), 'resolution' => htmlspecialchars($response->resolution)]) !!}"></i>
                                </td>
                                <td>{{ $response->created_at->formatLocalized('%d %B %Y om %H:%M') }}</td>
                                <td>{{ $response->url }}</td>
                                @foreach ($questions as $responseQuestion)
                                    <td>
                                        @forelse($response->items->where('question_id', $responseQuestion->id) as $answer)
                                            @if ($answer->answer === null)
                                                {{ ($answer->answer_text ?? '-') }}
                                            @else
                                                {{ $answer->answer->answer }}
                                            @endif

                                            @if ($loop->last === false)
                                                <br />
                                            @endif
                                        @empty
                                            -
                                        @endforelse
                                    </td>
                                @endforeach
                                <td>
                                    <span class="pull-right">
                                        <a class="action action-button delete" href="{{ route($settings['url_key'].'.response.delete', [$survey->id, $response->id]) }}"><i class="fa fa-trash-alt"></i></a>
                                    </span>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="{{ (3 + count($survey->questions)) }}">{{ __('Er zijn nog geen reacties op deze :single.', ['single' => strtolower($settings['name'])]) }}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            {{ $responses->links() }}
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="/assets/backend/js/moment-with-locales.min.js"></script>
    <script src="/assets/backend/js/daterangepicker.min.js"></script>
    <script>
        $(function() {
            $('#filter_url').on('change', function() {
                $('#export_filter_url').val(this.value);
            });
        });


        $(function() {
            @if ($periodStart !== null && $periodEnd !== null)
                var start = moment('{{ $periodStart }}');
                var end = moment('{{ $periodEnd }}');
            @else
                var start = moment().subtract(29, 'days');
                var end = moment();
            @endif

            function cb(start, end) {
                $('#reportrange span').html(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));

                $('input[name=period_start]').each(function() {
                    $(this).val(start.format('YYYY-MM-DD'));
                });
                $('input[name=period_end]').each(function() {
                    $(this).val(end.format('YYYY-MM-DD'));
                });
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                autoApply: true,
                ranges: {
                    'Vandaag': [moment(), moment()],
                    'Gisteren': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Laatste 7 dagen': [moment().subtract(6, 'days'), moment()],
                    'Laatste 30 dagen': [moment().subtract(29, 'days'), moment()],
                    'Deze maand': [moment().startOf('month'), moment().endOf('month')],
                    'Vorige maand': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                locale: {
                    applyLabel: "Toepassen",
                    cancelLabel: "Terug",
                    customRangeLabel: "Aangepaste periode",
                    daysOfWeek: [
                        "Zo",
                        "Ma",
                        "Di",
                        "Wo",
                        "Do",
                        "Vr",
                        "Za"
                    ],
                    monthNames: [
                        "Januari",
                        "Februari",
                        "Maart",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Augustus",
                        "September",
                        "Oktober",
                        "November",
                        "December"
                    ]
                }
            }, cb);

            cb(start, end);
        });
    </script>
@endsection