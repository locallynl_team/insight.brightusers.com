@extends('backend.layouts.app')

@section('content')
<div class="container-fluid pt-25">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <h6 class="panel-title txt-dark pull-left">
                        {{ __('Export met reacties van :type \':name\'', ['type' => $settings['name'], 'name' => $survey->title]) }}
                    </h6>
                    <span class="pull-right">{{ __('uw selectie bevat:') }} {{ $responseCount }} {{ ($responseCount === 1 ? __('reactie') : __('reacties')) }}</span>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper">
                    <div class="panel-body">
                        <p>
                            {{ __('Uw export wordt gegenereerd, zodra deze gereed is kunt u deze hier downloaden.') }}
                        </p>
                        <a href="javascript:void();" class="btn btn-default mt-25" id="download_url"><span class="fa fa-spinner fa-spin mr-10"></span> {{ __('Even geduld a.u.b.') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(function() {
            function checkExport() {
                $.ajax({
                    url: '{{ route($settings['url_key'].'.responses.export.status', [$survey->id, $export->id]) }}'
                }).done(function(data) {
                    if (data === '1') {
                        $('#download_url').removeClass('btn-default').addClass('btn-primary').attr('href', '{{ route($settings['url_key'].'.responses.export.download', [$survey->id, $export->id]) }}').html('<span class="fa fa-download mr-10"></span>  {{ __('Download export') }}');
                    } else {
                        setTimeout(checkExport, 1000);
                    }
                });
            }

            setTimeout(checkExport, 1000);
        });
    </script>
@endsection