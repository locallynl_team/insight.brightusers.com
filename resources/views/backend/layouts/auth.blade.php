<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>{{ config('app.name') }} - App</title>
    <link rel="apple-touch-icon" sizes="180x180" href="{{ static_url() }}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ static_url() }}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ static_url() }}/favicon-16x16.png">
    <link rel="manifest" href="{{ static_url() }}/site.webmanifest">
    <link rel="mask-icon" href="{{ static_url() }}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="apple-mobile-web-app-title" content="Surve">
    <meta name="application-name" content="Surve">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <link href="{{ static_url() }}/assets/backend/css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="wrapper pa-0">
    <header class="sp-header">
        <div class="sp-logo-wrap pull-left">
            <a href="{{ route('home') }}">
                <img class="brand-img mr-10" style="max-height: 40px;" src="{{ static_url() }}/assets/backend/images/logo-surve-white.png" alt="{{ config('app.name') }}" />
            </a>
        </div>
        @if(Route::is('register'))
            <div class="form-group mb-0 pull-right">
                <span class="inline-block pr-10 txt-light">{{ __('Heeft u al een account?') }}</span>
                <a class="inline-block btn btn-primary" href="{{ route('login') }}">{{ __('Inloggen') }}</a>
            </div>
        @else
            <div class="form-group mb-0 pull-right">
                <span class="inline-block pr-10 txt-light">{{ __('Heeft u nog geen account?') }}</span>
                <a class="inline-block btn btn-primary" href="{{ route('register') }}">{{ __('Meld u aan') }}</a>
            </div>
        @endif
        <div class="clearfix"></div>
    </header>

@yield('content')
</div>
<script src="{{ static_url() }}/assets/backend/js/auth.js"></script>
</body>
</html>
