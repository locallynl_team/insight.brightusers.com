<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>{{ config('app.name') }} - App</title>
    <link rel="apple-touch-icon" sizes="180x180" href="{{ static_url() }}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ static_url() }}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ static_url() }}/favicon-16x16.png">
    <link rel="manifest" href="{{ static_url() }}/site.webmanifest">
    <link rel="mask-icon" href="{{ static_url() }}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="apple-mobile-web-app-title" content="Surve">
    <meta name="application-name" content="Surve">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="{{ static_url() }}{{ mix('/assets/backend/css/app.css') }}" rel="stylesheet" type="text/css">
    @yield('css')
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116402224-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-116402224-2');
    </script>
</head>

<body>
<div class="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="mobile-only-brand pull-left">
            <div class="nav-header pull-left">
                <div class="logo-wrap">
                    <a href="{{ route('dashboard') }}">
                        <img class="brand-img" src="{{ static_url() }}/assets/backend/images/logo-surve-white.png" alt="{{ config('app.name') }}" />
                    </a>
                </div>
            </div>
            <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="fa fa-bars"></i></a>
            <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="fa fa-ellipsis-h"></i></a>
        </div>

        <div id="mobile_only_nav" class="mobile-only-nav pull-right">
            <ul class="nav navbar-right top-nav pull-right">
                <li>
                    <a href="{{ route('helpdesk') }}" class="txt-light"><i class="fa fa-question-circle mr-5 mb-icon"></i> <span>{{ __('Helpdesk') }}</span></a>
                </li>
                <li>
                    <a href="{{ route('roadmap') }}" class="txt-light"><i class="fa fa-calendar-alt mr-5 mb-icon"></i> <span>{{ __('Roadmap') }}</span></a>
                </li>
                @if (Auth::user()->isAdmin())
                    <li class="dropdown auth-drp">
                        <a href="#" class="dropdown-toggle txt-light" data-toggle="dropdown"><i class="fa fa-cogs mr-5 mb-icon"></i> <span class="hidden-xs">{{ __('Beheer') }}</span> <i class="fa fa-caret-down ml-5 mr-5"></i></a>
                        <ul class="dropdown-menu user-auth-dropdown">
                            <li>
                                <a href="{{ route('admin.user.index') }}"><i class="fa fa-users mr-15"></i> <span>{{ __('Gebruikers') }}</span></a>
                            </li>
                            <li>
                                <a href="{{ route('admin.site.index') }}"><i class="fa fa-home mr-15"></i><span>{{ __('Websites') }}</span></a>
                            </li>
                            <li>
                                <a href="{{ route('admin.statistics') }}"><i class="fa fa-chart-line mr-15"></i> <span>{{ __('Statistieken') }}</span></a>
                            </li>
                            <li>
                                <a href="{{ route('admin.invoice.index') }}"><i class="far fa-money-bill-alt mr-15"></i> <span>{{ __('Facturen') }}</span>
                                    @if ($open_invoices > 0)<span class="label label-danger ml-15">{{ $open_invoices }}</span> @endif
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.package.index') }}"><i class="fa fa-archive mr-15"></i><span>{{ __('Pakketten') }}</span></a>
                            </li>
                            <li>
                                <a href="{{ route('admin.roadmap.index') }}"><i class="fa fa-calendar-alt mr-15"></i><span>{{ __('Roadmap') }}</span></a>
                            </li>
                        </ul>
                    </li>
                @endif
                <li class="dropdown auth-drp">
                    <a href="#" class="dropdown-toggle txt-light" data-toggle="dropdown"><img src="{{ Avatar::create(Auth::user()->full_name)->toBase64() }}" alt="{{ Auth::user()->full_name }}" class="user-auth-img img-circle mr-5" style="width: 30px; height: 30px;" /> <span class="hidden-xs">{{ Auth::user()->full_name }}</span> <i class="fa fa-caret-down ml-5 mr-5"></i> </a>
                    <ul class="dropdown-menu user-auth-dropdown">
                        <li class="full-name">
                            {{ __('Account') }}
                        </li>
                        <li>
                            <a href="{{ route('site.index') }}"><i class="fa fa-home" style="font-size: 14px;"></i> <span>{{ __('Websites') }}</span></a>
                        </li>
                        <li>
                            <a href="{{ route('account.ip-filters') }}"><i class="fa fa-filter" style="font-size: 14px;"></i> <span>{{ __('IP-filters') }}</span></a>
                        </li>
                        <li class="divider"></li>
                        <li class="full-name">
                            {{ __('Facturatie') }}
                        </li>
                        <li>
                            <a href="{{ route('account.payment-method') }}"><i class="fa fa-credit-card" style="font-size: 14px;"></i><span>{{ __('Betaalmethode') }}</span></a>
                        </li>
                        <li>
                            <a href="{{ route('account.invoices') }}"><i class="fa fa-file-alt" style="font-size: 14px; position: relative; left: 2px;"></i><span>{{ __('Facturen') }}</span></a>
                        </li>
                        <li class="divider"></li>
                        <li class="full-name">
                            {{ Auth::user()->full_name }}
                        </li>
                        <li>
                            <a href="{{ route('account.index') }}"><i class="fa fa-user" style="font-size: 14px;"></i><span>{{ __('Mijn gegevens') }}</span></a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}"><i class="fa fa-power-off" style="font-size: 14px;"></i><span>{{ __('Uitloggen') }}</span></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

    <div class="fixed-sidebar-left">
        <ul class="nav navbar-nav side-nav nicescroll-bar" style="display: flex; flex-direction: column; aling-items: flex-start;">
            @if (count($user_sites) > 0)
                <li class="navigation-header current-site">
                    <img src="{{ Avatar::create(str_replace('www.', '', $current_site->domain))->toBase64() }}" alt="{{ $current_site->domain  }}" style="width: 25px; display: inline-block; position: relative; top: 8px; left: 10px;" class="img-circle mr-10" /> <span class="inline-block">{{ $current_site->domain }}</span>
                </li>
            @endif
            <li>
                <a href="{{ route('dashboard') }}" @if (Route::currentRouteName() === 'dashboard') class="active-page" @endif><div class="pull-left"><i class="fa fa-home mr-20"></i><span class="right-nav-text">{{ __('Dashboard') }} </span></div><div class="clearfix"></div></a>
            </li>
            <li>
                <a href="{{ route('feedbackButton.index') }}" @if (strpos(Route::currentRouteName(), 'feedbackButton.') !== false) class="active-page" @endif><div class="pull-left"><i class="fa fa-comment mr-20"></i><span class="right-nav-text">{{ __('Feedbackbuttons') }} </span></div><div class="pull-right"></div><div class="clearfix"></div></a>
            </li>
            <li>
                <a href="{{ route('survey.index') }}" @if (strpos(Route::currentRouteName(), 'survey.') !== false) class="active-page" @endif><div class="pull-left"><i class="fa fa-question mr-20"></i><span class="right-nav-text">{{ __('Surveys') }} </span></div><div class="pull-right"></div><div class="clearfix"></div></a>
            </li>
            <li>
                <a href="{{ route('poll.index') }}" @if (strpos(Route::currentRouteName(), 'poll.') !== false) class="active-page" @endif><div class="pull-left"><i class="fa fa-list-ul mr-20"></i><span class="right-nav-text">{{ __('Polls') }} </span></div><div class="pull-right"><span class="label label-danger">bèta</span></div><div class="clearfix"></div></a>
            </li>
            <li>
                <a href="{{ route('heatmap.index') }}" @if (strpos(Route::currentRouteName(), 'heatmap.') !== false) class="active-page" @endif><div class="pull-left"><i class="fa fa-fire mr-20"></i><span class="right-nav-text">{{ __('Heatmaps') }} </span></div><div class="pull-right"></div><div class="clearfix"></div></a>
            </li>
            <li>
                <a href="{{ route('form.index') }}" @if (strpos(Route::currentRouteName(), 'form.') !== false) class="active-page" @endif><div class="pull-left"><i class="fa fa-clipboard-list mr-20"></i><span class="right-nav-text">{{ __('Formulieren') }} </span></div><div class="pull-right"></div><div class="clearfix"></div></a>
            </li>
            <li>
                <a href="{{ route('style.index') }}" @if (strpos(Route::currentRouteName(), 'style.') !== false) class="active-page" @endif><div class="pull-left"><i class="fa fa-pencil-alt mr-20"></i><span class="right-nav-text">{{ __('Opmaak') }} </span></div><div class="pull-right"></div><div class="clearfix"></div></a>
            </li>
            @if (count($user_sites) > 0)
                <li>
                    <a href="{{ route('site.edit', $current_site->id) }}" @if (Route::currentRouteName() === 'site.edit') class="active-page" @endif><div class="pull-left"><i class="fa fa-edit mr-20"></i><span class="right-nav-text">{{ __('Instellingen') }} </span></div><div class="pull-right"></div><div class="clearfix"></div></a>
                </li>
                @if ($current_site->activeSubscription()->first() <> null && $current_site->activeSubscription()->first()->trial === true && $current_site->activeSubscription()->first()->daysLeft < 7)
                    <li>
                        <a href="{{ route('subscription.manage', $current_site->id) }}" class="trial">
                            {{ trans_choice('Uw trial verloopt over :days dag|Uw trial verloopt over :days dagen', $current_site->activeSubscription()->first()->daysLeft, ['days' => $current_site->activeSubscription()->first()->daysLeft]) }}
                            <i class="fa fa-info-circle" style="font-size: 16px; margin-top: 2px; left: 15px;"></i>
                        </a>
                    </li>
                @endif
                <li style="margin-top: auto;" class="navigation-header">
                    <hr class="hr-dark-color mb-5" />
                    <span>{{ __('Switch naar website') }}</span>
                    <i class="fa fa-exchange-alt"></i>
                    <ul>
                        @foreach ($user_sites as $site)
                            <li class="list-card @if ($site->id === $current_site->id) active @endif">
                                <a href="{{ route('site.switch', $site->id) }}">
                                    <div class="pull-left">
                                        <img src="{{ Avatar::create(str_replace('www.', '', $site->domain))->toBase64() }}" alt="{{ $site->domain  }}" style="width: 25px;" class="img-circle mr-15" />
                                        <span class="right-nav-text" style="position: relative; top: -8px;">
                                            {{ $site->domain }}
                                        </span>
                                    </div>
                                    <div class="pull-right"></div><div class="clearfix"></div>
                                </a>
                            </li>
                        @endforeach
                        <li><a href="{{ route('site.create') }}" class="btn btn-primary"><div class="pull-left"><i class="fa fa-plus mr-20" style="color: #fff;"></i><span class="right-nav-text" style="color: #fff;">{{ __('Website toevoegen') }}</span></div><div class="clearfix"></div></a></li>
                    </ul>
                </li>
            @else
                    <li><a href="{{ route('site.create') }}" class="btn btn-primary"><div class="pull-left"><i class="fa fa-plus mr-20" style="color: #fff;"></i><span class="right-nav-text" style="color: #fff;">{{ __('Website toevoegen') }}</span></div><div class="clearfix"></div></a></li>
            @endif
        </ul>
    </div>

    {{--@include('backend.right-sidebar')--}}

    <div class="page-wrapper">
        @if (!isset($hideBreadcrumbs))
            @include('backend.parts.breadcrumbs')
        @endif

        @yield('content')

        <footer class="footer container-fluid pl-30 pr-30">
            <div class="row">
                <div class="col-sm-12">
                    <p>&copy; 2014-{{ date("Y") }} {{ config('app.name') }} - {{ __('Alle rechten voorbehouden') }}</p>
                </div>
            </div>
        </footer>
    </div>
</div>

<div id="info-modal" class="modal fade"><div class="modal-dialog"><div class="modal-content"></div></div></div>

<script src="{{ static_url() }}/assets/backend/js/jquery.min.js"></script>
<script src="{{ static_url() }}/assets/backend/js/jquery-ui.min.js"></script>
<script src="{{ static_url() }}/assets/backend/js/bootstrap.min.js"></script>
<script src="{{ static_url() }}/assets/backend/js/jquery.dataTables.min.js"></script>
<script src="{{ static_url() }}/assets/backend/js/jquery.slimscroll.js"></script>
<script src="{{ static_url() }}/assets/backend/js/jquery.waypoints.min.js"></script>
<script src="{{ static_url() }}/assets/backend/js/jquery.counterup.min.js"></script>
<script src="{{ static_url() }}/assets/backend/js/switchery.min.js"></script>
<script src="{{ static_url() }}/assets/backend/js/jquery.toast.min.js"></script>
<script src="{{ static_url() }}/assets/backend/js/init.js"></script>

@if (Request::session()->exists('flash-message'))
    <script>
        $(window).on("load", function() {
            $.toast({
                heading: '{{ session('flash-message-title') }}',
                text: '{{ session('flash-message') }}',
                position: 'top-right',
                loaderBg: '{{ (session('flash-message-type') == 'success' ? '#27ae5f' : '#f33923') }}',
                icon: '{{ (session('flash-message-type') == 'success' ? 'success' : 'error') }}',
                hideAfter: 5500,
                stack: 6
            });
        });
    </script>
@endif

@yield('scripts')
</body>
</html>