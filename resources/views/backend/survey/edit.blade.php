@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Wijzig :single \':name\'', ['single' => strtolower($settings['name']), 'name' => $survey->title]) }}</h5>
                <div class="form-wrap">
                    @include('backend.parts.errors')

                    <form method="post" action="{{ route($settings['url_key'].'.edit', $survey->id) }}">
                        @csrf

                        <div class="form-group">
                            <label class="control-label mb-5 text-left" for="title">{{ __('Naam') }}</label>
                            <input class="form-control" value="{{ old('title', $survey->title) }}" id="title" name="title">
                        </div>

                        <div class="form-group no-margin-switcher">
                            <label class="control-label text-left" @if (hasPermission('email_notification') === false) style="color: #8f8d8d;" @endif>
                                <input class="js-switch" data-color="#4aa23c" type="checkbox" name="send_responses" value="true" @if (hasPermission('email_notification') === false) disabled @endif @if (old('send_responses', $survey->send_responses)) checked  @endif />
                                {{ __('E-mailnotificatie') }}
                                @if (hasPermission('email_notification') === false) @include('backend.parts.upgrade') @endif
                            </label>
                            <span class="block mt-10" style="font-size: 13px;">{{ __('Ontvang een e-mailnotificatie waneer er een nieuwe reactie binnenkomt.') }}</span>
                        </div>

                        <div class="row mt-25">
                            <div class="col-md-6">
                                <h5>{{ __('Activatie instellingen', ['single' => $settings['name']]) }}</h5>

                                <div class="form-group">
                                    <label class="block control-label mb-5 text-left">{{ __('Pagina instellingen') }}</label>
                                    <div class="input-group mb-15">
                                        <div class="input-group-btn">
                                            <input type="hidden" id="url_target_type" name="url_target_type" value="{{ old('url_target_type', $survey->url_target_type) }}" />
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span id="url_target_text">{{ $urlTargets[old('url_target_type', $survey->url_target_type)]->getSettings()['title'] }}</span> <span class="caret"></span></button>
                                            <ul class="dropdown-menu info-buttons">
                                                @foreach ($urlTargets as $type => $urlTarget)
                                                    <li>
                                                        <a href="javascript:void(0)" class="url-target-link" data-value="{{ $type }}" data-text="{{ $urlTarget->getSettings()['title'] }}">
                                                            {{ $urlTarget->getSettings()['title'] }}
                                                        </a>
                                                        <a href="{{ route('modal', 'url-targets') }}" data-toggle="modal" class="info-icon"><i class="fa fa-info-circle"></i></a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <input type="text" id="url_target" name="url_target" value="{{ old('url_target', $survey->url_target) }}" class="form-control" placeholder="{{ __('bijv. https://www.voorbeeld.nl') }}">
                                    </div>
                                </div>

                                <div class="form-group no-margin-switcher">
                                    <label class="block control-label mb-10 text-left" for="name">{{ __('Platforms') }}</label>
                                    <label class="block mb-10" @if (hasPermission('desktop_platform') === false) style="color: #8f8d8d;" @else style="color: #555;" @endif>
                                    <input class="js-switch" data-color="#4aa23c" type="checkbox" name="platforms[]" value="desktop"
                                        @if (hasPermission('desktop_platform') === false)
                                            disabled
                                        @endif
                                        @if (in_array('desktop', old('platforms', $platforms)))
                                            checked
                                        @endif  /> {{ __('Desktop') }}
                                        @if (hasPermission('desktop_platform') === false) @include('backend.parts.upgrade') @endif
                                    </label>
                                    <label class="block mb-10" @if (hasPermission('tablet_platform') === false) style="color: #8f8d8d;" @else style="color: #555;" @endif>
                                    <input class="js-switch js-mobile-platform" data-color="#4aa23c" type="checkbox" name="platforms[]" value="tablet"
                                        @if (hasPermission('tablet_platform') === false)
                                            disabled
                                        @endif
                                        @if (in_array('tablet', old('platforms', $platforms)))
                                            checked
                                        @endif  /> {{ __('Tablet') }}
                                        @if (hasPermission('tablet_platform') === false) @include('backend.parts.upgrade') @endif
                                    </label>
                                    <label class="block" @if (hasPermission('mobile_platform') === false) style="color: #8f8d8d;" @else style="color: #555;" @endif>
                                    <input class="js-switch js-mobile-platform" data-color="#4aa23c" type="checkbox" name="platforms[]" value="mobile"
                                        @if (hasPermission('mobile_platform') === false)
                                            disabled
                                        @endif
                                        @if (in_array('mobile', old('platforms', $platforms)))
                                            checked
                                        @endif /> {{ __('Mobiel') }}
                                        @if (hasPermission('mobile_platform') === false) @include('backend.parts.upgrade') @endif
                                    </label>
                                </div>

                                @if ($settings['type'] === 'survey' || $settings['type'] === 'poll')
                                    <h5 class="mb-10 mt-25">{{ __('Triggers') }}</h5>
                                    <div class="form-group no-margin-switcher">
                                        <input class="js-switch" data-color="#4aa23c" type="checkbox" @if(old('time_on_page', $survey->time_on_page)) checked @endif name="time_on_page" id="time_on_page" />
                                        <label class="control-label mb-20 text-left" for="time_on_page">{{ __('Tijd op pagina') }}</label>
                                        <div class="input-group mb-5">
                                            <input class="form-control" value="{{ old('time_on_page_miliseconds', $survey->time_on_page_miliseconds) }}" id="time_on_page_miliseconds" name="time_on_page_miliseconds">
                                            <span class="input-group-addon" style="font-size: 14px; height: 42px;">ms</span>
                                        </div>
                                        <span class="block mb-20" style="font-size: 13px;">{{ __('Na hoeveel miliseconden wilt u de :single weergeven.', ['single' => strtolower($settings['name'])]) }}</span>

                                        <input class="js-switch" data-color="#4aa23c" @if (hasPermission('target_leave_intent') === false) disabled @endif @if(old('leave_intent', $survey->leave_intent)) checked @endif type="checkbox" id="leave_intent" name="leave_intent" />
                                        <label @if (hasPermission('target_leave_intent') === false) style="color: #8f8d8d;" @endif class="control-label mb-15 text-left" for="leave_intent">{{ __('Verlaat intentie') }}</label> @if (hasPermission('target_leave_intent') === false) @include('backend.parts.upgrade') @endif
                                        <span class="block mb-20" style="font-size: 13px;">{{ __('Toon deze :single wanneer een bezoeker de intentie heeft om de pagina te verlaten.', ['single' => strtolower($settings['name'])]) }}</span>

                                        <input class="js-switch" data-color="#4aa23c" @if (hasPermission('target_external_activation') === false) disabled @endif @if(old('external_activation', $survey->external_activation)) checked @endif type="checkbox" id="external_activation" name="external_activation" />
                                        <label @if (hasPermission('target_external_activation') === false) style="color: #8f8d8d;" @endif class="control-label mb-15 text-left" for="external_activation">{{ __('Externe activatie') }}</label>@if (hasPermission('target_external_activation') === false) @include('backend.parts.upgrade') @endif
                                        <span class="block" style="font-size: 13px;">{{ __('Activeer deze :single door middel van een javascript-call.', ['single' => strtolower($settings['name'])]) }} <a href="{{ route('modal', 'external-activation') }}" data-toggle="modal">{{ __('Bekijk uitleg.') }}</a></span>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <h5>{{ __('Weergave') }} @if (hasPermission('button_texts') === false) @include('backend.parts.upgrade') @endif</h5>
                                @if ($settings['type'] === 'feedbackbutton')
                                    <div class="form-group">
                                        <label @if (hasPermission('button_texts') === false) style="color: #8f8d8d;" @endif class="control-label mb-5 text-left" for="feedback_button_text">{{ __('Tekst op feedbackbutton') }}</label>
                                        <input @if (hasPermission('button_texts') === false) style="color: #8f8d8d;" disabled @endif class="form-control mb-5" value="{{ old('feedback_button_text', $survey->feedback_button_text) }}" id="feedback_button_text" name="feedback_button_text">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label mb-5 text-left" for="feedback_button_location">{{ __('Locatie feedbackbutton') }}</label>
                                        <select class="form-control mb-5" id="feedback_button_location" name="feedback_button_location">
                                            <option value="right" {{ old('feedback_button_location', $survey->feedback_button_location) === 'right' ? 'selected' : '' }}>{{ __('Rechts') }}</option>
                                            <option value="left" {{ old('feedback_button_location', $survey->feedback_button_location) === 'left' ? 'selected' : '' }}>{{ __('Links') }}</option>
                                            <option value="bottom" {{ old('feedback_button_location', $survey->feedback_button_location) === 'bottom' ? 'selected' : '' }}>{{ __('Onder') }}</option>
                                        </select>
                                    </div>
                                @endif

                                @if ($settings['type'] <> 'poll')
                                    <div class="form-group">
                                        <label @if (hasPermission('button_texts') === false) style="color: #8f8d8d;" @endif class="control-label mb-5 text-left" for="button_send">{{ __('Tekst knop \'Versturen\'') }}</label>
                                        <input @if (hasPermission('button_texts') === false) style="color: #8f8d8d;" disabled @endif class="form-control" value="{{ old('button_send', $survey->button_send) }}" id="button_send" name="button_send">
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label @if (hasPermission('button_texts') === false) style="color: #8f8d8d;" @endif class="control-label mb-5 text-left" for="button_send_close">{{ __('Tekst knop \'Versturen en sluiten\'') }}</label>
                                    <input @if (hasPermission('button_texts') === false) style="color: #8f8d8d;" disabled @endif class="form-control mb-5" value="{{ old('button_send_close', $survey->button_send_close) }}" id="button_send_close" name="button_send_close">
                                    @if ($settings['type'] <> 'poll')
                                        <span style="font-size: 13px;">{{ __('Voor :plural die slechts één vraag bevatten, en voor de laatste vraag.', ['plural' => strtolower($settings['plural'])]) }}</span>
                                    @endif
                                </div>

                                @if ($settings['type'] === 'survey')
                                    <div id="mobile_bar_settings" style="display: none;">
                                        <h5 class="mt-25">{{ __('Mobiele balk teksten') }} @if (hasPermission('mobile_platform') === false && hasPermission('tablet_platform') === false) @include('backend.parts.upgrade') @endif</h5>
                                        <p class="mb-15">{{ __('Op telefoons en tablets wordt er voor het tonen van de survey een balk getoond waarin de bezoeker gevraagd wordt om deel te nemen.') }}</p>
                                        <div class="form-group">
                                            <label @if (hasPermission('mobile_platform') === false && hasPermission('tablet_platform') === false) style="color: #8f8d8d;" @endif class="control-label mb-5 text-left" for="mobile_bar_title">{{ __('Titel') }}</label>
                                            <input @if (hasPermission('mobile_platform') === false && hasPermission('tablet_platform') === false) style="color: #8f8d8d;" disabled @endif class="form-control" value="{{ old('mobile_bar_title', $survey->mobile_bar_title) }}" id="mobile_bar_title" name="mobile_bar_title">
                                        </div>

                                        <div class="form-group">
                                            <label @if (hasPermission('mobile_platform') === false && hasPermission('tablet_platform') === false) style="color: #8f8d8d;" @endif class="control-label mb-5 text-left" for="mobile_bar_subtitle">{{ __('Subtitel') }}</label>
                                            <input @if (hasPermission('mobile_platform') === false && hasPermission('tablet_platform') === false) style="color: #8f8d8d;" disabled @endif class="form-control" value="{{ old('mobile_bar_subtitle', $survey->mobile_bar_subtitle) }}" id="mobile_bar_subtitle" name="mobile_bar_subtitle">
                                        </div>

                                        <div class="form-group">
                                            <label @if (hasPermission('mobile_platform') === false && hasPermission('tablet_platform') === false) style="color: #8f8d8d;" @endif class="control-label mb-5 text-left" for="mobile_bar_button_yes">{{ __('Tekst knop \'Ja\'') }}</label>
                                            <input @if (hasPermission('mobile_platform') === false && hasPermission('tablet_platform') === false) style="color: #8f8d8d;" disabled @endif class="form-control" value="{{ old('mobile_bar_button_yes', $survey->mobile_bar_button_yes) }}" id="mobile_bar_button_yes" name="mobile_bar_button_yes">
                                        </div>

                                        <div class="form-group">
                                            <label @if (hasPermission('mobile_platform') === false && hasPermission('tablet_platform') === false) style="color: #8f8d8d;" @endif class="control-label mb-5 text-left" for="mobile_bar_button_no">{{ __('Tekst knop \'Nee\'') }}</label>
                                            <input @if (hasPermission('mobile_platform') === false && hasPermission('tablet_platform') === false) style="color: #8f8d8d;" disabled @endif class="form-control" value="{{ old('mobile_bar_button_no', $survey->mobile_bar_button_no) }}" id="mobile_bar_button_no" name="mobile_bar_button_no">
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group mb-0 mt-15">
                            <button class="btn btn-primary">{{ __('Wijzigen') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if ($settings['type'] === 'survey')
        <script>
            $(document).ready(function() {
                $('.js-mobile-platform').each(function() {
                    $(this).on('change', function() {
                        checkStatus();
                    });
                });

                function checkStatus() {
                    var active = false;

                    $('.js-mobile-platform').each(function() {
                        if ($(this).is(':checked')) {
                            active = true;
                        }
                    });

                    if (active === true) {
                        $("#mobile_bar_settings").show();
                    } else {
                        $("#mobile_bar_settings").hide();
                    }
                }

                checkStatus();
            });
        </script>
    @endif
    <script>
        $('.url-target-link').on('click', function() {
            $("#url_target_text").html($(this).data('text'));
            $("#url_target_type").val($(this).data('value'));
        });
    </script>
@endsection