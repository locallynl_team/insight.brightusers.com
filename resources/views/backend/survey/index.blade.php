@extends('backend.layouts.app')

@section('content')
<div class="container-fluid pt-25">
    <div class="row">
        <div class="col-md-12">
            <h5 class="mb-20">{{ $settings['plural'] }}</h5>
            @if ($surveys->count() > 0)
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>{{ __('ID') }}</th>
                                <th style="width: 40%;">{{ __('Naam') }}</th>
                                <th>{{ __('Reacties') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($surveys as $survey)
                                <tr>
                                    <td>{{ $survey->id }}</td>
                                    <td><a href="{{ route($settings['url_key'].'.manage', $survey->id) }}">{{ $survey->title }}</a></td>
                                    <td>
                                        <a href="{{ route($settings['url_key'].'.responses', $survey->id) }}">{{ $survey->responses_count.' '.($survey->responses_count === 1 ? __('reactie') : __('reacties')) }}</a>
                                    </td>
                                    <td><a href="{{ route($settings['url_key'].'.toggle', $survey->id) }}">
                                            @if ($survey->status === true)
                                                <span class="label label-primary">{{ __('Actief') }}</span>
                                            @else
                                                <span class="label label-default">{{ __('In-actief') }}</span>
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <div class="pull-right">
                                            <a class="action action-button mr-10" href="{{ route($settings['url_key'].'.manage', $survey->id) }}"><i class="fa fa-list"></i></a>
                                            <a class="action action-button mr-10" href="{{ route($settings['url_key'].'.edit', $survey->id) }}"><i class="fa fa-edit"></i></a>
                                            <a class="action action-button delete" href="{{ route('modal', ['name' => strtolower($settings['name']).'-delete', 'id' => $survey->id]) }}" data-toggle="modal"><i class="fa fa-trash-alt"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">{{ __('Voeg een :single toe om te beginnen.', ['single' => strtolower($settings['name'])]) }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <div style="max-width: 800px;">
                    @if ($settings['type'] === 'survey')
                        <p class="mb-20">
                            {{ __('Surveys helpen u om bezoekers van uw website beter te leren kennen. U kunt heel gericht vragenlijsten maken en precies instellen wanneer u deze wilt tonen aan uw bezoeker.') }}
                            {{ __('Vraag uw bezoekers naar hun ervaring op bijvoorbeeld de afrekenpagina, zo krijgt u direct waardevolle informatie waarmee u het proces kunt verbeteren.') }}
                        </p>
                        <p style="text-align: center;">
                            <a class="btn btn-primary" href="{{ route('survey.create') }}">{{ __('Beginnen met surveys') }} <i class="fa fa-question ml-10"></i></a>
                        </p>
                        <div class="row mb-30 mt-30" style="background: #f6f6f6; padding: 15px 0;">
                            <div class="col-md-6 col-xs-12 pull-right">
                                <img src="/assets/backend/images/icons/questions.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                                <p class="visible-xs">&nbsp;</p>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <h5>{{ __('Verschillende vragen') }}</h5>
                                <p class="mb-10">{{ __('Voor uw survey kunt u gebruik maken van meerdere soorten antwoordmogelijkheden. Denk hierbij aan korte tekst, lange tekst, multiple choice en ratings. Zo maakt u uw survey gebruiksvriendelijk voor uw websitebezoeker en verhoogt u het aantal reacties.') }}</p>
                            </div>
                        </div>
                        <div class="row mb-30">
                            <div class="col-md-6 col-xs-12 pull-left">
                                <img src="/assets/backend/images/icons/design.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                                <p class="visible-xs">&nbsp;</p>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <h5>{{ __('Opmaak naar uw wens') }}</h5>
                                <p class="mb-10">{{ __('Om de schermen van uw survey optimaal te integreren in uw website kunt u deze volledig aanpassen aan uw huisstijl. Het aanpassen van de styling is heel eenvoudig met onze opmaakmodule, kies in het menu links voor \'opmaak\'.') }}</p>
                            </div>
                        </div>
                        <div class="row mb-30" style="background: #f6f6f6; padding: 15px 0;">
                            <div class="col-md-6 col-xs-12 pull-right">
                                <img src="/assets/backend/images/icons/targeting.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                                <p class="visible-xs">&nbsp;</p>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <h5>{{ __('Precieze targeting') }}</h5>
                                <p class="mb-10">{{ __('Omdat uw survey niet altijd van toepassing is op elke bezoeker heeft u uitgebreide mogelijkheden voor targeting. Zo selecteert u eenvoudig op welke devices, pagina\'s en na hoeveel tijd de survey getoond moet worden.') }}</p>
                            </div>
                        </div>
                    @elseif ($settings['type'] === 'feedbackbutton')
                        <p class="mb-20">
                            {{ __('Feedbackbuttons voorzien u doorlopend van user feedback. Uw website bezoekers kunnen op de feedbackbutton klikken om u direct feedback te geven over uw product, website of service.') }}
                            {{ __('Met de feedback is het voor u eenvoudig om uw website of dienstverlening doorlopend te monitoren op verbeterpunten.') }}
                        </p>
                        <p style="text-align: center;">
                            <a class="btn btn-primary" href="{{ route('feedbackButton.create') }}">{{ __('Beginnen met feedbackbuttons') }} <i class="fa fa-comment ml-10"></i> </a>
                        </p>
                        <div class="row mb-30 mt-30" style="background: #f6f6f6; padding: 15px 0;">
                            <div class="col-md-6 col-xs-12 pull-right">
                                <img src="/assets/backend/images/icons/questions.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                                <p class="visible-xs">&nbsp;</p>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <h5>{{ __('Stel een eenvoudige vraag') }}</h5>
                                <p class="mb-10">{{ __('Voor de beste respons op uw feedbackbutton stelt u een eenvoudige vraag aan uw bezoekers. Denk hierbij aan \'Heeft u feedback voor ons om onze website te verbeteren?\'. Deze vraag kunt u stellen als multi-keuze, waarbij Ja en Nee de opties zijn. Afhankelijk van de keuze kunt u de volgende vraag stellen bij Ja, \'Wat is uw feedback voor ons?\'. Bij Nee kunt u de bezoeker het bedanktscherm tonen.') }}</p>
                            </div>
                        </div>
                        <div class="row mb-30">
                            <div class="col-md-6 col-xs-12 pull-left">
                                <img src="/assets/backend/images/icons/design.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                                <p class="visible-xs">&nbsp;</p>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <h5>{{ __('Opmaak naar uw wens') }}</h5>
                                <p class="mb-10">{{ __('Om de feedbackbutton en de vraagschermen optimaal te integreren in uw website kunt u deze volledig aanpassen aan uw huisstijl. Het aanpassen van de styling is heel eenvoudig met onze opmaakmodule, kies in het menu links voor \'opmaak\'.') }}</p>
                            </div>
                        </div>
                        <div class="row mb-30" style="background: #f6f6f6; padding: 15px 0;">
                            <div class="col-md-6 col-xs-12 pull-right">
                                <img src="/assets/backend/images/icons/targeting.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                                <p class="visible-xs">&nbsp;</p>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <h5>{{ __('Precieze targeting') }}</h5>
                                <p class="mb-10">{{ __('Uw feedbackbutton kunt u eenvoudig plaatsen op alle pagina\'s waar u deze wilt hebben. Voor het beste effect en de meeste response adviseren wij om deze op alle pagina\'s van uw website te plaatsen.') }}</p>
                            </div>
                        </div>
                    @else
                        <p class="mb-20">
                            {{ __('Polls helpen u om snel een multiple choice vraag aan uw websitebezoekers te stellen. De response op een poll is vaak hoger omdat deze slechts één vraag omvat en de vraagstelling kort en krachtig kan zijn.') }}
                            {{ __('Zo kunt u met een poll heel gericht uw doelgroep een vraag stellen.') }}
                        </p>
                        <p style="text-align: center;">
                            <a class="btn btn-primary" href="{{ route('poll.create') }}">{{ __('Beginnen met polls') }} <i class="fa fa-comment ml-10"></i> </a>
                        </p>
                        <div class="row mb-30 mt-30" style="background: #f6f6f6; padding: 15px 0;">
                            <div class="col-md-6 col-xs-12 pull-right">
                                <img src="/assets/backend/images/icons/questions.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                                <p class="visible-xs">&nbsp;</p>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <h5>{{ __('Stel een eenvoudige vraag') }}</h5>
                                <p class="mb-10">{{ __('Hou de vraagstelling simpel, dit verhoogt de response op uw poll. Zorg ook dat de antwoorden duidelijk zijn en dat de antwoorden voor iedereen een passende keus geven.') }}</p>
                            </div>
                        </div>
                        <div class="row mb-30">
                            <div class="col-md-6 col-xs-12 pull-left">
                                <img src="/assets/backend/images/icons/design.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                                <p class="visible-xs">&nbsp;</p>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <h5>{{ __('Opmaak naar uw wens') }}</h5>
                                <p class="mb-10">{{ __('Om de poll en het vraagscherm optimaal te integreren in uw website kunt u deze volledig aanpassen aan uw huisstijl. Het aanpassen van de styling is heel eenvoudig met onze opmaakmodule, kies in het menu links voor \'opmaak\'.') }}</p>
                            </div>
                        </div>
                        <div class="row mb-30" style="background: #f6f6f6; padding: 15px 0;">
                            <div class="col-md-6 col-xs-12 pull-right">
                                <img src="/assets/backend/images/icons/targeting.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                                <p class="visible-xs">&nbsp;</p>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <h5>{{ __('Precieze targeting') }}</h5>
                                <p class="mb-10">{{ __('Uw poll kunt u eenvoudig plaatsen op alle pagina\'s waar u deze wilt hebben. Voor het beste effect en de meeste response adviseren wij om deze op alle pagina\'s van uw website te plaatsen.') }}</p>
                            </div>
                        </div>
                    @endif
                </div>
            @endif
            <a class="btn btn-primary" href="{{ route($settings['url_key'].'.create') }}"><i class="fa fa-plus mr-10"></i> {{ __(':single toevoegen', ['single' => $settings['name']]) }}</a>
        </div>
    </div>
</div>
@endsection