@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                @if ($survey->type === 'poll')
                    <h5 class="mb-20">{{ __('Beheer vraag van :single \':name\'', ['single' => strtolower($settings['name']), 'name' => $survey->title]) }}</h5>
                @else
                    <h5 class="mb-20">{{ __('Beheer vragen van :single \':name\'', ['single' => strtolower($settings['name']), 'name' => $survey->title]) }}</h5>
                @endif
                <div class="form-wrap" style="max-width: 950px;">
                    @include('backend.parts.errors')

                    <form method="post" action="{{ route($settings['url_key'].'.manage', $survey->id) }}">
                        @csrf
                        <div id="app">
                            <manage-questions
                                    v-bind:question-types="{{ $questionTypes }}"
                                    v-bind:questions="{{ $questions }}"
                                    survey-type="{{ $survey->type }}"
                            ></manage-questions>
                        </div>

                        <div class="form-group mb-0 mt-25">
                            <button class="btn btn-primary pull-left">{{ __('Wijzigen') }}</button>
                            <a class="btn btn-primary pull-right js-preview">{{ __('Preview tonen') }}</a>
                            <div class="clearfix"></div>
                            <span class="pull-right" style="font-size: 13px; margin-top: 5px; text-align: right;">{!! __('Om uw wijzigingen te tonen<br />in de preview klikt u eerst op wijzigen.') !!}</span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/assets/backend/js/vue/app.js"></script>
    <script>
        $(function($) {
            var panelList = $('#question-list');

            panelList.sortable({
                handle: '.move-item',
                update: function() {
                    $('.panel', panelList).each(function(index, elem) {
                        var $listItem = $(elem), newIndex = $listItem.index();
                        var orderField = $listItem.find($('.order-field'));
                        $(orderField).val(newIndex);
                    });
                }
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $('.js-preview').on('click', function() {
                window.surve.activateSurveyExternal({{ $survey->id }});
            });
        });

        @include('javascript/v1', ['preview' => true])
    </script>
@endsection