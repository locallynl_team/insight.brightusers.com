@extends('backend.layouts.app')

@section('content')
<div class="container-fluid pt-25">
    <div class="row">
        <div class="col-md-12">
            <div class="row mb-30">
                <div class="col-md-6">
                    <h5>{{ __('Helpdesk Surve') }}</h5>
                    <p>{{ __('Onze helpdesk zit voor u klaar als u vragen of opmerkingen heeft over Surve.') }}</p>
                </div>
            </div>
            <div class="row mb-30">
                <div class="col-md-4">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <h6 class="panel-title txt-dark">{{ __('Per e-mail') }}</h6>
                        </div>
                        <div class="panel-wrapper">
                            <div class="panel-body row">
                                <div class="col-md-3">
                                    <img class="img-responsive" style="padding: 20px;" src="{{ static_url() }}/assets/backend/images/icons/email.png" />
                                </div>
                                <div class="col-md-9">
                                    <p class="mb-10">
                                        {{ __('Wij zijn bereikbaar per e-mail op: info@surve.nl') }}
                                    </p>
                                    <p>
                                        {{ __('Als u ons een mail stuurt willen wij u vragen om zoveel mogelijk informatie te vermelden die relevant kan zijn. Op deze manier kunnen wij u het snelste van dienst zijn.') }}
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <h6 class="panel-title txt-dark">{{ __('Telefonisch') }}</h6>
                        </div>
                        <div class="panel-wrapper">
                            <div class="panel-body row">
                                <div class="col-md-3">
                                    <img class="img-responsive" style="padding: 20px;" src="{{ static_url() }}/assets/backend/images/icons/telephone.png" />
                                </div>
                                <div class="col-md-9">
                                    <p class="mb-10">
                                        {{ __('Telefonisch zijn wij bereikbaar op: 038 - 202 264 1') }}
                                    </p>
                                    <p>
                                        {{ __('De helpdesk is bereikbaar op werkdagen van 09:00 tot 18:00') }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <h6 class="panel-title txt-dark">{{ __('Op Twitter') }}</h6>
                        </div>
                        <div class="panel-wrapper">
                            <div class="panel-body row">
                                <div class="col-md-3">
                                    <img class="img-responsive" style="padding: 20px;" src="{{ static_url() }}/assets/backend/images/icons/twitter.png" />
                                </div>
                                <div class="col-md-9">
                                    <p class="mb-10">
                                        {{ __('Mention ons op Twitter met @SurveNL en we zullen zo snel mogelijk reageren.') }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection