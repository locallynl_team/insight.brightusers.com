<!-- Right Sidebar Menu -->
<div class="fixed-sidebar-right">
    <ul class="right-sidebar">
        <li>
            <div class="tab-struct custom-tab-1">
                <div class="tab-content" id="right_sidebar_content">
                    <div id="messages_tab" class="tab-pane fade active in">
                        <div class="message-box-wrap">
                            <div class="msg-search">
                                <a href="javascript:void(0)" class="inline-block txt-grey">
                                    <i class="fa fa-ellipsis-h"></i>
                                </a>
                                <span class="inline-block txt-dark">messages</span>
                                <a href="javascript:void(0)" class="inline-block text-right txt-grey"><i class="zmdi zmdi-search"></i></a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="set-height-wrap">
                                <div class="streamline message-box nicescroll-bar">
                                    <a href="javascript:void(0)">
                                        <div class="sl-item unread-message">
                                            <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                <img class="img-responsive img-circle" src="dist/img/user.png" alt="avatar"/>
                                            </div>
                                            <div class="sl-content">
                                                <span class="inline-block capitalize-font   pull-left message-per">Clay Masse</span>
                                                <span class="inline-block font-11  pull-right message-time">12:28 AM</span>
                                                <div class="clearfix"></div>
                                                <span class=" truncate message-subject">Themeforest message sent via your envato market profile</span>
                                                <p class="txt-grey truncate">Neque porro quisquam est qui dolorem ipsu messm quia dolor sit amet, consectetur, adipisci velit</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <div class="sl-item">
                                            <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                <img class="img-responsive img-circle" src="dist/img/user1.png" alt="avatar"/>
                                            </div>
                                            <div class="sl-content">
                                                <span class="inline-block capitalize-font   pull-left message-per">Evie Ono</span>
                                                <span class="inline-block font-11  pull-right message-time">1 Feb</span>
                                                <div class="clearfix"></div>
                                                <span class=" truncate message-subject">Pogody theme support</span>
                                                <p class="txt-grey truncate">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <div class="sl-item">
                                            <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                <img class="img-responsive img-circle" src="dist/img/user2.png" alt="avatar"/>
                                            </div>
                                            <div class="sl-content">
                                                <span class="inline-block capitalize-font   pull-left message-per">Madalyn Rascon</span>
                                                <span class="inline-block font-11  pull-right message-time">31 Jan</span>
                                                <div class="clearfix"></div>
                                                <span class=" truncate message-subject">Congratulations from design nominees</span>
                                                <p class="txt-grey truncate">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <div class="sl-item unread-message">
                                            <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                <img class="img-responsive img-circle" src="dist/img/user3.png" alt="avatar"/>
                                            </div>
                                            <div class="sl-content">
                                                <span class="inline-block capitalize-font   pull-left message-per">Ezequiel Merideth</span>
                                                <span class="inline-block font-11  pull-right message-time">29 Jan</span>
                                                <div class="clearfix"></div>
                                                <span class=" truncate message-subject">Themeforest item support message</span>
                                                <p class="txt-grey truncate">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <div class="sl-item unread-message">
                                            <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                <img class="img-responsive img-circle" src="dist/img/user4.png" alt="avatar"/>
                                            </div>
                                            <div class="sl-content">
                                                <span class="inline-block capitalize-font   pull-left message-per">Jonnie Metoyer</span>
                                                <span class="inline-block font-11  pull-right message-time">27 Jan</span>
                                                <div class="clearfix"></div>
                                                <span class=" truncate message-subject">Help with beavis contact form</span>
                                                <p class="txt-grey truncate">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <div class="sl-item">
                                            <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                <img class="img-responsive img-circle" src="dist/img/user.png" alt="avatar"/>
                                            </div>
                                            <div class="sl-content">
                                                <span class="inline-block capitalize-font   pull-left message-per">Priscila Shy</span>
                                                <span class="inline-block font-11  pull-right message-time">19 Jan</span>
                                                <div class="clearfix"></div>
                                                <span class=" truncate message-subject">Your uploaded theme is been selected</span>
                                                <p class="txt-grey truncate">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <div class="sl-item">
                                            <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                <img class="img-responsive img-circle" src="dist/img/user1.png" alt="avatar"/>
                                            </div>
                                            <div class="sl-content">
                                                <span class="inline-block capitalize-font   pull-left message-per">Linda Stack</span>
                                                <span class="inline-block font-11  pull-right message-time">13 Jan</span>
                                                <div class="clearfix"></div>
                                                <span class=" truncate message-subject"> A new rating has been received</span>
                                                <p class="txt-grey truncate">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>