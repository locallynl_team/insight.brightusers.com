@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Opmaak van :domain', ['domain' => $site->domain]) }}</h5>
                <div class="form-wrap">
                    @include('backend.parts.errors')

                    <form method="post" enctype="multipart/form-data" action="{{ route('style.index') }}" id="app">
                        @csrf

                        <styling v-bind:site="{{ $site->style }}"></styling>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        {!! $surveyCss !!}

        {!! $surveyMobileCss !!}

        #surve-modal {
            position: absolute;
        }

        #surve-feedback {
            display: block;
        }

        #surve-bar {
            position: absolute;
            display: block;
        }
    </style>
@endsection

@section('scripts')
    <script src="/assets/backend/js/vue/app.js"></script>
    <script src="/assets/backend/js/bootstrap-colorpicker.min.js"></script>
    <script src="/assets/backend/js/dropify.min.js"></script>
@endsection