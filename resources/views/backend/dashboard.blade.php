@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($visitsToday) }}</span></span>
                                            <span class="weight-500 uppercase-font block">{{ __('bezoekers vandaag') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($pageViewsToday) }}</span></span>
                                            <span class="weight-500 uppercase-font block">{{ __('views vandaag') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($reactionsToday) }}</span></span>
                                            <span class="weight-500 uppercase-font block">{{ __('reacties vandaag') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($heatmapSessionsToday) }}</span></span>
                                            <span class="weight-500 uppercase-font block">{{ __('heatmap sessies vandaag') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <h5>{{ __('Statistieken gebruik') }}</h5>
                 <div id="line_chart_statistics" class="morris-chart" style="height: 520px; margin-bottom: 20px;"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h5>{{ __('Gebruik per apparaat') }}</h5>
                <?php
                $deviceColors = [
                    'desktop' => 'ac235c',
                    'mobile' => 'f8b32d',
                    'tablet' => 'f33923',
                    'unknown' => '878787'
                ];
                $deviceNames = [
                    'desktop' => __('Desktop'),
                    'mobile' => __('Mobiel'),
                    'tablet' => __('Tablet'),
                    'unknown' => __('Onbekend')
                ];
                ?>
                <div id="line_chart_platforms" class="morris-chart" style="height: 280px;"></div>
                <hr class="light-grey-hr row mt-10 mb-15"/>
                @forelse ($deviceStatistics as $statistic)
                    <div class="label-charts">
                        <span style="background-color: #{{ $deviceColors[$statistic->platform] }};" class="clabels clabels-lg inline-block mr-10 mt-10 pull-left"></span>
                        <span class="clabels-text font-12 txt-dark capitalize-font">
                            <span class="block font-15 weight-500">
                                <span class="inline-block mt-10 pull-left">
                                    {{ $deviceNames[$statistic->platform] }}
                                </span>
                                <span class="inline-block pull-right">
                                    {{ price(($statistic->visits_count / $deviceTotalVisits) * 100) }}%
                                </span>
                                <div class="clearfix"></div>
                            </span>
                            <span class="block txt-grey pull-right" style="text-transform: none;">{{ trans_choice(':visits bezoeker|:visits bezoekers', $statistic->visits_count, ['visits' => thousand($statistic->visits_count)]) }}</span>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                    @if(!$loop->last)
                        <hr class="light-grey-hr row mt-10 mb-15"/>
                    @endif
                @empty
                   <p>{{ __('Geen data beschikbaar') }}</p>
                @endforelse
                <hr class="light-grey-hr row mt-10 mb-5"/>
                <span class="txt-grey font-12">{{ __('Afgelopen 30 dagen') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt-20">
                <h5>{{ __('Nieuwste reacties') }}</h5>
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>{{ __('Survey/Feedbackbutton') }}</th>
                                <th>{{ __('Datum') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($latestResponses as $response)
                                <tr>
                                    <td>{{ $response->survey->title }}</td>
                                    <td>{{ $response->created_at->formatLocalized('%d %B %Y om %H:%M') }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">{{ __('Nog geen reacties ontvangen') }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt-20">
                <h5>{{ __('Top surveys & feedbackbuttons') }}</h5>
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>{{ __('Survey/Feedbackbutton') }}</th>
                                <th>{{ __('Reacties') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($topSurveys as $survey)
                                <tr>
                                    <td>{{ $survey->survey->title }}</td>
                                    <td>{{ thousand($survey->responses_count) }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">{{ __('Nog geen reacties ontvangen') }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection

@section('scripts')
    <script src="/assets/backend/js/raphael.min.js"></script>
    <script src="/assets/backend/js/morris.min.js"></script>
    <script>
        //Graph for devices
        Morris.Bar({
            element: 'line_chart_platforms',
            data: [
            @foreach($deviceStatistics->reverse() as $statistic)
                {
                    platform: '{{ $deviceNames[$statistic->platform] }}',
                    visits: {{ $statistic->visits_count }}
                },
            @endforeach],
            xkey: 'platform',
            ykeys: ['visits'],
            labels: ['{{ __('Bezoeken') }}'],
            barRatio: 0.4,
            xLabelAngle: 35,
            pointSize: 1,
            barOpacity: 1,
            pointStrokeColors: ['#4aa23c'],
            grid: true,
            gridTextColor: '#878787',
            hideHover: 'true',
            barColors: function(row, series, type) {
                @foreach($deviceStatistics as $statistic)
                    if (row.label === '{{ $deviceNames[$statistic->platform] }}') {
                        return '#{{ $deviceColors[$statistic->platform] }}';
                    }
                @endforeach
            },
            resize: true,
            gridTextFamily: 'Roboto'
        });

        //Graph for statistics
        Morris.Line({
            element: 'line_chart_statistics',
            data: [
                @for($i = (29*24*60*60); $i >= 0; $i-= 86400)
                    {
                        date: '{{ date("Y-m-d", (time()-$i)) }} 00:00:00.000',
                        visits: {{ $visitsData->where('date', date("Y-m-d", (time()-$i)))->sum('visits') }},
                        views: {{ $visitsData->where('date', date("Y-m-d", (time()-$i)))->sum('views') }},
                        responses: {{ $responseData->where('date', date("Y-m-d", (time()-$i)))->first()->responses_count ?? 0 }},
                        sessions: {{ $sessionData->where('date', date("Y-m-d", (time()-$i)))->first()->sessions_count ?? 0 }}
                    },
                @endfor
            ],
            xkey: 'date',
            dateFormat: function(x) {
                var date = new Date(x);
                return (date.getDate() <= 9 ? '0' + date.getDate() : date.getDate()) + '-' + (date.getMonth() <= 9 ? '0' + date.getMonth() : date.getMonth()) + '-' + date.getFullYear();
            },
            ykeys: ['visits', 'views', 'responses', 'sessions'],
            labels: ['{{ __('Bezoekers') }}', '{{ __('Pageviews') }}', '{{ __('Reacties') }}', '{{ __('Heatmap sessies') }}'],
            pointSize: 5,
            pointStrokeColors: ['#27ae5f', '#ac235c', '#f8b32d', '#f33923'],
            behaveLikeLine: true,
            grid: true,
            gridTextColor: '#878787',
            lineWidth: 4,
            smooth: true,
            hideHover: 'true',
            lineColors: ['#27ae5f', '#ac235c', '#f8b32d', '#f33923'],
            resize: true,
            gridTextFamily: "Roboto"
        });
    </script>
@endsection