@extends('backend.layouts.modal')

@section('title')
    {{ __('Formulier heeft nog geen sessies') }}
@endsection

@section('body')
    <p>{{ __('Dit formulier heeft nog geen data verzameld, zodra er sessies beschikbaar zijn kunt u de analyse bekijken.') }}</p>
@endsection

@section('footer')
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Sluiten') }}</button>
@endsection