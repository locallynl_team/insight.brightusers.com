@extends('backend.layouts.modal')

@section('title')
    {{ __('Externe activatie') }}
@endsection

@section('body')
    <h4>{{ __('Activeer een survey handmatig') }}</h4>
    <p>{{ __('Wanneer u een survey wilt tonen zodra uw bezoeker een bepaalde actie op uw website uitvoert kunt u hier zelf een trigger voor instellen.') }}</p>
    <p>{{ __('Door het uitvoeren van een javascript-code wordt de survey dan getoond aan de bezoeker.') }}</p>
    <h4>{{ __('Instellingen') }}</h4>
    <p>{{ __('Zodra u de externe activatie doet zal de survey eerst zijn triggers instellen, wilt u de survey direct tonen zodra u deze activeert? Dan kunt u de \'Tijd op pagina\' trigger op 0 of uitzetten. De externe activatie trigger dient altijd aan te staan om deze functie te gebruiken, wanneer deze trigger aan staat wordt de survey alleen getoond met externe activatie.') }}</p>
    <h4>{{ __('Javascript-code') }}</h4>
    <p>{{ __('Om de survey te triggeren gebruikt u de onderstaande javascript code. Deze toont automatisch de survey die matcht met uw pagina-instellingen en platforms.') }}</p>
    <pre style="padding: 20px; max-width: 800px;" id="javascript_code">{{ '<script>surve.start();</script>' }}</pre>
    <p>{{ __('Voor een juiste werking dient deze code pas uitgevoerd te worden nadat de surve-implementatie code is ingeladen.') }}</p>
    <h4>{{ __('Support') }}</h4>
    <p>{{ __('Heeft u hulp nodig bij het installeren van externe activatie? Wij helpen u graag, neem contact op met de helpdesk voor de mogelijkheden.') }}</p>
@endsection

@section('footer')
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Sluiten') }}</button>
@endsection