@extends('backend.layouts.modal')

@section('title')
    {{ __('Formulier verwijderen') }}
@endsection

@section('body')
    <h4>{{ __('Weet u zeker dat u dit formulier wilt verwijderen?') }}</h4>
    <p>{{ __('Het verwijderen kan niet ongedaan worden gemaakt, uw formulier en alle bijhorende data wordt verwijderd.') }}</p>
@endsection

@section('footer')
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Sluiten') }}</button>
    <form method="post" action="{{ route('form.delete', $id) }}" style="display: inline;">
        @csrf
        <button type="submit" class="btn btn-danger ml-10">{{ __('Verwijderen') }}</button>
    </form>
@endsection