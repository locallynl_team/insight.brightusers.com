@extends('backend.layouts.modal')

@section('title')
    {{ __('Niet gevonden') }}
@endsection

@section('body')
    {{ __('Deze popup kon niet geladen worden.') }}
@endsection

@section('footer')
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Sluiten') }}</button>
@endsection