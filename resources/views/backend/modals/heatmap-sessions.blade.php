@extends('backend.layouts.modal')

@section('title')
    {{ __('Heatmap sessies') }}
@endsection

@section('body')
    <h4>{{ __('Dataverzameling') }}</h4>
    <p>{{ __('Voor het genereren van een heatmap wordt er data verzameld op uw website. Deze verzamelde data wordt gegroepeerd als een heatmapsessie.') }}</p>
    <p>{{ __('Elke heatmapsessie bevat de beweeg, klik en scroll activiteiten van één bezoeker die een pagina bezoekt die voldoet aan de door u opgegeven voorwaarden.') }}</p>
    <h4>{{ __('Limieten') }}</h4>
    <p>{{ __('Heatmaps geven een beter beeld wanneer er meer sessies zijn geregistreerd. Afhankelijk van uw pakket verzameld een heatmap data tot de gestelde limiet.') }}</p>
    <p>{{ __('De limieten voor de heatmaps zijn als volgt, en gelden per heatmap:') }}</p>
    <ul class="limits">
        <li><span class="label label-package" style="background-color: #3869cf;">{{ __('Basis') }}</span> 1.000 {{ __('sessies') }}</li>
        <li><span class="label label-package" style="background-color: #f5a135;">{{ __('Plus') }}</span> 2.500 {{ __('sessies') }}</li>
        <li><span class="label label-package" style="background-color: #9c2096;">{{ __('Premium') }}</span> 5.000 {{ __('sessies') }}</li>
    </ul>
@endsection

@section('footer')
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Sluiten') }}</button>
@endsection