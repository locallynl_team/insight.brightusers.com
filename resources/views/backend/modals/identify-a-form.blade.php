@extends('backend.layouts.modal')

@section('title')
    {{ __('Formulieren identificeren') }}
@endsection

@section('body')
    <p>{{ __('Voordat er data verzameld kan worden over uw formulieren zult u dit formulier moeten identificeren. Zo weet de tracking-code van Surve voor welk formulier er een meting wordt uitgevoerd.') }}</p>
    <h4>{{ __('Automatisch zoeken') }}</h4>
    <p>{{ __('De eenvoudigste optie is om automatisch alle formulieren op een pagina op te halen. Hiervoor vult u de URL van de pagina in waar het formulier op staat.') }}</p>
    <p>{{ __('De gevonden formulieren worden getoond. U kunt eenvoudig het formulier selecteren dat u wilt gebruiken voor de meting.') }}</p>
    <h4>{{ __('Handmatige selectie') }}</h4>
    <p>{{ __('- nog niet beschikbaar -') }}</p>
@endsection

@section('footer')
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Sluiten') }}</button>
@endsection