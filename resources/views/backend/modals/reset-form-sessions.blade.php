@extends('backend.layouts.modal')

@section('title')
    {{ __('Formulier sessies verwijderen') }}
@endsection

@section('body')
    <p>{{ __('Verwijder alle formulierdata die tot nu toe is verzameld voor dit formulier. Dit kan niet ongedaan gemaakt worden.') }}</p>
@endsection

@section('footer')
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Sluiten') }}</button>
    <a href="{{ route('form.reset', $id) }}" class="btn btn-danger">{{ __('Verwijderen') }}</a>
@endsection