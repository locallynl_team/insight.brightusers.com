@extends('backend.layouts.modal')

@section('title')
    {{ __('Feedbackbutton verwijderen') }}
@endsection

@section('body')
    <h4>{{ __('Weet u zeker dat u deze feedbackbutton wilt verwijderen?') }}</h4>
    <p>{{ __('Het verwijderen van kan niet ongedaan worden gemaakt, uw feedbackbutton en alle bijhorende data wordt verwijderd.') }}</p>
@endsection

@section('footer')
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Sluiten') }}</button>
    <form method="post" action="{{ route('feedbackButton.delete', $id) }}" style="display: inline;">
        @csrf
        <button type="submit" class="btn btn-danger ml-10">{{ __('Verwijderen') }}</button>
    </form>
@endsection