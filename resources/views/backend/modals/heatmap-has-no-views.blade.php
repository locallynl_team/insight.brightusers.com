@extends('backend.layouts.modal')

@section('title')
    {{ __('Heatmap heeft nog geen sessies') }}
@endsection

@section('body')
    <p>{{ __('Deze heatmap heeft nog geen data verzameld, zodra er sessies beschikbaar zijn kunt u de heatmaps bekijken.') }}</p>
@endsection

@section('footer')
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Sluiten') }}</button>
@endsection