@extends('backend.layouts.modal')

@section('title')
    {{ __('Welkom bij Surve') }}
@endsection

@section('body')
    <p>{{ __('Elke dag een beetje beter, dat is wat wij met Surve willen bereiken. Uw website is nooit af, en kan altijd beter! Wij gaan er alles aan doen om u hier bij te helpen!') }}</p>
    <h4>{{ __('Direct beginnen') }}</h4>
    <p>{{ __('Voeg uw eerste website toe, u krijgt van ons 14 dagen de tijd om alle functionaliteiten gratis te proberen. Zodra uw website is toegevoegd kunt u gelijk beginnen met Feedbackbuttons, Surveys en Heatmaps.') }}</p>
    <p>{{ __('Heeft u hulp nodig bij de opstart? Onze helpdesk helpt u graag!') }}</p>
    <p>{{ __('Groeten,') }}</p>
    <p>{{ __('Matthijs van Surve') }}</p>
@endsection

@section('footer')
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Let\'s go!') }}</button>
@endsection