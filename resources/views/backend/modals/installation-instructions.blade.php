@extends('backend.layouts.modal')

@section('title')
    {{ __('Surve installeren op uw website') }}
@endsection

@section('body')
    <p>{{ __('Om te starten met het gebruik van Surve op uw website moet er een code-fragment worden geplaatst. Dit fragment kunt u direct in uw broncode toevoegen, of via een tagmanager als Google Tagmanager.') }}</p>
    <p>{{ __('Het code-fragment werkt het beste wanneer dit onderaan uw pagina, net voor het sluiten van de <body> tag geplaatst wordt.') }}</p>

    <h4>{{ __('Handmatige installatie') }}</h4>
    <p>{{ __('Om de installatie van Surve handmatig te doen gebruikt u het volgende code-fragment. Dit plaatst u net voor </body> in uw code.') }}</p>
    <pre style="padding: 20px; max-width: 800px;" class="mb-15" id="javascript_code">@include('backend.parts.implementation', ['site' => $id])</pre>

    <h4>{{ __('Wordpress Plugin') }}</h4>
    <p>{{ __('Voor Wordpress kunt u gebruik maken van de Surve plugin. Deze plugin voegt de tracking-code automatisch toe aan uw website, zo hoeft u niet in de code te duiken.') }}</p>
    <p>{!! __('U installeert de plugin zoals iedere andere plugin. Na de installatie kunt u bij Instellingen -> Surve uw website ID invullen. Voor deze website is dat: <strong>:id</strong>', ['id' => $id]) !!}</p>
    <a href="https://wordpress.org/plugins/surve" target="_blank" class="btn btn-primary mb-15">{{ __('Download de Wordpress Plugin') }}</a>

    <h4>{{ __('Andere platforms') }}</h4>
    <p>{{ __('Op dit moment zijn er nog geen kant-en-klare plugins of add-ons voor andere platforms beschikbaar. Heeft u hulp nodig met de installatie? Laat het ons weten, dan helpen wij u hier graag bij.') }}</p>
@endsection

@section('footer')
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Sluiten') }}</button>
@endsection