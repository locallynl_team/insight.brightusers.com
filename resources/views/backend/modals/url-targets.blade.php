@extends('backend.layouts.modal')

@section('title')
    {{ __('URL Instellen') }}
@endsection

@section('body')
    <h4>{{ __('Simpele URL') }}</h4>
    <p>{{ __('Bij deze standaard methode kunt u de pagina opgeven op uw website die u wilt meten. Hierbij worden o.a. het protocol, de aanwezigheid van www. en de querystrings genegeerd.') }}</p>
    <h4>{{ __('Exacte URL') }}</h4>
    <p>{{ __('Deze methode matcht een specifieke url waarbij het protocol, querystring en fragment overeenkomen.') }}</p>
    <h4>{{ __('URL begint met') }}</h4>
    <p>{{ __('Match alle pagina\'s waarbij de url begint met de ingevoerde waarde, incl. protocol match.') }}</p>
    <h4>{{ __('URL eindigt met') }}</h4>
    <p>{{ __('Match alle pagina\'s waarbij het einde van de URL overeenkomt met de opgegeven waarde.') }}</p>
    <h4>{{ __('URL bevat') }}</h4>
    <p>{{ __('Wanneer een URL de opgegeven waarde bevat zal dit een match zijn.') }}</p>
    <h4>{{ __('Reguliere expressie') }}</h4>
    <p>{{ __('Reguliere expressies, ook wel regex genoemd, zijn zoekpatronen waarmee complexere URL\'s gematcht kunnen worden.') }}</p>
@endsection

@section('footer')
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Sluiten') }}</button>
@endsection