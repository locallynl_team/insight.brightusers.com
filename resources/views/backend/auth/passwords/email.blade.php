@extends('backend.layouts.auth')

@section('content')
<div class="page-wrapper pa-0 ma-0 auth-page">
    <div class="container-fluid">
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle auth-form-wrap">
                <div class="auth-form  ml-auto mr-auto no-float">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="mb-30">
                                <h3 class="text-center txt-light mb-10">{{ __('Wachtwoord vergeten') }}</h3>
                                <h6 class="text-center nonecase-font txt-light">{{ __('Vraag met uw e-mailadres een nieuw wachtwoord aan') }}</h6>

                                @if (session('status'))
                                    <div class="alert alert-success mt-20">
                                        {{ session('status') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-wrap">
                                <form method="POST" action="{{ route('password.email') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label class="control-label mb-5 txt-light" for="email">{{ __('E-mailadres') }}</label>
                                        <input id="email" name="email" type="email" value="{{ old('email') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required="">

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                {{ $errors->first('email') }}
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-primary mt-15">{{ __('Wachtwoord aanvragen') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
