@extends('backend.layouts.auth')

@section('content')
<div class="page-wrapper pa-0 ma-0 auth-page">
    <div class="container-fluid">
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle auth-form-wrap">
                <div class="auth-form  ml-auto mr-auto no-float">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="mb-30">
                                <h3 class="text-center txt-light mb-10">{{ __('Inloggen bij') }} {{ config('app.name') }}</h3>
                                <h6 class="text-center nonecase-font txt-light">{{ __('Log in met uw gegevens') }}</h6>

                                @if (session('status'))
                                    <div class="alert alert-success mt-20">
                                        {{ session('status') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-wrap">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label class="control-label mb-5 txt-light" for="email">{{ __('E-mailadres') }}</label>
                                        <input tabindex="1" id="email" name="email" type="email" value="{{ old('email') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required="">

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                {{ $errors->first('email') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label mb-5 txt-light" for="password">{{ __('Wachtwoord') }}</label>
                                        <input tabindex="2" type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required="" id="password">
                                        <a tabindex="4" class="capitalize-font block mt-10 pull-right font-12 txt-light" href="{{ route('password.request') }}">{{ __('wachtwoord vergeten?') }}</a>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                {{ $errors->first('password') }}
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox checkbox-primary pr-10 pull-left">
                                            <input tabindex="3" id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label for="remember" class="txt-light"> {{ __('Blijf ingelogd') }}</label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-primary ">{{ __('Inloggen') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
