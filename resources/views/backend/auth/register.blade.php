@extends('backend.layouts.auth')

@section('content')
    <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container-fluid">
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="mb-30">
                                    <h3 class="text-center txt-light mb-10">{{ __('Aanmelden bij') }} {{ config('app.name') }}</h3>
                                    <h6 class="text-center nonecase-font txt-light">{{ __('Vul uw gegevens in om u gratis aan te melden') }}</h6>
                                </div>
                                <div class="form-wrap">
                                    <form method="POST" action="{{ route('register') }}">
                                        @csrf

                                        <div class="form-group">
                                            <label class="control-label mb-5 txt-light" for="first_name">{{ __('Voornaam') }}</label>
                                            <input id="first_name" name="first_name" value="{{ old('first_name') }}" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" required="">

                                            @if ($errors->has('first_name'))
                                                <span class="invalid-feedback">
                                                {{ $errors->first('first_name') }}
                                            </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label mb-5 txt-light" for="last_name">{{ __('Achternaam') }}</label>
                                            <input id="last_name" name="last_name" value="{{ old('last_name') }}" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" required="">

                                            @if ($errors->has('last_name'))
                                                <span class="invalid-feedback">
                                                {{ $errors->first('last_name') }}
                                            </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label mb-5 txt-light" for="email">{{ __('E-mailadres') }}</label>
                                            <input id="email" name="email" type="email" value="{{ old('email') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required="">

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback">
                                                    {{ $errors->first('email') }}
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label mb-5 txt-light" for="password">{{ __('Wachtwoord') }}</label>
                                            <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required="" id="password">
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback">
                                                    {{ $errors->first('password') }}
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label mb-5 txt-light" for="password-confirm">{{ __('Herhaal wachtwoord') }}</label>
                                            <input type="password" name="password_confirmation" class="form-control" required="" id="password-confirm">
                                        </div>

                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary pr-10 pull-left">
                                                <input type="checkbox" id="terms" name="terms" value="true" required {{ (old('terms') ? 'checked' : '') }} />
                                                <label for="terms" class="txt-light"> {{ __('Bij het aanmelden ga ik akkoord met de algemene voorwaarden en de bijhorende verwerking van persoonsgegevens.') }}</label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-primary  mt-15">{{ __('Gratis aanmelden') }}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
