@if ($mandate->method === 'creditcard')
    <table class="table">
        <tr>
            <th style="border-top: 0; text-transform: uppercase; font-weight: 700; font-size: 13px;">
                {{ __('Creditcard') }}
            </th>
            @if (!isset($delete))
                <td style="text-align: right; border-top: 0;">
                    <a href="{{ route('account.cancel-mandate') }}" class="action action-button delete"><i class="fa fa-trash-alt"></i></a>
                </td>
            @endif
        </tr>
        <tr>
            <th style="text-transform: uppercase; font-weight: 400; font-size: 13px;">{{ __('Kaarthouder') }}</th>
            <td>{{ $mandate->details->cardHolder }}</td>
        </tr>
        <tr>
            <th style="text-transform: uppercase; font-weight: 400; font-size: 13px;">{{ __('Kaartsoort') }}</th>
            <td>{{ $mandate->details->cardLabel }}</td>
        </tr>
        <tr>
            <th style="text-transform: uppercase; font-weight: 400; font-size: 13px;">{{ __('Kaartnummer') }}</>
            <td>xxxx-xxxx-xxxx-{{ $mandate->details->cardNumber }}</td>
        </tr>
        <tr>
            <th style="text-transform: uppercase; font-weight: 400; font-size: 13px;">{{ __('Verloopdatum') }}</th>
            <td>{{ \Carbon\Carbon::parse($mandate->details->cardExpiryDate)->formatLocalized('%d %B %Y') }}</td>
        </tr>
        <tr>
            <th style="text-transform: uppercase; font-weight: 400; font-size: 13px;">{{ __('Datum machtiging') }}</th>
            <td>{{ \Carbon\Carbon::parse($mandate->signatureDate)->formatLocalized('%d %B %Y') }}</td>
        </tr>
    </table>
@elseif ($mandate->method === 'directdebit')
    <table class="table">
        <tr>
            <th style="border-top: 0; text-transform: uppercase; font-weight: 700; font-size: 13px;">
                {{ __('Automatische incasso') }}
            </th>
            @if (!isset($delete))
                <td style="text-align: right; border-top: 0;">
                    <a href="{{ route('account.cancel-mandate') }}" class="action action-button delete"><i class="fa fa-trash-alt"></i></a>
                </td>
            @endif
        </tr>
        <tr>
            <th style="text-transform: uppercase; font-weight: 400; font-size: 13px;">{{ __('Rekeninghouder') }}</th>
            <td>{{ $mandate->details->consumerName }}</td>
        </tr>
        <tr>
            <th style="text-transform: uppercase; font-weight: 400; font-size: 13px;">{{ __('IBAN') }}</th>
            <td>{{ $mandate->details->consumerAccount }}</td>
        </tr>
        <tr>
            <th style="text-transform: uppercase; font-weight: 400; font-size: 13px;">{{ __('BIC') }}</>
            <td>{{ $mandate->details->consumerBic }}</td>
        </tr>
        <tr>
            <th style="text-transform: uppercase; font-weight: 400; font-size: 13px;">{{ __('Datum machtiging') }}</th>
            <td>{{ \Carbon\Carbon::parse($mandate->signatureDate)->formatLocalized('%d %B %Y') }}</td>
        </tr>
    </table>
@elseif ($mandate->method === 'paypal')

@elseif ($mandate->method === 'invoice')
    <table class="table">
        <tr>
            <th colspan="2" style="border-top: 0; text-transform: uppercase; font-weight: 700; font-size: 13px;">
                {{ __('Betaling op factuur') }}
            </th>
            @if (!isset($delete))
                <td style="text-align: right; border-top: 0;">
                    <a href="{{ route('account.cancel-mandate') }}" class="action action-button delete"><i class="fa fa-trash-alt"></i></a>
                </td>
            @endif
        </tr>
    </table>
@endif