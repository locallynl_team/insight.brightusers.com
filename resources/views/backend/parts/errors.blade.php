@if ($errors->any())
    <div class="alert alert-danger" style="max-width: 800px;">
        <strong>{{ __('Controleer onderstaande meldingen en probeer het dan opnieuw:') }}</strong>
        <ul style="margin-top: 5px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif