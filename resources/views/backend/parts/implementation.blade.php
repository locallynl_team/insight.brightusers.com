{!! htmlspecialchars("<!-- ".__('Implementatiecode van Surve.nl')." -->
<script type=\"text/javascript\">
    (function(s,u,r,v,e) {
        var c=s.createElement(u);c.type='text/javascript';c.async=r;
        c.src='".route('request.implementation')."/'+v;
        e=s.getElementsByTagName(u)[0];e.parentNode.insertBefore(c,e);
    })(document,'script',true,".$site.");
</script>") !!}