@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Installeer Surve op :domain', ['domain' => $currentSite->domain]) }}</h5>
                <div class="form-wrap">
                    <form method="post" action="{{ route('site.implementation', $currentSite->id) }}">
                        @csrf
                        <p style="max-width: 800px; margin-bottom: 25px;">
                            {{ __('Om te beginnen met het gebruik van Surve plaatst u de volgende JavaScript code op uw website. Deze code plaatst u voor de afsluitende </body> tag in uw pagina.') }}
                        </p>

                        <p style="max-width: 800px;">
                            <a href="javascript:void(0);" onclick="copyToClipboard('javascript_code');" class="pull-right" style="font-weight: 500;">{{ __('Kopieer naar klembord') }}</a>
                        </p>
                        <div class="clearfix"></div>
                        <pre style="padding: 20px; max-width: 800px;" id="javascript_code">@include('backend.parts.implementation', ['site' => $currentSite->id])</pre>

                        <div class="form-group mb-0 mt-25">
                            <button class="btn btn-primary">{{ __('De Surve-code is geplaatst') }}</button>
                        </div>
                    </form>
                </div>

                <h5 class="mt-40 mb-20">{{ __('Heeft u hulp nodig met de installatie?') }}</h5>
                <p>{{ __('Lees in onze documentatie hoe u Surve installeert op de meest populaire platforms.') }}</p>
                <a href="{{ route('modal', ['type' => 'installation-instructions', 'id' => $currentSite->id]) }}" class="btn btn-default mt-10" data-toggle="modal">{{ __('Open documentatie') }}</a>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
    function copyToClipboard(elementId) {
        // Create a "hidden" input
        var aux = document.createElement("input");

        // Assign it the value of the specified element
        aux.setAttribute("value", document.getElementById(elementId).innerHTML);

        // Append it to the body
        document.body.appendChild(aux);

        // Highlight its content
        aux.select();

        // Copy the highlighted text
        document.execCommand("copy");

        // Remove it from the body
        document.body.removeChild(aux);
    }
    </script>
@endsection