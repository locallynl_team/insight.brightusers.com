@extends('backend.layouts.app')

@section('content')
<div class="container-fluid pt-25">
    <div class="row">
        <div class="col-md-12">
            <div class="row mb-30">
                <div class="col-md-6">
                    <h5>{{ __('Roadmap Surve') }}</h5>
                    <p>{{ __('Onze roadmap geeft u een overzicht van welke functionaliteiten u de komende periode kunt verwachten op Surve. Deze roadmap wordt regelmatig geüpdated met nieuwe ideeën. Heeft u zelf een goede toevoeging voor Surve? Laat het ons weten! Wellicht ziet u deze functie dan binnenkort op de roadmap.') }}</p>
                </div>
            </div>
            <ul class="timeline">
                @foreach ($milestones as $milestone)
                    <li {!! ($loop->iteration % 2 === 0 ? " class=\"timeline-inverted\"" : "") !!}>
                        <div class="timeline-badge bg-{{ ($milestone->status === true ? "green" : $colors[$loop->iteration % 4])  }}">
                            <i class="fa {{ $milestone->status === false ? "fa-calendar-alt" : "fa-check" }}"></i>
                        </div>
                        <div class="timeline-panel pa-30">
                            <div class="timeline-heading">
                                <h6 class="mb-15">{{ $milestone->date }}</h6>
                            </div>
                            <div class="timeline-body">
                                <p class="lead head-font mb-20">{{ $milestone->title }}</p>
                                <p>{{ $milestone->description }}</p>
                            </div>
                        </div>
                    </li>
                @endforeach
                <li class="clearfix no-float"></li>
            </ul>
        </div>
    </div>
</div>
@endsection