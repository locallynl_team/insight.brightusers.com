@extends('backend.layouts.app')

@section('content')

    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Website verwijderen') }}</h5>
                <div class="form-wrap">
                    @include('backend.parts.errors')

                    <form method="post" action="{{ route('site.delete', $site->id) }}">
                        @csrf

                        <p>{{ __('Weet u zeker dat u de website \':site\' wilt verwijderen? Dit kan niet ongedaan gemaakt worden.', ['site' => $site->domain]) }}</p>

                        <div class="form-group mb-0 mt-15">
                            <a href="{{ route('site.index') }}" class="btn btn-default mr-15">{{ __('Nee, ga terug') }}</a> <button type="submit" class="btn btn-danger">{{ __('Verwijder') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @endsection