@extends('backend.layouts.app')

@section('content')
<div class="container-fluid pt-25">
    <div class="row">
        <div class="col-md-12">
            <h5 class="mb-20">{{ __('Websites') }}</h5>
            <div class="table-wrap">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('ID') }}</th>
                            <th style="width: 25%;">{{ __('Domein') }}</th>
                            <th>{{ __('Pakket') }}</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($sites as $site)
                            <tr>
                                <td>{{ $site->id }}</td>
                                <td style="line-height: 200%;">
                                    <a href="{{ route('site.switch', $site->id) }}">{{ $site->domain }}</a>
                                    @if (Auth::user()->base_site_id === $site->id)
                                        <span class="txt-grey weight-300 block" style="font-size: 13px;">{{ __('Uw standaard website') }}</span>
                                    @endif
                                </td>
                                <td style="line-height: 200%;">
                                    <a href="{{ route('subscription.manage', $site->id) }}">
                                        @if ($site->active === false || $site->activeSubscription === null)
                                            <span class="label label-default label-package">{{ __('In-actief') }}</span>
                                            <span style="font-size: 13px; display: block;">{{ __('Website activeren') }}</span>
                                        @else
                                            <span class="label label-package" style="background-color: {{ $site->activeSubscription->package->color }};">{{ $site->activeSubscription->package->title }}</span>
                                            @if ($site->activeSubscription->trial === true)
                                                <span style="font-size: 13px; display: block;">{{ __('Trial eindigt op:') }} {{ $site->activeSubscription->ends_at->formatLocalized('%d %B %Y') }}</span>
                                            @elseif ($site->activeSubscription->cancelled_at <> null)
                                                <span style="font-size: 13px; display: block;">{{ __('Opgezegd per:') }} {{ $site->activeSubscription->ends_at->formatLocalized('%d %B %Y') }}</span>
                                            @else
                                                <span style="font-size: 13px; display: block;">{{ __('Verlenging op:') }} {{ $site->activeSubscription->ends_at->formatLocalized('%d %B %Y') }}</span>
                                            @endif
                                        @endif
                                    </a>
                                </td>
                                <td><a class="action" data-toggle="collapse" data-target="#code_site_{{ $site->id }}" href="javascript:void(0);">{{ __('Toon integratiecode') }} <i class="fa fa-caret-down ml-5"></i></a></td>
                                <td>
                                    <div class="pull-right">
                                        <a class="action action-button mr-10" href="{{ route('site.edit', $site->id) }}"><i class="fa fa-edit"></i></a>
                                        <a class="action action-button delete" href="{{ route('site.delete', $site->id) }}"><i class="fa fa-trash-alt"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr id="code_site_{{ $site->id }}" class="collapse">
                                <td colspan="8">
                                    {{ __('Voeg onderstaande code toe aan uw website, net voor uw </body> tag:') }}
                                    <pre class="code"><code>@include('backend.parts.implementation', ['site' => $site->id])</code></pre>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="8">{{ __('Voeg een website toe om te beginnen.') }}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <a class="btn btn-primary" href="{{ route('site.create') }}"><i class="fa fa-plus mr-10"></i> {{ __('Website toevoegen') }}</a>
        </div>
    </div>
</div>
@endsection