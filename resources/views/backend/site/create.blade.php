@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Nieuwe website') }}</h5>
                <div class="form-wrap" style="max-width: 800px;">
                    @include('backend.parts.errors')

                    <form method="post" action="{{ route('site.create') }}">
                        @csrf

                        <div class="form-group">
                            <label class="control-label mb-5 text-left" for="domain">{{ __('Domein') }}</label>
                            <input class="form-control" value="{{ old('domain') }}" id="domain" name="domain">
                            <span class="help-block mt-5">{{ __('http(s):// en subdomeinen kunt u weglaten, bijv. surve.nl (ook www. is niet nodig, maar mag wel)') }}</span>
                        </div>

                        <div class="form-group">
                            <label class="control-label mb-10" for="package">{{ __('Selecteer een pakket') }}</label>
                            <span class="pull-right"><a href="{{ route('packages') }}" target="_blank" class="compare-packages">{{ __('vergelijk pakketten') }}</a></span>
                            <div class="clearfix"></div>
                            @if (auth()->user()->had_trial === false)
                                <div class="trial">
                                    <i class="fa fa-info-circle"></i>
                                    {!! __('Dit is uw eerste website, u krijgt een <span>gratis 14-daagse trialperiode</span>, ongeacht welk pakket u kiest. Deze trialperiode <span>loopt automatisch af</span>. Uw pakket kunt u altijd aanpassen.') !!}
                                </div>
                            @endif
                            @foreach ($packages as $package)
                                <div id="package-{{ $package->id }}" class="package-card {{ (old('package', ($loop->iteration === 2 ? $package->id : -1)) == $package->id ? 'active' : '') }} {{ ($loop->last ? 'last' : '') }}" data-id="{{ $package->id }}" data-price-month="{{ round(auth()->user()->calculateDiscountPrice($package->price_monthly)) }}" data-price-year="{{ round(auth()->user()->calculateDiscountPrice($package->price_yearly)) }}">
                                    <i class="fa fa-check-circle"></i>
                                    <span class="title" style="color: {{ $package->color }}">{{ $package->title }}</span>
                                    <span class="description">{{ __('tot :views views per dag', ['views' => thousand($package->getValueByIdentifier('views'))]) }}</span>

                                    <span class="price-sign">&euro;</span>
                                    <span class="price">{{ round(auth()->user()->calculateDiscountPrice($package->price_yearly)) }}</span>
                                    <span class="price-period">{{ __('p/m') }}</span>

                                    <span class="period">&euro;{{ round(auth()->user()->calculateDiscountPrice($package->price_monthly)) }} {{ __('bij betaling p/m') }}</span>
                                </div>
                            @endforeach
                            <input id="current-package" type="hidden" name="package" value="{{ old('package', $packages[1]->id) }}" />
                            <div class="clearfix"></div>
                            <p style="max-width: 800px; margin-top: 10px; font-size: 13px;">
                                {{ __('Prijzen gelden bij jaarlijkse betaling en zijn exclusief 21% BTW.') }}
                                @if (auth()->user()->discount > 0)
                                    {{ __('Uw korting van :discount% is reeds verrekend.', ['discount' => auth()->user()->discount]) }}
                                @endif
                            </p>
                        </div>

                        @if (auth()->user()->had_trial === true)
                            <div class="form-group no-margin-switcher">
                                <input type="checkbox" class="js-switch" data-color="#4aa23c" name="pay_yearly" id="pay_yearly" {{ (old('pay_yearly') == true ? 'checked' : '') }} />
                                <label class="control-label text-left mb-10" for="pay_yearly">{!! __('Ik bespaar graag &euro;<span id="savings"></span>,00 en betaal jaarlijks &euro;<span id="yearly_costs"></span>,00.') !!}</label>
                            </div>
                        @endif

                        <div class="form-group no-margin-switcher">
                            <input type="checkbox" class="js-switch" data-color="#4aa23c" name="process_confirmation" id="process_confirmation" {{ (old('process_confirmation') == true ? 'checked' : '') }} />
                            <label class="control-label text-left mb-10" for="process_confirmation">{{ __('Ik verklaar dat dit mijn website is en ga akkoord met het verwerken van bezoekersgegevens.') }}</label>
                        </div>

                        @if (auth()->user()->had_trial === true && auth()->user()->hasActivePaymentMethod() === true && auth()->user()->pay_on_invoice === false)
                            <div class="form-group no-margin-switcher">
                                <input type="checkbox" class="js-switch" data-color="#4aa23c" name="payment_confirmation" id="payment_confirmation" {{ (old('payment_confirmation') == true ? 'checked' : '') }} />
                                <label class="control-label text-left mb-10" for="payment_confirmation">{{ __('Ik geef toestemming om de betaling automatisch uit te voeren.') }}</label>
                            </div>
                        @endif

                        <div class="form-group mb-15 mt-25">
                            @if (auth()->user()->had_trial === true)
                                <button class="btn btn-primary">{{ __('Website toevoegen') }}</button>
                            @else
                                <button class="btn btn-primary">{{ __('Gratis 14-daagse trial starten') }}</button>
                            @endif
                        </div>

                        @if (auth()->user()->had_trial === true)
                            <p style="font-size: 13px;">
                                @if (auth()->user()->hasActivePaymentMethod() === true && auth()->user()->pay_on_invoice === false)
                                    {{ __('Voor uw betaling maken wij gebruik van de door u opgegeven betaalmethode: :paymentmethod.', ['paymentmethod' => \App\User::getPaymentMethodString(auth()->user()->getActiveMandate())]) }}
                                    {!! __('U kunt uw betaalmethode aanpassen bij <a href=":route">betaalmethode</a>', ['route' => route('account.payment-method')]) !!}
                                @elseif (auth()->user()->pay_on_invoice === true)
                                    {{ __('Voor uw betaling ontvangt u van ons een factuur, uw pakket wordt direct geactiveerd.') }}
                                @else
                                    {{ __('U heeft geen actieve betaalmethode, deze kunt u in de volgende stap selecteren.') }}
                                @endif
                            </p>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            //calculate savings
            var activeCard = $('.package-card.active');
            var savings = (activeCard.data('price-month')*12)-(activeCard.data('price-year')*12);
            $("#savings").html(savings);
            $("#yearly_costs").html(parseInt(activeCard.data('price-year') * 12));

            $('.package-card').on('click', function() {
                var currentPackage = $('#current-package');
                var newPackage = parseInt($(this).data('id'));

                //change classes
                $('.package-card.active').removeClass('active');
                $('#package-' + newPackage).addClass('active');
                currentPackage.val(newPackage);

                //calculate savings
                var savings = ($(this).data('price-month')*12)-($(this).data('price-year')*12);
                $("#savings").html(savings);
                $("#yearly_costs").html(parseInt($(this).data('price-year') * 12));
            });
        });
    </script>
    @if (session()->has('welcome'))
        <script>
            //load modal content
            $('#info-modal .modal-content').load('{{ route('modal', 'welcome') }}');
            $('#info-modal').modal();
        </script>
    @endif
@endsection