@extends('backend.layouts.app')

@section('content')

    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Website wijzigen') }}</h5>

                <div class="form-wrap">
                    @include('backend.parts.errors')

                    <form method="post" action="{{ route('site.edit', $site->id) }}">
                        @csrf

                        <div class="form-group">
                            <label class="control-label mb-5 text-left" for="domain">{{ __('Domein') }}</label>
                            <input class="form-control" value="{{ old('domain', $site->domain) }}" id="domain" name="domain">
                            <span class="help-block mt-5">{{ __('http(s):// en subdomeinen kunt u weglaten, bijv. surve.nl (ook www. is niet nodig, maar mag wel)') }}</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-5 text-left" for="test_domains">{{ __('Test-domeinen') }}</label>
                            <input class="form-control" value="{{ old('test_domains', $site->test_domains) }}" id="test_domains" name="test_domains">
                            <span class="help-block mt-5">{{ __('Tot twee extra domeinnamen voor uw testomgeving kunt u hier opgeven, gescheiden met een komma') }}</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-5" for="exclude_ip_addresses">{{ __('IP-adressen uitsluiten') }}</label>
                            <textarea style="height: {{ (80 + (count(explode("\n", $site->exclude_ip_addresses))*20)) }}px;" name="exclude_ip_addresses" class="form-control" id="exclude_ip_addresses">{{ old('exclude_ip_addresses', $site->exclude_ip_addresses) }}</textarea>
                            <span class="help-block mb-10 mt-5">{{ __('Geef IP-adressen op die uitgesloten moeten worden bij het verzamelen van gegevens voor de heatmaps. IPv6 en IPv4 worden ondersteund, gebruik een aparte regel voor elk IP-adres.') }}</span>
                            <span class="help-block mt-5">{{ __('Uw huidige IP-adres is:') }} {{ request()->ip() }}</span>
                        </div>
                        @if ($site->id !== Auth::user()->base_site_id)
                            <div class="form-group">
                                <label class="control-label mb-0 text-left block" for="base_site">{{ __('Instellen als standaard') }}</label>
                                <input type="checkbox" class="js-switch" data-color="#4aa23c" name="base_site" id="base_site" />
                            </div>
                        @endif
                        <div class="form-group mb-0 mt-25">
                            <button type="submit" class="btn btn-primary">{{ __('Website wijzigen') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @endsection