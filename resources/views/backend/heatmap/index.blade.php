@extends('backend.layouts.app')

@section('content')
<div class="container-fluid pt-25">
    <div class="row">
        <div class="col-md-12">
            <h5 class="mb-20">{{ __('Heatmaps') }}</h5>
            @if (count($heatmaps) > 0)
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>{{ __('ID') }}</th>
                                <th>{{ __('Naam') }}</th>
                                <th>{{ __('Locaties') }}</th>
                                <th>{{ __('Sessies') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($heatmaps as $heatmap)
                                <tr>
                                    <td>{{ $heatmap->id }}</td>
                                    <td>
                                        <a href="{{ route('heatmap.edit', $heatmap->id) }}">{{ $heatmap->title }}</a><br />
                                    </td>
                                    <td>
                                        <a href="{{ route('heatmap.view', $heatmap->id) }}">{{ $urlTargets[$heatmap->url_target_type]->getSettings()['title'] }}: {{ $heatmap->url_target }}</a>
                                    </td>
                                    <td>
                                        {{ __(':count/:max', ['count' => thousand($heatmap->sessions_count), 'max' => thousand(\App\SubscriptionManager\SubscriptionManager::instance()->option('max_heatmap_views')->allowed_usage)]) }}
                                        <a href="{{ route('modal', 'heatmap-sessions') }}" data-toggle="modal"><i class="fa fa-info-circle ml-5"></i></a>
                                    </td>
                                    <td><a href="{{ route('heatmap.toggle', $heatmap->id) }}">
                                            @if ($heatmap->status === true)
                                                <span class="label label-primary">{{ __('Actief') }}</span>
                                            @else
                                                <span class="label label-default">{{ __('In-actief') }}</span>
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <div class="pull-right">
                                            @if ($heatmap->sessions_count > 0)
                                                <a class="action action-button mr-10" href="{{ route('heatmap.view', $heatmap->id) }}"><i class="fa fa-fire"></i></a>
                                            @else
                                                <a class="action action-button mr-10" data-toggle="modal" href="{{ route('modal', 'heatmap-has-no-views') }}"><i class="fa fa-fire"></i></a>
                                            @endif
                                            <a class="action action-button mr-10" href="{{ route('heatmap.edit', $heatmap->id) }}"><i class="fa fa-edit"></i></a>
                                            <a class="action action-button delete" data-toggle="modal" href="{{ route('modal', ['name' => 'heatmap-delete', 'id' => $heatmap->id]) }}"><i class="fa fa-trash-alt"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">{{ __('Voeg een heatmap toe om te beginnen.') }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <div style="max-width: 800px;">
                    <p class="mb-20">
                        {{ __('Heatmaps geven u waardevolle insights in hoe bezoekers uw website gebruiken. Elke heatmap verzameld data om 3 soorten heatmaps te genereren. Heatmaps voor kliks, muisbewegingen en scrolldiepte.') }}
                        {{ __('Voeg een heatmap toe voor een enkele pagina, of bijvoorbeeld al uw productpagina\'s tegelijk.') }}
                    </p>
                    <p style="text-align: center;">
                        <a class="btn btn-primary" href="{{ route('heatmap.create') }}">{{ __('Beginnen met heatmaps') }} <i class="fa fa-fire ml-10"></i> </a>
                    </p>

                    <div class="row mb-30 mt-30" style="background: #f6f6f6; padding: 15px 0;">
                        <div class="col-md-6 col-xs-12 pull-right">
                            <img src="/assets/backend/images/icons/click-heatmaps.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                            <p class="visible-xs">&nbsp;</p>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <h5>{{ __('Klik heatmaps') }}</h5>
                            <p class="mb-10">{{ __('Klikken uw bezoekers op uw call-to-action? Of klikken ze op die afbeelding... die niet klikbaar is. Met de klikheatmap ziet u gelijk of uw pagina door uw bezoekers gebruikt wordt zoals u dat verwacht.') }}</p>
                        </div>
                    </div>
                    <div class="row mb-30">
                        <div class="col-md-6 col-xs-12 pull-left">
                            <img src="/assets/backend/images/icons/move-heatmaps.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                            <p class="visible-xs">&nbsp;</p>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <h5>{{ __('Muisbewegingen heatmaps') }}</h5>
                            <p class="mb-10">{{ __('Websitebezoekers volgen vaak met de muis hun ogen. Met een heatmap van alle muisbewegingen ziet u dus snel welke content op uw pagina populair is, en welke niet. Zo stuurt u eenvoudig de inrichting van uw pagina naar de meest ideale variant.') }}</p>
                        </div>
                    </div>
                    <div class="row mb-30" style="background: #f6f6f6; padding: 15px 0;">
                        <div class="col-md-6 col-xs-12 pull-right">
                            <img src="/assets/backend/images/icons/scroll-heatmaps.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                            <p class="visible-xs">&nbsp;</p>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <h5>{{ __('Scroll heatmaps') }}</h5>
                            <p class="mb-10">{{ __('Komen uw bezoekers wel tot die belangrijke call-to-action, of moet u deze toch iets hoger plaatsen... Het antwoord op deze vraag wordt u gegeven door de scrollheatmap. Zie voor alle devices tot hoever een bezoeker naar beneden scrollt, dat maakt optimaliseren makkelijk.') }}</p>
                        </div>
                    </div>
                </div>
            @endif
            <a class="btn btn-primary" href="{{ route('heatmap.create') }}"><i class="fa fa-plus mr-10"></i> {{ __('Heatmap toevoegen') }}</a>
        </div>
    </div>
</div>
@endsection