@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Nieuwe heatmap') }}</h5>
                <div class="form-wrap">
                    @include('backend.parts.errors')

                    <form method="post" action="{{ route('heatmap.create') }}">
                        @csrf

                        <div class="form-group">
                            <label class="control-label mb-5 text-left" for="title">{{ __('Titel') }}</label>
                            <input class="form-control" value="{{ old('title') }}" id="title" name="title">
                        </div>
                        <div class="form-group">
                            <label class="block control-label mb-5 text-left">{{ __('Verzamel locaties') }}</label>
                            <div class="input-group mb-15">
                                <div class="input-group-btn">
                                    <input type="hidden" id="url_target_type" name="url_target_type" value="{{ old('url_target_type', 'simple') }}" />
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span id="url_target_text">{{ $urlTargets[old('url_target_type', 'simple')]->getSettings()['title'] }}</span> <span class="caret"></span></button>
                                    <ul class="dropdown-menu info-buttons">
                                        @foreach ($urlTargets as $type => $urlTarget)
                                            <li>
                                                <a href="javascript:void(0)" class="url-target-link" data-value="{{ $type }}" data-text="{{ $urlTarget->getSettings()['title'] }}">
                                                    {{ $urlTarget->getSettings()['title'] }}
                                                </a>
                                                <a href="{{ route('modal', 'url-targets') }}" data-toggle="modal" class="info-icon"><i class="fa fa-info-circle"></i></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <input type="text" id="url_target" name="url_target" value="{{ old('url_target') }}" class="form-control" placeholder="{{ __('bijv. https://www.voorbeeld.nl') }}">
                            </div>
                        </div>

                        <div class="form-group no-margin-switcher">
                            <label class="block control-label mb-5 text-left" for="name">{{ __('Platforms') }}</label>
                            <label class="block mb-10" @if (hasPermission('desktop_platform') === false) style="color: #8f8d8d;" @else style="color: #555;" @endif>
                                <input class="js-switch" data-color="#4aa23c" type="checkbox" name="platforms[]" value="desktop"
                                    @if (hasPermission('desktop_platform') === false)
                                        disabled
                                    @endif
                                    @if (is_array(old('platforms')) && in_array('desktop', old('platforms')))
                                        checked
                                    @endif  /> {{ __('Desktop') }}
                                @if (hasPermission('desktop_platform') === false) @include('backend.parts.upgrade') @endif
                            </label>
                            <label class="block mb-10" @if (hasPermission('tablet_platform') === false) style="color: #8f8d8d;" @else style="color: #555;" @endif>
                                <input class="js-switch js-mobile-platform" data-color="#4aa23c" type="checkbox" name="platforms[]" value="tablet"
                                    @if (hasPermission('tablet_platform') === false)
                                        disabled
                                    @endif
                                    @if (is_array(old('platforms')) && in_array('tablet', old('platforms')))
                                        checked
                                    @endif  /> {{ __('Tablet') }}
                                @if (hasPermission('tablet_platform') === false) @include('backend.parts.upgrade') @endif
                            </label>
                            <label class="block" @if (hasPermission('mobile_platform') === false) style="color: #8f8d8d;" @else style="color: #555;" @endif>
                                <input class="js-switch js-mobile-platform" data-color="#4aa23c" type="checkbox" name="platforms[]" value="mobile"
                                    @if (hasPermission('mobile_platform') === false)
                                        disabled
                                    @endif
                                    @if (is_array(old('platforms')) && in_array('mobile', old('platforms')))
                                        checked
                                    @endif />
                                {{ __('Mobiel') }}
                                @if (hasPermission('mobile_platform') === false) @include('backend.parts.upgrade') @endif
                            </label>
                        </div>

                        <div class="form-group">
                            <label class="control-label mb-5 text-left" for="static_screenshot_url">{{ __('Screenshot URL (optioneel)') }}</label>
                            <input class="form-control" value="{{ old('static_screenshot_url') }}" id="static_screenshot_url" name="static_screenshot_url">
                            <span class="block mt-5" style="font-size: 13px;">{{ __('Standaard wordt er een screenshot gemaakt van de eerst gevonden URL die voldoet aan de gestelde voorwaarden. Om zelf een URL voor het screenshot te kiezen kunt u deze hier opgeven.') }}</span>
                        </div>

                        <div class="form-group mb-0 mt-15">
                            <button class="btn btn-primary">{{ __('Toevoegen') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.url-target-link').on('click', function() {
            $("#url_target_text").html($(this).data('text'));
            $("#url_target_type").val($(this).data('value'));
        });
    </script>
@endsection