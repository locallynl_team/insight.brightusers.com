@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <form method="get" action="{{ route('heatmap.view', $heatmap->id) }}">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="panel panel-default card-view pa-0">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body" style="padding: 8px 0;">
                                        <label class="control-label col-md-4" style="line-height: 40px;" for="period">{{ __('Periode') }}:</label>
                                        <div class="col-md-8">
                                        <span class="input-group">
                                            <div id="reportrange" style="color: #212121; background: #fff; cursor: pointer; line-height: 42px; padding: 0px 12px; height: 42px; border: 1px solid rgba(33, 33, 33, 0.12); width: 100%">
                                                <i class="fa fa-calendar mr-5"></i>&nbsp;
                                                <span></span> <i class="fa fa-caret-down ml-5"></i>

                                                <input type="hidden" name="period_start" />
                                                <input type="hidden" name="period_end" />
                                            </div>
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary ml-10">{{ __('Filter') }}</button>
                                            </span>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="panel panel-default card-view pa-0">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body" style="padding: 8px 0;">
                                        <label class="control-label col-md-4" style="line-height: 40px;" for="period">{{ __('Type apparaat') }}:</label>
                                        <div class="col-md-8">
                                            <span class="input-group">
                                                <select class="form-control" id="heatmap_profile" name="heatmap_profile">
                                                    @foreach ($profiles as $profile)
                                                        <option value="{{ $profile->id }}" @if ($selectedProfile == $profile->id) selected @endif {{ ($profile->session_count === 0 ? "disabled" : "") }}>
                                                            {{ $profile->title }} - {{ ($profile->session_count === 0 ? __("geen data beschikbaar") : trans_choice(":count sessie|:count sessies", thousand($profile->session_count), ['count' => $profile->session_count])) }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-primary ml-10">{{ __('Tonen') }}</button>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="panel panel-default card-view pa-0">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body" style="padding: 8px 0;">
                                        <label class="control-label col-md-4" style="line-height: 40px;" for="period">{{ __('Type heatmap') }}:</label>
                                        <div class="col-md-8">
                                            <span class="input-group">
                                                <div class="checkbox checkbox-success checkbox-circle" style="padding-left: 0; margin-top: 2px; float: left;">
													<input id="heatmap_type_click" value="clicks" type="radio" name="heatmap_type" {{ ($selectedType === 'clicks' ? "checked" : "") }}>
													<label for="heatmap_type_click" style="padding-left: 5px;"> {{ __('Kliks') }} </label>
												</div>

                                                 <div class="checkbox checkbox-success checkbox-circle" style="padding-left: 20px; margin-top: 2px; float: left;">
													<input id="heatmap_type_moves" value="moves" type="radio" name="heatmap_type" {{ ($selectedType === 'moves' ? "checked" : "") }}>
													<label for="heatmap_type_moves" style="padding-left: 5px;"> {{ __('Bewegingen') }} </label>
												</div>

                                                <div class="checkbox checkbox-success checkbox-circle" style="padding-left: 20px; margin-top: 2px; float: left;">
													<input id="heatmap_type_scroll" value="scroll" type="radio" name="heatmap_type" {{ ($selectedType === 'scroll' ? "checked" : "") }}>
													<label for="heatmap_type_scroll" style="padding-left: 5px;"> {{ __('Scroll') }} </label>
												</div>

                                                <span class="input-group-btn">
                                                    <button class="btn btn-primary ml-10">{{ __('Tonen') }}</button>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body pa-0">
                                    <div class="sm-data-box">
                                        <div class="container-fluid">
                                            <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($totalSessionCount) }}</span></span>
                                                <span class="weight-500 uppercase-font block font-13">{{ __('totaal sessies') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body pa-0">
                                    <div class="sm-data-box">
                                        <div class="container-fluid">
                                            <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($currentSessionCount) }}</span></span>
                                                <span class="weight-500 uppercase-font block font-13">{{ __('sessies in selectie') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body pa-0">
                                    <div class="sm-data-box">
                                        <div class="container-fluid">
                                            <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($heatmap->days_active) }}</span></span>
                                                <span class="weight-500 uppercase-font block font-13">{{ __('dagen actief') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body pa-0">
                                    <div class="sm-data-box">
                                        <div class="container-fluid">
                                            <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="{{ ($heatmap->status === true ? 'txt-primary' : 'txt-danger') }} block counter">{{ ($heatmap->status === true ? __('actief') : __('niet actief')) }}</span>
                                                <span class="weight-500 uppercase-font block font-13">{{ __('huidige status') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
        <div class="row">
            <div class="col-md-12" style="padding-top: 20px;">
                @if ($heatmapScreenshot !== null && $currentSessionCount > 0)
                    <span class="pull-right"><a href="{{ route('heatmap.reset', $heatmap->id) }}" class="block btn btn-small btn-danger ml-10 mb-15"><span class="fa fa-trash-alt mr-10"></span> {{ __('Reset heatmap sessies') }}</a></span>
                    <span class="pull-right"><a href="{{ route('heatmap.refresh', [$heatmap->id, $selectedProfile]) }}" class="block btn btn-small btn-primary mb-15"><span class="fa fa-sync mr-10"></span> {{ __('Vernieuw screenshot') }}</a></span>
                @endif
                <div class="clearfix"></div>

                @if ($currentSessionCount === 0 || $heatmapScreenshot === null)
                    <div class="alert alert-success">{{ $message }}</div>
                @else
                    <div id="heatmap_fg" style="margin: 0 auto; height: {{ $heatmapScreenshot->screen_height }}px; width: {{ $heatmapScreenshot->screen_width }}px;"></div>
                    <div id="heatmap_bg" style="position: relative; margin: 0 auto; border: 1px solid #555; background: linear-gradient( rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url('{{ $heatmapScreenshot->url }}') no-repeat; width: {{ $heatmapScreenshot->screen_width }}px; background-size: 100%;  max-width: 100%;">
                        @if ($averageFold && $averageFold > 0 && $averageFold < $heatmapScreenshot->screen_height)
                            <div style="position: absolute; border-top: 2px solid #000; width: 100%; top: {{ (($averageFold / $heatmapScreenshot->screen_height) * 100) }}%;">
                                <div style="position: relative; display: inline-block; padding: 5px 10px; top: -31px; background: rgba(0,0,0,0.5); color: #fff; font-size: 13px;">
                                    {{ __('Gemiddelde fold') }}
                                </div>
                            </div>
                        @endif

                        @if ($selectedType === 'scroll')
                            <div id="scroll_percentage_line" style="position: absolute; text-align: center; border-top: 2px solid #000; width: 100%; top: 32px;">
                                <div style="position: relative; display: inline-block; padding: 5px 10px; top: -32px; background: rgba(0,0,0,0.5); color: #fff; font-size: 13px;">
                                    <span id="scroll_percentage">100</span>% {{ __('van uw bezoekers kwam hier') }}
                                </div>
                            </div>
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/assets/backend/js/moment-with-locales.min.js"></script>
    <script src="/assets/backend/js/daterangepicker.min.js"></script>
    <script>
        $(function() {
            @if ($periodStart !== null && $periodEnd !== null)
                var start = moment('{{ $periodStart }}');
                var end = moment('{{ $periodEnd }}');
            @else
                var start = moment().subtract(365, 'days');
                var end = moment();
            @endif

            function cb(start, end) {
                $('#reportrange span').html(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));

                $('input[name=period_start]').each(function() {
                    $(this).val(start.format('YYYY-MM-DD'));
                });
                $('input[name=period_end]').each(function() {
                    $(this).val(end.format('YYYY-MM-DD'));
                });
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                autoApply: true,
                ranges: {
                    'Vandaag': [moment(), moment()],
                    'Gisteren': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Laatste 7 dagen': [moment().subtract(6, 'days'), moment()],
                    'Laatste 30 dagen': [moment().subtract(29, 'days'), moment()],
                    'Deze maand': [moment().startOf('month'), moment().endOf('month')],
                    'Vorige maand': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                locale: {
                    applyLabel: "Toepassen",
                    cancelLabel: "Terug",
                    customRangeLabel: "Aangepaste periode",
                    daysOfWeek: [
                        "Zo",
                        "Ma",
                        "Di",
                        "Wo",
                        "Do",
                        "Vr",
                        "Za"
                    ],
                    monthNames: [
                        "Januari",
                        "Februari",
                        "Maart",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Augustus",
                        "September",
                        "Oktober",
                        "November",
                        "December"
                    ]
                }
            }, cb);

            cb(start, end);
        });
    </script>

    @if ($currentSessionCount > 0 && $heatmapScreenshot !== null && $selectedType <> 'scroll')
        <script src="/assets/backend/js/heatmap.js"></script>

        <script>
            var heatmapData = {!! $heatmapData !!};
            var heatmap = undefined;
            $(document).ready(function() {
                heatmap = h337.create({
                    container: document.getElementById("heatmap_fg"),
                    radius: 10,
                    maxOpacity: .7,
                    minOpacity: 0.0,
                    blur: .9
                });

                heatmap.setData({
                    max: 1,
                    data: heatmapData
                });

                var heatmapImg = $(".heatmap-canvas")[0].toDataURL();

                // scale and redraw the canvas content
                var e = document.createElement("img");
                e.classList.add('img-responsive');
                e.src = heatmapImg;

                $("#heatmap_bg").append(e);
                $("#heatmap_fg").remove();
            });
        </script>
    @endif

    @if ($selectedType === 'scroll' && $heatmapScreenshot !== null)
        <script>
            function getXY(evt, element) {
                var rect = element.getBoundingClientRect();
                var scrollTop = document.documentElement.scrollTop ?
                    document.documentElement.scrollTop:document.body.scrollTop;
                var scrollLeft = document.documentElement.scrollLeft ?
                    document.documentElement.scrollLeft:document.body.scrollLeft;
                var elementLeft = rect.left+scrollLeft;
                var elementTop = rect.top+scrollTop;

                var x = evt.pageX - elementLeft;
                var y = evt.pageY - elementTop;

                return {x:x, y:y};
            }

            var scrollPercentages = {!! json_encode($scrollDepthData['pixels']) !!};

            $(document).ready(function() {
                var heatmapImg = "data:image/png;base64,{{ $scrollDepthData['image'] }}";

                // scale and redraw the canvas content
                var e = document.createElement("img");
                e.classList.add('img-responsive');
                e.src = heatmapImg;

                $('#heatmap_bg').append(e);
                $('#heatmap_fg').remove();

                //add scrolling line
                $('#heatmap_bg').on('mousemove', function(e) {
                    var m = getXY(e, this);
                    var percentage = 100;
                    var next = false;
                    var heightPercentage = (($(this).height() / {{ $heatmapScreenshot->screen_height }}) * 100);
                    console.log({heightPercentage});

                    Object.keys(scrollPercentages).forEach(function eachKey(key) {
                        if (next === true) {
                            percentage = scrollPercentages[key];
                        }
                        next = ((m.y * (100 / heightPercentage)) >= key);
                    });

                    $('#scroll_percentage').html(percentage);
                    $("#scroll_percentage_line").css('top', m.y + 'px');
                });
            });
        </script>
    @endif
@endsection