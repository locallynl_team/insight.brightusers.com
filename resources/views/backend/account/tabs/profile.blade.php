<div class="row">
    <div class="col-lg-12">
        <div class="panel-wrapper collapse in">
            <div class="panel-body pa-0">
                <div class="col-sm-12 col-xs-12">
                    <div class="form-wrap">
                        <form action="{{ route('account.update.profile') }}#profile" method="post">
                            @csrf

                            @include('backend.parts.errors', ['errors' => $errors->profile])

                            <div class="form-body overflow-hide">
                                <h5>{{ __('Persoonlijke informatie') }}</h5>
                                <div class="form-group">
                                    <label class="control-label mb-5" for="first_name">{{ __('Voornaam') }}</label>
                                    <input name="first_name" class="form-control" value="{{ old('first_name', $user->first_name) }}" id="first_name">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-5" for="last_name">{{ __('Achternaam') }}</label>
                                    <input name="last_name" class="form-control" value="{{ old('last_name', $user->last_name) }}" id="last_name">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-5" for="email">{{ __('E-mailadres') }}</label>
                                    <input name="email" class="form-control" value="{{ old('email', $user->email) }}" id="email">
                                </div>

                                <h5 class="mt-30">{{ __('Wachtwoord wijzigen') }}</h5>
                                <p>{{ __('Alleen invullen wanneer u uw wachtwoord wilt wijzigen:') }}</p>
                                <div class="form-group">
                                    <label class="control-label mb-5" for="password_old">{{ __('Huidige wachtwoord') }}</label>
                                    <input type="password" class="form-control" id="password_old" name="password_old">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-5" for="password">{{ __('Nieuw wachtwoord') }}</label>
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-5" for="password_confirmation">{{ __('Bevestig wachtwoord') }}</label>
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                                </div>
                                <div class="form-group mb-0 mt-25">
                                    <button type="submit" class="btn btn-primary">{{ __('Gegevens aanpassen') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>