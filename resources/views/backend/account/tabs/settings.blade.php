<div class="row">
    <div class="col-lg-12">
        <div class="panel-wrapper collapse in">
            <div class="panel-body pa-0">
                <div class="col-sm-12 col-xs-12">
                    <div class="form-wrap">
                        <form action="{{ route('account.update.settings') }}#settings" method="post">
                            @csrf

                            @include('backend.parts.errors', ['errors' => $errors->settings])

                            <div class="form-body overflow-hide">
                                <h5 class="mb-10">{{ __('Account instellingen') }}</h5>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exclude_ip_addresses">{{ __('IP-adressen uitsluiten') }}</label>
                                    <textarea style="height: {{ (80 + (count(explode("\n", $user->exclude_ip_addresses))*20)) }}px;" name="exclude_ip_addresses" class="form-control" id="exclude_ip_addresses">{{ old('exclude_ip_addresses', $user->exclude_ip_addresses) }}</textarea>
                                    <span class="block mb-10 mt-5">{{ __('geef IP-adressen op die uitgesloten moeten worden bij het verzamelen van gegevens voor de heatmaps. IPv6 en IPv4 worden ondersteund, gebruik een aparte regel voor elk IP-adres.') }}</span>
                                    <span class="block mb-20 mt-5"><strong>{{ __('Uw huidige IP-adres is:') }}</strong> {{ request()->ip() }}</span>
                                </div>

                                <div class="form-actions mt-10">
                                    <button class="btn btn-success mr-10 mb-30">{{ __('Instellingen opslaan') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>