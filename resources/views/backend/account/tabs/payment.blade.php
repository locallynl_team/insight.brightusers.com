<div class="row">
    <div class="col-lg-12">
        <div class="panel-wrapper collapse in">
            <div class="panel-body pa-0">
                <div class="col-sm-12 col-xs-12">
                    <div class="form-wrap">
                        <form action="{{ route('account.update.paymentDetails') }}#payment" method="post">
                            @csrf

                            @include('backend.parts.errors', ['errors' => $errors->paymentDetails])

                            <div class="form-body overflow-hide">
                                <h5 class="mb-10">{{ __('Factuurgegevens') }}</h5>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="company_name">{{ __('Bedrijfsnaam') }}</label>
                                    <input name="company_name" class="form-control" value="{{ old('company_name', $user->company_name) }}" id="company_name">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="address">{{ __('Adres') }}</label>
                                    <input name="address" class="form-control" value="{{ old('address', $user->address) }}" id="address">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="postal_code">{{ __('Postcode') }}</label>
                                    <input name="postal_code" class="form-control" value="{{ old('postal_code', $user->postal_code) }}" id="postal_code">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="city">{{ __('Plaats') }}</label>
                                    <input name="city" class="form-control" value="{{ old('city', $user->city) }}" id="city">
                                </div>

                                <div class="form-actions mt-10">
                                    <button class="btn btn-success mr-10 mb-30">{{ __('Gegevens aanpassen') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>