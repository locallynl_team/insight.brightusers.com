<div class="row">
    <div class="col-lg-12">
        <div class="col-md-12">
            <h5>{{ __('Uw betalingen') }}</h5>
        </div>
        <div class="clearfix"></div>
        <div class="table-wrap">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>{{ __('Pakket') }}</th>
                        <th>{{ __('Datum') }}</th>
                        <th>{{ __('Bedrag') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th>{{ __('Factuur') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($payments as $payment)
                        <tr>
                            <td>
                                {{ $payment->package->title }}
                                <br />
                                <span style="font-size: 13px;">{{ __('Periode: :period', ['period' => $payment->package->period($payment->period)['text']]) }}</span>
                            </td>
                            <td>
                                {{ $payment->created_at->formatLocalized('%d %B %Y') }}
                                @if ($payment->status != 'paid')
                                    <br />
                                    <span style="font-size: 12px;">{{ __('te  voldoen binnen :days dagen', ['days' => (30 - $payment->created_at->diffInDays(\Carbon\Carbon::now(), false))]) }}</span>
                                @endif
                            </td>
                            <td>&euro;{{ price($payment->total_amount) }}</td>
                            <td><span class="label label-{{ ($payment->status === 'paid' ? 'success' : 'danger') }}">{{ ($payment->status == 'paid' ? 'betaald' : 'open') }}</span></td>
                            <td>
                                @if ($payment->invoice && $payment->invoice->generated === true)
                                    <a class="btn btn-sm btn-info" href="{{ route('invoice', $payment->invoice->id) }}" target="_blank"><i class="fa fa-file"></i> <span class="hidden-xs ml-10">{{ __('Download') }}</span></a>
                                @elseif ($payment->invoice)
                                    <a class="btn btn-sm btn-info"><i class="fa fa-spinner fa-spin"></i> <span class="hidden-xs ml-10">{{ __('Wordt gegenereerd') }}</span></a>
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">{{ __('Geen facturen beschikbaar') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>