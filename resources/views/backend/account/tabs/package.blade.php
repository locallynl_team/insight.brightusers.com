<div class="row">
    <div class="col-lg-12">
        <div class="col-md-12">
            @include('backend.parts.errors', ['errors' => $errors->package])

            <h5>{{ __('Selecteer een pakket') }}</h5>
            <p class="mb-25">{{ __('U kunt uw pakket op elk moment omhoog of omlaag aanpassen. Wanneer onze standaard pakketten niet voldoen aan uw wensen kunt u contact opnemen met de helpdesk.') }}</p>
        </div>
        @foreach ($packages as $package)
            <div class="col-lg-4 col-md-4 col-sm-12 text-center mb-30">
                <div class="panel panel-pricing mb-0">
                    <form method="post" action="{{ route('account.order', $package->id) }}">
                        @csrf

                        <div class="panel-heading">
                            <span class="panel-price">
                                {{ $package->title }}
                            </span>
                            <span class="block period">&euro;{{ __(':price per maand', ['price' => round($user->calculateDiscountPrice($package->price))]) }}</span>
                        </div>
                        <div class="panel-body text-center pl-0 pr-0 mt-0">
                            <hr class="mb-30"/>
                            <ul class="list-group mb-0 text-center">
                                @foreach ($package->packageItems->sortBy('packageOption.sort_order') as $packageItem)
                                    @if ($packageItem->packageOption->visible === true)
                                        <li class="list-group-item">
                                            <span class="pull-left">{{ $packageItem->packageOption->title }}</span>
                                            @if ($packageItem->packageOption->type == 'integer')
                                                <span class="pull-right"><strong>{{ number_format($packageItem->amount, 0, "", ".") }}</strong></span>
                                            @elseif ($packageItem->packageOption->type == 'boolean')
                                                <span class="pull-right"><strong>{{ $packageItem->amount === 1 ? __('Ja') : __('Nee') }}</strong></span>
                                            @elseif ($packageItem->packageOption->type == 'time_period')
                                                <span class="pull-right"><strong>{{ __(':months maanden', ['months' => $packageItem->amount]) }}</strong></span>
                                            @endif
                                            <div class="clearfix"></div>
                                        </li>
                                        @if ($loop->last === false)
                                            <li><hr class="mt-5 mb-5"/></li>
                                        @endif
                                    @endif
                                @endforeach
                            </ul>

                            <hr class="mb-30 mt-30" />

                            <strong>Uw betaalperiode</strong>
                            <ul class="list-group mb-0 text-center">
                                @foreach ($package->periods as $key => $period)
                                     <li onclick="$('#{{ $package->id }}-{{ $key }}').prop('checked', true);" class="list-group-item{{ ($subscription && $subscription->package_id === $package->id && $subscription->period === $key ? ' active' : '') }}">
                                         <label class="pull-left" for="{{ $package->id }}-{{ $key }}">
                                             <input type="radio" name="period" value="{{ $key }}" class="mr-5" id="{{ $package->id }}-{{ $key }}" {{ ($subscription && $subscription->package_id === $package->id && $subscription->period === $key ? ' checked ' : '') }} />
                                             {{ ucfirst($period['text']) }} @if ($period['discount'] > 0) <span class="label label-primary ml-10">{{ $period['discount'] }}% korting</span> @endif
                                         </label>
                                         <span class="pull-right">
                                             &euro;{{ price($user->calculateDiscountPrice($period['costs']/$period['multiply'])) }} p/m
                                         </span>
                                         <div class="clearfix"></div>
                                     </li>
                                    @if ($loop->last === false)
                                        <li><hr class="mt-5 mb-5"/></li>
                                    @endif
                                @endforeach
                            </ul>
                            @if ($user->discount > 0)
                                <div class="mt-25">
                                    {{ __('Uw extra korting van :discount% is reeds verwerkt in de prijs.', ['discount' => $user->discount]) }}
                                </div>
                            @endif
                        </div>
                        <div class="panel-footer pb-35 mt-20">
                            @if ($missingValues === 0)
                                <button class="btn btn-primary btn-lg">{{ __('Selecteer dit pakket') }}</button>
                            @else
                                <p>{!! __('Vul eerst uw <a href="#payment" onclick=":onclick">factuurgegevens</a> in om dit pakket te bestellen.', ['onclick' => '$(\'#account_tabs #payment_tab\').tab(\'show\')']) !!}</p>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        @endforeach

        @if ($subscription && $subscription->trial === false)
            <div class="col-md-12 mb-30">
                <h5>{{ __('Pakket opzeggen') }}</h5>
                @if ($subscription->cancelled_at === null)
                    <p>{{ __('Om uw pakket niet automatisch te verlengen kunt u uw pakket opzeggen door op onderstaande button te klikken.') }}</p>
                    <p>
                        {!! __('Uw huidige pakket zal dan aflopen op <strong>:date.</strong>', ['date' => $subscription->firstCancellationDate()->formatLocalized('%d %B %Y')]) !!}
                        @if ($subscription->daysLeft < 30)
                            {{ __('Hierbij is rekening gehouden met de opzegtermijn van 30 dagen.') }}
                            @if ($subscription->period !== 'monthly')
                                </p>
                                <p>
                                {{ __('Wanneer u eerder wilt opzeggen kunt u uw hierboven eerst uw betaalperiode aanpassen naar maandelijks.') }}
                            @endif
                        @endif
                    </p>
                    <form method="post" action="{{ route('account.cancel') }}">
                        @csrf

                        <button class="btn btn-primary mt-20">{{ __('Verlenging stopzetten') }}</button>
                    </form>
                @else
                    <p>{{ __('Uw huidige pakket is opgezegd en loopt nog :days dagen.', ['days' => $subscription->daysLeft]) }}</p>
                @endif
            </div>
        @endif
    </div>
</div>