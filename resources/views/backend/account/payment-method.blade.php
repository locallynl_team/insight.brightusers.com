@extends('backend.layouts.app')

@section('content')

    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <div class="form-wrap" style="max-width: 800px;">
                    @include('backend.parts.errors')

                    <h5 class="mb-20">{{ __('Betaalmethode') }}</h5>
                    @if ($user->getActiveMandate() <> false)
                        <p class="mb-20">{{ __('Hieronder ziet u uw actieve betaalmethode. Deze betaalmethode wordt gebruikt voor al uw betalingen aan Surve. Om uw betaalmethode aan te passen verwijdert u eerst deze betaalmethode. Hierna kiest u de gewenste nieuwe betaalmethode.') }}</p>
                    @else
                        <p class="mb-20">{{ __('U heeft geen actieve betaalmethode. Selecteer hieronder een betaalmethode om te gebruiken voor al uw betalingen aan Surve. Na het selecteren van een betaalmethode voert u een controlebetaling uit van €0,01 om de machtiging te bevestiging.') }}</p>
                    @endif
                    <div class="trial">
                        <i class="fa fa-info-circle"></i>
                        {{ __('Wanneer u geen actieve betaalmethode heeft op het moment dat er een betaling uitgevoerd wordt, zal het bijhorende pakket verlopen.') }}
                    </div>

                    @if ($user->getActiveMandate() <> false)
                        @include('backend.parts.mandate', ['mandate' => $user->getActiveMandate()])
                    @else
                        <form method="post" action="{{ route('account.payment-method') }}">
                            @csrf
                            @foreach ($paymentMethods as $paymentMethod)
                                <div id="package-{{ $paymentMethod->id }}" class="package-card {{ (old('payment_method') == $paymentMethod->id ? 'active' : '') }} {{ $loop->last ? 'last' : '' }}" data-id="{{ $paymentMethod->id }}">
                                    <i class="fa fa-check-circle"></i>
                                    <span class="title">{{ ($paymentMethod->id == 'directdebit' ? 'Autom. incasso' : $paymentMethod->description) }}</span>
                                    <img style="margin-top: 5px;" src="{{ $paymentMethod->image->size2x }}" alt="{{ $paymentMethod->description }}" />
                                </div>
                            @endforeach
                            <input id="current-method" type="hidden" name="payment_method" value="{{ old('payment-method') }}" />
                            <div class="clearfix"></div>

                            <div class="form-group mb-0 mt-25">
                                <button type="submit" class="btn btn-primary">{{ __('Betaalmethode selecteren') }}</button>
                            </div>
                            <p style="margin-top: 20px; font-size: 13px;">
                                {{ __('Betalingen met automatische incasso en creditcard worden afgehandeld door onze betaalprovider Mollie. Wij sturen u door naar de beveiligde omgeving van Mollie om uw betaling af te ronden.') }}
                            </p>
                        </form>
                    @endif

                    <form method="post" action="{{ route('account.payment-details') }}">
                        @csrf
                        <h5 class="mt-30">{{ __('Factuurinformatie') }}</h5>
                        <div class="form-group">
                            <label class="control-label mb-5" for="company_name">{{ __('Bedrijfsnaam') }}</label>
                            <input name="company_name" class="form-control" value="{{ old('company_name', $user->company_name) }}" id="company_name">
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-5" for="address">{{ __('Adres') }}</label>
                            <input name="address" class="form-control" value="{{ old('address', $user->address) }}" id="address">
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-5" for="postal_code">{{ __('Postcode') }}</label>
                            <input name="postal_code" class="form-control" value="{{ old('postal_code', $user->postal_code) }}" id="postal_code">
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-5" for="city">{{ __('Plaats') }}</label>
                            <input name="city" class="form-control" value="{{ old('city', $user->city) }}" id="city">
                        </div>
                        <div class="form-group mb-0 mt-25">
                            <button type="submit" class="btn btn-primary">{{ __('Factuurinformatie aanpassen') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.package-card').on('click', function() {
                var currentMethod = $('#current-method');
                var newMethod = $(this).data('id');

                //change classes
                $('.package-card.active').removeClass('active');
                $('#package-' + newMethod).addClass('active');
                currentMethod.val(newMethod);
            });
        });
    </script>
@endsection