@extends('backend.layouts.app')

@section('content')

    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('IP Filters') }}</h5>

                <div class="form-wrap">
                    @include('backend.parts.errors')

                    <form method="post" action="{{ route('account.ip-filters') }}">
                        @csrf

                        <div class="form-group">
                            <label class="control-label mb-5" for="exclude_ip_addresses">{{ __('IP-adressen uitsluiten') }}</label>
                            <textarea style="height: {{ (80 + (count(explode("\n", auth()->user()->exclude_ip_addresses))*20)) }}px;" name="exclude_ip_addresses" class="form-control" id="exclude_ip_addresses">{{ old('exclude_ip_addresses', auth()->user()->exclude_ip_addresses) }}</textarea>
                            <span class="help-block mb-10 mt-5">{{ __('Geef IP-adressen op die uitgesloten moeten worden bij het verzamelen van gegevens voor de heatmaps. IPv6 en IPv4 worden ondersteund, gebruik een aparte regel voor elk IP-adres. Deze regels zijn van toepassing op alle websites in uw account.') }}</span>
                            <span class="help-block mb-20 mt-5">{{ __('Uw huidige IP-adres is:') }} {{ request()->ip() }}</span>
                        </div>

                        <div class="form-group mb-0 mt-25">
                            <button type="submit" class="btn btn-primary">{{ __('Filters opslaan') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection