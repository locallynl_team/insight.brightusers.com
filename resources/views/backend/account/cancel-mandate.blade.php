@extends('backend.layouts.app')

@section('content')

    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <div class="form-wrap" style="max-width: 800px;">
                    @include('backend.parts.errors')

                    <h5 class="mb-20">{{ __('Machtiging intrekken') }}</h5>
                    <div class="trial">
                        <i class="fa fa-info-circle"></i>
                        {{ __('Wanneer u geen actieve betaalmethode heeft op het moment dat er een betaling uitgevoerd wordt, zal het bijhorende pakket verlopen.') }}
                    </div>

                    @include('backend.parts.mandate', ['mandate' => $user->getActiveMandate(), 'delete' => true])

                    <form method="post" action="{{ route('account.cancel-mandate') }}">
                        @csrf

                        <div class="form-group mb-0 mt-25">
                            <a class="btn btn-default mr-10" href="{{ route('account.payment-method') }}">{{ __('Nee, ga terug') }}</a>
                            <button type="submit" class="btn btn-primary">{{ __('Machtiging intrekken') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection