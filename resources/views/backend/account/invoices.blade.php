@extends('backend.layouts.app')

@section('content')

    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                @include('backend.parts.errors')

                <h5 class="mb-20">{{ __('Facturen') }}</h5>
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 150px;">{{ __('Factuurnummer') }}</th>
                                <th>{{ __('Website') }}</th>
                                <th>{{ __('Datum') }}</th>
                                <th>{{ __('Bedrag') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th>{{ __('Factuur') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($payments as $payment)
                                <tr>
                                    <td>
                                        @if ($payment->invoice)
                                            {{ $payment->invoice->id }}
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        {{ $payment->site->domain }}
                                        <span class="block mt-5" style="font-size: 13px;">{{ __('Periode: :period', ['period' => $payment->package->period($payment->period)['text']]) }}</span>
                                    </td>
                                    <td>
                                        {{ $payment->created_at->formatLocalized('%d %B %Y') }}
                                        @if ($payment->status != 'paid')
                                            <br />
                                            <span style="font-size: 12px;">{{ __('te voldoen binnen :days dagen', ['days' => (30 - $payment->created_at->diffInDays(\Carbon\Carbon::now(), false))]) }}</span>
                                        @endif
                                    </td>
                                    <td>&euro;{{ price(($payment->total_amount + (($payment->total_amount / 100) * $payment->tax))) }}</td>
                                    {{-- label-success label-danger --}}
                                    <td><span class="label label-{{ ($payment->status === 'paid' ? 'success' : 'danger') }}">{{ ($payment->status == 'paid' ? 'betaald' : 'open') }}</span></td>
                                    <td>
                                        @if ($payment->invoice && $payment->invoice->generated === true)
                                            <a class="action action-button" href="{{ route('invoice', $payment->invoice->id) }}" target="_blank"><i class="fa fa-download"></i></a>
                                        @elseif ($payment->invoice)
                                            <a class="action action-button"><i class="fa fa-spinner fa-spin"></i></a>
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">{{ __('Geen facturen beschikbaar') }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection