@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Formulier \':name\'', ['name' => $form->title]) }}</h5>
                <div class="form-analyses">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <td class="grey-bg"></td>
                                @foreach ($fields as $field)
                                    <td>
                                        <h6 class="field-title mb-10" style="white-space: nowrap;">{{ $field->title }}</h6>
                                        <p>
                                            <i class="fa fa-clock mr-10" data-toggle="tooltip" data-placement="right" title="{{ __('Gemiddelde invultijd') }}"></i> {{ round($field->results->timing ?? 0, 2) }} sec.<br />
                                            <i class="fa fa-redo mr-10" data-toggle="tooltip" data-placement="right" title="{{ __('Opnieuw ingevuld') }}"></i> {{ round($field->results->redo ?? 0, 2) }}%<br />
                                            <i class="fa fa-question-circle mr-10" data-toggle="tooltip" data-placement="right" title="{{ __('Leeg gelaten') }}"></i> {{ round($field->results->empty ?? 0,2) }}%
                                        </p>
                                    </td>
                                @endforeach
                                <td class="grey-bg"></td>
                            </tr>
                            <tr class="results-row">
                                <td style="@include('backend.parts.background-form-analyse', ['percentage' => 100])">
                                    <h6 class="caps-heading">{{ __('Sessies') }}</h6>
                                    <p class="sessions mt-10 mb-10">{{ $formResults->sessions }}</p>
                                    <p class="mt-20 mb-20">{{ __(':dropoff bezoekers (:percentage%) verlieten de pagina zonder het formulier te gebruiken.', ['dropoff' => ($formResults->sessions - $formResults->interactive), 'percentage' => round((100 / $formResults->sessions) * ($formResults->sessions - $formResults->interactive), 2)]) }}</p>
                                </td>
                                @foreach ($fields as $field)
                                    <td style="@include('backend.parts.background-form-analyse', ['percentage' => ((100 / $formResults->sessions) * ($field->results->interactions ?? 0))])">
                                        <h6 class="caps-heading">{{ __('Interacties') }}</h6>
                                        <p class="results-paragraph mt-10">{{ $field->results->interactions ?? 0 }}</p>
                                    </td>
                                @endforeach
                                <td class="grey-bg" style="@include('backend.parts.background-form-analyse', ['percentage' => ((100 / $formResults->sessions) * ($field->results->interactions ?? 0))])">
                                    <h6 class="caps-heading">{{ __('Succesvolle verzendingen') }}</h6>
                                    <p class="results-paragraph mt-10">{{ $formResults->successful }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="grey-bg drop-off">
                                    <span class="drop-off-icon"><i class="fa fa-arrow-circle-down"></i></span>
                                    <h6 class="caps-heading">{{ __('Uitval') }}</h6>
                                    <p class="results-paragraph mt-10">{{ round((100 / $formResults->sessions) * ($formResults->sessions - $formResults->interactive), 2) }}%</p>
                                    <p>{{ $formResults->sessions - $formResults->interactive }}</p>
                                </td>
                                @foreach ($fields as $field)
                                    <td class="drop-off">
                                        <span class="drop-off-icon"><i class="fa fa-arrow-circle-down"></i></span>
                                        <h6 class="caps-heading">{{ __('Uitval') }}</h6>
                                        @if ($field->results)
                                            <p class="results-paragraph mt-10">{{ round(($field->results->drop_off_count ? (($field->results->drop_off_count / $formResults->sessions) * 100) : 0), 2) }}%</p>
                                            <p>({{ $field->results->drop_off_count ?? 0 }})</p>
                                        @else
                                            <p class="results-paragraph mt-10">0%</p>
                                            <p>(0)</p>
                                        @endif
                                    </td>
                                @endforeach
                                <td class="grey-bg drop-off">
                                    <span class="drop-off-icon"><i class="fa fa-exclamation-circle"></i></span>
                                    <h6 class="caps-heading">{{ __('Verzendingen met fouten') }}</h6>
                                    <p class="results-paragraph mt-10">{{ __(':dropoff van :sessions', ['dropoff' => $formResults->send - $formResults->successful, 'sessions' => $formResults->send]) }}</p>
                                    <p>({{ ($formResults->successful > 0 ? round((100 / $formResults->send) * ($formResults->send - $formResults->successful), 2) : 0) }}%)</p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="{{ count($fields)+2 }}" style="text-align: center;">
                                    <h6 class="caps-heading">{{ __('Conversiepercentage') }}</h6>
                                    <p class="results-paragraph">{{ round((100 / $formResults->sessions) * $formResults->successful, 2) }}%</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <a href="{{ route('modal', ['type' => 'reset-form-sessions', 'id' => $form->id]) }}" data-toggle="modal" class="btn btn-danger"><i class="fa fa-trash-alt mr-10"></i> {{ __('Reset formulier-sessies') }}</a>
            </div>
        </div>
    </div>
@endsection