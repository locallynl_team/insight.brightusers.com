@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Nieuw formulier') }}</h5>
                <div class="form-wrap" style="max-width: 800px;">
                    @include('backend.parts.errors')

                    <form method="post" action="{{ route('form.create') }}" id="app">
                        @csrf

                        <div class="form-group">
                            <label class="control-label mb-5 text-left" for="title">{{ __('Titel') }}</label>
                            <input class="form-control" value="{{ old('title') }}" id="title" name="title">
                            <span class="invalid-feedback" id="error_title">{{ $errors->first('title') }}</span>
                        </div>

                        <form-identification v-bind:modalroute="{{ json_encode(['url' => route('modal', 'identify-a-form')]) }}"></form-identification>
                        <span class="invalid-feedback" id="error_selected_form">{{ $errors->first('selected_form') }}</span>

                        <div class="form-group mt-20 mb-15">
                            <label class="block control-label mb-5 text-left">{{ __('Verzamel locaties') }}</label>
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <input type="hidden" id="url_target_type" name="url_target_type" value="{{ old('url_target_type', 'simple') }}" />
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span id="url_target_text">{{ $urlTargets[old('url_target_type', 'simple')]->getSettings()['title'] }}</span> <span class="caret"></span></button>
                                    <ul class="dropdown-menu info-buttons">
                                        @foreach ($urlTargets as $type => $urlTarget)
                                            <li>
                                                <a href="javascript:void(0)" class="url-target-link" data-value="{{ $type }}" data-text="{{ $urlTarget->getSettings()['title'] }}">
                                                    {{ $urlTarget->getSettings()['title'] }}
                                                </a>
                                                <a href="{{ route('modal', 'url-targets') }}" data-toggle="modal" class="info-icon"><i class="fa fa-info-circle"></i></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <input type="text" id="url_target" name="url_target" value="{{ old('url_target') }}" class="form-control" placeholder="{{ __('bijv. https://www.voorbeeld.nl') }}">
                            </div>
                            <span class="invalid-feedback" id="error_url_target">{{ $errors->first('url_target') }}</span>
                        </div>

                        <div class="form-group no-margin-switcher">
                            <label class="block control-label mb-5 text-left" for="name">{{ __('Platforms') }}</label>
                            <label class="block mb-10" @if (hasPermission('desktop_platform') === false) style="color: #8f8d8d;" @else style="color: #555;" @endif>
                                <input class="js-switch" data-color="#4aa23c" type="checkbox" name="platforms[]" value="desktop"
                                    @if (hasPermission('desktop_platform') === false)
                                        disabled
                                    @endif
                                    @if (is_array(old('platforms')) && in_array('desktop', old('platforms')))
                                        checked
                                    @endif  /> {{ __('Desktop') }}
                                @if (hasPermission('desktop_platform') === false) @include('backend.parts.upgrade') @endif
                            </label>
                            <label class="block mb-10" @if (hasPermission('tablet_platform') === false) style="color: #8f8d8d;" @else style="color: #555;" @endif>
                                <input class="js-switch js-mobile-platform" data-color="#4aa23c" type="checkbox" name="platforms[]" value="tablet"
                                    @if (hasPermission('tablet_platform') === false)
                                        disabled
                                    @endif
                                    @if (is_array(old('platforms')) && in_array('tablet', old('platforms')))
                                        checked
                                    @endif  /> {{ __('Tablet') }}
                                @if (hasPermission('tablet_platform') === false) @include('backend.parts.upgrade') @endif
                            </label>
                            <label class="block" @if (hasPermission('mobile_platform') === false) style="color: #8f8d8d;" @else style="color: #555;" @endif>
                                <input class="js-switch js-mobile-platform" data-color="#4aa23c" type="checkbox" name="platforms[]" value="mobile"
                                    @if (hasPermission('mobile_platform') === false)
                                        disabled
                                    @endif
                                    @if (is_array(old('platforms')) && in_array('mobile', old('platforms')))
                                        checked
                                    @endif />
                                {{ __('Mobiel') }}
                                @if (hasPermission('mobile_platform') === false) @include('backend.parts.upgrade') @endif
                            </label>
                            <span class="invalid-feedback" id="error_platforms">{{ $errors->first('platforms') }}</span>
                        </div>

                        <div class="form-group mb-0 mt-15">
                            <button class="btn btn-primary js-submit-button">{{ __('Toevoegen') }} <span class="form-loader js-form-loader"><i class="fa fa-spinner fa-spin"></i></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/assets/backend/js/vue/app.js"></script>
    <script>
        const fields = ['title', 'selected_form', 'platforms', 'url_target'];
        let can_submit = false;

        //set laravel csrf token in header
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //on submit send form
        //TODO: make global function to use on all pages
        $('#app').on('submit', function(e) {
            //submit this form
            if (can_submit === true) {
                return true;
            }

            //prevent submit
            e.preventDefault();

            //show loading screen
            startLoading();

            //check results
            $.ajax({
                type: "POST",
                url: '{{ route('form.save') }}',
                data: $(this).serialize(),
                statusCode: {
                    //errors in input
                    422: function(xhr) {
                        const result = JSON.parse(xhr.responseText);
                        const errors = Object.entries(result.errors);

                        //reset error messages
                        fields.map(function(field) {
                            $('#error_' + field).html('');
                        });

                        errors.map(function(error) {
                            $('#error_' + error[0]).show().html(error[1][0]);
                        });

                        endLoading();
                    }
                },
                //success, redirect user
                success: function(result) {
                    if (result.message === 'success') {
                        can_submit = true;
                        $('#app').submit();
                    }

                    endLoading();
                }
            });
        });

        //start loading
        function startLoading() {
            $('.js-form-loader').show();
            $('.js-submit-button').attr('disabled', 'disabled');
        }

        //end loading //TODO: move to general spot
        function endLoading() {
            $('.js-form-loader').hide();
            $('.js-submit-button').removeAttr('disabled');
        }
    </script>
    <script>
        $('.url-target-link').on('click', function() {
            $("#url_target_text").html($(this).data('text'));
            $("#url_target_type").val($(this).data('value'));
        });
    </script>
@endsection