@extends('backend.layouts.app')

@section('content')
<div class="container-fluid pt-25">
    <div class="row">
        <div class="col-md-12">
            <h5 class="mb-20">{{ __('Formulieren') }}</h5>
            @if (count($forms) > 0)
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>{{ __('ID') }}</th>
                                <th>{{ __('Naam') }}</th>
                                <th>{{ __('Sessies') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($forms as $form)
                                <tr>
                                    <td>{{ $form->id }}</td>
                                    <td>
                                        <a href="{{ route('form.edit', $form->id) }}">{{ $form->title }}</a><br />
                                    </td>
                                    <td>
                                        {{ __(':count/:max', ['count' => thousand($form->sessions_count), 'max' => thousand(\App\SubscriptionManager\SubscriptionManager::instance()->option('max_form_views')->allowed_usage)]) }}
                                        <a href="{{ route('modal', 'form-sessions') }}" data-toggle="modal"><i class="fa fa-info-circle ml-5"></i></a>
                                    </td>
                                    <td><a href="{{ route('form.toggle', $form->id) }}">
                                            @if ($form->status === true)
                                                <span class="label label-primary">{{ __('Actief') }}</span>
                                            @else
                                                <span class="label label-default">{{ __('In-actief') }}</span>
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <div class="pull-right">
                                            @if ($form->sessions_count === 0)
                                                <a class="action action-button mr-10" href="{{ route('modal', 'form-has-no-views') }}" data-toggle="modal"><i class="fa fa-clipboard-check"></i></a>
                                            @else
                                                <a class="action action-button mr-10" href="{{ route('form.view', $form->id) }}"><i class="fa fa-clipboard-check"></i></a>
                                            @endif
                                            <a class="action action-button mr-10" href="{{ route('form.edit', $form->id) }}"><i class="fa fa-edit"></i></a>
                                            <a class="action action-button delete" data-toggle="modal" href="{{ route('modal', ['type' => 'form-delete', 'id' => $form->id]) }}"><i class="fa fa-trash-alt"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <div style="max-width: 800px;">
                    <p class="mb-20">
                        {{ __('Met formulieren krijgt u een uitgebreide analyse van de formulieren op uw website. Zo ziet u hoe uw bezoekers zich gedragen wanneer ze een formulier invullen.') }}
                        {{ __('De informatie die u hieruit haalt kunt u direct inzetten om blokkerende factoren in uw conversiefunnel te verwijderen.') }}
                    </p>
                    <p style="text-align: center;">
                        <a class="btn btn-primary" href="{{ route('form.create') }}">{{ __('Beginnen met formulieren') }} <i class="fa fa-clipboard-list ml-10"></i> </a>
                    </p>

                    <div class="row mb-30 mt-30" style="background: #f6f6f6; padding: 15px 0;">
                        <div class="col-md-6 col-xs-12 pull-right">
                            <img src="/assets/backend/images/icons/scroll-heatmaps.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                            <p class="visible-xs">&nbsp;</p>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <h5>{{ __('Conversiepercentage') }}</h5>
                            <p class="mb-10">{{ __('Uw formulieren zijn vaak een belangrijk punt van uw website. Hier komen de leads binnen die geld opleveren. Hoe hoger uw conversiepercentage, des te meer omzet. Gebruik de formulieranalyses om uw conversiepercentage te meten en te verbeteren.') }}</p>
                        </div>
                    </div>
                    <div class="row mb-30">
                        <div class="col-md-6 col-xs-12 pull-left">
                            <img src="/assets/backend/images/icons/move-heatmaps.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                            <p class="visible-xs">&nbsp;</p>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <h5>{{ __('Optimale formuliervelden') }}</h5>
                            <p class="mb-10">{{ __('Identificeer de velden in een formulier waarop uw bezoekers vastlopen of afhaken. Verwijder velden of maak deze optioneel en meet het effect. Zo wordt uw gebruiksvriendelijkheid voor bezoekers eenvoudig hoger en zal ook uw conversie toenemen.') }}</p>
                        </div>
                    </div>
                    <div class="row mb-30" style="background: #f6f6f6; padding: 15px 0;">
                        <div class="col-md-6 col-xs-12 pull-right">
                            <img src="/assets/backend/images/icons/click-heatmaps.png" class="img-responsive" style="max-height: 170px; margin: 0 auto;" />
                            <p class="visible-xs">&nbsp;</p>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <h5>{{ __('Platformspecifieke optimalisatie') }}</h5>
                            <p class="mb-10">{{ __('Van elke formuliersessie wordt het platform geregistreerd. Zo kunt u optimaliseren door op mobiel een ander formulier te tonen dan op desktop sessies. Hiermee kunt u de gebruikesvriendelijkheid voor uw mobiele bezoekers optimaliseren.') }}</p>
                        </div>
                    </div>
                </div>
            @endif
            <a class="btn btn-primary" href="{{ route('form.create') }}"><i class="fa fa-plus mr-10"></i> {{ __('Formulier toevoegen') }}</a>
        </div>
    </div>
</div>
@endsection