@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Overzicht pakket :site', ['site' => $site->domain]) }}</h5>
                <div class="form-wrap" style="max-width: 800px;">
                    @include('backend.parts.errors')

                    <table class="table">
                        <tr>
                            <th style="width: 40%; text-transform: uppercase; font-weight: 400; font-size: 13px; border-top: 0;">{{ __('Lopende pakket') }}</th>
                            <td style="border-top: 0;"><span class="label label-package" style="background-color: {{ $site->activeSubscription->package->color }};">{{ $site->activeSubscription->package->title  }}</span></td>
                        </tr>
                        <tr>
                            <th style="text-transform: uppercase; font-weight: 400; font-size: 13px;">
                                @if ($site->activeSubscription->cancelled_at !== null)
                                    {{ __('Beëindigd per') }}
                                @elseif ($site->activeSubscription->trial === true)
                                    {{ __('Trial eindigt op') }}
                                @else
                                    {{ __('Verlenging op') }}
                                @endif
                            </th>
                            <td>
                                {{ $site->activeSubscription->ends_at->formatLocalized('%d %B %Y') }}
                                <span style="font-size: 13px; display: block; margin-top: 5px;">{{ trans_choice('over :days dag|over :days dagen', $site->activeSubscription->days_left, ['days' => $site->activeSubscription->days_left]) }}
                                    @if ($site->activeSubscription->trial === false && $site->activeSubscription->cancelled_at === null)
                                        <a class="pull-right" href="{{ route('subscription.cancel', $site->id) }}">{{ __('beëindigen') }}</a>
                                    @endif
                                </span>
                            </td>
                        </tr>
                        @if ($site->activeSubscription->trial === false)
                            <tr>
                                <th style="text-transform: uppercase; font-weight: 400; font-size: 13px;">{{ __('Kosten') }}</th>
                                <td>
                                    &euro;{{ price(auth()->user()->calculateDiscountPrice($site->activeSubscription->price)) }}
                                    @if ($site->activeSubscription->period === "yearly")
                                        <span style="font-size: 13px; display: block; margin-top: 5px;">{{ __('€:price p/m', ['price' => price(auth()->user()->calculateDiscountPrice($site->activeSubscription->price / $site->activeSubscription->package->period($site->activeSubscription->period)['multiply']))]) }}</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th style="text-transform: uppercase; font-weight: 400; font-size: 13px;">{{ __('Betaling') }}</th>
                                <td>{{ $site->activeSubscription->package->period($site->activeSubscription->period)['text'] }}</td>
                            </tr>
                        @endif
                    </table>
                </div>

                @if ($site->package_change_allowed === true)
                    @if ($site->activeSubscription->trial === true)
                        <h5 class="mb-20 mt-25">{{ __('Pakket selecteren') }}</h5>
                    @else
                        <h5 class="mb-20 mt-25">{{ __('Pakket aanpassen') }}</h5>
                    @endif
                    <div class="form-wrap" style="max-width: 800px;">
                        <form method="post" action="{{ route('subscription.update', $site->id) }}">
                            @csrf
                            <div class="form-group">
                                @foreach ($packages as $package)
                                    <div id="package-{{ $package->id }}" class="package-card {{ (old('package', $site->activeSubscription->package->id) == $package->id ? 'active' : '') }} {{ ($loop->last ? 'last' : '') }}" data-id="{{ $package->id }}" data-price-month="{{ round(auth()->user()->calculateDiscountPrice($package->price_monthly)) }}" data-price-year="{{ round(auth()->user()->calculateDiscountPrice($package->price_yearly)) }}">
                                        <i class="fa fa-check-circle"></i>
                                        <span class="title" style="color: {{ $package->color }}">{{ $package->title }}</span>
                                        <span class="description">{{ __('tot :views views per dag', ['views' => thousand($package->getValueByIdentifier('views'))]) }}</span>

                                        <span class="price-sign">&euro;</span>
                                        <span class="price">{{ round(auth()->user()->calculateDiscountPrice($package->price_yearly)) }}</span>
                                        <span class="price-period">{{ __('p/m') }}</span>

                                        <span class="period">&euro;{{ round(auth()->user()->calculateDiscountPrice($package->price_monthly)) }} {{ __('bij betaling p/m') }}</span>
                                    </div>
                                @endforeach
                                <input id="current-package" type="hidden" name="package" value="{{ old('package', $site->activeSubscription->package->id) }}" />
                                <div class="clearfix"></div>
                                <p style="max-width: 800px; margin-top: 10px; font-size: 13px;">
                                    {{ __('Prijzen gelden bij jaarlijkse betaling en zijn exclusief 21% BTW.') }}
                                    @if ($site->activeSubscription->trial === false)
                                        {{ __('Uw lopende pakket wordt naar rato omgezet in dagen voor uw nieuwe pakket.') }}
                                    @endif
                                    @if (auth()->user()->discount > 0)
                                        {{ __('Uw korting van :discount% is reeds verrekend.', ['discount' => auth()->user()->discount]) }}
                                    @endif
                                </p>
                            </div>

                            <div class="form-group no-margin-switcher">
                                <input type="checkbox" class="js-switch" data-color="#4aa23c" name="pay_yearly" id="pay_yearly" {{ (old('pay_yearly', ($site->activeSubscription->period === 'yearly')) == true ? 'checked' : '') }} />
                                <label class="control-label text-left mb-10" for="pay_yearly">{!! __('Ik bespaar graag &euro;<span id="savings"></span> en betaal jaarlijks &euro;<span id="yearly_costs"></span>.') !!}</label>
                            </div>

                            @if (auth()->user()->hasActivePaymentMethod() === true && auth()->user()->pay_on_invoice === false)
                                <div class="form-group no-margin-switcher">
                                    <input type="checkbox" class="js-switch" data-color="#4aa23c" name="payment_confirmation" id="payment_confirmation" {{ (old('payment_confirmation') == true ? 'checked' : '') }} />
                                    <label class="control-label text-left mb-10" for="payment_confirmation">{{ __('Ik geef toestemming om de betaling automatisch uit te voeren.') }}</label>
                                </div>
                            @endif

                            <div class="form-group mb-15 mt-25">
                                <button class="btn btn-primary">{{ __('Pakket aanpassen') }}</button>
                            </div>
                            <p style="font-size: 13px;">
                                @if (auth()->user()->hasActivePaymentMethod() === true && auth()->user()->pay_on_invoice === false)
                                    {{ __('Voor uw betaling maken wij gebruik van de door u opgegeven betaalmethode: :paymentmethod.', ['paymentmethod' => \App\User::getPaymentMethodString(auth()->user()->getActiveMandate())]) }}
                                @elseif (auth()->user()->pay_on_invoice === true)
                                    {{ __('Voor uw betaling ontvangt u van ons een factuur, uw pakket wordt direct geactiveerd.') }}
                                @endif
                                @if (auth()->user()->hasActivePaymentMethod() === true)
                                    {!! __('U kunt uw betaalmethode aanpassen bij <a href=":route">betaalmethode</a>.', ['route' => route('account.payment-method')]) !!}
                                @else
                                    {{ __('U heeft geen actieve betaalmethode, deze kunt u in de volgende stap selecteren indien nodig.') }}
                                @endif
                            </p>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            //calculate savings
            var activeCard = $('.package-card.active');
            var savings = (activeCard.data('price-month')*12)-(activeCard.data('price-year')*12);
            $("#savings").html(savings);
            $("#yearly_costs").html(parseInt(activeCard.data('price-year') * 12));

            $('.package-card').on('click', function() {
                var currentPackage = $('#current-package');
                var newPackage = parseInt($(this).data('id'));

                //change classes
                $('.package-card.active').removeClass('active');
                $('#package-' + newPackage).addClass('active');
                currentPackage.val(newPackage);

                //calculate savings
                var savings = ($(this).data('price-month')*12)-($(this).data('price-year')*12);
                $("#savings").html(savings);
                $("#yearly_costs").html(parseInt($(this).data('price-year') * 12));
            });
        });
    </script>
@endsection