@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20 mt-25">{{ __('Pakket beëindigen') }}</h5>
                @include('backend.parts.errors')

                <div class="form-wrap" style="max-width: 800px;">
                    <form method="post" action="{{ route('subscription.cancel', $site->id) }}">
                        @csrf
                        @if ($site->activeSubscription->trial === true)
                            <p>{{ __('Uw trial verloopt automatisch op :renewal als u hierboven geen nieuw pakket selecteert.', ['renewal' => $site->activeSubscription->ends_at->formatLocalized('%d %B %Y')]) }}</p>
                        @else
                            <p>{{ __('Uw pakket kunt u altijd beëindigen. Klik op de onderstaande button om uw pakket niet opnieuw te verlengen op :renewal. Uw pakket zal dan automatisch aflopen. Na het aflopen van uw pakket worden de website en alle bijhorende gegevens gewoon bewaard, u kunt de website later altijd opnieuw activeren door weer een pakket te selecteren.', ['renewal' => $site->activeSubscription->ends_at->formatLocalized('%d %B %Y')]) }}</p>
                            <a class="btn btn-default mt-25 mr-10" href="{{ route('subscription.manage', $site->id) }}">{{ __('Nee, ga terug') }}</a>
                            <button class="btn btn-primary mt-25">{{ __('Pakket beëindigen') }}</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection