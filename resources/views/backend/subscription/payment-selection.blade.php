@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Selecteer een betaalmethode') }}</h5>
                <div class="form-wrap" style="max-width: 800px;">
                    @include('backend.parts.errors')

                    <form method="post" action="{{ route('subscription.process-payment', $site->id) }}">
                        @csrf
                        <input type="hidden" name="package" value="{{ $package->id }}" />
                        <input type="hidden" name="period" value="{{ $period }}" />
                        <div class="trial">
                            <i class="fa fa-info-circle"></i>
                            {{ __('Om uw betalingen aan Surve te doen selecteert u hieronder uw gewenste betaalmethode. Nadat u uw factuurgegevens heeft gecontroleert wordt uw eerste betaling gestart ter verificatie.') }}
                        </div>
                        @foreach ($paymentMethods as $paymentMethod)
                            <div id="package-{{ $paymentMethod->id }}" class="package-card {{ (old('payment_method') == $paymentMethod->id ? 'active' : '') }} {{ $loop->last ? 'last' : '' }}" data-id="{{ $paymentMethod->id }}">
                                <i class="fa fa-check-circle"></i>
                                <span class="title">{{ ($paymentMethod->id == 'directdebit' ? 'Autom. incasso' : $paymentMethod->description) }}</span>
                                <img style="margin-top: 5px;" src="{{ $paymentMethod->image->size2x }}" alt="{{ $paymentMethod->description }}" />
                            </div>
                        @endforeach
                        <input id="current-method" type="hidden" name="payment_method" value="{{ old('payment-method') }}" />
                        <div class="clearfix"></div>

                        <h5 class="mb-10 mt-25">{{ __('Factuurgegevens') }}</h5>
                        <p class="mb-15">
                            {{ __('Vul indien nodig uw factuurgegevens aan, uw factuur ontvangt u automatisch in uw account en per e-mail.') }}
                        </p>
                        <div class="form-group">
                            <label class="control-label mb-5" for="company_name">{{ __('Bedrijfsnaam') }}</label>
                            <input name="company_name" class="form-control" value="{{ old('company_name', $user->company_name) }}" id="company_name">
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-5" for="address">{{ __('Adres') }}</label>
                            <input name="address" class="form-control" value="{{ old('address', $user->address) }}" id="address">
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-5" for="postal_code">{{ __('Postcode') }}</label>
                            <input name="postal_code" class="form-control" value="{{ old('postal_code', $user->postal_code) }}" id="postal_code">
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-5" for="city">{{ __('Plaats') }}</label>
                            <input name="city" class="form-control" value="{{ old('city', $user->city) }}" id="city">
                        </div>

                        <h5 class="mb-10 mt-25">{{ __('Overzicht') }}</h5>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>{{ __('Website') }}</th>
                                    <th>{{ __('Pakket') }}</th>
                                    <th>{{ __('Kosten p/m') }}</th>
                                </tr>
                            </thead>
                            <tr>
                                <td>{{ $site->domain  }}</td>
                                <td>{{ $package->title }}</td>
                                <th>&euro;{{ price($price / $package->period($period)['multiply']) }}</th>
                            </tr>
                            <tr style="color: #2f2f2f;">
                                <td></td>
                                <td>{{ __('Betaling :text', ['text' => $package->period($period)['text']]) }}:</td>
                                <td>&euro;{{ price($price) }}</td>
                            </tr>
                        </table>

                        <div class="form-group mb-0 mt-25">
                            <button class="btn btn-primary">{{ __('Betaalmethode activeren') }}</button>
                        </div>
                        <p style="margin-top: 20px; font-size: 13px;">
                            {{ __('Genoemde prijzen zijn exclusief 21% BTW. Betalingen met automatische incasso en creditcard worden afgehandeld door onze betaalprovider Mollie. Wij sturen u door naar de beveiligde omgeving van Mollie om uw betaling af te ronden.') }}
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.package-card').on('click', function() {
                var currentMethod = $('#current-method');
                var newMethod = $(this).data('id');

                //change classes
                $('.package-card.active').removeClass('active');
                $('#package-' + newMethod).addClass('active');
                currentMethod.val(newMethod);
            });
        });
    </script>
@endsection