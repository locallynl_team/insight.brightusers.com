@extends('backend.layouts.app')

@section('content')
<div class="container-fluid pt-25">
    <div class="row">
        <div class="col-md-12">
            <h5 class="mb-20">{{ __('Abonnement wijzigen') }}</h5>
            <div class="form-wrap">
                @include('backend.parts.errors')

                <form method="post" action="{{ route('admin.subscription.edit', $subscription->id) }}">
                    @csrf

                    <h5 class="mb-10">{{ __('Abonnement gegevens') }}</h5>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="package">{{ __('Pakket') }}</label>
                        <select class="form-control" name="package">
                            @foreach ($packages as $package)
                                <option value="{{ $package->id }}" {{ (old('package', $subscription->package_id) == $package->id ? 'selected' : '') }}>{{ $package->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" class="js-switch" data-color="#4aa23c" name="active" id="active" {{ (old('active', $subscription->active) == true ? 'checked' : '') }} />
                        <label class="control-label text-left mb-10" style="position: relative; top: 8px;" for="active">{{ __('Actief') }}</label>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" class="js-switch" data-color="#4aa23c" name="trial" id="trial" {{ (old('active', $subscription->trial) == true ? 'checked' : '') }} />
                        <label class="control-label text-left mb-10" style="position: relative; top: 8px;" for="trial">{{ __('Trial') }}</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="period">{{ __('Periode') }}</label>
                        <select class="form-control" name="period">
                            <option value="trial" {{ (old('period', $subscription->period) == 'trial' ? 'selected' : '') }}>{{ __('Trial') }}</option>
                            @foreach ($subscription->package->periods as $period => $values)
                                <option value="{{ $period }}" {{ (old('period', $subscription->period) == $period ? 'selected' : '') }}>{{ ucfirst($values['text']) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="price">{{ __('Prijs') }}</label>
                        <input class="form-control" value="{{ old('price', $subscription->price) }}" id="price" name="price">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="cancelled_at">{{ __('Geannuleerd op') }}</label>
                        <input class="form-control" value="{{ old('cancelled_at', $subscription->cancelled_at) }}" id="cancelled_at" name="cancelled_at">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="cancelled_at">{{ __('Eindigt op') }}</label>
                        <input class="form-control" value="{{ old('ends_at', $subscription->ends_at) }}" id="ends_at" name="ends_at">
                    </div>

                    <h5 class="mb-10 mt-25">{{ __('Pakket opties') }}</h5>

                    @foreach ($packageOptions as $packageOption)
                        <div class="form-group">
                            <label class="control-label mb-5 text-left" for="option_{{ $packageOption->id }}">{{ $packageOption->title }}</label>
                            @if ($packageOption->type === "integer")
                                <input class="form-control" value="{{ old('options.'.$packageOption->id, ($packageOption->packageItems->where('package_id', $subscription->package_id)->first()->subscriptionItems->first()->allowed_usage ?? 0)) }}" id="option_{{ $packageOption->id }}" name="options[{{ $packageOption->id }}]">
                            @elseif ($packageOption->type === "boolean")
                                <select class="form-control" name="options[{{ $packageOption->id }}]">
                                    <option value="0" {{ old('options.'.$packageOption->id, ($packageOption->packageItems->where('package_id', $subscription->package_id)->first()->subscriptionItems->first()->allowed_usage ?? 0)) == 0 ? 'selected' : '' }}>{{ __('Nee') }}</option>
                                    <option value="1" {{ old('options.'.$packageOption->id, ($packageOption->packageItems->where('package_id', $subscription->package_id)->first()->subscriptionItems->first()->allowed_usage ?? 0)) == 1 ? 'selected' : '' }}>{{ __('Ja') }}</option>
                                </select>
                            @elseif ($packageOption->type === "time_period")
                                <input class="form-control" value="{{ old('options.'.$packageOption->id, ($packageOption->packageItems->where('package_id', $subscription->package_id)->first()->subscriptionItems->first()->allowed_usage ?? 0)) }}" id="option_{{ $packageOption->id }}" name="options[{{ $packageOption->id }}]">
                            @endif
                        </div>
                    @endforeach

                    <div class="form-group mb-0 mt-15">
                        <button class="btn btn-primary">{{ __('Wijzigen') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection