@extends('backend.layouts.app')

@section('content')
<div class="container-fluid pt-25">
    <div class="row">
        <div class="col-md-12">
            <h5 class="mb-20">{{ __('Pakket wijzigen') }}</h5>
            <div class="form-wrap">
                @include('backend.parts.errors')

                <form method="post" action="{{ route('admin.package.edit', $package->id) }}">
                    @csrf

                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="title">{{ __('Titel') }}</label>
                        <input class="form-control" value="{{ old('title', $package->title) }}" id="title" name="title">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="price_monthly">{{ __('Prijs per maand') }}</label>
                        <input class="form-control" value="{{ old('price_monthly', $package->price_monthly) }}" id="price_monthly" name="price_monthly">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="price_yearly">{{ __('Prijs per jaar') }}</label>
                        <input class="form-control" value="{{ old('price_yearly', $package->price_yearly) }}" id="price_yearly" name="price_yearly">
                    </div>

                    <h5 class="mb-10 mt-25">{{ __('Pakket opties') }}</h5>
                    @foreach ($packageOptions as $packageOption)
                        <div class="form-group">
                            <label class="control-label mb-5 text-left" for="option_{{ $packageOption->id }}">{{ $packageOption->title }}</label>
                            @if ($packageOption->type === "integer")
                                <input class="form-control" value="{{ old('options.'.$packageOption->id, ($packageOption->packageItems->first()->amount ?? 0)) }}" id="option_{{ $packageOption->id }}" name="options[{{ $packageOption->id }}]">
                            @elseif ($packageOption->type === "boolean")
                                <select id="option_{{ $packageOption->id }}" class="form-control" name="options[{{ $packageOption->id }}]">
                                    <option value="0" {{ old('options.'.$packageOption->id, ($packageOption->packageItems->first()->amount ?? 0)) == 0 ? 'selected' : '' }}>{{ __('Nee') }}</option>
                                    <option value="1" {{ old('options.'.$packageOption->id, ($packageOption->packageItems->first()->amount ?? 0)) == 1 ? 'selected' : '' }}>{{ __('Ja') }}</option>
                                </select>
                            @elseif ($packageOption->type === "time_period")
                                <input class="form-control" value="{{ old('options.'.$packageOption->id, ($packageOption->packageItems->first()->amount ?? 0)) }}" id="option_{{ $packageOption->id }}" name="options[{{ $packageOption->id }}]">
                            @endif
                        </div>
                    @endforeach

                    <div class="form-group mb-0 mt-15">
                        <button class="btn btn-primary">{{ __('Wijzigen') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection