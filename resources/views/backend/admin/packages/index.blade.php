@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Pakketten') }}</h5>
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 30px;">{{ __('ID') }}</th>
                                    <th style="width: 50%;">{{ __('Titel') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Prijs per maand') }}</th>
                                    <th>{{ __('Prijs per jaar') }}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($packages as $package)
                                <tr>
                                    <td>{{ $package->id }}</td>
                                    <td>
                                        <a href="{{ route('admin.package.edit', $package->id) }}">
                                            <span class="label label-package" style="background: {{ $package->color }};">{{ $package->title }}</span>
                                        </a>
                                    </td>
                                    <td><a href="{{ route('admin.package.toggle', $package->id) }}">
                                            @if ($package->active === 1)
                                                <span class="label label-primary">{{ __('Actief') }}</span>
                                            @else
                                                <span class="label label-default">{{ __('In-actief') }}</span>
                                            @endif
                                        </a>
                                    </td>
                                    <th>
                                        &euro; {{ price($package->price_monthly) }}<br />
                                        <span style="font-size: 12px;">{{ __('Per jaar:') }} &euro; {{ price($package->price_monthly*12) }}</span>
                                    </th>
                                    <th>
                                        &euro; {{ price($package->price_yearly) }}<br />
                                        <span style="font-size: 12px;">{{ __('Per jaar:') }} &euro; {{ price($package->price_yearly*12) }}</span>
                                    </th>
                                    <td>
                                        <div class="pull-right">
                                            <a class="action action-button" href="{{ route('admin.package.edit', $package->id) }}"><i class="fa fa-edit"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <a class="btn btn-primary" href="{{ route('admin.packageoption.index') }}">{{ __('Pakket opties beheren') }}</a>
            </div>
        </div>
    </div>
@endsection