@extends('backend.layouts.app')

@section('content')
<div class="container-fluid pt-25">
    <div class="row">
        <div class="col-md-12">
            <h5 class="mb-20">{{ __('Gebruiker wijzigen') }}</h5>
            <div class="form-wrap">
                @include('backend.parts.errors')

                <form method="post" action="{{ route('admin.user.edit', $user->id) }}">
                    @csrf

                    <h5 class="mb-10">{{ __('Persoonlijke gegevens') }}</h5>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="first_name">{{ __('Voornaam') }}</label>
                        <input class="form-control" value="{{ old('first_name', $user->first_name) }}" id="first_name" name="first_name">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="last_name">{{ __('Achternaam') }}</label>
                        <input class="form-control" value="{{ old('last_name', $user->last_name) }}" id="last_name" name="last_name">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="email">{{ __('E-mailadres') }}</label>
                        <input class="form-control" value="{{ old('email', $user->email) }}" id="email" name="email">
                    </div>

                    <h5 class="mt-10 mb-5">{{ __('Factuurgegevens') }}</h5>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="company_name">{{ __('Bedrijfsnaam') }}</label>
                        <input class="form-control" value="{{ old('company_name', $user->company_name) }}" id="company_name" name="company_name">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="address">{{ __('Adres') }}</label>
                        <input class="form-control" value="{{ old('address', $user->address) }}" id="address" name="address">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="postal_code">{{ __('Postcode') }}</label>
                        <input class="form-control" value="{{ old('postal_code', $user->postal_code) }}" id="postal_code" name="postal_code">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="city">{{ __('Stad') }}</label>
                        <input class="form-control" value="{{ old('city', $user->city) }}" id="city" name="city">
                    </div>

                    <h5 class="mb-5 mt-10">{{ __('Betaling') }}</h5>
                    <div class="form-group">
                        <input type="checkbox" class="js-switch" data-color="#4aa23c" name="pay_on_invoice" id="pay_on_invoice" {{ (old('pay_on_invoice', $user->pay_on_invoice) == true ? 'checked' : '') }} />
                        <label class="control-label text-left mb-10" style="position: relative; top: 8px;" for="pay_on_invoice">{{ __('Betaling op factuur') }}</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="discount">{{ __('Korting') }}</label>
                        <input class="form-control" value="{{ old('discount', $user->discount) }}" id="discount" name="discount">
                    </div>

                    <div class="form-group mb-0 mt-15">
                        <button class="btn btn-primary">{{ __('Wijzigen') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection