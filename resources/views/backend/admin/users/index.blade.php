@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Gebruikers') }}</h5>
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 30px;">{{ __('ID') }}</th>
                                    <th>{{ __('Naam') }}</th>
                                    <th>{{ __('E-mailadres') }}</th>
                                    <th>{{ __('Gebruiker sinds') }}</th>
                                    <th>{{ __('Gebruik S/S/F/H/F') }}</th>
                                    <th>{{ __('Korting') }}</th>
                                    <th>{{ __('Omzet') }}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td><a href="{{ route('admin.user.edit', $user->id) }}">{{ $user->full_name }}</a></td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        {{ $user->created_at->formatLocalized('%d %B %Y') }}<br />
                                        <span style="font-size: 13px">{{ __('Last login:') }} {{ $user->last_logged_in->formatLocalized('%d %B %Y') }}
                                    </td>
                                    <td>{{ $user->sites_count }} / {{ $user->surveys_count }} / {{ $user->feedbackbuttons_count }} / {{ $user->heatmaps_count }} / {{ $user->forms_count }}</td>
                                    <td>{{ $user->discount }}%</td>
                                    <td>&euro;{{ price($user->payments->sum('total_amount')) }}</td>
                                    <td>
                                        <div class="pull-right">
                                            <a class="action action-button mr-10" href="{{ route('admin.user.edit', $user->id) }}"><i class="fa fa-pencil-alt"></i></a>
                                            <a class="action action-button" href="{{ route('admin.user.login', $user->id) }}"><i class="fa fa-user"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $users->links() }}
            </div>
        </div>
    </div>
@endsection