@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Websites') }}</h5>
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>{{ __('ID') }}</th>
                                <th style="width: 25%;">{{ __('Domein') }}</th>
                                <th>{{ __('Pakket') }}</th>
                                <th colspan="2">{{ __('Gebruiker') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($sites as $site)
                                <tr>
                                    <td>{{ $site->id }}</td>
                                    <td style="line-height: 200%;">{{ $site->domain }}</td>
                                    <td style="line-height: 200%;">
                                        @if ($site->active === false || $site->activeSubscription === null)
                                            <span class="label label-default label-package">{{ __('In-actief') }}</span>
                                            <span style="font-size: 13px; display: block;">{{ __('Website activeren') }}</span>
                                        @else
                                        <a href="{{ route('admin.subscription.edit', $site->activeSubscription->id) }}">
                                            <span class="label label-package" style="background-color: {{ $site->activeSubscription->package->color }};">{{ $site->activeSubscription->package->title }}</span>
                                            @if ($site->activeSubscription->trial === true)
                                                <span style="font-size: 13px; display: block;">{{ __('Trial eindigt op:') }} {{ $site->activeSubscription->ends_at->formatLocalized('%d %B %Y') }}</span>
                                            @elseif ($site->activeSubscription->cancelled_at <> null)
                                                <span style="font-size: 13px; display: block;">{{ __('Opgezegd per:') }} {{ $site->activeSubscription->ends_at->formatLocalized('%d %B %Y') }}</span>
                                            @else
                                                <span style="font-size: 13px; display: block;">{{ __('Verlenging op:') }} {{ $site->activeSubscription->ends_at->formatLocalized('%d %B %Y') }}</span>
                                            @endif
                                        </a>
                                        @endif
                                    </td>
                                    <td style="line-height: 200%;">
                                        {{ $site->user->email }}<br />
                                        <span style="font-size: 13px;">{{ $site->user->full_name }}</span>
                                    </td>
                                    <td>
                                        <a class="action action-button" href="{{ route('admin.user.login', $site->user->id) }}"><i class="fa fa-user"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection