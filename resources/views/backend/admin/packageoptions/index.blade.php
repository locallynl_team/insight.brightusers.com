@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Pakketopties') }}</h5>
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 30%;">{{ __('Titel') }}</th>
                                    <th>{{ __('Identifier') }}</th>
                                    <th>{{ __('Type') }}</th>
                                    <th>{{ __('Position') }}</th>
                                    <th>{{ __('Zichtbaar') }}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($packageOptions as $packageOption)
                                <tr>
                                    <td><a href="{{ route('admin.packageoption.edit', $packageOption->id) }}">{{ $packageOption->title }}</a></td>
                                    <td>{{ $packageOption->identifier }}</td>
                                    <td>{{ $packageOption->type }}</td>
                                    <td>{{ $packageOption->sort_order }}</td>
                                    <td>{{ $packageOption->visible === true ? __('Ja') : __('Nee') }}</td>
                                    <td>
                                        <div class="pull-right">
                                            <a class="action action-button mr-10" href="{{ route('admin.packageoption.edit', $packageOption->id) }}"><i class="fa fa-pencil-alt"></i></a>
                                            <a class="action action-button delete" href="{{ route('admin.packageoption.delete', $packageOption->id) }}"><i class="fa fa-trash-alt"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <a class="btn btn-primary" href="{{ route('admin.packageoption.create') }}"><i class="fa fa-plus mr-10"></i> {{ __('Pakket optie toevoegen') }}</a>
            </div>
        </div>
    </div>
@endsection