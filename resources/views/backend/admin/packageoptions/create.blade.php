@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Nieuwe pakket optie') }}</h5>
                <div class="form-wrap">
                    @include('backend.parts.errors')

                    <form method="post" action="{{ route('admin.packageoption.create') }}">
                        @csrf

                        <div class="form-group">
                            <label class="control-label mb-5 text-left" for="title">{{ __('Titel') }}</label>
                            <input class="form-control" value="{{ old('title') }}" id="title" name="title">
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-5 text-left" for="identifier">{{ __('Identifier') }}</label>
                            <input class="form-control" value="{{ old('identifier') }}" id="identifier" name="identifier">
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-5 text-left" for="type">{{ __('Type') }}</label>
                            <select class="form-control" name="type" id="type">
                                <option value="integer">{{ __('Integer') }}</option>
                                <option value="boolean">{{ __('Boolean') }}</option>
                                <option value="time_period">{{ __('Time period') }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-5 text-left" for="sort_order">{{ __('Positie') }}</label>
                            <input class="form-control" value="{{ old('sort_order') }}" id="sort_order" name="sort_order">
                        </div>

                        <div class="form-group no-margin-switcher">
                            <input type="checkbox" class="js-switch" data-color="#4aa23c" name="visible" id="visible" {{ (old('visible') == true ? 'checked' : '') }} />
                            <label class="control-label text-left mb-10" for="visible">{{ __('Zichtbaar') }}</label>
                        </div>

                        <div class="form-group mb-0 mt-15">
                            <button class="btn btn-primary">{{ __('Toevoegen') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection