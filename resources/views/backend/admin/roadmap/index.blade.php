@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Milestones') }}</h5>
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th style="width: 30px;">#</th>
                                <th>{{ __('Titel') }}</th>
                                <th>{{ __('Datum verwacht') }}</th>
                                <th>{{ __('Afgerond') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($milestones as $milestone)
                                <tr>
                                    <td>{{ $milestone->id }}</td>
                                    <td>{{ $milestone->title }}</td>
                                    <td>{{ $milestone->date_expected->formatLocalized('%d %B %Y') }} <span style="font-size: 12px;" class="ml-10">({{__('Getoond als:') }} {{ $milestone->date }})</span></td>
                                    <td>
                                        <a href="{{ route('admin.roadmap.toggle', $milestone->id) }}"><span class="label label-{{ ($milestone->status === true ? 'success' : 'danger') }}">{{ ($milestone->status === true ? 'afgerond' : 'open') }}</span></a></td>
                                    <td>
                                        <div class="pull-right">
                                            <a class="action action-button mr-10" href="{{ route('admin.roadmap.edit', $milestone->id) }}"><i class="fa fa-edit"></i></a>
                                            <a class="action action-button delete" href="{{ route('admin.roadmap.delete', $milestone->id) }}"><i class="fa fa-trash-alt"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">{{ __('Nog geen milestones toegevoegd.') }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <a class="btn btn-primary" href="{{ route('admin.roadmap.create') }}"><i class="fa fa-plus mr-10"></i> {{ __('Milestone toevoegen') }}</a>

            {{ $milestones->links() }}
        </div>
    </div>
@endsection