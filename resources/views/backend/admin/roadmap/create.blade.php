@extends('backend.layouts.app')

@section('content')
<div class="container-fluid pt-25">
    <div class="row">
        <div class="col-md-12">
            <h5 class="mb-20">{{ __('Milestone toevoegen') }}</h5>
            <div class="form-wrap">
                @include('backend.parts.errors')

                <form method="post" action="{{ route('admin.roadmap.create') }}">
                    @csrf

                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="title">{{ __('Titel') }}</label>
                        <input class="form-control" value="{{ old('title') }}" id="title" name="title">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="date">{{ __('Datum getoond') }}</label>
                        <input class="form-control" value="{{ old('date') }}" id="date" name="date">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="date_expected">{{ __('Datum verwacht') }}</label>
                        <input class="form-control" value="{{ old('date_expected', 'yyyy-mm-dd') }}" id="date_expected" name="date_expected">
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-5 text-left" for="description">{{ __('Omschrijving') }}</label>
                        <textarea class="form-control" id="description" name="description">{{ old('description') }}</textarea>
                    </div>

                    <div class="form-group mb-0 mt-15">
                        <button class="btn btn-primary">{{ __('Toevoegen') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection