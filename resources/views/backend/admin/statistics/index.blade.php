@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-lg-12">
                <h5>{{ __('Statistieken gebruik') }}</h5>
                <div id="line_chart_statistics" class="morris-chart" style="height: 520px;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($users) }}</span></span>
                                            <span class="weight-500 uppercase-font block">{{ __('gebruikers') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter">&euro; <span class="counter-anim">{{ thousand($totalRevenue) }}</span></span>
                                            <span class="weight-500 uppercase-font block">{{ __('omzet') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($sites) }}</span></span>
                                            <span class="weight-500 uppercase-font block">{{ __('sites') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($surveys) }}</span></span>
                                            <span class="weight-500 uppercase-font block">{{ __('surveys') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($feedbackButtons) }}</span></span>
                                            <span class="weight-500 uppercase-font block">{{ __('feedbackbuttons') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($heatmaps) }}</span></span>
                                            <span class="weight-500 uppercase-font block">{{ __('heatmaps') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter">{{ thousand($visits) }}</span>
                                            <span class="weight-500 uppercase-font block">{{ __('bezoekers') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter">{{ thousand($views) }}</span>
                                            <span class="weight-500 uppercase-font block">{{ __('views') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($responses) }}</span></span>
                                            <span class="weight-500 uppercase-font block">{{ __('reacties') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-dark block counter"><span class="counter-anim">{{ thousand($heatmapSessions) }}</span></span>
                                            <span class="weight-500 uppercase-font block">{{ __('heatmapsessies') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script src="/assets/backend/js/raphael.min.js"></script>
    <script src="/assets/backend/js/morris.min.js"></script>
    <script>
        //Graph for statistics
        Morris.Line({
            element: 'line_chart_statistics',
            data: [
                    @for($i = (29*24*60*60); $i >= 0; $i-= 86400)
                {
                    date: '{{ date("Y-m-d", (time()-$i)) }} 00:00:00.000',
                    visits: {{ $visitsData->where('date', date("Y-m-d", (time()-$i)))->sum('visits') }},
                    views: {{ $visitsData->where('date', date("Y-m-d", (time()-$i)))->sum('views') }},
                    responses: {{ $responseData->where('date', date("Y-m-d", (time()-$i)))->first()->responses_count ?? 0 }},
                    sessions: {{ $sessionData->where('date', date("Y-m-d", (time()-$i)))->first()->sessions_count ?? 0 }}
                },
                @endfor
            ],
            xkey: 'date',
            dateFormat: function(x) {
                var date = new Date(x);
                return (date.getDate() <= 9 ? '0' + date.getDate() : date.getDate()) + '-' + (date.getMonth() <= 9 ? '0' + date.getMonth() : date.getMonth()) + '-' + date.getFullYear();
            },
            ykeys: ['visits', 'views', 'responses', 'sessions'],
            labels: ['{{ __('Bezoekers') }}', '{{ __('Pageviews') }}', '{{ __('Reacties') }}', '{{ __('Heatmap sessies') }}'],
            pointSize: 5,
            pointStrokeColors: ['#27ae5f', '#ac235c', '#f8b32d', '#f33923'],
            behaveLikeLine: true,
            grid: true,
            gridTextColor: '#878787',
            lineWidth: 4,
            smooth: true,
            hideHover: 'true',
            lineColors: ['#27ae5f', '#ac235c', '#f8b32d', '#f33923'],
            resize: true,
            gridTextFamily: "Roboto"
        });
    </script>
@endsection