@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid pt-25">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-20">{{ __('Facturen') }}</h5>
                <div class="table-wrap">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 30px;">#</th>
                                    <th>{{ __('Klant') }}</th>
                                    <th>{{ __('Datum') }}</th>
                                    <th>{{ __('Bedrag') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($payments as $payment)
                                <tr>
                                    <td>{{ $payment->id }}</td>
                                    <td>
                                        <a href="{{ route('admin.user.edit', $payment->user->id) }}">
                                            {{ $payment->user->fullName }}<br />
                                            <span style="font-size: 12px;">{{ $payment->user->company_name }}</span>
                                        </a>
                                    </td>
                                    <td>
                                        {{ $payment->created_at->formatLocalized('%d %B %Y') }}
                                        @if ($payment->status != 'paid')
                                            <br />
                                            <span style="font-size: 12px;">{{ __('verloopt over :days dagen', ['days' => (30 - $payment->created_at->diffInDays(\Carbon\Carbon::now(), false))]) }}</span>
                                        @endif
                                    </td>
                                    <td>&euro;{{ price(($payment->total_amount + (($payment->total_amount / 100) * $payment->tax))) }}</td>
                                    <td><span class="label label-{{ ($payment->status === 'paid' ? 'success' : 'danger') }}">{{ $payment->status }}</span></td>
                                    <td>
                                        <div class="pull-right">
                                            @if (isset($payment->invoice) && $payment->invoice->generated === true)
                                                <a class="action action-button mr-10" href="{{ route('invoice', $payment->invoice->id) }}" target="_blank"><i class="fa fa-file"></i></a>
                                            @else
                                                <a class="action action-button mr-10"><i class="fa fa-spinner fa-spin"></i></a>
                                            @endif
                                            @if (isset($payment->invoice) && $payment->status != 'paid')
                                                <a class="action action-button" href="{{ route('admin.invoice.payment', $payment->invoice->id) }}"><i class="fa fa-check"></i></a>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $payments->links() }}
            </div>
        </div>
    </div>
@endsection