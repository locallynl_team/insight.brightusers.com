@extends('frontend.layouts.layout')

@section('content')
</div>
</div>

<div class="container">
    <div class="row">
        <div class="breadcrumb_list">
            <ul>
                <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                <li><a href="{{ route('contact') }}">{{ __('Contact') }}</a></li>
            </ul>
        </div>
    </div>
    <div class="row" style="margin-top: 40px;">
        <div class="col-lg-12 content-page">
            <div class="heading">
                <span>{{ __('vragen of opmerkingen?') }}</span>
                <h2>{{ __('Contact met Surve') }}</h2>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <p style="max-width: 800px; margin: 0 auto; text-align: center;">{{ __('Heeft u een vraag of opmerking over Surve, of heeft u hulp nodig bij het gebruik? Neem contact met ons op, dan helpen wij u verder.') }}</p>
                    <p style="max-width: 800px; margin: 20px auto; text-align: center;">{{ __('Onderaan deze pagina vindt u onze adresgegevens, telefoonnummer en e-mailadres. Daarlangs kunt u uiteraard ook contact met ons opnemen.') }}</p>

                    @if ($errors->any())
                        <div class="alert alert-danger" style="max-width: 800px; margin: 0 auto; padding: 15px;">
                            <strong>{{ __('Het formulier kon niet worden verstuurd, controleer onderstaande fouten en probeer het opnieuw:') }}</strong>
                            <ul style="margin: 5px 0 0 20px; padding: 0;">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(session('message'))
                        <div class="alert alert-success" style="max-width: 800px; margin: 0 auto; padding: 15px;">
                            {{ session('message') }}
                        </div>
                    @else
                        <form method="post" action="{{ route('contact') }}">
                            @csrf

                            <div class="form">
                                {!! Honeypot::generate('first_name', 'time') !!}
                                <label for="name" style="text-align: left; display: block;">{{ __("Uw naam") }}</label>
                                <input id="name" type="text" value="{{ old('name') }}" name="name">
                                <label for="email" style="text-align: left; display: block;">{{ __('Uw e-mailadres') }}</label>
                                <input id="email" type="text" value="{{ old('email') }}" name="email">
                                <label for="phone" style="text-align: left; display: block;">{{ __('Uw telefoonnummer') }}</label>
                                <input id="phone" type="text" value="{{ old('phone') }}" name="phone" placeholder="{{ __('optioneel') }}">
                                <label for="message" style="text-align: left; display: block;">{{ __('Uw bericht') }}</label>
                                <textarea id="message" name="message">{{ old('message') }}</textarea>
                                <button>{{ __('Verstuur bericht') }}</button>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection