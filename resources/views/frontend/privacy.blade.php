@extends('frontend.layouts.layout')

@section('content')
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="breadcrumb_list">
                <ul>
                    <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                    <li><a href="{{ route('privacy') }}">{{ __('Privacyverklaring') }}</a></li>
                </ul>
            </div>
        </div>
        <div class="row" style="margin-top: 40px;">
            <div class="col-lg-12 content-page">
                <div class="heading">
                    <span>{{ __('wat doet Surve met uw gegevens') }}</span>
                    <h2>{{ __('Privacyverklaring Surve') }}</h2>
                </div>
                <p style="margin-top: 40px;">
                    <strong>Privacyverklaring Surve</strong>
                    <strong class="pull-right">Laatste aanpassing: 18-04-2018</strong>
                </p>
                <p>
                    Surve, onderdeel van Blue Popsicle B.V., gevestigd aan de Schuttevaerkade 80, 8021DB te Zwolle, is verantwoordelijk voor de verwerking van persoonsgegevens zoals weergegeven in deze privacyverklaring.
                </p>
                <p>
                    <strong>Persoonsgegevens die wij verwerken</strong><br />
                    Surve verwerkt uw persoonsgegevens doordat u gebruik maakt van onze diensten en/of u een website bezoekt waarop Surve gebruikersonderzoek verricht voor haar klanten. Wij verwerken de volgende gegevens:
                </p>
                <ul>
                    <li>Uw internetbrowser en type apparaat</li>
                    <li>Uw schermresolutie</li>
                    <li>Uw besturingssysteem</li>
                    <li>Uw ingevulde informatie in Surve vragenlijsten of feedback formulieren</li>
                </ul>
                <p>De informatie die verzameld wordt met gebruikersonderzoek is geanonimiseerd, en hierdoor niet terug te herleiden naar een specifieke bezoeker.</p>

                <p>Wanneer u een account heeft bij Surve verwerken wij ook onderstaande informatie:</p>
                <ul>
                    <li>Uw voor en achternaam</li>
                    <li>Uw adresgegevens</li>
                    <li>Uw telefoonnummer</li>
                    <li>Uw e-mailadres</li>
                    <li>Uw betaalinformatie</li>
                </ul>

                <p>
                    <strong>Met welk doel en op basis van welke grondslag wij persoonsgegevens verwerken</strong><br />
                    Surve verwerkt uw gegevens om onderzoek te kunnen doen naar het gebruik van een website. Door middel van vragenlijsten helpt u een website-eigenaar om een betere gebruikerservaring te creëren. Daarnaast wordt door middel van heatmaps bekeken hoe u als bezoeker een pagina gebruikt.
                </p>
                <p>Voor gebruikers van Surve verwerken wij uw persoonsgegevens om u een account te kunnen bieden om uw diensten te beheren. Daarnaast kunnen wij u telefonisch benaderen voor informatie over onze diensten of supportverzoeken.</p>

                <p>
                    <strong>Hoe lang we persoonsgegevens bewaren</strong><br />
                    Gegevens worden maximaal 12 maanden bewaard door Surve. Wanneer een gebruiker besluit de dienstverlening van Surve niet langer te gebruiken wordt alle relevante informatie direct verwijderd.
                </p>

                <p>
                    <strong>Delen van persoonsgegevens met derden</strong><br />
                    Surve verstrekt alleen aan derden als dit nodig is voor de uitvoering van onze overeenkomst met u of om te voldoen aan een wettelijke verplichting.
                </p>

                <p>
                    <strong>Cookies, of vergelijkbare technieken, die wij gebruiken</strong><br />
                    Surve gebruikt functionele, analytische en tracking cookies. Een cookie is een klein tekstbestand dat bij het eerste bezoek aan deze website wordt opgeslagen in de browser van uw computer, tablet of smartphone. Surve gebruikt cookies met een puur technische functionaliteit. Deze zorgen ervoor dat de website naar behoren werkt en dat bijvoorbeeld uw voorkeursinstellingen onthouden worden. Deze cookies worden ook gebruikt om de website te kunnen optimaliseren.
                </p>
                <p>Door de gebruikte trackingpixel van Surve worden geen cookies van derden geplaatst, hier wordt enkel gebruik gemaakt van functionele cookies.</p>

                <p>
                    <strong>Gegevens inzien, aanpassen of verwijderen</strong><br />
                    U heeft het recht om uw persoonsgegevens in te zien, te corrigeren of te verwijderen. Wanneer u een account heeft kunt u dit zelf doen via de persoonlijke instellingen van uw account. Daarnaast heeft u het recht om uw eventuele toestemming voor de gegevensverwerking in te trekken of bezwaar te maken tegen de verwerking van uw persoonsgegevens door ons bedrijf. Ook heeft u het recht op gegevensoverdraagbaarheid. Dat betekent dat u bij ons een verzoek kunt indienen om de persoonsgegevens die wij van u beschikken in een computerbestand naar u of een ander (een door u genoemde organisatie) te sturen.
                </p>
                <p>Wilt u gebruik maken van uw recht op bezwaar en/of recht op gegevensoverdraagbaarheid of heeft u andere vragen/opmerkingen over de gegevensverwerking, stuur dan een gespecificeerd verzoek naar info@surve.nl. Om er zeker van te zijn dat het verzoek tot inzage door u is gedaan, vragen wij u een kopie van uw identiteitsbewijs bij het verzoek mee te sturen. Maak in deze kopie uw pasfoto, MRZ (machine readable zone, de strook met nummers onderaan het paspoort), paspoortnummer en Burgerservicenummer (BSN) zwart. Dit ter bescherming van uw privacy. Surve zal zo snel mogelijk op uw verzoek reageren.</p>

                <p>
                    <strong>Hoe wij persoonsgegevens beveiligen</strong><br />
                    Surve neemt de bescherming van uw gegevens serieus en neemt passende maatregelen om misbruik, verlies, onbevoegde toegang, ongewenste openbaarmaking en ongeoorloofde wijziging tegen te gaan. Als u de indruk heeft dat uw gegevens niet goed beveiligd zijn of er aanwijzingen zijn van misbruik neemt u dan contact op met onze klantenservice op info@surve.nl.
                </p>
            </div>
        </div>
    </div>

@endsection
