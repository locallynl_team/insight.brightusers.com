@extends('frontend.layouts.layout')

@section('content')
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="breadcrumb_list">
                <ul>
                    <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                    <li><a href="{{ route('terms') }}">{{ __('Algemene voorwaarden') }}</a></li>
                </ul>
            </div>
        </div>
        <div class="row" style="margin-top: 40px;">
            <div class="col-lg-12 content-page">
                <div class="heading">
                    <span>{{ __('rechten en plichten') }}</span>
                    <h2>{{ __('Algemene voorwaarden Surve') }}</h2>
                </div>
                <p style="margin-top: 40px;">
                    <strong>Algemene voorwaarden Surve</strong>
                    <strong class="pull-right">Laatste aanpassing: 17-09-2018</strong>
                </p>
                <p>
                    <strong>Artikel 1. ALGEMEEN</strong>
                    <br/>
                    1.1. Deze voorwaarden zijn van toepassing op iedere vorm van
                    dienstverlening, opdrachten en overeenkomsten van Surve, handelsnaam van
                    Blue Popsicle B.V. De algemene voorwaarden zijn terug te vinden op
                    www.surve.nl en worden op verzoek gratis toegezonden;
                    <br/>
                    1.2. Aanvullingen of afwijkingen van deze voorwaarden moeten schriftelijk
                    overeengekomen worden en gelden alleen voor die overeenkomst waarvoor ze
                    gemaakt zijn;
                    <br/>
                    1.3. De rechten en verplichtingen uit overeenkomsten tussen Surve en de
                    wederpartij kunnen door de wederpartij niet aan derden worden overgedragen,
                    tenzij met schriftelijke toestemming van Surve;
                    <br/>
                    1.4. Andersluidende algemene voorwaarden, daaronder begrepen die van de
                    wederpartij, worden door Surve niet geaccepteerd, tenzij schriftelijk
                    anders is overeengekomen en door Surve is bevestigd;
                </p>
                <p>
                    <strong>Artikel 2. OVEREENKOMSTEN</strong>
                    <br/>
                    2.1. Een overeenkomst wordt eerst geacht rechtsgeldig tot stand te zijn
                    gekomen nadat de aanmelding bij Surve is afgerond. De inhoud van de
                    overeenkomst wordt bepaald door deze algemene voorwaarden;
                    <br/>
                    2.2. De wederpartij zal zich ter zake van de opdracht nooit kunnen beroepen
                    op de omstandigheid dat hij namens een derde handelde, tenzij hij zulks
                    uitdrukkelijk aan de Surve ter kennis heeft gebracht en Surve alsdan onder
                    deze voorwaarde de opdracht schriftelijk heeft aanvaard;
                    <br/>
                    2.3. Surve zal haar diensten naar beste inzicht en vermogen en in
                    overeenstemming met de eisen van goed vakmanschap uitvoeren en op grond van
                    de op dat moment bekende stand van de wetenschap en met inachtneming van de
                    gedrag- en beroepsregels. Surve verklaart tegenover de wederpartij de zorg
                    te betrachten die van een redelijk bekwaam en redelijk handelend vakgenoot
                    mag worden verwacht echter kan niet instaan voor het bereiken van een
                    uiteindelijk resultaat of rechtsgevolg, maar zal zich naar beste vermogen
                    inspannen (inspanningsverbintenis);
                    <br/>
                    2.4. Een overeenkomst wordt aangegaan voor een periode van een (1) maand,
                    of 12 maanden, waarna het contract stilzwijgend met eenzelfde periode wordt
                    verlengd, tenzij naar inhoud, aard of strekking van de overeenkomst anderszins
                    voortvloeit, of partijen nadrukkelijk anders zijn overeengekomen;
                </p>
                <p>
                    <strong>Artikel 3. OPZEGGING OVEREENKOMST</strong>
                    <br/>
                    Opzegging van de overeenkomst dient via de Beheeromgeving door de wederpartij
                    te worden gedaan, er wordt geen opzeggingstermijn gehanteerd;
                </p>
                <p>
                    <strong>Artikel 4. NIET-NAKOMING / ONTBINDING / OPSCHORTING</strong>
                    <br/>
                    4.1. Surve is bevoegd de overeenkomst met onmiddellijke ingang, zonder
                    rechterlijke tussenkomst, geheel of gedeeltelijk te ontbinden of de
                    uitvoering op te schorten, zulks onverminderd de haar overigens toekomende
                    rechten (op nakoming en/of schadevergoeding), indien:
                    <br/>
                    – wederpartij in strijd handelt met enige bepaling van de overeenkomst
                    tussen partijen;
                    <br/>
                    – wederpartij overlijdt, surséance van betaling aanvraagt of aangifte tot
                    faillietverklaring doet;
                    <br/>
                    – faillissement van wederpartij wordt aangevraagd of het bedrijf van
                    wederpartij wordt stilgelegd of geliquideerd;
                    <br/>
                    – een onderhands akkoord wordt aangeboden of op enig vermogensbestanddeel
                    van wederpartij beslag wordt gelegd;
                    <br/>
                    4.2. Het in lid 1 van dit artikel bepaalde is van overeenkomstige
                    toepassing indien wederpartij, na daar toe schriftelijk te zijn
                    uitgenodigd, niet binnen zeven dagen naar het oordeel van Surve passende
                    zekerheid heeft gesteld;
                    <br/>
                    4.3. Voorts is Surve bevoegd de overeenkomst te (doen) ontbinden indien
                    zich omstandigheden voordoen welke van dien aard zijn dat nakoming van de
                    overeenkomst onmogelijk – of naar maatstaven van redelijkheid en
                    billijkheid – niet langer kan worden gevergd dan wel indien zich anderszins
                    omstandigheden voordoen welke van dien aard zijn dat ongewijzigde
                    instandhouding van de overeenkomst in redelijkheid niet mag worden
                    verwacht;
                    <br/>
                    4.4. Indien de overeenkomst wordt ontbonden zijn de vorderingen van Surve
                    op de wederpartij onmiddellijk opeisbaar. Indien Surve de nakoming van de
                    verplichtingen opschort, behoudt zij haar aanspraken uit de wet en
                    overeenkomst;
                    <br/>
                    4.5. Surve behoudt steeds het recht schadevergoeding te vorderen.
                </p>
                <p>
                    <strong>Artikel 4a. OPSCHORTING / BUITENGEBRUIKSTELLING</strong>
                    <br/>
                    4a.1. Surve heeft het recht de dienst (tijdelijk) buiten gebruik te stellen
                    en/of het gebruik ervan te beperken indien de wederpartij ter zake van de
                    overeenkomst een verplichting jegens Surve niet nakomt, dan wel in strijd
                    handelt met deze algemene voorwaarden;
                    <br/>
                    4a.2. De verplichting tot betaling van de verschuldigde bedragen blijft ook
                    tijdens de buitengebruikstelling bestaan;
                    <br/>
                    4a.3. Tot indienststelling wordt overgegaan indien de wederpartij binnen
                    een door Surve gestelde termijn zijn verplichtingen is nagekomen;
                    <br/>
                    4a.4. De kosten van opheffing buitengebruikstelling komen voor rekening van
                    de wederpartij;
                </p>
                <p>
                    <strong>Artikel 5. PRIJZEN</strong>
                    <br/>
                    5.1. Alle prijsopgaven en de prijzen die Surve in rekening brengt, zijn de
                    op het moment van de aanbieding c.q. van het tot stand komen van de
                    overeenkomst geldende prijzen exclusief btw en andere op de overeenkomst
                    vallende kosten en tarieven in euro’s, tenzij schriftelijk anders is
                    overeengekomen;
                    <br/>
                    5.2. Indien zich na het tot stand komen van de overeenkomst een wijziging
                    in één der prijsbepalende factoren voordoet, is Surve gerechtigd de prijzen
                    dienovereenkomstig aan te passen;
                </p>
                <p>
                    <strong>Artikel 6. BETALING</strong>
                    <br/>
                    6.1. Betaling dient binnen 14 dagen na factuurdatum te geschieden zonder
                    enige opschorting of verrekening;
                    <br/>
                    6.2. De wederpartij is in verzuim na verloop van de in lid 1 van dit
                    artikel genoemde betalingstermijn zonder dat daartoe een ingebrekestelling
                    is vereist, ongeacht of de overschrijding daarvan wederpartij al dan niet
                    kan worden toegerekend:
                    <br/>
                    6.3. Onverminderd de haar verder toekomende rechten, is Surve alsdan
                    bevoegd de wettelijke (handels)rente te berekenen over het openstaande
                    bedrag vanaf de betreffende vervaldag;
                    <br/>
                    6.4. Alle door Surve gemaakte buitengerechtelijke en gerechtelijke kosten
                    in het kader van een geschil met wederpartij, zowel eisende als verwerende,
                    komen voor rekening van wederpartij;
                    <br/>
                    6.5. Binnenkomende betalingen strekken tot voldoening van de oudst
                    openstaande posten- rente en kosten daaronder begrepen, zelfs al verklaart
                    de wederpartij te dien aanzien anders;
                    <br/>
                    6.6. In geval van een gezamenlijke opdracht is iedere wederpartij
                    individueel hoofdelijk aansprakelijk voor betaling van het hele
                    factuurbedrag.
                </p>
                <p>
                    <strong>Artikel 7. UITVOERING OVEREENKOMST</strong>
                    <br/>
                    7.1. Toegang tot de diensten van Surve verkrijgt wederpartij door middel
                    van vertrouwelijke inloggegevens;
                    <br/>
                    7.2. Indien derden zich (onrechtmatig) toegang verschaffen tot het account
                    van de wederpartij, dan is de wederpartij verplicht direct na constatering
                    hiervan Surve op de hoogte te brengen. Surve is nimmer aansprakelijk voor
                    eventuele schade van de wederpartij als gevolg van ongeautoriseerd toegang;
                    <br/>
                    7.3. De wederpartij is verplicht om Surve onverwijld op de hoogte te
                    stellen wanneer zich wijzigingen voordoen in de naam, e-mailadres(sen) of
                    overige gegevens van de wederpartij;
                    <br/>
                    7.4. Surve kan op elk moment onderhoud (laten) uitvoeren aan het netwerk,
                    servers, websites, applicaties, hosting services of andere dienstverlening
                    waardoor de diensten tijdelijk niet beschikbaar zijn;
                    <br/>
                    7.5. De helpdesk verzorgt per e-mail en telefoon ondersteunende service aan
                    de wederpartij. De wederpartij kan geen rechten ontlenen aan informatie,
                    verstrekt door de medewerkers van de helpdesk;
                </p>
                <p>
                    <strong>Artikel 8 LICENTIE en INTELLECTUEEL EIGENDOM</strong>
                    <br/>
                    8.1. Alle rechten van intellectuele of industriële eigendom op alle
                    krachtens de overeenkomst ontwikkelde of ter beschikking gestelde diensten
                    – daaronder begrepen maar niet beperkt – zoals software inclusief broncode,
                    databases, analyses, ontwerpen, documentatie, rapportages alsmede
                    voorbereidend materiaal berusten bij Surve of diens licentiegevers;
                    <br/>
                    8.2. De wederpartij verkrijgt de gebruikersrechten en bevoegdheden welke
                    behoren bij de overeengekomen diensten. De door Surve verstrekte diensten
                    of producten mogen niet worden bewerkt, vereenvoudigd of gekopieerd, tenzij
                    hiervoor expliciet toestemming is verleend;
                    <br/>
                    8.3. Het is wederpartij niet toegestaan door Surve aangebrachte merk- en
                    herkenningstekens te wijzigen of te verwijderen zonder voorafgaande
                    toestemming van Surve;
                    <br/>
                    8.4. Indien inbreuk wordt gemaakt op het in lid 2 en 3 van dit artikel
                    genoemde, kan een onmiddellijk opeisbare vordering worden opgelegd, van een
                    boete van € 10.000,00 met een boete van € 500,00 voor iedere dag dat de
                    inbreuk voortduurt.
                </p>
                <p>
                    <strong>Artikel 9. RECLAME</strong>
                    <br/>
                    9.1. Indien de door Surve geleverde dienst niet voldoet aan de daaraan te
                    stellen eisen, is de wederpartij verplicht dit direct na constatering
                    schriftelijk of per e-mail te melden aan Surve. Surve is niet aansprakelijk
                    voor de gevolgen zoals rekenfouten. Surve zal een storing zo spoedig
                    mogelijk opheffen. Surve geeft geen garanties af over doorlooptijden;
                    <br/>
                    9.2. Reclames m.b.t. de factuur en/of verrichtte werkzaamheden zoals online
                    dienstverlening dienen binnen 8 dagen na ontvangst en/of voltooiing te
                    worden ingediend (per e-mail). Na het verstrijken van deze termijn worden
                    reclames niet meer in behandeling genomen. De klacht dient een zo
                    gedetailleerd mogelijke beschrijving van de tekortkoming te bevatten, zodat
                    Surve in staat is adequaat te reageren. Surve zal wederpartij binnen 18
                    werkdagen na ontvangst van de klacht schriftelijk of per e-mail op de
                    hoogte brengen van de gegronde, dan wel ongegronde bevindingen;
                    <br/>
                    9.3. In geval van een terecht uitgebrachte reclame heeft de wederpartij de
                    keuze tussen aanpassing van het in rekening gebrachte bedrag, of het
                    kosteloos verbeteren en/of opnieuw verrichten van de afgekeurde
                    dienst/werkzaamheden;
                    <br/>
                    9.4. Reclame is niet mogelijk indien:
                    <br/>
                    – wederpartij niet aan zijn verplichtingen jegens Surve (zowel financieel
                    als anderszins) heeft voldaan.
                    <br/>
                    – Indien aan de uitgevoerde opdracht/dienst andere en/of zwaardere eisen
                    zijn gesteld, dan bij het tot stand komen van de overeenkomst bekend waren;
                    <br/>
                    – een hostingdienst, applicatie of website tijdelijk niet beschikbaar is
                    door storting of (spoed) onderhoud;
                    <br/>
                    9.5. Het in behandeling nemen van een reclame schort de
                    betalingsverplichting van wederpartij niet op.
                    <br/>
                    9.6. Indien buiten de boven omschreven gevallen aan een klacht aandacht
                    wordt geschonken, geschiedt dit geheel onverplicht en kan wederpartij
                    hieraan geen rechten ontlenen.
                </p>
                <p>
                    <strong>Artikel 10. AANSPRAKELIJKHEID</strong>
                    <br/>
                    10.1. Surve is niet aansprakelijk voor schade ontstaan als gevolg van enige
                    tekortkoming in de nakoming van haar verbintenis(sen) jegens wederpartij
                    tenzij er sprake is van opzet of grove schuld. De nakoming van de
                    verplichtingen uit garantie/reclame zoals omschreven in artikel 9 hiervoor
                    geldt als enige en algehele schadevergoeding. Elke andere vordering tot
                    schadevergoeding, uit welken hoofde dan ook, is uitgesloten;
                    <br/>
                    10.2. Surve aanvaardt geen aansprakelijkheid voor, door of namens haar
                    verstrekte adviezen;
                    <br/>
                    10.3. De aansprakelijkheid van Surve voor schade als gevolg van een
                    toerekenbare tekortkoming in de nakoming van deze overeenkomst, is per
                    gebeurtenis (een reeks opeenvolgende gebeurtenissen geldt als één
                    gebeurtenis) beperkt tot de vergoeding van directe schade, tot maximaal het
                    bedrag van de door Surve ontvangen vergoedingen voor de werkzaamheden onder
                    deze overeenkomst over de maand voorafgaande aan de schadeveroorzakende
                    gebeurtenis, althans dat gedeelte van de opdracht waarop de
                    aansprakelijkheid betrekking heeft, althans tot maximaal EURO 2.500,00
                    (Zegge: vijfentwintighonderd euro). De aansprakelijkheid is te allen tijde
                    beperkt tot maximaal het bedrag van de door de assuradeur van Surve in het
                    voorkomende geval te verstrekken uitkering. Voldoening aan deze bepaling
                    geldt als enige en volledige schadevergoeding;
                    <br/>
                    10.4. Onder directe schade wordt uitsluitend verstaan:
                    <br/>
                    – de redelijke kosten ter vaststelling van de oorzaak en de omvang van de
                    schade, voor zover de vaststelling betrekking heeft op schade in de zin van
                    deze voorwaarden;
                    <br/>
                    – de eventuele redelijke kosten gemaakt om de gebrekkige prestatie van
                    Surve aan de overeenkomst te laten beantwoorden, tenzij deze niet aan Surve
                    toegerekend kunnen worden;
                    <br/>
                    – de redelijke kosten, gemaakt ter voorkoming of beperking van schade, voor
                    zover wederpartij aantoont dat deze kosten hebben geleid tot beperking van
                    directe schade als bedoeld in deze algemene voorwaarden;
                    <br/>
                    10.5. Surve is nimmer aansprakelijk voor indirecte schade, daaronder
                    begrepen gevolgschade, gederfde winst, gemiste besparingen en schade door
                    bedrijfsstagnatie;
                    <br/>
                    10.6. Surve kan nimmer aansprakelijk worden gesteld voor beschadiging of
                    teloorgang van de bij haar of derden opgeslagen gegevens en bescheiden van
                    de wederpartij;
                    <br/>
                    10.7. De wederpartij vrijwaart Surve tegen alle aanspraken van derden,
                    welke direct of indirect met de uitvoering van de overeenkomst samenhangen.
                    De plicht van wederpartij tot vrijwaring van Surve vervalt indien en voor
                    zover de wederpartij aantoont dat de schade het directe gevolg is van een
                    toerekenbare tekortkoming van Surve;
                </p>
                <p>
                    <strong>Artikel 11. OVERMACHT</strong>
                    <br/>
                    11.1. Partijen zijn niet gehouden tot het nakomen van enige verplichting,
                    indien zij daartoe gehinderd worden als gevolg van een omstandigheid die
                    niet is te wijten aan schuld, en noch krachtens de wet, een rechtshandeling
                    of in het maatschappelijk verkeer geldende opvattingen, voor hun rekening
                    komt;
                    <br/>
                    11.2. Onder overmacht wordt in deze algemene voorwaarden verstaan naast
                    hetgeen daaromtrent in de wet en jurisprudentie wordt begrepen, alle van
                    buiten komende oorzaken, voorzien of niet voorzien, waarop Surve geen
                    invloed kan uitoefenen, doch waardoor Surve niet in staat is haar
                    verplichtingen na te komen. Werkstakingen in het bedrijf van Surve worden
                    daaronder begrepen;
                    <br/>
                    11.3. Surve heeft ook het recht zich op overmacht te beroepen indien de
                    omstandigheid die (verdere) nakoming verhindert, intreedt nadat Surve haar
                    verplichtingen had moeten nakomen;
                    <br/>
                    11.4. Partijen kunnen gedurende de periode dat de overmacht voortduurt de
                    verplichtingen uit de overeenkomst opschorten. Indien deze periode langer
                    duurt dan twee maanden is ieder der partijen gerechtigd de overeenkomst te
                    ontbinden, zonder verplichting tot vergoeding van schade aan de andere
                    partij;
                    <br/>
                    11.5. Voor zover Surve ten tijde van het intreden van overmacht inmiddels
                    gedeeltelijk haar verplichtingen uit de overeenkomst is nagekomen of deze
                    zal kunnen nakomen, en aan het nagekomen respectievelijk na te komen
                    gedeelte zelfstandige waarde toekomt, is Surve gerechtigd om het reeds
                    nagekomen respectievelijk na te komen gedeelte separaat te declareren.
                    Wederpartij is gehouden deze declaratie te voldoen als ware het een
                    afzonderlijke overeenkomst.
                </p>
                <p>
                    <strong>Artikel 12. GEHEIMHOUDING</strong>
                    <br/>
                    12.1. Surve is – behoudens wettelijke verplichtingen tot openbaarmaking –
                    verplicht tot geheimhouding tegenover derden, die niet bij de uitvoering
                    van de opdracht zijn betrokken. Dit betreft alle aan Surve verstrekte
                    informatie, alsmede alle uit de werkzaamheden voortvloeiende resultaten en
                    gegevens. Tevens betreft dit alle door Surve aan wederpartij verstrekte
                    adviezen of andere al dan niet schriftelijke uitingen, die niet zijn
                    bedoeld om derden hiervan te voorzien;
                    <br/>
                    12.2. Wederpartij verbindt zich tot volledige geheimhouding van alle
                    gegevens en informatie betreffende Surve of haar bedrijf, zowel tijdens als
                    na beëindiging van de overeenkomst en de relatie tussen partijen, voor
                    zover deze gegevens vertrouwelijk zijn verstrekt of een kennelijk
                    vertrouwelijk karakter hebben.
                </p>
                <p>
                    <strong>Artikel 13. PERSOONSGEGEVENS</strong>
                    <strong>
                        <br/>
                    </strong>
                    13.1. De wederpartij beschikt over persoonsgegevens van diverse betrokkenen
                    en is Verwerkingsverantwoordelijke in de zin van de Algemene Verordening
                    Gegevensbescherming (AVG).
                    <br/>
                    13.2. De Verwerkingsverantwoordelijke bepaalt de vormen van verwerking van
                    deze persoonsgegevens en wil deze laten verrichten door Surve, waarbij de
                    Verwerkingsverantwoordelijke doel en middelen aanwijst en dat Surve daarom
                    als Verwerker wordt gekwalificeerd in de zin van de AVG.
                    <br/>
                    13.3. Dat de artikelen 13 tot en met 24 worden gekwalificeerd als een
                    Verwerkersovereenkomst.
                </p>
                <p>
                    <strong>Artikel 14. DOELEINDEN VAN VERWERKING</strong>
                    <strong>
                        <br/>
                    </strong>
                    14.1. Verwerker verbindt zich onder de voorwaarden van deze
                    Verwerkersovereenkomst in opdracht van Verwerkingsverantwoordelijke
                    persoonsgegevens te werken. Verwerking zal uitsluitend plaats vinden in het
                    kader van de uitvoering van de levering van producten en diensten in het
                    kader van de onderliggende overeenkomst en de doeleinden die daarmee
                    redelijkerwijs samenhangen of die met nadere instemming worden bepaald.
                    <br/>
                    14.2. Verwerker zal de persoonsgegevens niet voor enig ander doel verwerken
                    dan zoals door Verwerkingsverantwoordelijke is vastgesteld.
                    Verwerkingsverantwoordelijke zal Verwerker op de hoogte stellen van het
                    onderwerp en de duur van de verwerking, de aard en het doel van de
                    verwerking, het soort persoonsgegevens en de categorieën van betrokkenen,
                    en de rechten en verplichtingen van de verwerkingsverantwoordelijke jegens
                    betrokkenen, zoals bedoeld in artikel 28 lid 3 AVG, voor zover deze
                    gegevens niet reeds in deze Verwerkersovereenkomst of de onderliggende
                    overeenkomst zijn genoemd.
                    <br/>
                    14.3. De in opdracht van Verwerkingsverantwoordelijke te verwerken
                    persoonsgegevens blijven eigendom van Verwerkingsverantwoordelijke en / of
                    de betreffende betrokkenen.
                </p>
                <p>
                    <strong>Artikel 15. VERPLICHTINGEN VERWERKER</strong>
                    <strong>
                        <br/>
                    </strong>
                    15.1. Ten aanzien van de in artikel 14 genoemde verwerkingen zal Verwerker
                    zorg dragen voor de naleving van de toepasselijke wet- en regelgeving,
                    waaronder in ieder geval begrepen de wet- en regelgeving op het gebied van
                    de bescherming van persoonsgegevens.
                    <br/>
                    15.2. De verplichtingen van de Verwerker die uit deze
                    Verwerkersovereenkomst voortvloeien, gelden ook voor degenen die
                    persoonsgegevens verwerken onder het gezag van Verwerker, waaronder
                    begrepen maar niet beperkt tot werknemers, in de ruimste zin van het woord.
                    <br/>
                    15.3. Het is de Verwerker toegestaan om andere Verwerkers in te schakelen.
                    De Verwerkingsverantwoordelijke zal hiervan vooraf schriftelijk op de
                    hoogte worden gesteld.
                </p>
                <p>
                    <strong>Artikel 16. DOORGIFTE VAN PERSOONSGEVENS</strong>
                    <strong>
                        <br/>
                    </strong>
                    16.1. Verwerker mag de persoonsgegevens verwerken in landen binnen de
                    Europese Unie. Doorgifte naar landen buiten de Europese Unie is verboden.
                    <br/>
                    16.2. Verwerker zal Verwerkingsverantwoordelijke melden om welk land of
                    landen het gaat.
                </p>
                <p>
                    <strong>Artikel 17. VERDELING VAN VERANTWOORDELIJKHEID</strong>
                    <strong>
                        <br/>
                    </strong>
                    17.1. De toegestane verwerkingen zullen door medewerkers van Verwerker
                    worden uitgevoerd binnen een geautomatiseerde omgeving.
                    <br/>
                    17.2. Verwerker is louter verantwoordelijk voor de verwerking van de
                    persoonsgegevens onder deze Verwerkersovereenkomst, overeenkomstig de
                    instructies van Verwerkingsverantwoordelijke en onder de uitdrukkelijke
                    (eind) verantwoordelijkheid van Verwerkingsverantwoordelijke. Voor de
                    overige verwerkingen van persoonsgegevens, waaronder in ieder geval
                    begrepen maar niet beperkt tot de verzameling van de persoonsgegevens door
                    de Verwerkingsverantwoordelijke, verwerkingen voor doeleinden die niet door
                    Verwerkingsverantwoordelijke aan Verwerker zijn gemeld, verwerkingen door
                    derden en/of voor andere doeleinden, is Verwerker uitdrukkelijk niet
                    verantwoordelijk.
                    <br/>
                    17.3. Verwerkingsverantwoordelijke garandeert dat de inhoud, het gebruik en
                    de opdracht tot de verwerkingen van de persoonsgegevens zoals bedoeld in
                    deze Overeenkomst, niet onrechtmatig is en geen inbreuk maken op enig recht
                    van derden.
                </p>
                <p>
                    <strong>Artikel 18. BEVEILIGING</strong>
                    <strong>
                        <br/>
                    </strong>
                    18.1. Verwerker zal zich inspannen voldoende technische en organisatorische
                    maatregelen, zoals bedoeld in artikel 32 AVG te nemen met betrekking tot de
                    te verrichten verwerkingen van persoonsgegevens, tegen verlies of tegen
                    enige vorm van onrechtmatige verwerking (zoals onbevoegde kennisname,
                    aantasting, wijziging of verstrekking van de persoonsgegevens).
                    <br/>
                    18.2. Verwerker heeft in ieder geval de volgende maatregelen genomen:
                    <br/>
                    – Logische toegangscontrole, gebruik makend van wachtwoorden;
                    <br/>
                    – Encryptie (versleuteling) van wachtwoorden;
                    <br/>
                    – Beperkte fysieke toegang tot de servers waar de persoonsgegevens zijn
                    opgeslagen;
                    <br/>
                    – Beveiligde communicatie met server (HTTPS/SSL).
                    <br/>
                    18.3. Verwerker staat er niet voor in dat de beveiliging onder alle
                    omstandigheden doeltreffend is. Indien een uitdrukkelijk omschreven
                    beveiliging in de Verwerkersovereenkomst ontbreekt, zal Verwerker zich
                    inspannen dat de beveiliging zal voldoen aan een niveau dat, gelet op de
                    stand van de techniek, de gevoeligheid van de persoonsgegevens en de aan
                    het treffen van de beveiliging verbonden kosten, niet onredelijk is.
                    <br/>
                    18.4. Verwerkingsverantwoordelijke stelt enkel persoonsgegevens aan
                    Verwerker ter beschikking voor verwerking, indien zij zich ervan heeft
                    verzekerd dat de vereiste beveiligingsmaatregelen zijn getroffen.
                    Verwerkingsverantwoordelijke is verantwoordelijk voor de naleving van de
                    door Partijen afgesproken maatregelen.
                </p>
                <p>
                    <strong>Artikel 19. MELDPLICHT</strong>
                    <strong>
                        <br/>
                    </strong>
                    19.1. In het geval van een beveiligingsinbreuk en/of een datalek in de zin
                    van artikel 33 AVG zal Verwerker de Verwerkingsverantwoordelijke daarover
                    zonder onredelijke vertraging informeren.
                    <br/>
                    19.2. De Verwerker zal de Verwerkingsverantwoordelijke waar mogelijk
                    bijstaan in het nakomen van zijn verantwoordelijkheden jegens de
                    toezichthoudende autoriteit en/of betrokkenen zoals bedoeld in artikel 33
                    en 34 AVG. Voor deze werkzaamheden zal een redelijke vergoeding in rekening
                    worden gebracht, tenzij in de Onderliggende Overeenkomst anders is
                    overeengekomen.
                </p>
                <p>
                    <strong>Artikel 20. AFHANDELING VERZOEKEN VAN BETROKKENEN</strong>
                    <strong>
                        <br/>
                    </strong>
                    20.1. In het geval dat een betrokkene een verzoek tot inzage, rectificatie,
                    wissing en/of beperking van de verwerking zoals bedoeld in artikel 15 – 19
                    AVG, richt aan Verwerker, zal Verwerker het verzoek zelf afhandelen voor zo
                    ver hij dit zelf kan doen. Hij zal Verwerkingsverantwoordelijke van het
                    verzoek op de hoogte te stellen.
                    <br/>
                    20.2. Verwerker mag de kosten voor de afhandeling van het verzoek
                    doorbelasten aan Verwerkingsverantwoordelijke.
                </p>
                <p>
                    <strong>Artikel 21. GEHEIMHOUDING EN VERTROUWELIJKHEID</strong>
                    <strong>
                        <br/>
                    </strong>
                    21.1. Op alle persoonsgegevens die Verwerker van
                    Verwerkingsverantwoordelijke ontvangt en/of zelf verzamelt in het kader van
                    deze Verwerkersovereenkomst, rust een geheimhoudingsplicht jegens derden.
                    Verwerker zal deze informatie niet voor een ander doel gebruiken dan
                    waarvoor zij deze heeft verkregen.
                    <br/>
                    21.2. Deze geheimhoudingsplicht is niet van toepassing voor zover
                    Verwerkingsverantwoordelijke uitdrukkelijke toestemming heeft gegeven om de
                    informatie aan derden te verschaffen, indien het verstrekken van de
                    informatie aan derden logischerwijs noodzakelijk is gezien de aard van de
                    verstrekte opdracht en de uitvoering van deze Verwerkersovereenkomst, of
                    indien er een wettelijke verplichting bestaat om de informatie aan een
                    derde te vertrekken.
                </p>
                <p>
                    <strong>Artikel 22. AUDIT</strong>
                    <strong>
                        <br/>
                    </strong>
                    22.1. De Verwerker zal de Verwerkingsverantwoordelijke de medewerking
                    verlenen die nodig is voor de in artikel 28 lid 3 onder h AVG bedoelde
                    verantwoordingsplicht. Voor deze werkzaamheden zal een redelijke vergoeding
                    in rekening worden gebracht, tenzij in de Onderliggende Overeenkomst anders
                    is overeengekomen.
                </p>
                <p>
                    <strong>Artikel 23. AANSPRAKELIJKHEID</strong>
                    <strong>
                        <br/>
                    </strong>
                    23.1. De aansprakelijkheid van Verwerker voor schade als gevolg van een
                    toerekenbare tekortkoming in de nakoming van deze Verwerkersovereenkomst,
                    is per gebeurtenis (een reeks opeenvolgende gebeurtenissen geldt als één
                    gebeurtenis) beperkt tot de vergoeding van directe schade, tot maximaal het
                    bedrag van de door Verwerker ontvangen vergoedingen voor de werkzaamheden
                    onder deze Verwerkersovereenkomst over de maand voorafgaande aan de
                    schadeveroorzakende gebeurtenis. De aansprakelijkheid van de Verwerker voor
                    directe schade zal in totaal nooit meer bedragen dan het bedrag, dat wordt
                    gedekt door de aansprakelijkheidsverzekering van Verwerker.
                    <br/>
                    23.2. Onder directe schade wordt uitsluitend verstaan alle schade bestaande
                    uit:
                    <br/>
                    – schade direct toegebracht aan stoffelijke zaken (“zaakschade’’) of
                    personen;
                    <br/>
                    – redelijke en aantoonbare kosten om de Verwerker er toe te manen de
                    Verwerkersovereenkomst (weer) deugdelijk na te komen;
                    <br/>
                    – redelijke kosten ter vaststelling van de oorzaak en de omvang van de
                    schade voor zover betrekking hebbende op de directe schade zoals in dit
                    artikel bedoeld.
                    <br/>
                    23.3. De aansprakelijkheid van Verwerker voor indirecte schade is
                    uitgesloten. Onder indirecte schade wordt verstaan alle schade die geen
                    directe schade is. Onder indirecte schade valt in ieder geval gevolgschade,
                    gederfde winst, gemiste besparingen, verminderde goodwill, schade door
                    bedrijfsstagnatie, schade verband houdende met het gebruik van door
                    Verwerkingsverantwoordelijke voorgeschreven gegevens of databestanden, of
                    schade door verlies, verminking of vernietiging van gegevens of
                    databestanden.
                    <br/>
                    23.4. De in dit artikel bedoelde uitsluitingen en beperkingen komen te
                    vervallen indien en voor zover de schade het gevolg is van opzet of bewuste
                    roekeloosheid van Verwerker of haar bedrijfsleiding.
                    <br/>
                    23.5. Tenzij nakoming door Verwerker blijvend onmogelijk is, ontstaat de
                    aansprakelijkheid van Verwerker wegens toerekenbare tekortkoming in de
                    nakoming van de Overeenkomst slechts indien Verwerkingsverantwoordelijke de
                    Verwerker onverwijld schriftelijk in gebreke stelt, waarbij een redelijke
                    termijn voor de zuivering van de tekortkoming wordt gesteld, en Verwerker
                    ook na die termijn toerekenbaar blijft tekortschieten in de nakoming van
                    haar verplichtingen. De ingebrekestelling dient een zo volledig en
                    gedetailleerd mogelijke omschrijving van de tekortkoming te bevatten, opdat
                    Verwerker in de gelegenheid wordt gesteld adequaat te reageren.
                    <br/>
                    23.6. Iedere vordering tot schadevergoeding door
                    Verwerkingsverantwoordelijke tegen Verwerker die niet gespecificeerd en
                    expliciet is gemeld, vervalt door het enkele verloop van twaalf (12)
                    maanden na net ontstaan van de vordering.
                </p>
                <p>
                    <strong>Artikel 24. DUUR EN BEËINDIGING</strong>
                    <strong>
                        <br/>
                    </strong>
                    24.1. Deze Verwerkersovereenkomst komt tot stand nadat de aanmelding bij
                    Surve is afgerond.
                    <br/>
                    24.2. De Verwerkersovereenkomst wordt aangegaan voor de duur van de
                    onderliggende overeenkomst en eindigt zodra de onderliggende overeenkomst
                    eindigt.
                    <br/>
                    24.3. Zodra de Verwerkersovereenkomst, om welke reden en op welke wijze dan
                    ook, is beëindigd, zal Verwerker alle persoonsgegevens van
                    Verwerkingsverantwoordelijke die bij haar aanwezig zijn deze en eventuele
                    kopieën daarvan binnen redelijke termijn vernietigen.
                    <br/>
                    24.4. Partijen mogen deze overeenkomst alleen wijzigen met wederzijdse
                    instemming.
                </p>
                <p>
                    <strong>Artikel 25. PARTIELE NIETIGHEID </strong>
                    <br/>
                    Indien één of meer bepalingen uit deze overeenkomst met wederpartij niet of
                    niet geheel rechtsgeldig zijn, blijven de overige bepalingen volledig in
                    stand. In plaats van de ongeldige bepalingen geldt een passende regeling,
                    die de bedoeling van partijen en het door hen nagestreefde economische
                    resultaat op juridisch effectieve wijze zo dicht mogelijk benadert.
                </p>
                <p>
                    <strong>
                        Artikel 26. PLAATS VAN NAKOMING, TOEPASSELIJK RECHT, BEVOEGDE RECHTER
                    </strong>
                    <br/>
                    26.1. De vestigingsplaats van Surve is de plaats waar wederpartij aan zijn
                    verplichtingen jegens Surve moet voldoen, tenzij dwingende bepalingen zich
                    daartegen verzetten;
                    <br/>
                    26.2. Op alle aanbiedingen en overeenkomsten van Surve is uitsluitend het
                    Nederlands recht van toepassing;
                    <br/>
                    26.3. Alle geschillen, die ontstaan naar aanleiding van de tussen
                    wederpartij en Surve gesloten overeenkomst dan wel van nadere
                    overeenkomsten, die daarvan het gevolg mochten zijn, zullen worden beslecht
                    door de daartoe bevoegde rechter.
                </p>
                <p>
                    <strong>
                        Artikel 27. WIJZIGING, UITLEG EN VINDPLAATS VAN DE VOORWAARDEN
                    </strong>
                    <br/>
                    27.1. Deze algemene voorwaarden zijn beschikbaar op www.surve.nl en worden
                    op aanvraag gratis per e-mail toegezonden;
                    <br/>
                    27.2. In geval van uitleg van de inhoud en strekking van deze algemene
                    voorwaarden, is de Nederlandse tekst daarvan steeds bepalend;
                    <br/>
                    27.3. Van toepassing is steeds de laatst gedeponeerde versie c.q. de versie
                    zoals die gold ten tijde van de totstandkoming van de overeenkomst.
                </p>
            </div>
        </div>
    </div>

@endsection
