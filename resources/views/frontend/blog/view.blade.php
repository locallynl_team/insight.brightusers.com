@extends('frontend.layouts.layout')

@section('content')
</div>
</div>

<div class="container">
    <div class="row">
        <div class="breadcrumb_list">
            <ul>
                <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                <li><a href="{{ route('blog') }}">{{ __('Blog') }}</a></li>
                <li><a href="{{ route('blog.post', $post->post_name) }}">{{ $post->title }}</a></li>
            </ul>
        </div>

        <div class="col-lg-12 content-page">
            @if ($post->thumbnail !== null)
                <div class="blog-post-single-img">
                    <div class="heading">
                        <span>{{ $post->post_date->formatLocalized('%d %B %Y') }}</span>
                        <h2>{{ $post->title }}</h2>
                    </div>

                    <img src="{{ ($post->thumbnail->size('featured-image-blog')['url'] ?? $post->thumbnail) }}" alt="{{ $post->title }}" class="blog-image">
                </div>
            @endif

            {{-- Add wordpress css classes here: aligncenter --}}

            <div class="blog-post-content-area">
                {!! formatBlog($post->content) !!}
            </div>
        </div>
    </div>
</div>

@if ($posts->count() > 0)
    <div id="blog">
        <div class="container">
            <div class="row" style="margin-top: 40px;">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="heading">
                        <span>{{ __('meer berichten') }}</span>
                        <h2>{{ __('Surve Blog') }}</h2>
                    </div>
                </div>
            </div>

            <div class="row pdtop2" style="display: flex; flex-wrap: wrap;">
                @foreach ($posts as $postItem)
                    <div class="col-lg-6 col-md-6 col-sm-6 module" style="margin-bottom: 40px;">
                        <div class="h-blog-box">
                            <div class="h-blog-box-photo">
                                <a href="{{ route('blog.post', $postItem->post_name) }}"><img src="{{ ($postItem->thumbnail !== null ? ($postItem->thumbnail->size('blog-thumbnail')['url'] ?? $postItem->thumbnail->size('large')['url']) : static_url().'/assets/frontend/images/no-image.png') }}" alt="{{ $postItem->title }}"></a>
                            </div>
                            <div class="h-blog-content">
                                <div class="h-blog-date">{{ $postItem->post_date->formatLocalized('%d %B %Y') }}
                                </div>
                                <div class="h-blog-ttl">
                                    <a href="{{ route('blog.post', $postItem->post_name) }}"><h2>{{ $postItem->title }}</h2></a>
                                </div>
                                <div class="h-blog-more">
                                    <a href="{{ route('blog.post', $postItem->post_name) }}">{{ __('Verder lezen') }} &raquo;</a>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif

@endsection