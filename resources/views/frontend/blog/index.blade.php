@extends('frontend.layouts.layout')

@section('content')
</div>
</div>

<div id="blog">
    <div class="container">
        <div class="row">
            <div class="breadcrumb_list">
                <ul>
                    <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                    <li><a href="{{ route('blog') }}">{{ __('Blog') }}</a></li>
                </ul>
            </div>
        </div>
        <div class="row" style="margin-top: 40px;">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="heading">
                    <span>{{ __('usability, gebruikersonderzoek, conversieoptimalisatie') }}</span>
                    <h2>{{ __('Surve Blog') }}</h2>
                </div>
            </div>
        </div>

        <div class="row pdtop2" style="display: flex; flex-wrap: wrap;">
            @foreach ($posts as $post)
                <div class="col-lg-6 col-md-6 col-sm-6 module" style="margin-bottom: 40px;">
                    <div class="h-blog-box">
                        <div class="h-blog-box-photo">
                            <a href="{{ route('blog.post', $post->post_name) }}"><img src="{{ ($post->thumbnail !== null ? ($post->thumbnail->size('blog-thumbnail')['url'] ?? $post->thumbnail->size('large')['url']) : static_url().'/assets/frontend/images/no-image.png') }}" alt="{{ $post->title }}"></a>
                        </div>
                        <div class="h-blog-content">
                            <div class="h-blog-date">{{ $post->post_date->formatLocalized('%d %B %Y') }}
                            </div>
                            <div class="h-blog-ttl">
                                <a href="{{ route('blog.post', $post->post_name) }}"><h2>{{ $post->title }}</h2></a>
                            </div>
                            <div class="h-blog-more">
                                <a href="{{ route('blog.post', $post->post_name) }}">{{ __('Verder lezen') }} &raquo;</a>
                            </div>
                        </div>

                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection