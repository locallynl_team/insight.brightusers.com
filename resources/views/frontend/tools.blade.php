@extends('frontend.layouts.layout')

@section('content')
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="breadcrumb_list">
                <ul>
                    <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                    <li><a href="{{ route('tools') }}">{{ __('Tools') }}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="section6">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 module">
                    <div class="img4">
                        <img class="img-responsive" src="{{ static_url() }}/assets/frontend/images/dashboard.png" alt="icon-image">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 module">
                    <div class="content-area">
                        <div class="left-heading">
                            <h2>{{ __('Tools van Surve') }}</h2>
                        </div>
                        <p>{{ __('Optimaliseer uw website met de handige tools van Surve en verbeter uw klanttevredenheid en conversie.') }}</p>

                        <div class="nomber-text">
                            <div class="nomber-text-row">
                                <div class="nomber-text-row-inner">
                                    <h2>{{ __('1. Surveys') }}</h2>
                                    <p>{{ __('Stel gebruikers gerichte vragen over het gebruik van uw website of diensten en vind nieuwe inzichten. Met geavanceerde mogelijkheden kunt u uw vragenlijst volledig naar wens inrichten en stylen.') }}</p>
                                    <p></p>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="nomber-text-row">
                                <div class="nomber-text-row-inner">
                                    <h2>{{ __('2. Feedbackbuttons') }}</h2>
                                    <p>{{ __('Blijf doorlopend op de hoogte van de feedback die bezoekers u geven. Hiermee haalt u snel fouten uit uw website, of kunt u inspelen op informatie die gebruikers missen. U zult uw klanttevredenheid zien stijgen!') }}</p>
                                    <p></p>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="nomber-text-row">
                                <div class="nomber-text-row-inner">
                                    <h2>{{ __('3. Heatmaps') }}</h2>
                                    <p>{{ __('Bekijk hoe uw bezoekers bewegen op uw website. Zie uw best presterende Call-to-actions, en optimaliseer uw pagina\'s met de inzichten uit uw heatmaps.') }}</p>
                                    <p></p>
                                </div>
                                <div class="clear"></div>
                            </div>

                        </div>

                        <a href="#register-modal" data-toggle="modal" class="acc">{{ __('Probeer Surve gratis') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section2 dark-section dark-color-bg" id="features">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="heading">
                        <span>{{ __('ontdek de mogelijkheden') }}</span>
                        <h2>{{ __('Optimaliseer uw website') }}</h2>
                    </div>
                </div>
            </div>
            <div class="row" style="display: flex; flex-wrap: wrap;">
                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/survey.png" alt="{{ __('Surveys') }}"></div>
                        <h3>{{ __('Surveys') }}</h3>
                        <p>{{ __('Vraag uw bezoekers gerichte vragen over uw website en optimaliseer uw pagina\'s tot in perfectie.') }}</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/feedbackbuttons.png" alt="{{ __('Feedbackbuttons') }}"></div>
                        <h3>{{ __('Feedbackbuttons') }}</h3>
                        <p>{{ __('Feedback van uw bezoekers helpt u om fouten uit uw website te halen die uw bezoekers frustreren.') }}</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/heatmaps.png" alt="{{ __('Heatmaps') }}"></div>
                        <h3>{{ __('Heatmaps') }} <span class="label label-danger" style="margin-left: 10px;">{{ __('Nieuw') }}</span></h3>
                        <p>{{ __('Precies zien hoe uw bezoekers een pagina gebruiken helpt u met het vinden van optimalisatiepunten.') }}</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/devices.png" alt="{{ __('Alle devices') }}"></div>
                        <h3>{{ __('Alle devices') }}</h3>
                        <p>{{ __('Perfecte werking op elk device, met eigen targeting en styling.') }}</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/customer-satisfaction.png" alt="{{ __('NPS-metingen') }}"></div>
                        <h3>{{ __('NPS-metingen') }}</h3>
                        <p>{{ __('Meet uw klanttevredenheid via de Net Promotor Score, met cijfers of emoticons.') }}</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/skip-logic.png" alt="{{ __('Skip-logic') }}"></div>
                        <h3>{{ __('Skip-logic') }}</h3>
                        <p>{{ __('Laat de volgorde van vragen afhangen van eerder gegeven antwoorden.') }}</p>
                    </div>
                </div>


                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/export.png" alt="{{ __('Eenvoudig exporteren') }}"></div>
                        <h3>{{ __('Eenvoudig exporteren') }}</h3>
                        <p>{{ __('Exporteer resultaten van surveys en feedbackbuttons eenvoudig naar Excel of PDF.') }}</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/url-targeting.png" alt="{{ __('URL-Targeting') }}"></div>
                        <h3>{{ __('URL-Targeting') }}</h3>
                        <p>{{ __('Stel voor specifieke pagina\'s een survey of heatmap in en verzamel gericht informatie.') }}</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/exit-intent-targeting.png" alt="{{ __('Exit intent targeting') }}"></div>
                        <h3>{{ __('Exit intent targeting') }}</h3>
                        <p>{{ __('Target bezoekers die uw website lijken te verlaten, dit levert u waardevolle insights op.') }}</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/time-on-page-targeting.png" alt="{{ __('Time-on-page targeting') }}"></div>
                        <h3>{{ __('Time-on-page targeting') }}</h3>
                        <p>{{ __('Target bezoekers die een bepaalde duur op een pagina zijn, zo stuurt u uw surveys gerichter.') }}</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/security.png" alt="{{ __('Beveiligde date opslag') }}"></div>
                        <h3>{{ __('Beveiligde data opslag') }}</h3>
                        <p>{{ __('Uw verzamelde data staat opgeslagen op goed beveiligde webservers in Nederland.') }}</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/avg-proof.png" alt="{{ __('AVG-proof') }}"></div>
                        <h3>{{ __('AVG-proof') }}</h3>
                        <p>{{ __('Surve werkt volgens een privacy-first principe, uw gegevens worden AVG-proof verwerkt.') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section1 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="search-sec">
                        <span>{{ __('Begin direct') }}</span>
                        <h2>{{ __('Meld u aan voor een gratis proefperiode') }}</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" action="{{ route('register') }}">
                        <div class="grop">
                            <div class="input-group">
                                <input type="text" class="form-control text1" name="name" placeholder="{{ __('Uw voornaam') }}">
                                <input type="text" class="form-control text2" name="email" placeholder="{{ __('Uw e-mailadres') }}">
                                <input type="submit" class="btn btn-secondary" value="{{ __('Start proefperiode') }}">
                            </div>
                            <p>{{ __('Maak gratis uw account aan en krijg een proefperiode van 7 dagen. Geen betaalinformatie nodig.') }}</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
