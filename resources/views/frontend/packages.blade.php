@extends('frontend.layouts.layout')

@section('content')
</div>
</div>

<div class="container">
    <div class="row">
        <div class="breadcrumb_list">
            <ul>
                <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                <li><a href="{{ route('packages') }}">{{ __('Prijzen en pakketten') }}</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="section9 dark-section" id="pricing" style="padding-top: 0;">
    <div class="pricing-bg-large dark-color-bg"></div>
    <div class="container">
        <div class="pricing-inner-container-large" >
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="heading">
                    <span>{{ __('onze prijzen') }}</span>
                    <h2>{{ __('Het juiste pakket voor uw website') }}</h2>
                    <p style="color: #fff; max-width: 500px; text-align: center; margin: 40px auto 0;">
                        {{ __('Met transparante pakketten en prijzen komt u nooit voor verrassingen te staan.') }}
                        <br />
                        {{ __('Betaald u per jaar? Dan krijgt u korting, simpel.') }}
                    </p>
                </div>
            </div>

            @include('frontend.parts.pricing-boxes')
        <div class="clearfix"></div>

            <p style="max-width: 500px; text-align: center; margin: 80px auto 40px; line-height: 35px;">{{ __('Genoemde prijzen zijn bij betaling per jaar en exclusief 21% BTW.') }}<br />{{ __('Neem contact met ons op voor enterprise pakketten.') }}</p>
    </div>
</div>

<div class="section1 gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="search-sec">
                    <span>{{ __('Begin direct') }}</span>
                    <h2>{{ __('Meld u aan voor een gratis proefperiode') }}</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <form method="get" action="{{ route('register') }}">
                    <div class="grop">
                        <div class="input-group">
                            <input type="text" class="form-control text1" name="name" placeholder="{{ __('Uw voornaam') }}">
                            <input type="text" class="form-control text2" name="email" placeholder="{{ __('Uw e-mailadres') }}">
                            <input type="submit" class="btn btn-secondary" value="{{ __('Start proefperiode') }}">
                        </div>
                        <p>{{ __('Maak gratis uw account aan en krijg een proefperiode van 14 dagen. Geen betaalinformatie nodig.') }}</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection