<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }}</title>
    <meta name="description" content="{{ $description }}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
    <link href="{{ static_url() }}{{ mix('/assets/frontend/css/app.css') }}" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ static_url() }}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ static_url() }}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ static_url() }}/favicon-16x16.png">
    <link rel="manifest" href="{{ static_url() }}/site.webmanifest">
    <link rel="mask-icon" href="{{ static_url() }}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="apple-mobile-web-app-title" content="Surve">
    <meta name="application-name" content="Surve">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <meta property="og:image:height" content="401">
    <meta property="og:image:width" content="767">
    @if (!isset($og_settings))
<meta property="og:description" content="{{ __('Optimaliseer uw website, verhoog uw klanttevredenheid en boost uw conversie met de tools van Surve. Gratis proefaccount voor 7 dagen.') }}">
    <meta property="og:title" content="{{ __('Surve - Surveys, Website Feedback & Heatmaps') }}">
    <meta property="og:url" content="{{ config('app.url') }}">
    <meta property="og:image" content="{{ static_url() }}/og-image.jpg">
    @else
<meta property="og:description" content="{{ $og_settings['description'] }}">
    <meta property="og:title" content="{{ $og_settings['title'] }}">
    <meta property="og:url" content="{{ $og_settings['url'] }}">
    <meta property="og:image" content="{{ $og_settings['image'] }}">
    @endif

    @if(config('app.env') === "production")
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116402224-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-116402224-2');
        </script>
    @endif
</head>
<body>
<div class="header{{ (Route::currentRouteName() !== "home" ? " no-hero" : "") }}" id="header">
    <div class="container">
        <div class="row header-top" data-spy="affix" data-offset-top="85">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="logo"><a href="{{ route('home') }}"><img src="{{ static_url() }}/assets/frontend/images/logo.png" data-rjs="2" alt="Surve Logo" title="{{ __('Terug naar Homepage') }}"></a></div>
                        <div id="menu-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <div class="clear"></div>

                    </div>
                    <div class="col-lg-9 col-md-9" id="menu">
                        <div class="menu">
                            <ul>
                                <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                                <li><a href="{{ route('tools') }}">{{ __('Tools') }}</a></li>
                                <li><a href="{{ route('packages') }}">{{ __('Prijzen') }}</a></li>
                                <li><a href="{{ route('blog') }}">{{ __('Blog') }}</a></li>
                                <li><a href="{{ route('contact') }}">{{ __('Contact') }}</a></li>
                                <li><a href="{{ route('login') }}">{{ __('Inloggen') }}</a></li>
                                <li><a href="#register-modal" data-toggle="modal">{{ __('Aanmelden') }}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@yield('content')

<div class="section14 blue-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="fool-box">
                    <i class="icofont icofont-address-book"></i>
                    <div class="footer-box-content">
                        <h4>{{ __('Surve') }}</h4>
                        <p>
                            Schuttevaerkade 80<br />
                            8021 DB&nbsp;&nbsp;Zwolle<br />
                            <br />
                            KVK: 70214220<br />
                            BTW: NL858195525B01
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="fool-box">
                    <i class="icofont icofont-phone"></i>
                    <div class="footer-box-content">
                        <h4>{{ __('Bel ons') }}</h4>
                        <p>{{ __('Heeft u vragen over Surve? Wij staan telefonisch voor u klaar.') }}</p>
                        <a href="{{ route('contact') }}">038 - 202 264 1</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="fool-box">
                    <i class="icofont icofont-mail"></i>
                    <div class="footer-box-content">
                        <h4>{{ __('Stuur ons een mailtje') }}</h4>
                        <p>{{ __('Onze helpdesk is bereikbaar per e-mail op info@surve.nl') }}</p>
                        <a href="{{ route('contact') }}" class="acc">contact</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="last-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="footer-copy">
                        <p>
                            &copy; {{ __('Copyright') }} 2014-{{ date("Y") }} <a href="{{ route('home') }}">Surve</a>
                        </p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="footer-copy text-right">
                        <p>
                            <a href="{{ route('terms') }}" rel="nofollow">{{ __('Algemene voorwaarden') }}</a> /
                            <a href="{{ route('privacy') }}" rel="nofollow">{{ __('Privacyverklaring') }}</a>
                        </p>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="foot-social">
                        <a href="https://twitter.com/SurveNL"><i class="icofont icofont-social-twitter"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="register-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ __('Probeer Surve 14 dagen gratis') }}</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="form-wrap">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf

                                <div class="form-group">
                                    <input id="first_name" placeholder="{{ __('Voornaam') }}" name="first_name" value="{{ old('first_name') }}" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" required="">
                                </div>

                                <div class="form-group">
                                    <input id="last_name" placeholder="{{ __('Achternaam') }}" name="last_name" value="{{ old('last_name') }}" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" required="">
                                </div>

                                <div class="form-group">
                                    <input id="email" placeholder="{{ __('E-mailadres') }}" name="email" type="email" value="{{ old('email') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required="">
                                </div>

                                <div class="form-group">
                                    <input placeholder="{{ __('Wachtwoord') }}" type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required="" id="password">
                                </div>

                                <div class="form-group">
                                    <input type="password" placeholder="{{ __('Herhaal wachtwoord') }}" name="password_confirmation" class="form-control" required="" id="password-confirm">
                                </div>

                                <div class="form-group">
                                    <div class="checkbox">
                                        <input class="pull-left" type="checkbox" id="terms" name="terms" value="true" required {{ (old('terms') ? 'checked' : '') }} />
                                        <label for="terms"> {{ __('Bij het aanmelden ga ik akkoord met de algemene voorwaarden en de bijhorende verwerking van persoonsgegevens.') }}</label>
                                    </div>
                                </div>

                                <div class="form-group text-center">
                                    <button type="submit" class="btn mt-15">{{ __('Gratis aanmelden') }}</button>
                                </div>
                            </form>
                            <p>{{ __('Maak gratis uw account aan en krijg 14 dagen om alles te proberen. Geen betaalinformatie nodig.') }}</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 hidden-xs image">
                        <img src="{{ static_url() }}/assets/frontend/images/icons/feedbackbuttons.png" class="img-responsive" />
                        <p>{{ __('Zet user feedback in om uw klanttevredenheid te verhogen.') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ static_url() }}/assets/frontend/js/app.js"></script>
@yield('scripts')

@if(config('app.env') === "production")
<script type="text/javascript">
    (function(s,u,r,v,e) {
        var c=s.createElement(u);c.type='text/javascript';c.async=r;c.src='https://static.surve.nl/request/'+v;
        e=s.getElementsByTagName(u)[0];e.parentNode.insertBefore(c,e);
    })(document,'script',true,1);
</script>
@else
<script type="text/javascript">
    (function(s,u,r,v,e) {
        var c=s.createElement(u);c.type='text/javascript';c.async=r;c.src='http://static.surve2.test/request/'+v;
        e=s.getElementsByTagName(u)[0];e.parentNode.insertBefore(c,e);
    })(document,'script',true,1);
</script>
@endif
</body>
</html>