<div class="pricing-boxes">
    <div class="col-lg-4 col-md-4 col-sm-4 pos module">
        <div class="plan">
            <div class="plan-ttl">
                <span class="title" style="color: #3869cf;">Basis</span>
                <h3><strong class="currency-sign">&euro;</strong><span class="price">19</span><strong class="plan-time">{{ __('p/m') }}</strong></h3>
                <p>{!! __('&euro;24 bij betaling per maand') !!}</p>
            </div>

            <ul>
                <li>{!! __('Tot <strong>2.500 weergaven</strong> per dag') !!}</li>
                <li>{!! __('<strong>Surveys</strong> om gerichte vragen te stellen') !!}</li>
                <li>{!! __('<strong>Feedbackbuttons</strong> voor doorlopende gebruikersfeedback') !!}</li>
                <li>{!! __('<strong>Heatmaps</strong> om gebruik te analyseren') !!}</li>
                <li>{!! __('<strong>Opmaak aanpassen</strong> aan uw huisstijl') !!}</li>
                <li>{!! __('<strong>Exporteren</strong> van resultaten naar Excel') !!}</li>
                <li style="color: #dadada;">{!! __('<strong>E-mailnotificaties</strong> bij nieuwe reacties') !!}</li>
                <li style="color: #dadada;">{!! __('<strong>Verlaat intentie</strong> als trigger inzetten') !!}</li>
                <li style="color: #dadada;">{!! __('<strong>Extern activeren</strong> van surveys &amp; feedbackbuttons') !!}</li>
                <li style="border-bottom: 0; color: #dadada;">{!! __('<strong>Telefonische support</strong> tijdens kantooruren') !!}</li>
            </ul>

            <div class="plan-btn">
                <a href="{{ route('register') }}" style="color: #fff; background: #3869cf; border: 0;">{{ __('Probeer 14 dagen gratis') }}</a>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 pos module">
        <div class="plan">
            <div class="plan-ttl">
                <span class="title" style="color: #f5a135;">Plus</span>
                <h3><strong class="currency-sign">&euro;</strong><span class="price">39</span><strong class="plan-time">{{ __('p/m') }}</strong></h3>
                <p>{!! __('&euro;49 bij betaling per maand') !!}</p>
            </div>

            <ul>
                <li>{!! __('Tot <strong>10.000 weergaven</strong> per dag') !!}</li>
                <li>{!! __('<strong>Surveys</strong> om gerichte vragen te stellen') !!}</li>
                <li>{!! __('<strong>Feedbackbuttons</strong> voor doorlopende gebruikersfeedback') !!}</li>
                <li>{!! __('<strong>Heatmaps</strong> om gebruik te analyseren') !!}</li>
                <li>{!! __('<strong>Opmaak aanpassen</strong> aan uw huisstijl') !!}</li>
                <li>{!! __('<strong>Exporteren</strong> van resultaten naar Excel') !!}</li>
                <li>{!! __('<strong>E-mailnotificaties</strong> bij nieuwe reacties') !!}</li>
                <li>{!! __('<strong>Verlaat intentie</strong> als trigger inzetten') !!}</li>
                <li style="color: #dadada;">{!! __('<strong>Extern activeren</strong> van surveys &amp; feedbackbuttons') !!}</li>
                <li style="border-bottom: 0; color: #dadada;">{!! __('<strong>Telefonische support</strong> tijdens kantooruren') !!}</li>
            </ul>

            <div class="plan-btn">
                <a href="{{ route('register') }}" style="color: #fff; background: #f5a135; border: 0;">{{ __('Probeer 14 dagen gratis') }}</a>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 pos module">
        <div class="plan">
            <div class="plan-ttl">
                <span class="title" style="color: #9c2096;">Premium</span>
                <h3><strong class="currency-sign">&euro;</strong><span class="price">79</span><strong class="plan-time">{{ __('p/m') }}</strong></h3>
                <p>{!! __('&euro;99 bij betaling per maand') !!}</p>
            </div>

            <ul>
                <li>{!! __('Tot <strong>50.000 weergaven</strong> per dag') !!}</li>
                <li>{!! __('<strong>Surveys</strong> om gerichte vragen te stellen') !!}</li>
                <li>{!! __('<strong>Feedbackbuttons</strong> voor doorlopende gebruikersfeedback') !!}</li>
                <li>{!! __('<strong>Heatmaps</strong> om gebruik te analyseren') !!}</li>
                <li>{!! __('<strong>Opmaak aanpassen</strong> aan uw huisstijl') !!}</li>
                <li>{!! __('<strong>Exporteren</strong> van resultaten naar Excel') !!}</li>
                <li>{!! __('<strong>E-mailnotificaties</strong> bij nieuwe reacties') !!}</li>
                <li>{!! __('<strong>Verlaat intentie</strong> als trigger inzetten') !!}</li>
                <li>{!! __('<strong>Extern activeren</strong> van surveys &amp; feedbackbuttons') !!}</li>
                <li style="border-bottom: 0;">{!! __('<strong>Telefonische support</strong> tijdens kantooruren') !!}</li>
            </ul>

            <div class="plan-btn">
                <a href="{{ route('register') }}" style="color: #fff; background: #9c2096; border: 0;">{{ __('Probeer 14 dagen gratis') }}</a>
            </div>
        </div>
    </div>
</div>