@extends('frontend.layouts.layout')

@section('content')
            <div class="row">
                <div class="col-lg-12">
                    <h1>{!! __('Website analyse tools<br />voor iedere website') !!}</h1>
                    <p>
                       {!! __(' Surveys, feedbackbuttons en heatmaps geven u belangrijke insights over het gebruik van uw website.<br />Zet de tools van Surve in en verhoog de gebruiksvriendelijkheid en klanttevredenheid.') !!}
                    </p>
                    <div class="bnts">
                        <a href="#register-modal" data-toggle="modal" class="btn2">{{ __('14 dagen gratis proberen') }}</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="toping">
                        <img class="img-responsive center-block" src="{{ static_url() }}/assets/frontend/images/dashboard.png" alt="hero-image">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section2 dark-section dark-color-bg" id="features">
        <div class="container">
                    <div class="row">
                <div class="col-lg-12">
                    <div class="heading">
                        <span>{{ __('verzamel user feedback') }}</span>
                        <h2>{{ __('Verhoog uw rendement') }}</h2>
                    </div>
                    <p style="text-align: center; color: #fff; max-width: 750px; margin: 0 auto;">
                        {{ __('Met Surve bent u in staat om op een laagdrempelige manier feedback te verzamelen van uw bezoekers.') }}
                        {{ __('Dit kan via een feedbackbutton, of via surveys die u binnen uw website naar wens kunt targetten.') }}
                        {{ __('Zet deze feedback in om uw website en dienstverlening te optimaliseren.') }}
                    </p>
                    <p style="text-align: center; font-weight: bold; color: #fff; max-width: 750px; margin: 5px auto 15px;">
                        {{ __('Zo verhoogt u uw klanttevredenheid en rendement.') }}
                    </p>
                </div>
            </div>
            <div class="row" style="display: flex; flex-wrap: wrap;">
                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img">
                            <a href="{{ route('tools') }}"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/survey.png" alt="{{ __('Surveys') }}"></a>
                        </div>
                        <a href="{{ route('tools') }}">{{ __('Surveys') }}</a>
                        <p>{{ __('Vraag uw bezoekers gerichte vragen over uw website en optimaliseer uw pagina\'s tot in perfectie.') }}</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img">
                            <a href="{{ route('tools') }}"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/feedbackbuttons.png" alt="{{ __('Feedbackbuttons') }}"></a>
                        </div>
                        <a href="{{ route('tools') }}">{{ __('Feedbackbuttons') }}</a>
                        <p>{{ __('Feedback van uw bezoekers helpt u om fouten uit uw website te halen die uw bezoekers frustreren.') }}</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 module">
                    <div class="f-box">
                        <div class="f-img">
                            <a href="{{ route('tools') }}"><img data-rjs="2" src="{{ static_url() }}/assets/frontend/images/icons/heatmaps.png" alt="{{ __('Heatmaps') }}"></a>
                        </div>
                        <a href="{{ route('tools') }}">{{ __('Heatmaps') }}</a>
                        <p>{{ __('Precies zien hoe uw bezoekers een pagina gebruiken helpt u met het vinden van optimalisatiepunten.') }}</p>
                    </div>
                </div>

                <a href="{{ route('tools') }}" class="acc" style="margin: 60px auto 0; background: #fff; color: #27ae5f!important;">{{ __('Bekijk alle functionaliteiten') }}</a>

            </div>
        </div>
    </div>

    <div class="section12">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="slider1">
                        @foreach ($companies as $company)
                            <div class="slide">
                                <img src="{{ static_url() }}/assets/frontend/images/logos/{{ strtolower($company) }}.png" alt="{{ $company }}" data-rjs="2">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section1 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="search-sec">
                        <span>{{ __('Begin direct') }}</span>
                        <h2>{{ __('Meld u aan voor een gratis proefperiode') }}</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" action="{{ route('register') }}">
                        <div class="grop">
                            <div class="input-group">
                                <input type="text" class="form-control text1" name="name" placeholder="{{ __('Uw voornaam') }}">
                                <input type="text" class="form-control text2" name="email" placeholder="{{ __('Uw e-mailadres') }}">
                                <input type="submit" class="btn btn-secondary" value="{{ __('Start registratie') }}">
                            </div>
                            <p>{{ __('Maak gratis uw account aan en krijg een proefperiode van 14 dagen. Geen betaalinformatie nodig.') }}</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="section6">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 module">
                    <div class="content-area">
                        <div class="left-heading">
                            <h2>{{ __('Optimaliseer uw website') }}</h2>
                        </div>

                        <div class="nomber-text">
                            <div class="nomber-text-row">
                                <div class="nomber-text-row-inner">
                                    <h2>{{ __('1. Usability') }}</h2>
                                    <p>{{ __('Optimaliseer uw website door fouten te achterhalen die uw bezoekers frustreren. Verhoog het gebruiksgemak en meet direct uw verhoogde klanttevredenheid met een doorlopende NPS-meting.') }}</p>
                                    <p></p>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="nomber-text-row">
                                <div class="nomber-text-row-inner">
                                    <h2>{{ __('2. Informatievoorziening') }}</h2>
                                    <p>{{ __('Stel uw gebruikers gerichte vragen om te ontdekken welke content er mist op uw website. Voorkom ontevreden bezoekers en negatieve reviews. Ontlast uw klantenservice door een verbeterde informatievoorziening.') }}</p>
                                    <p></p>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="nomber-text-row">
                                <div class="nomber-text-row-inner">
                                    <h2>{{ __('3. Conversie verhogen') }}</h2>
                                    <p>{{ __('Vraag uw klanten wat ze tegenhoudt om bij u te kopen. Combineer en valideer inzichten uit Google Analytics. Verbeter uw A/B-testen en verhoog uw conversiepercentage.') }}</p>
                                    <p></p>
                                </div>
                                <div class="clear"></div>
                            </div>

                        </div>

                        <a href="{{ route('register') }}" class="acc">{{ __('Gratis aanmelden') }}</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 module">
                    <div class="img3">
                        <img class="img-responsive" src="{{ static_url() }}/assets/frontend/images/dashboard.png" alt="icon-image" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section9 dark-section" id="pricing" style="padding-top: 0;">
        <div class="pricing-bg dark-color-bg"></div>
        <div class="container">
            <div class="pricing-inner-container" >
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="heading">
                        <span>{{ __('onze prijzen') }}</span>
                        <h2>{{ __('Het juiste pakket voor uw website') }}</h2>
                    </div>
                </div>

                @include('frontend.parts.pricing-boxes')
            </div>
            <div class="clearfix"></div>

            <p style="max-width: 500px; text-align: center; margin: 80px auto 40px; line-height: 35px;">{{ __('Genoemde prijzen zijn bij betaling per jaar en exclusief 21% BTW.') }}<br />{{ __('Neem contact met ons op voor enterprise pakketten.') }}</p>
        </div>
    </div>

    <div class="section11 dark-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="heading">
                        <span>{{ __('Klaar voor meer conversie?') }}</span>
                        <h2>{{ __('Start gelijk met kansen benutten!') }}</h2>
                    </div>
                    <p>
                        {!! __('Installeer Surve eenvoudig op uw website, het maakt niet uit of uw website Wordpress, Magento, Typo3, Joomla of een ander CMS heeft.<br />Surve werkt op alle systemen en is te integreren met Google Tagmanager en andere tagmanagers. ') !!}
                    </p>
                    <div class="getbtn"><a href="#register-modal" data-toggle="modal" class="get">Starten</a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="section16" id="blog">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="heading">
                        <span>{{ __('Laatste nieuws') }}</span>
                        <h2>{{ __('Onze blog') }}</h2>
                    </div>
                </div>
            </div>

            <div class="row pdtop2" style="display: flex; flex-wrap: wrap;">
                @foreach ($blogs as $blog)
                    <div class="col-lg-6 col-md-6 col-sm-6 module" style="margin-bottom: 40px;">
                        <div class="h-blog-box">
                            <div class="h-blog-box-photo">
                                <a href="{{ route('blog.post', $blog->post_name) }}">
                                    <img src="{{ ($blog->thumbnail !== null ? ($blog->thumbnail->size('blog-thumbnail')['url'] ?? $blog->thumbnail->size('large')['url']) : static_url().'/assets/frontend/images/no-image.png') }}" alt="{{ $blog->title }}">
                                </a>
                            </div>
                            <div class="h-blog-content">
                                <div class="h-blog-date">{{ $blog->post_date->formatLocalized('%d %B %Y') }}
                                </div>
                                <div class="h-blog-ttl">
                                    <a href="{{ route('blog.post', $blog->post_name) }}"><h2>{{ $blog->title }}</h2></a>
                                </div>
                                <div class="h-blog-more">
                                    <a href="{{ route('blog.post', $blog->post_name) }}">{{ __('Verder lezen') }} &raquo;</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <a href="{{ route('blog') }}" class="acc" style="margin-left: 50%; transform: translate(-50%);">{{ __('Lees meer blogs') }}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="section1 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="search-sec">
                        <span>{{ __('14 dagen gratis') }}</span>
                        <h2>{{ __('Begin uw gratis proefperiode') }}</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <form method="get" action="{{ route('register') }}">
                        <div class="grop">
                            <div class="input-group">
                                <input type="text" class="form-control text1" name="name" placeholder="{{ __('Uw voornaam') }}">
                                <input type="text" class="form-control text2" name="email" placeholder="{{ __('Uw e-mailadres') }}">
                                <input type="submit" class="btn btn-secondary" value="{{ __('Start proefperiode') }}">
                            </div>
                            <p>{{ __('Maak gratis uw account aan en krijg een proefperiode van 14 dagen. Geen betaalinformatie nodig.') }}</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection