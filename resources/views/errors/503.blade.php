@extends('backend.layouts.error')

@section('content')
    <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container-fluid">
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="mb-30">
                                    <h1 class="txt-light">503</h1>
                                    <h4 class="txt-light-grey">{{ __('De server is bezig met onderhoud, we zijn zo terug.') }}</h4>
                                </div>
                                 <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-primary btn-large mr-10">{{ __('Vorige pagina') }}</a> <a href="{{ route('home') }}" class="btn btn-primary btn-large">{{ __('Naar homepage') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection