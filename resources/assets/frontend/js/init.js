$(document).ready(function () {
    //Screenshot in details slider
    $(function () {
        $('#dg-container').gallery();
    });

    //Testimonials slider
    $(".center").slick({
        dots: true,
        infinite: true,
        centerMode: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 600,
                settings: {
                    mobileFirst: true,
                    slidesToShow: 1
                }
            }
        ]
    });

    // Logo Slider
    $('.slider1').bxSlider({
        slideWidth: 260,
        minSlides: 2,
        maxSlides: 4,
        slideMargin: 10
    });

    //Responsive Menu
    $("#menu-icon").click(function () {
        $("#menu").slideToggle();
    });

    // Number Count for Our strong numbers
    /**
    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
     **/

    /**
    //Horizontal Tab
    $('#parentHorizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function (event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
     **/
});