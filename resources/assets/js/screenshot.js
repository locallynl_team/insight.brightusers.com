#!/usr/bin/env node

// Script to generate a screenshot of a page, and find the offset and size of an element on that page
// Usage: npm screenshot "url" "device" "filename" "selector"

const puppeteer = require('puppeteer');
const allDevices = require('puppeteer/DeviceDescriptors');
const fs = require('fs');

// Convert parameters to an array
const parameters = process.argv.slice(2);

//Required inputs
const url = parameters[0];
const deviceName = parameters[1];
let filename = parameters[2];
// let selector = parameters[3];


//Check for screenshots folder, if not found then raise an error
const dir = './storage/app/public/screenshots/';
if (!fs.existsSync(dir)) {
    console.log(`Dir ${dir} does not exists`);
    return;
}

const path = dir + filename + '.jpg';
// const jsonpath = dir + filename + '.json';

console.log(`Creating a screenshot from ${url} with emulated device ${deviceName}, and save it as ${path}.`);

function wait (ms) {
    return new Promise(resolve => setTimeout(() => resolve(), ms));
}

let generate = (async () => {
    const browser = await puppeteer.launch({headless: true, ignoreHTTPSErrors: true, args: ['--no-sandbox']});
    let page = await browser.newPage();
    let device;
    try {
        if (deviceName === "desktop-normal") {
            await page.setViewport({width: 1200, height: 1000, deviceScaleFactor: 1});
        } else if (deviceName === "desktop-large") {
            await page.setViewport({width: 1400, height: 1000, deviceScaleFactor: 1});
        } else {
            device = allDevices[deviceName];
            device.viewport.deviceScaleFactor = 1;
            console.log("Device:",device);
            await page.emulate(device);
        }

        await page.goto(url, {waitUntil: 'networkidle'});

        page.on('console', (...args) => console.log(...args));

        let height = await page.evaluate(() => document.documentElement.offsetHeight);

        // Scroll one viewport at a time, pausing to let content load
        const viewportHeight = page.viewport().height;
        let viewportIncr = 0;
        while (viewportIncr + viewportHeight < height) {
            await page.evaluate(_viewportHeight => {
                window.scrollBy(0, _viewportHeight);
            }, viewportHeight);
            await wait(20);
            viewportIncr = viewportIncr + viewportHeight;
        }

        // Scroll back to top
        await page.evaluate(_ => {
           window.scrollTo(0, 0);
        });

        await page.waitFor(2000);
        await page.evaluate(get_tree);
        await page.screenshot({path: path, fullPage: true});
        console.log('Created file: ' + path);
    } catch (e) {
        console.log(e);
        process.exitCode = 1;
    }

    browser.close();
});

function get_tree() {
    var surve = {};
    surve.heatmap_ref = {};

    surve.nested_path = function (tree, path, on_create, on_end, depth) {
        if (depth === undefined) {
            depth = 0;
        }

        // For first element
        if (!tree.hasOwnProperty('c')) {
            tree.c = {};
        }

        if (depth === path.length) {
            tree = on_end(tree);
            return tree;
        }

        var current = path[depth];
        var subtree;

        if (tree.c.hasOwnProperty(current)) {
            subtree = tree.c[current];
        } else {
            subtree = on_create(depth);
        }

        tree.c[current] = surve.nested_path(subtree, path, on_create, on_end, depth + 1);
        return tree;
    };

    surve.insert_content_at_path = function (tree, path, contents) {
        return surve.nested_path(tree, path,
            function (index) {
                return {
                    c: {}
                };
            }, function (tree) {
                return contents;
            });
    };

    surve.get_b = function (el) {
        var r = el.getBoundingClientRect();
        var _x = 0;
        var _y = 0;
        while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
            _x += el.offsetLeft - el.scrollLeft;
            _y += el.offsetTop - el.scrollTop;
            el = el.offsetParent;
        }
        return { t: _y, l: _x, h: ~~r.height, w: ~~r.width};
    };

    surve.getPath = function (node) {
        var path = [];
        var ignore = ["script", "style", "link"];
        while (node) {
            //get name
            var name = node.localName;

            //no name? continue
            if (!name) break;

            //lowercase name
            name = surve.escapeCSS(name.toLowerCase());

            //ignore some elements
            if (ignore.indexOf(name) > 0) {
                return [];
            }

            //get parent
            var parent = node.parentNode;

            //get sibblings with same tag, we need to know the nth-child
            var sameTagSiblings = [].filter.call(parent.children, function(e) {
                return surve.escapeCSS(e.localName.toLowerCase()) == name;
            });
            if (sameTagSiblings.length > 1) {
                var allSiblings = parent.children;
                var index = [].indexOf.call(allSiblings, node) + 1;
                if (index > 1) {
                    name += ':nth-child(' + index + ')';
                }
            }

            path.push(name);
            node = parent;
        }

        if (path[1] === "head") {
            return [];
        }

        path.reverse();
        return path.slice(2);
    };

    surve.it = function () {
        console.log("start");
        var all = document.getElementsByTagName("*");

        for (var i = 0, max = all.length; i < max; i++) {
            var p = surve.getPath(all[i]);

            if (!p.length) {
                continue;
            }

            var s = "html > body > " + p.join(" > ");

            var el = document.querySelectorAll(s)[0];
            if (el === undefined) {
                continue;
            }

            var b = surve.get_b(el);
            if (b.t >= 0 && b.l >= 0 && b.h > 2 && b.w > 2) {
                surve.insert_content_at_path(surve.heatmap_ref, p, b);
            }
        }

        console.log("surve-output" + JSON.stringify(surve.heatmap_ref));
    };

    surve.escapeCSS = function(value) {
        var string = String(value);
        var length = string.length;
        var index = -1;
        var codeUnit;
        var result = '';
        var firstCodeUnit = string.charCodeAt(0);
        while (++index < length) {
            codeUnit = string.charCodeAt(index);
            // Note: there’s no need to special-case astral symbols, surrogate
            // pairs, or lone surrogates.

            // If the character is NULL (U+0000), then the REPLACEMENT CHARACTER
            // (U+FFFD).
            if (codeUnit == 0x0000) {
                result += '\uFFFD';
                continue;
            }

            if (
                // If the character is in the range [\1-\1F] (U+0001 to U+001F) or is
            // U+007F, […]
                (codeUnit >= 0x0001 && codeUnit <= 0x001F) || codeUnit == 0x007F ||
                // If the character is the first character and is in the range [0-9]
                // (U+0030 to U+0039), […]
                (index == 0 && codeUnit >= 0x0030 && codeUnit <= 0x0039) ||
                // If the character is the second character and is in the range [0-9]
                // (U+0030 to U+0039) and the first character is a `-` (U+002D), […]
                (
                    index == 1 &&
                    codeUnit >= 0x0030 && codeUnit <= 0x0039 &&
                    firstCodeUnit == 0x002D
                )
            ) {
                // https://drafts.csswg.org/cssom/#escape-a-character-as-code-point
                result += '\\' + codeUnit.toString(16) + ' ';
                continue;
            }

            if (
                // If the character is the first character and is a `-` (U+002D), and
            // there is no second character, […]
                index == 0 &&
                length == 1 &&
                codeUnit == 0x002D
            ) {
                result += '\\' + string.charAt(index);
                continue;
            }

            // If the character is not handled by one of the above rules and is
            // greater than or equal to U+0080, is `-` (U+002D) or `_` (U+005F), or
            // is in one of the ranges [0-9] (U+0030 to U+0039), [A-Z] (U+0041 to
            // U+005A), or [a-z] (U+0061 to U+007A), […]
            if (
                codeUnit >= 0x0080 ||
                codeUnit == 0x002D ||
                codeUnit == 0x005F ||
                codeUnit >= 0x0030 && codeUnit <= 0x0039 ||
                codeUnit >= 0x0041 && codeUnit <= 0x005A ||
                codeUnit >= 0x0061 && codeUnit <= 0x007A
            ) {
                // the character itself
                result += string.charAt(index);
                continue;
            }

            // Otherwise, the escaped character.
            // https://drafts.csswg.org/cssom/#escape-a-character
            result += '\\' + string.charAt(index);

        }
        return result;
    };

    (function () {
        surve.it();
    })();
}

generate();
