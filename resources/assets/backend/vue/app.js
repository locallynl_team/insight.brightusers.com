/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');

Vue.component('manage-questions', require('./components/ManageQuestions.vue'));
Vue.component('styling', require('./components/Styling.vue'));
Vue.component('survey-preview', require('./components/SurveyPreview.vue'));
Vue.component('form-identification', require('./components/FormIdentification.vue'));

const app = new Vue({
    el: '#app'
});