<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LaunchNewPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //remove all old data -> packages, package options, subscriptions, payments, invoices
        \App\Payment::truncate();
        \App\Invoice::truncate();
        \App\InvoiceRow::truncate();

        \App\Subscription::truncate();
        \App\SubscriptionOption::truncate();

        \App\Package::truncate();
        \App\PackageOption::truncate();
        \App\PackageItem::truncate();

        (new \App\User)->update(['had_trial' => false]);
        (new \App\Site)->update(['active' => false]);

        $package_options = [
            [
                'title' => 'Aantal views per dag',
                'identifier' => 'views',
                'type' => 'integer',
                'sort_order' => 0
            ],
            [
                'title' => 'Aantal surveys',
                'identifier' => 'survey',
                'type' => 'integer',
                'sort_order' => 2
            ],
            [
                'title' => 'Aantal feedbackbuttons',
                'identifier' => 'feedbackbutton',
                'type' => 'integer',
                'sort_order' => 3
            ],
            [
                'title' => 'Aantal heatmaps',
                'identifier' => 'heatmaps',
                'type' => 'integer',
                'sort_order' => 4
            ],
            [
                'title' => 'Voor desktop bezoekers',
                'identifier' => 'desktop_platform',
                'type' => 'boolean',
                'sort_order' => 5
            ],
            [
                'title' => 'Voor tablet bezoekers',
                'identifier' => 'tablet_platform',
                'type' => 'boolean',
                'sort_order' => 6
            ],
            [
                'title' => 'Voor mobiele bezoekers',
                'identifier' => 'mobile_platform',
                'type' => 'boolean',
                'sort_order' => 7
            ],
            [
                'title' => 'Target verlaat intentie',
                'identifier' => 'target_leave_intent',
                'type' => 'boolean',
                'sort_order' => 8
            ],
            [
                'title' => 'Aanpasbare button teksten',
                'identifier' => 'button_texts',
                'type' => 'boolean',
                'sort_order' => 9
            ],
            [
                'title' => 'Gebruik externe activatie',
                'identifier' => 'target_external_activation',
                'type' => 'boolean',
                'sort_order' => 10
            ],
            [
                'title' => 'E-mailnotifications',
                'identifier' => 'email_notification',
                'type' => 'boolean',
                'sort_order' => 11
            ]
        ];

        //create new packages
        $packages = [
            [
                'title' => 'Basis',
                'price_monthly' => 24.00,
                'price_yearly' => 19.00,
                'active' => 1,
                'trial' => 1,
                'trial_days' => 14,
                'color' => '#3869cf',
                'items' => [
                    'views' => 2500,
                    'feedbackbutton' => 25,
                    'survey' => 25,
                    'heatmaps' => 25,
                    'target_leave_intent' => false,
                    'target_external_activation' => false,
                    'mobile_platform' => true,
                    'tablet_platform' => true,
                    'desktop_platform' => true,
                    'button_texts' => true,
                    'email_notification' => false
                ]
            ],
            [
                'title' => 'Plus',
                'price_monthly' => 49.00,
                'price_yearly' => 39.00,
                'active' => 1,
                'trial' => 1,
                'trial_days' => 14,
                'color' => '#f5a135',
                'items' => [
                    'views' => 25000,
                    'feedbackbutton' => 50,
                    'survey' => 50,
                    'heatmaps' => 50,
                    'target_leave_intent' => true,
                    'target_external_activation' => false,
                    'mobile_platform' => true,
                    'tablet_platform' => true,
                    'desktop_platform' => true,
                    'button_texts' => true,
                    'email_notification' => true
                ]
            ],
            [
                'title' => 'Premium',
                'price_monthly' => 99.00,
                'price_yearly' => 79.00,
                'active' => 1,
                'trial' => 1,
                'trial_days' => 14,
                'color' => '#9c2096',
                'items' => [
                    'views' => 250000,
                    'feedbackbutton' => 500,
                    'survey' => 500,
                    'heatmaps' => 500,
                    'target_leave_intent' => true,
                    'target_external_activation' => true,
                    'mobile_platform' => true,
                    'tablet_platform' => true,
                    'desktop_platform' => true,
                    'button_texts' => true,
                    'email_notification' => true
                ]
            ],
        ];

        //add package options
        \App\PackageOption::insert($package_options);

        foreach ($packages as $package) {
            $new_package = \App\Package::create([
                'title' => $package['title'],
                'price_monthly' => $package['price_monthly'],
                'price_yearly' => $package['price_yearly'],
                'active' => $package['active'],
                'trial' => $package['trial'],
                'trial_days' => $package['trial_days'],
                'color' => $package['color']
            ]);

            foreach ($package['items'] as $packageItemIdentifier => $packageItemAmount) {
                \App\PackageItem::create([
                    'package_id' => $new_package->id,
                    'amount' => $packageItemAmount,
                    'package_option_id' => \App\PackageOption::where('identifier', $packageItemIdentifier)->first()->id
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
