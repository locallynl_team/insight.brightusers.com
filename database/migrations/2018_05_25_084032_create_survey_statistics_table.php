<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_id')->index();
            $table->enum('platform', ['mobile', 'desktop', 'tablet', 'unknown'])->index()->default('unknown');
            $table->date('date')->index();
            $table->integer('views')->default(0);
            $table->integer('opens')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_statistics');
    }
}
