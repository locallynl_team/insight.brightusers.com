<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormInteractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_interactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('form_session_id')->index();
            $table->integer('form_field_id')->index();
            $table->boolean('redo')->default(false);
            $table->boolean('empty')->default(false);
            $table->boolean('drop_off')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_interactions');
    }
}
