<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToFormSessions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_sessions', function(Blueprint $table) {
            $table->boolean('send')->default(false);
            $table->string('fingerprint', 32);
            $table->enum('platform', ['desktop', 'tablet', 'mobile']);
            $table->text('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('form_sessions', function(Blueprint $table) {
            $table->dropColumn('fingerprint');
            $table->dropColumn('platform');
            $table->dropColumn('url');
        });
    }
}
