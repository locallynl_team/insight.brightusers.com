<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_styles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->index();
            $table->string('header_background_color', 10);
            $table->string('header_text_color', 10);
            $table->string('header_text', 50)->nullable();
            $table->string('header_type', 10);
            $table->string('header_logo', 255)->nullable();

            $table->string('survey_background_color', 10);
            $table->string('survey_border_color', 10);
            $table->string('survey_text_color', 10);
            $table->string('survey_text_align', 20);
            $table->string('answer_background_color', 10);
            $table->string('answer_text_color', 10);
            $table->string('survey_position', 20);

            $table->string('feedbackbutton_background_color', 10);
            $table->string('feedbackbutton_text_color', 10);
            $table->string('feedbackbutton_text', 35)->nullable();

            $table->string('button_background_color', 10);
            $table->string('button_text_color', 10);
            $table->string('button_align', 20);

            $table->text('advanced_styling')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_styles');
    }
}
