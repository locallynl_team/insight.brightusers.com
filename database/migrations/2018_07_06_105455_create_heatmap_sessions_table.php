<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeatmapSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heatmap_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('heatmap_id');
            $table->integer('heatmap_profile_id');
            $table->integer('heatmap_url_id');
            $table->string('fingerprint', 32);
            $table->integer('time_on_page')->default(0);
            $table->longText('data')->nullable();
            $table->enum('platform', ['desktop', 'tablet', 'mobile'])->nullable();
            $table->string('browser')->nullable();
            $table->string('os')->nullable();
            $table->enum('orientation', ['landscape', 'portrait'])->nullable();
            $table->integer('screen_width')->nullable();
            $table->integer('screen_height')->nullable();
            $table->integer('client_width')->nullable();
            $table->integer('client_height')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heatmap_sessions');
    }
}
