<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHtmlFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('html_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id');
            $table->string('html_id')->nullable();
            $table->string('html_name')->nullable();
            $table->text('attributes');
            $table->text('path');
            $table->text('fields');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('html_forms');
    }
}
