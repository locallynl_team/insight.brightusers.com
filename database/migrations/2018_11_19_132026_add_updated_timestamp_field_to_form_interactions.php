<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdatedTimestampFieldToFormInteractions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_interactions', function(Blueprint $table) {
            $table->bigInteger('last_update_timestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('form_interactions', function(Blueprint $table) {
            $table->dropColumn('last_update_timestamp');
        });
    }
}
