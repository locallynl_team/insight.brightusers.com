<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFormFieldsSelector extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_fields', function(Blueprint $table) {
            $table->string('selector');
            $table->string('selector_value');
            $table->dropColumn('html_name');
            $table->dropColumn('html_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('form_fields', function(Blueprint $table) {
            $table->dropColumn('selector');
            $table->dropColumn('selector_value');
            $table->string('html_name');
            $table->string('html_type');
        });
    }
}
