<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->index();
            $table->enum('platform', ['mobile', 'desktop', 'tablet', 'unknown'])->index()->default('unknown');
            $table->date('date')->index();
            $table->integer('visits')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_statistics');
    }
}
