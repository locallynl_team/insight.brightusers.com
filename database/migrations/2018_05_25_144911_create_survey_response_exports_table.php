<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyResponseExportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_response_exports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_id');
            $table->dateTime('start_period')->nullable();
            $table->dateTime('end_period')->nullable();
            $table->text('url')->nullable();
            $table->string('type', 10);
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_response_exports');
    }
}
