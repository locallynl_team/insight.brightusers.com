<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeHeatmapTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('heatmaps', function (Blueprint $table) {
            $table->text('screenshot_url')->after('url_target')->nullable()->default(null);
        });

        Schema::table('heatmap_screenshots', function (Blueprint $table) {
            $table->integer('heatmap_id')->after('id');
            $table->dropColumn('heatmap_url_id');
        });

        Schema::table('heatmap_sessions', function (Blueprint $table) {
            $table->dropColumn('heatmap_url_id');
        });

        DB::table('heatmap_sessions')->truncate();
        DB::table('heatmap_screenshots')->truncate();
        DB::table('heatmaps')->truncate();

        Schema::dropIfExists('heatmap_urls');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('heatmaps', function (Blueprint $table) {
            $table->dropColumn('screenshot_url');
        });

        Schema::table('heatmap_screenshots', function (Blueprint $table) {
            $table->dropColumn('heatmap_id');
            $table->integer('heatmap_url_id')->after('id');
        });

        Schema::table('heatmap_sessions', function (Blueprint $table) {
            $table->integer('heatmap_url_id')->after('id');
        });

        Schema::create('heatmap_urls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('heatmap_id');
            $table->string('url');
            $table->string('page_title');
            $table->timestamps();
        });
    }
}
