<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeatmapScreenshotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heatmap_screenshots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('heatmap_url_id');
            $table->integer('heatmap_profile_id');
            $table->string('filename')->nullable();
            $table->longText('metadata');
            $table->string('status', 1000);
            $table->integer('screen_width')->nullable();
            $table->integer('screen_height')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heatmap_screenshots');
    }
}
