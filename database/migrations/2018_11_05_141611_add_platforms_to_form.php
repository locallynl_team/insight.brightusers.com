<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlatformsToForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forms', function(Blueprint $table) {
            $table->boolean('desktop_platform')->default(true);
            $table->boolean('tablet_platform')->default(false);
            $table->boolean('mobile_platform')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forms', function(Blueprint $table) {
            $table->dropColumn('desktop_platform');
            $table->dropColumn('tablet_platform');
            $table->dropColumn('mobile_platform');
        });
    }
}
