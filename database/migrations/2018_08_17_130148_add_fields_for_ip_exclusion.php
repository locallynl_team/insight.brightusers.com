<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsForIpExclusion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('exclude_ip_addresses')->nullable();
        });

        Schema::table('sites', function (Blueprint $table) {
            $table->text('exclude_ip_addresses')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('exclude_ip_addresses');
        });

        Schema::table('sites', function (Blueprint $table) {
            $table->dropColumn('exclude_ip_addresses');
        });
    }
}
