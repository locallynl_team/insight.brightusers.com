<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHashFieldToFormInteractions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_interactions', function(Blueprint $table) {
            $table->text('value_hash');
            $table->integer('timing')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('form_interactions', function(Blueprint $table) {
            $table->dropColumn('value_hash');
            $table->dropColumn('timing');
        });
    }
}
