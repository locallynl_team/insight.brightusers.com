<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeatmapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heatmaps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id');
            $table->boolean('status')->default(false);
            $table->string('title', 150);
            $table->text('url_targets');
            $table->boolean('desktop_platform')->default(true);
            $table->boolean('tablet_platform')->default(false);
            $table->boolean('mobile_platform')->default(false);
            $table->integer('seconds_active')->default(0);
            $table->integer('last_status_change')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heatmaps');
    }
}
