<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->rememberToken();
            $table->softDeletes();
			$table->timestamps();
			$table->string('first_name', 50);
			$table->string('last_name', 50);
			$table->string('company_name', 100)->nullable();
			$table->string('address', 150)->nullable();
			$table->string('postal_code', 25)->nullable();
			$table->string('city', 100)->nullable();
			$table->integer('base_site_id')->nullable();
			$table->boolean('is_admin')->default(false);
            $table->boolean('pay_on_invoice')->default(false);
			$table->string('mollie_customer_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}



}
