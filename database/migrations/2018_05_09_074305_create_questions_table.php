<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_id')->index();
            $table->string('type', 25);
            $table->string('question', 255);
            $table->text( 'intro')->nullable();
            $table->tinyInteger('position');
            $table->string('rating_type')->nullable();
            $table->string('rating_label_start')->nullable();
            $table->string('rating_label_end')->nullable();
            $table->boolean('multi_line')->nullable();
            $table->string('button_next')->nullable();
            $table->boolean('multi_select')->nullable();
            $table->boolean('random_order')->nullable();
            $table->integer('after_action')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
