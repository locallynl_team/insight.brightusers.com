<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPageSessionIdToFormSessions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_sessions', function(Blueprint $table) {
            $table->string('page_session_id')->index();
            $table->boolean('successful', false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('form_sessions', function(Blueprint $table) {
            $table->dropColumn('page_session_id');
            $table->dropColumn('successful');
        });
    }
}
