<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->index();
            $table->string('type', 100)->default('survey');
            $table->string('title', 150);
            $table->boolean('status')->default(0);
            $table->text('url_targets')->nullable();
            $table->boolean('desktop_platform')->default(true);
            $table->boolean('tablet_platform')->default(false);
            $table->boolean('mobile_platform')->default(false);
            $table->boolean('time_on_page')->default(false);
            $table->integer('time_on_page_miliseconds')->nullable();
            $table->boolean('leave_intent')->default(false);
            $table->boolean('external_activation')->default(false);
            $table->string('button_send')->nullable();
            $table->string('button_send_close')->nullable();
            $table->string('mobile_bar_title')->nullable();
            $table->string('mobile_bar_subtitle')->nullable();
            $table->string('mobile_bar_button_yes')->nullable();
            $table->string('mobile_bar_button_no')->nullable();
            $table->integer('seconds_active')->default(0);
            $table->integer('last_status_change')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
