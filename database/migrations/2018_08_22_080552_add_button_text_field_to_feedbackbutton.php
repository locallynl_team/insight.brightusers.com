<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddButtonTextFieldToFeedbackbutton extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_styles', function (Blueprint $table) {
            $table->dropColumn('feedbackbutton_text');
        });

        Schema::table('surveys', function (Blueprint $table) {
            $table->string('feedback_button_text')->nullable()->after('mobile_bar_button_no');
            $table->string('feedback_button_location')->nullable()->after('mobile_bar_button_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_styles', function (Blueprint $table) {
            $table->string('feedbackbutton_text');
        });

        Schema::table('surveys', function (Blueprint $table) {
            $table->dropColumn('feedback_button_text');
            $table->dropColumn('feedback_button_location');
        });
    }
}
