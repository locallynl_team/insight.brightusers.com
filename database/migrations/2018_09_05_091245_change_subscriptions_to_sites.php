<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSubscriptionsToSites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('subscriptions')->truncate();

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->integer('site_id');
        });

        Schema::table('packages', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->decimal('price_monthly', 6, 2);
            $table->decimal('price_yearly', 6, 2);
        });

        Schema::table('invoice_rows', function(Blueprint $table) {
            $table->dropColumn('amount_ex');
            $table->decimal('tax', 6, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->integer('user_id');
            $table->dropColumn('site_id');
        });

        Schema::table('packages', function (Blueprint $table) {
            $table->decimal('price', 6, 2);
        });

        Schema::table('invoice_rows', function(Blueprint $table) {
            $table->decimal('amount_ex', 6, 2);
            $table->dropColumn('tax');
        });
    }
}
