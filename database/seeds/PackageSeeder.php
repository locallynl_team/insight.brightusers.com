<?php

use App\Package;
use App\PackageItem;
use App\PackageOption;
use Illuminate\Database\Seeder;

class PackageSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        $package_options = [
            [
                'title' => 'Aantal views per dag',
                'identifier' => 'views',
                'type' => 'integer',
                'sort_order' => 0
            ],
            [
                'title' => 'Actieve websites',
                'identifier' => 'websites',
                'type' => 'integer',
                'sort_order' => 1
            ],
            [
                'title' => 'Aantal surveys',
                'identifier' => 'survey',
                'type' => 'integer',
                'sort_order' => 2
            ],
            [
                'title' => 'Aantal feedbackbuttons',
                'identifier' => 'feedbackbutton',
                'type' => 'integer',
                'sort_order' => 3
            ],
            [
                'title' => 'Aantal heatmaps',
                'identifier' => 'heatmaps',
                'type' => 'integer',
                'sort_order' => 4
            ],
            [
                'title' => 'Voor desktop bezoekers',
                'identifier' => 'desktop_platform',
                'type' => 'boolean',
                'sort_order' => 5
            ],
            [
                'title' => 'Voor tablet bezoekers',
                'identifier' => 'tablet_platform',
                'type' => 'boolean',
                'sort_order' => 6
            ],
            [
                'title' => 'Voor mobiele bezoekers',
                'identifier' => 'mobile_platform',
                'type' => 'boolean',
                'sort_order' => 7
            ],
            [
                'title' => 'Target verlaat intentie',
                'identifier' => 'target_leave_intent',
                'type' => 'boolean',
                'sort_order' => 8
            ],
            [
                'title' => 'Aanpasbare button teksten',
                'identifier' => 'button_texts',
                'type' => 'boolean',
                'sort_order' => 9
            ],
            [
                'title' => 'Gebruik externe activatie',
                'identifier' => 'target_external_activation',
                'type' => 'boolean',
                'sort_order' => 10
            ],
        ];

        $packages = [
            [
                'title' => 'Basis',
                'price' => 39.00,
                'active' => 1,
                'trial' => 0,
                'items' => [
                    'views' => 5000,
                    'websites' => 1,
                    'feedbackbutton' => 1,
                    'survey' => 1,
                    'heatmaps' => 1,
                    'target_leave_intent' => false,
                    'target_external_activation' => false,
                    'mobile_platform' => false,
                    'tablet_platform' => false,
                    'desktop_platform' => true,
                    'button_texts' => false
                ]
            ],
            [
                'title' => 'Plus',
                'price' => 129.00,
                'active' => 1,
                'trial' => 0,
                'items' => [
                    'views' => 25000,
                    'websites' => 5,
                    'feedbackbutton' => 10,
                    'survey' => 10,
                    'heatmaps' => 10,
                    'target_leave_intent' => true,
                    'target_external_activation' => false,
                    'mobile_platform' => true,
                    'tablet_platform' => true,
                    'desktop_platform' => true,
                    'button_texts' => true
                ]
            ],
            [
                'title' => 'Premium',
                'price' => 349.00,
                'active' => 1,
                'trial' => 1,
                'items' => [
                    'views' => 100000,
                    'websites' => 10,
                    'feedbackbutton' => 25,
                    'survey' => 25,
                    'heatmaps' => 25,
                    'target_leave_intent' => true,
                    'target_external_activation' => true,
                    'mobile_platform' => true,
                    'tablet_platform' => true,
                    'desktop_platform' => true,
                    'button_texts' => true
                ]
            ],
        ];

        PackageOption::insert($package_options);

        foreach ($packages as $package) {
            $new_package = Package::create([
                'title' => $package['title'],
                'price_monthly' => $package['price'],
                'active' => $package['active'],
                'trial' => $package['trial']
            ]);

            foreach ($package['items'] as $packageItemIdentifier => $packageItemAmount) {
                PackageItem::create([
                    'package_id' => $new_package->id,
                    'amount' => $packageItemAmount,
                    'package_option_id' => PackageOption::where('identifier', $packageItemIdentifier)->first()->id
                ]);
            }
        }
    }
}