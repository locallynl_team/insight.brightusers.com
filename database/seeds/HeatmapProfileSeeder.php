<?php

use App\HeatmapProfile;
use Illuminate\Database\Seeder;

class HeatmapProfileSeeder extends Seeder
{
    /**
     * Add heatmap profiles
     */
    public function run() {
        $profiles = [
            [
                'title' => 'Desktop (large)',
                'slug' => 'desktop-large',
                'platform' => 'desktop',
                'orientation' => 'landscape',
                'emulator' => 'desktop-large',
                'device_ratio' => 1,
            ],
            [
                'title' => 'Desktop (normal)',
                'slug' => 'desktop-normal',
                'platform' => 'desktop',
                'orientation' => 'landscape',
                'emulator' => 'desktop-normal',
                'device_ratio' => 1,
            ],
            [
                'title' => 'Tablet (landscape)',
                'slug' => 'tablet-landscape',
                'platform' => 'tablet',
                'orientation' => 'landscape',
                'emulator' => 'iPad Pro landscape',
                'device_ratio' => 1,
            ],
            [
                'title' => 'Tablet (portrait)',
                'slug' => 'tablet-portrait',
                'platform' => 'tablet',
                'orientation' => 'portrait',
                'emulator' => 'iPad Pro',
                'device_ratio' => 1,
            ],
            [
                'title' => 'Mobile (landscape)',
                'slug' => 'mobile-landscape',
                'platform' => 'mobile',
                'orientation' => 'landscape',
                'emulator' => 'iPhone 6 Plus landscape',
                'device_ratio' => 1,
            ],
            [
                'title' => 'Mobile (portrait)',
                'slug' => 'mobile-portrait',
                'platform' => 'mobile',
                'orientation' => 'portrait',
                'emulator' => 'iPhone 6 Plus',
                'device_ratio' => 1,
            ]
        ];

        HeatmapProfile::insert($profiles);
    }
}