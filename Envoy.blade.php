@servers(['web' => 'deployer@37.139.26.24'])

@task('list', ['on' => 'web'])
ls -l
@endtask

@setup
    $repository = 'git@gitlab.com:bluepopsicle/surve-new.git';
    $releases_dir = '/var/www/surve/releases';
    $app_dir = '/var/www/surve';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
    $env = 'production';
@endsetup

@story('deploy')
    clone_repository
    run_composer
    build_assets
    update_symlinks
    down
    migrate
    up
    restart_queue
    clean_old_releases
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
    chmod 777 -R bootstrap/cache
@endtask

@task('build_assets')
    cd {{ $new_release_dir }};
    npm set progress=false
    npm install
    npm run production
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    # Optimise installation
    cd {{ $new_release_dir }};

    echo "Creating symlink to public storage"
    php artisan storage:link;

    echo 'Optimising installation';
    php artisan clear-compiled --env={{ $env }};
    php artisan config:cache --env={{ $env }};
    php artisan cache:clear --env={{ $env }};

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current

    echo 'Reload php7.1-fpm'
    sudo service php7.1-fpm reload

    echo 'Empty Redis cache'
    sudo redis-cli flushall
@endtask

@task('migrate')
    echo 'Running migrations';
    cd {{ $new_release_dir }};
    php artisan migrate --env={{ $env }} --force;
@endtask

@task('restart_queue')
    cd {{ $new_release_dir }};
    php artisan queue:restart
@endtask

@task('down')
    cd {{ $new_release_dir }};
    php artisan down;
@endtask

@task('up')
    cd {{ $app_dir }}/current;
    php artisan up;
@endtask

@task('clean_old_releases')
    sync

    # This will list our releases by modification time and delete all but the 3 most recent.
    purging=$(ls -dt {{ $releases_dir }}/* | tail -n +5);

    if [ "$purging" != "" ]; then
        echo Purging old releases: $purging;
        rm -rf $purging;
    else
        echo "No releases found for purging at this time";
    fi
@endtask